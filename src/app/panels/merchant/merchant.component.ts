import { Component, OnInit, Renderer2 } from '@angular/core';
import { HelperService, OResponse, OOverview, DataHelperService, OList, OSelect } from '../../service/service';
import { Observable } from 'rxjs';
import { NavigationEnd, Router } from '@angular/router';
import swal from 'sweetalert2';
declare var moment: any;
declare var $: any;
declare var PerfectScrollbar: any;
import * as Feather from 'feather-icons';
import * as Chart from "../../../../node_modules/chart.js/dist/Chart.js";
import * as Cookies from '../../../assets/js/js.cookie.js'

@Component({
    selector: 'merchant-root',
    templateUrl: './merchant.component.html',
})
export class TUMerchantComponent implements OnInit {

    public _Store_Option: Select2Options;
    public isAccountSettingActive: boolean = false
    constructor(
        public _DataHelperService: DataHelperService,
        public _Router: Router,
        public _HelperService: HelperService,
        private renderer2: Renderer2
    ) {

    }

    ngAfterViewInit(): void {
    }
    ngOnInit() {

        this._HelperService.IsSubscriptionActive = this._HelperService.GetStorage("hca").Subscription.IsSubscriptionActive
        this._HelperService.IsOpenCloseLoyalty = this._HelperService.GetStorage("hca").UserAccount.IsOpenCloseLoyalty;
        if (this._HelperService.GetStorage("toggleDB")) {
            this._HelperService.DisplayToggleDashboard = true
        }
        // this.isAccountSettingActive = location.href.includes('/settings');
        const conditions = ["/settings", "store/", "terminal/", "cashier/", "subaccounts/", "app/"];
        this._HelperService.iseditProfile = location.href.includes("settings/editprofile")
        this._HelperService.isAccountSettingActive = conditions.some(el => location.href.includes(el))
        this.isAccountSettingActive = location.href.includes('/settings');
        this._HelperService.hideAccountSetting = location.href.includes('/settings/');
        this._HelperService.DisplayDealsDashboard = location.href.includes("dealsdashboard") || !this._HelperService.GetStorage("hcaDisplayLoyalty")
        this._HelperService.DisplayLoyaltyDashboard = location.href.includes("loyaltydashboard") || this._HelperService.GetStorage("hcaDisplayLoyalty")
        if (this._HelperService.DisplayLoyaltyDashboard || this._HelperService.DisplayDealsDashboard) {

            this._HelperService.isDisplayLoyaltyConfig = this._HelperService.DisplayLoyaltyDashboard
        }

        this._Router.events.subscribe((events: any) => {
            if (events instanceof NavigationEnd) {
                this._HelperService.hideAccountSetting = events.url.includes('/settings/');
                this._HelperService.isAccountSettingActive = conditions.some(el => events.url.includes(el))
                this._HelperService.iseditProfile = events.url.includes("settings/editprofile")
                // this.isAccountSettingActive = events.url.includes('/settings');
                this._HelperService.DisplayDealsDashboard = events.url.includes("dealsdashboard") || !this._HelperService.GetStorage("hcaDisplayLoyalty")
                this._HelperService.DisplayLoyaltyDashboard = events.url.includes("loyaltydashboard") || this._HelperService.GetStorage("hcaDisplayLoyalty")
                if (this._HelperService.DisplayLoyaltyDashboard || this._HelperService.DisplayDealsDashboard) {

                    this._HelperService.isDisplayLoyaltyConfig = this._HelperService.DisplayLoyaltyDashboard
                }
               
            }
        })
        setTimeout(() => {
            this._HelperService.ValidateDataPermission();
        }, 500);
        this._HelperService.darkStyle = Cookies.get("mapColor");
        this._HelperService.setTheme()
        // Chart.defaults.global.defaultFontColor = 'white';
        // Chart.defaults.global.defaultFontSize = '16';

        $("li").click(() => {

            var backdrop: HTMLElement = document.getElementById("backdrop");
            backdrop.classList.remove("show");

            var modal_backdrop: HTMLCollectionOf<Element> = document.getElementsByClassName("modal");
            if (modal_backdrop.length > 0) {
                const element = modal_backdrop.item(0);
                this._HelperService.CloseModal(element.id);
            }

            var modal_backdrop: HTMLCollectionOf<Element> = document.getElementsByClassName("modal-backdrop");
            if (modal_backdrop.length > 0) {
                const element = modal_backdrop.item(0);
                element.classList.remove("modal-backdrop");
            }

        });

        $('body').on('click', '.df-mode', (e) => {
            e.preventDefault();

            for (let index = 0; index < $('.df-mode').length; index++) {
                const element = $('.df-mode')[index];
                setTimeout(() => {
                    if (element.classList.length == 3) {
                        if (element.getAttribute('data-title') === 'dark') {
                            this._HelperService.CheckMode = 'dark';
                            this._HelperService.darkStyle = [
                                {
                                    elementType: "geometry",
                                    stylers: [
                                        {
                                            color: "#242f3e"
                                        }
                                    ]
                                },
                                {
                                    elementType: "labels.text.stroke",
                                    stylers: [
                                        {
                                            color: "#242f3e"
                                        }
                                    ]
                                },
                                {
                                    elementType: "labels.text.fill",
                                    stylers: [
                                        {
                                            color: "#746855"
                                        }
                                    ]
                                },
                                {
                                    featureType: "administrative.locality",
                                    elementType: "labels.text.fill",
                                    stylers: [
                                        {
                                            color: "#d59563"
                                        }
                                    ]
                                },
                                {
                                    featureType: "poi",
                                    elementType: "labels.text.fill",
                                    stylers: [
                                        {
                                            color: "#d59563"
                                        }
                                    ]
                                },
                                {
                                    featureType: "poi.park",
                                    elementType: "geometry",
                                    stylers: [
                                        {
                                            color: "#263c3f"
                                        }
                                    ]
                                },
                                {
                                    featureType: "poi.park",
                                    elementType: "labels.text.fill",
                                    stylers: [
                                        {
                                            color: "#6b9a76"
                                        }
                                    ]
                                },
                                {
                                    featureType: "road",
                                    elementType: "geometry",
                                    stylers: [
                                        {
                                            color: "#38414e"
                                        }
                                    ]
                                },
                                {
                                    featureType: "road",
                                    elementType: "geometry.stroke",
                                    stylers: [
                                        {
                                            color: "#212a37"
                                        }
                                    ]
                                },
                                {
                                    featureType: "road",
                                    elementType: "labels.text.fill",
                                    stylers: [
                                        {
                                            color: "#9ca5b3"
                                        }
                                    ]
                                },
                                {
                                    featureType: "road.highway",
                                    elementType: "geometry",
                                    stylers: [
                                        {
                                            color: "#746855"
                                        }
                                    ]
                                },
                                {
                                    featureType: "road.highway",
                                    elementType: "geometry.stroke",
                                    stylers: [
                                        {
                                            color: "#1f2835"
                                        }
                                    ]
                                },
                                {
                                    featureType: "road.highway",
                                    elementType: "labels.text.fill",
                                    stylers: [
                                        {
                                            color: "#f3d19c"
                                        }
                                    ]
                                },
                                {
                                    featureType: "transit",
                                    elementType: "geometry",
                                    stylers: [
                                        {
                                            color: "#2f3948"
                                        }
                                    ]
                                },
                                {
                                    featureType: "transit.station",
                                    elementType: "labels.text.fill",
                                    stylers: [
                                        {
                                            color: "#d59563"
                                        }
                                    ]
                                },
                                {
                                    featureType: "water",
                                    elementType: "geometry",
                                    stylers: [
                                        {
                                            color: "#17263c"
                                        }
                                    ]
                                },
                                {
                                    featureType: "water",
                                    elementType: "labels.text.fill",
                                    stylers: [
                                        {
                                            color: "#515c6d"
                                        }
                                    ]
                                },
                                {
                                    featureType: "water",
                                    elementType: "labels.text.stroke",
                                    stylers: [
                                        {
                                            color: "#17263c"
                                        }
                                    ]
                                }
                            ];


                            Cookies.set('mapColor', this._HelperService.darkStyle);
                        } else {
                            this._HelperService.CheckMode = 'light';
                            this._HelperService.darkStyle = [];
                            Cookies.set('mapColor', this._HelperService.darkStyle);
                        }
                    }
                }, 50);
            }
        });

        setTimeout(() => {
            var hasMode = Cookies.get('df-mode');

            this._HelperService.CheckMode = (hasMode === 'dark') ? 'dark' : 'light';
            if (hasMode == 'dark') {
                this._HelperService.darkStyle = [
                    {
                        elementType: "geometry",
                        stylers: [
                            {
                                color: "#242f3e"
                            }
                        ]
                    },
                    {
                        elementType: "labels.text.stroke",
                        stylers: [
                            {
                                color: "#242f3e"
                            }
                        ]
                    },
                    {
                        elementType: "labels.text.fill",
                        stylers: [
                            {
                                color: "#746855"
                            }
                        ]
                    },
                    {
                        featureType: "administrative.locality",
                        elementType: "labels.text.fill",
                        stylers: [
                            {
                                color: "#d59563"
                            }
                        ]
                    },
                    {
                        featureType: "poi",
                        elementType: "labels.text.fill",
                        stylers: [
                            {
                                color: "#d59563"
                            }
                        ]
                    },
                    {
                        featureType: "poi.park",
                        elementType: "geometry",
                        stylers: [
                            {
                                color: "#263c3f"
                            }
                        ]
                    },
                    {
                        featureType: "poi.park",
                        elementType: "labels.text.fill",
                        stylers: [
                            {
                                color: "#6b9a76"
                            }
                        ]
                    },
                    {
                        featureType: "road",
                        elementType: "geometry",
                        stylers: [
                            {
                                color: "#38414e"
                            }
                        ]
                    },
                    {
                        featureType: "road",
                        elementType: "geometry.stroke",
                        stylers: [
                            {
                                color: "#212a37"
                            }
                        ]
                    },
                    {
                        featureType: "road",
                        elementType: "labels.text.fill",
                        stylers: [
                            {
                                color: "#9ca5b3"
                            }
                        ]
                    },
                    {
                        featureType: "road.highway",
                        elementType: "geometry",
                        stylers: [
                            {
                                color: "#746855"
                            }
                        ]
                    },
                    {
                        featureType: "road.highway",
                        elementType: "geometry.stroke",
                        stylers: [
                            {
                                color: "#1f2835"
                            }
                        ]
                    },
                    {
                        featureType: "road.highway",
                        elementType: "labels.text.fill",
                        stylers: [
                            {
                                color: "#f3d19c"
                            }
                        ]
                    },
                    {
                        featureType: "transit",
                        elementType: "geometry",
                        stylers: [
                            {
                                color: "#2f3948"
                            }
                        ]
                    },
                    {
                        featureType: "transit.station",
                        elementType: "labels.text.fill",
                        stylers: [
                            {
                                color: "#d59563"
                            }
                        ]
                    },
                    {
                        featureType: "water",
                        elementType: "geometry",
                        stylers: [
                            {
                                color: "#17263c"
                            }
                        ]
                    },
                    {
                        featureType: "water",
                        elementType: "labels.text.fill",
                        stylers: [
                            {
                                color: "#515c6d"
                            }
                        ]
                    },
                    {
                        featureType: "water",
                        elementType: "labels.text.stroke",
                        stylers: [
                            {
                                color: "#17263c"
                            }
                        ]
                    }
                ];
                Cookies.set('mapColor', this._HelperService.darkStyle);
            }
            else {
                this._HelperService.darkStyle = [];
                Cookies.set('mapColor', this._HelperService.darkStyle);
            }
        }, 50);
        const s = this.renderer2.createElement('script');
        s.type = 'text/javascript';
        s.src = '../../../assets/js/dashforge.aside.js';
        s.text = ``;
        this.renderer2.appendChild(document.body, s);

        const s2 = this.renderer2.createElement('script');
        s2.type = 'text/javascript';
        s2.src = '../../../../assets/js/dashforge.settings.js';
        s2.text = ``;
        this.renderer2.appendChild(document.body, s2);
        Feather.replace();

    }

    MoveToAccountSetting() {
        this._Router.navigateByUrl(this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.AccountSetting)       
    }

    displayDropDown(){
        $(".custom-dropdown-menu").show()
    }

    loadSubMenu() {
        setTimeout(() => {
            $('.submenuLabel').on('click', function (e) {
                e.preventDefault();
                $(this).parent().toggleClass('show');
            })
        }, 1000);
    }

    NavigateToLoyalty() {
        this._HelperService.SaveStorage("hcaDisplayLoyalty", true)
        this.loadSubMenu();
        this.MoveToMerchantPanel('loyalty')
    }

    NavigateToDeals() {
        this._HelperService.SaveStorage("hcaDisplayLoyalty", false)
        this.loadSubMenu()
        this.MoveToMerchantPanel('deals')
    }

    MoveToMerchantPanel(route) {
        var TAccessKey = this._HelperService.AccessKey;
        var TPublicKey = this._HelperService.PublicKey;
        var Key = btoa(TAccessKey + "|" + TPublicKey + "|" + route);

        window.location.href = this._HelperService.AppConfig.MerchantPanelURL + '/account/auth/' + Key, '_blank';
    }

    displayLoyaltyModal(){
        $(".loyaltyModal").modal("show");
        $(".custom-dropdown-menu").hide()
    }

    displayDealsModal(){
        $(".dealsModal").modal("show");
        $(".custom-dropdown-menu").hide()
    }

    AddDealsAccountType() {
        var _PostData = {
            Task: "ob_merchant_requestaccountconfiguration",
            ReferenceId: this._HelperService.GetStorage("hca").UserAccount.AccountId,
            ReferenceKey: this._HelperService.GetStorage("hca").UserAccount.AccountKey,
            AccountTypes: [{ AccountTypeCodes: "thankucashdeals", Value: "2" }]
        }
        this._HelperService.IsFormProcessing = true;
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.MOnBoardV2, _PostData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsFormProcessing = false;
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    $(".dealsModal").modal("hide");
                    this._HelperService.SaveStorage("hcaDisplayLoyalty", false)
                    this.loadSubMenu()
                    this._HelperService.SaveStorage("toggleDB", true)
                    this._HelperService.DisplayToggleDashboard = true
                    this._HelperService.DeleteStorage("hcuat");
                    this._HelperService.SaveStorage("hcuat", [{ "TypeId": 795, "TypeCode": "accounttype.thankucashdeals" }, { "TypeId": 793, "TypeCode": "accounttype.thankucashloyalty" }])
                    this._HelperService.isDisplayLoyaltyConfig = false
                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);
            });
    }

    AddLoyaltyAccountType() {        
        var _PostData = {
            Task: "ob_merchant_requestaccountconfiguration",
            ReferenceId: this._HelperService.GetStorage("hca").UserAccount.AccountId,
            ReferenceKey: this._HelperService.GetStorage("hca").UserAccount.AccountKey,
            AccountTypes: [{
                AccountTypeCodes: "thankucashloyalty", ConfigurationValue: "closedloyaltymodel",
                Value: null
            }]
        }
        this._HelperService.IsFormProcessing = true;
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.MOnBoardV2, _PostData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsFormProcessing = false;
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    $(".loyaltyModal").modal("hide");
                    this._HelperService.SaveStorage("hcaDisplayLoyalty", true)
                    this.loadSubMenu()
                    this._HelperService.SaveStorage("toggleDB", true)
                    this._HelperService.DisplayToggleDashboard = true
                    this._HelperService.DeleteStorage("hcuat");
                    this._HelperService.SaveStorage("hcuat", [{ "TypeId": 795, "TypeCode": "accounttype.thankucashdeals" }, { "TypeId": 793, "TypeCode": "accounttype.thankucashloyalty" }])
                    this._HelperService.isDisplayLoyaltyConfig = true
                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);
            });
    }  

    NavigateCampaigns() {
        // var TAccessKey = this._HelperService.AccessKey;
        // var TPublicKey = this._HelperService.PublicKey;
        // var Key = btoa(TAccessKey + "|" + TPublicKey);
        // console.log("Mkey", Key)
        // if (this._HelperService.AppConfig.HostType == HostType.Live) {

        //     window.open("https://campaigns.thankucash.com/account/auth/" + Key, '_blank').focus();
        //     // window.open("https://campaigns.thankucash.com/account/auth/" + TAccessKey + "/" + TPublicKey, '_blank').focus();
        // }
        // else if (this._HelperService.AppConfig.HostType == HostType.Test) {
        //     window.open("https://testcampaigns.thankucash.com/account/auth/" + Key, '_blank').focus();
        // }
        // else {
        //     window.open("https://testcampaigns.thankucash.com/account/auth/" + TAccessKey + "/" + TPublicKey, '_blank').focus();
        // }
    }

    Upgrade() {
        this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.UpgradePlan])
    }

    ProcessLogout() {

        swal({
            position: "center",
            title: this._HelperService.AppConfig.CommonResource.LogoutTitle,
            text: "Click on Logout button to confirm Logout",
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Red,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: "Logout",
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel

        }).then((result) => {
            if (result.value) {
                var pData = {
                    Task: this._HelperService.AppConfig.Api.Logout
                };
                let _OResponse: Observable<OResponse>;
                _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V1.System, pData);
                _OResponse.subscribe(
                    _Response => {
                        if (_Response.Status == this._HelperService.StatusSuccess) {
                            this._HelperService.NotifySuccess(_Response.Message);
                            this._HelperService.DeleteStorage('templocmerchant');

                            this._HelperService.templocmerchant = false;

                        }
                        else {
                            this._HelperService.NotifyError(_Response.Message);
                        }
                    },
                    _Error => {
                        console.log(_Error);
                    });
                this._HelperService.DeleteStorage(this._HelperService.AppConfig.Storage.Account);
                this._HelperService.DeleteStorage(this._HelperService.AppConfig.Storage.Location);
                this._HelperService.DeleteStorage(this._HelperService.AppConfig.Storage.OReqH);
                this._HelperService.DeleteStorage(this._HelperService.AppConfig.Storage.Permissions);
                this._Router.navigate([this._HelperService.AppConfig.Pages.System.Login]);
            }
            else { }
        });



    }
}

enum HostType {
    Live,
    Test,
    Tech,
    Dev
}  