import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { TUMerchantComponent } from './merchant.component';
import { TUMerchantRoutingModule } from './merchant.routing.module';
import { ReactiveFormsModule } from '@angular/forms';
@NgModule({
    declarations: [
        TUMerchantComponent,
    ],
    imports: [
        TUMerchantRoutingModule,
        TranslateModule,
        CommonModule,
        ReactiveFormsModule,
    ],
    providers: [],
    bootstrap: [TUMerchantComponent]
})
export class TUMerchantModule { }
