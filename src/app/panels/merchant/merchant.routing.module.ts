import { NgModule } from "@angular/core";
import { Routes, RouterModule, CanActivateChild } from "@angular/router";
import { TUMerchantComponent } from "./merchant.component";
import { HelperService } from "../../service/helper.service";
import { MerchantguardGuard } from 'src/app/service/guard/merchantguard.guard';
const routes: Routes = [
  {
    path: "m",
    component: TUMerchantComponent,
    canActivateChild: [HelperService],
    children: [
      //End 
      { path: 'apps', data: { permission: 'appskeys', PageName: 'System.Menu.Administration' }, loadChildren: '../../modules/accounts/tumerchants/hcapps/tuapplist/tuapplist.module#TUAppListModule' },
      { path: 'app', data: { permission: 'appskeys', PageName: 'System.Menu.Administration' }, loadChildren: '../../modules/accounts/tumerchants/hcapps/hcapps.module#HCAppsModule' },
      { path: "dashboard", data: { permission: "profile", PageName: "System.Menu.Dashboard" }, loadChildren: "./../../panel/merchant/dashboards/root/dashboard.module#TUDashboardModule" },
      { path: "settings", data: { permission: "profile", PageName: "System.Menu.AccountSettings" }, loadChildren: "../../modules/accountdetails/tumerchant/tusettings/tusettings.module#TUSettingsModule" },
      { path: "store", data: { permission: "bsstores", PageName: "System.Menu.Store" }, loadChildren: "../../modules/accountdetails/tumerchant/tustore/tustore.module#TUStoreModule" },
      { path: "terminal", data: { permission: "bspos", PageName: "System.Menu.Terminal" }, loadChildren: "../../modules/accountdetails/tumerchant/tuterminal/tuterminal.module#TUTerminalModule" },
      { path: "cashier", data: { permission: "bscashiers", PageName: "System.Menu.Cashier" }, loadChildren: "../../modules/accountdetails/tumerchant/tucashier/tucashier.module#TUCashierModule" },
      { path: "subaccount", data: { permission: "subaccounts", PageName: "System.Menu.Cashier" }, loadChildren: "../../modules/accountdetails/tumerchant/tusubaccount/tusubaccount.module#TUSubAccountModule" },
      { path: "bank", data: { permission: "bsstores", PageName: "System.Menu.BankReconcialiation" }, loadChildren: "../../modules/accounts/tumerchants/bankreconciliation/bank.module#TUBankModule" },
      { path: "bulkterminals", data: { permission: "bspos", PageName: "System.Menu.BulkTerminal" }, loadChildren: "../../modules/accounts/tumerchants/bulkterminalupload/tuterminals.module#TUBulkTerminalsModule" },
      { path: "bulkterminalslist", data: { permission: "bspos", PageName: "System.Menu.BulkTerminal" }, loadChildren: "../../modules/accounts/tumerchants/bulkterminallist/tuterminals.module#TUBulkTerminalsModule" },
      // { path: "upgrade", data: { permission: "account", PageName: "System.Menu.Upgrade" }, loadChildren: "../../modules/accounts/tumerchants/NoAccess/topup.module#TUTopUpModule" },


      { path: "profile", data: { permission: "profile", PageName: "System.Menu.Profile" }, loadChildren: "./../../pages/hcprofile/hcprofile.module#HCProfileModule" },
     
      // FAQs
      { path: 'faqcategories', data: { 'permission': 'faq', PageName: 'System.Menu.Administration' }, loadChildren: '../../modules/accounts/tumerchants/tufaqcategories/tufaqcategories.module#TUFAQCategoriesModule' },
      { path: 'faqs/:referencekey/:referenceid', data: { 'permission': 'faq', PageName: 'System.Menu.Administration' }, loadChildren: '../../modules/accounts/tumerchants/tufaqs/tufaqs.module#TUFAQsModule' },



      { path: "bankmanager", data: { permission: "bankmanager", PageName: "System.Menu.Bank" }, loadChildren: "./../../modules/accounts/tumerchants/bankmanager/bankmanager.module#TUBankManagerModule" },
      { path: "banks", data: { permission: "banks", PageName: "System.Menu.Bank" }, loadChildren: "./../../modules/accounts/tumerchants/bankmanager/banks/banks.module#TUBanksModule" },

      //  CashOuts
      { path: "cashouts", data: { permission: "bscashouts", PageName: "System.Menu.CashOuts" }, loadChildren: "./../../modules/accounts/tumerchants/tucashouts/tucashouts.module#TUCashoutsModule" },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TUMerchantRoutingModule { }
