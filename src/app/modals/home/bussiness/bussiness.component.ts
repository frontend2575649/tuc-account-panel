import { AfterViewInit, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import * as Feather from 'feather-icons';
declare var $: any;
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Select2OptionData } from 'ng2-select2';
import { Address } from "ngx-google-places-autocomplete/objects/address";
import { Observable } from 'rxjs/internal/Observable';
import { DataHelperService, HelperService, OResponse, OSelect } from '../../../service/service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-bussiness',
  templateUrl: './bussiness.component.html',
  styleUrls: ['./bussiness.component.css']
})
export class BussinessComponent implements OnInit, AfterViewInit {
  documentForm: FormGroup;
  documentationForm: FormGroup;
  // bankList: any[] = [];
  base64textString: String;

  documentType = [
    { id: 'driversLicense', text: 'Drivers License' },
    { id: 'internationalPassport', text: 'International Passport' },
    { id: 'nationalIdNumber', text: 'National Identity Number (NIN)' },
    { id: 'utilityBill', text: 'Utility Bill' },
    { id: 'voterCard', text: 'Voters Card', },
    { id: 'CAC', text: 'CAC' }];

  public _S2Banks_Data: Array<Select2OptionData>;
  public _S2DocumentType_Data: Array<Select2OptionData>;
  public HCXLocManager_S2States_Option: Select2Options;
  public BCategory_Option: Select2Options;
  public _S2DocumentType_Option: Select2Options;

  public displayMap = false;
  ShowCity = true;
  ShowState = true;
  ShowCategory = true
  IsAddressSet = false;
  selectedCategoryId = null;
  selectedCategoryKey = null;
  categoryValue = null
  DocValue = null
  sizeRestriction:boolean = false

  // selectedBank = null;
  docName;
  fileName;
  fileSize;
  fileType;
  accountId
  accountKey
  public modifiedName
  public data =
    {

      Address: null,
      Latitude: null,
      Longitude: null,
      CityAreaId: null,
      CityAreaCode: null,
      CityAreaName: null,
      CityId: null,
      CityCode: null,
      CityName: null,
      StateId: null,
      StateCode: null,
      StateName: null,
      CountryId: null,
      CountryCode: null,
      CountryName: null,
      PostalCode: null,
      MapAddress: null,
      ReferralCode: null,
      draggable: true
    };
  isLoaded: boolean = true;
  public MerchantAddress: any = {};
  public MerchantContactPerson: any = {};
  public _MerchantDetails: any =
    {
      ReferenceId: null,
      ReferenceKey: null,
      TypeCode: null,
      TypeName: null,
      SubTypeCode: null,
      SubTypeName: null,
      UserAccountKey: null,
      UserAccountDisplayName: null,
      Name: null,
      Description: null,
      StartDate: null,
      StartDateS: null,
      EndDate: null,
      EndDateS: null,
      SubTypeValue: null,
      MinimumInvoiceAmount: null,
      MaximumInvoiceAmount: null,
      MinimumRewardAmount: null,
      MaximumRewardAmount: null,
      ManagerKey: null,
      ManagerDisplayName: null,
      SmsText: null,
      Comment: null,
      CreateDate: null,
      CreatedByKey: null,
      CreatedByDisplayName: null,
      ModifyDate: null,
      ModifyByKey: null,
      ModifyByDisplayName: null,
      StatusId: null,
      StatusCode: null,
      StatusName: null,
      Latitude: null,
      Longitude: null,
      CreateDateS: null,
      ModifyDateS: null,
      StatusI: null,
      StatusB: null,
      StatusC: null,
    }

  public _BankDetails = {
    BankId: null,
    BankName: null,
    BankCode: null,
    AccountNumber: null,
    AccountName: null
  }
  public _ContactInfo: boolean;
  public businessEmail
  public businessName
  public _cpFirstName
  public _cpLastName

  public _cpMobileNumber
  constructor(
    private router: Router,
    public formBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef,
    public _location: Location) { 
      this._ContactInfo = true;
    }

  ngOnInit() {
    this._MerchantDetails = this._HelperService.GetStorage("merchantDetails");
    this.MerchantContactPerson = this._HelperService.GetStorage("merchantDetails").ContactPerson;
    this.MerchantAddress = this._MerchantDetails.AddressComponent;
    this.accountId = this._HelperService.GetStorage("hca").UserAccount.AccountId
    this.accountKey = this._HelperService.GetStorage("hca").UserAccount.AccountKey
    this.businessName = this._MerchantDetails.DisplayName
    this.businessEmail = this._MerchantDetails.EmailAddress
    this._cpMobileNumber = this._MerchantDetails.MobileNumber
    this._cpFirstName = this._MerchantDetails.FirstName ? this._MerchantDetails.FirstName :null
    this._cpLastName = this._MerchantDetails.LastName ? this._MerchantDetails.LastName : null
    this.documentForm = this.formBuilder.group({
      'businessName': [this.businessName, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(128)])],
      'bussinessEmail': [this.businessEmail, Validators.compose([Validators.required, Validators.email, Validators.minLength(2), Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')])],
      'accountNumber': [null, Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(14),, Validators.pattern("^((\\+243-?)|0)?[0-9]{10,14}$")])],
      'bank': [null, Validators.compose([Validators.required])],
      'accountName': [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(128), Validators.pattern("^[a-zA-Z _-]*$")])],
      'selectCategory': [null, Validators.required],
      'streetAddress': [null, Validators.required],
      'city': [null, Validators.compose([Validators.required])],
      'state': [null, Validators.compose([Validators.required])],
      'detailAddress': [null, Validators.required],
      'lat': [null],
      'lng': [null],
      'docType': [null, Validators.required],
      'docFile': [null, Validators.required],
      'cpFirstName':[null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(128), Validators.pattern("^[a-zA-Z _-]*$")])],
      'cpLastName':[null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(128), Validators.pattern("^[a-zA-Z _-]*$")])],
      'cpMobileNumber': [null, Validators.compose([Validators.required, Validators.minLength(11), Validators.maxLength(14), Validators.pattern("^((\\+243-?)|0)?[0-9]{10,14}$")])],
      'cpEmailAddress':[null, Validators.compose([Validators.required, Validators.email, Validators.minLength(2), Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')])],

    });
    this.documentationForm = this.formBuilder.group({
      docType: [null, Validators.compose([Validators.required])],
      docFile: [null, Validators.compose([Validators.required])]
    });

    this._S2DocumentType_Option = {
      placeholder: "Select Document",
      multiple: false,
      allowClear: false,
    };

    this.modal();
    this.TUTr_Filter_Banks_Load();
    this.GetBusinessCategories_List();
    this._S2DocumentType_Data = this.documentType

    this.data.CountryId = 1;
    this.data.CountryCode = "nigeria";
    this.data.CountryName = "Nigeria";
    // this.documentForm.patchValue({ "streetAddress": this.data.MapAddress })
    this.HCXLoc_Manager_GetStates();

    setTimeout(() => {
      $(".pac-container").prependTo("#mapMoveHere");
    }, 500);
  }

  _ToogleContactInfo() {
    this._ContactInfo = !this._ContactInfo;
    if (!this._ContactInfo) {
        this.documentForm.controls['cpMobileNumber'].patchValue(this._cpMobileNumber);
        this.documentForm.controls['cpEmailAddress'].patchValue(this.businessEmail);
        this.documentForm.controls['cpFirstName'].patchValue(this._cpFirstName);
        this.documentForm.controls['cpLastName'].patchValue(this._cpLastName);
    } else {
        this.documentForm.controls['cpMobileNumber'].patchValue(null);
        this.documentForm.controls['cpEmailAddress'].patchValue(null);
        this.documentForm.controls['cpFirstName'].patchValue(null);
        this.documentForm.controls['cpLastName'].patchValue(null);
        this.documentForm.controls['cpMobileNumber'].markAsUntouched();
        this.documentForm.controls['cpEmailAddress'].markAsUntouched();
        this.documentForm.controls['cpFirstName'].markAsUntouched();
        this.documentForm.controls['cpLastName'].markAsUntouched();
    }
}

  GetDocType_Selected(event: any) {
    this.documentationForm.patchValue({ "docType": event.data[0].text })
    this.documentForm.patchValue({ "docType": event.data[0].text })
  }

  markerDragEnd(e) {
    // console.log('dragEnd', e.coords);
    this.data.Latitude = e.coords.lat;
    this.data.Longitude = e.coords.lng;
    this.displayMap = false
    this.documentForm.patchValue({ "lat": this.data.Latitude })
    this.documentForm.patchValue({ "lng": this.data.Longitude })
  }
  ngAfterViewInit() {
    Feather.replace();
  }

  modal() {
    $("#modal").modal("show");
  }
  docModalOpen() {
    $("#modal").modal("hide");
    this.displayMap = false
    $("#meansofidentification").modal("show");
  }

  NavigateHome() {
    $("#modal").modal("hide");
    this._location.back()
  }

  uploadDoc() {
    // console.log(this.documentationForm);

    if (this.documentationForm.value.docType == null) {
      this._HelperService.NotifyError("Please select document type")
    }
    else if (this.documentationForm.value.docFile == null) {
      this._HelperService.NotifyError("Please select Document")
    }
    else if (this.documentationForm.valid) {
      // console.log("valid");
      this.docModalClose()
    }
  }

  clearName(){
    this.modifiedName = ''    
  }

  onFileChanged(event: { target: { files: any[]; }; }) {
    if (event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      this.docName = file.name
      this.fileName = file.name;
      this.fileSize = file.size;
      this.fileType = file.type

      this.modifiedName = this.fileName.length > 20 ? this.fileName.slice(0, 20) + "..." + this.fileType.split("/")[1] : this.fileName
      this.sizeRestriction = file.size/1000000 > 1
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => {
        // console.log(reader.result);
        this.documentForm.patchValue({ "docFile": reader.result })
      };
    }
  }


  docModalClose() {
    $("#modal").modal("show");
    $("#meansofidentification").modal("hide");
  }

  cancelDoc(){
    this.documentForm.patchValue({ "docFile": null })
    this.documentationForm.patchValue({ "docFile": null })
    this.documentationForm.controls['docFile'].markAsUntouched()
    this.modifiedName = null
    this.docName = null
    this.docModalClose()
  }

  backHome() {
    $("#modal").modal("hide");
    this.router.navigate(['/m/home/setting'], { queryParams: { isComplete: true } });
  }

  public TUTr_Filter_Bank_Option: Select2Options;
  TUTr_Filter_Banks_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.getbankcodes,
      Location: this._HelperService.AppConfig.NetworkLocation.V3.Bank,
      TypeId: 1,
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },
        {
          SystemName: "Name",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        },
        {
          SystemName: "Code",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        }
      ]

    };
    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.TUTr_Filter_Bank_Option = {
      placeholder: 'Select Bank',
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }
  TUTr_Filter_Banks_Change(event: any) {
    // console.log(event.data[0]);
    this._BankDetails.BankCode = event.data[0].Code;
    this._BankDetails.BankName = event.data[0].Name;
    this._BankDetails.BankId = event.data[0].ReferenceId
    this.documentForm.patchValue({ "bank": this._BankDetails.BankName })
  }


  public _S2Categories_Data: Array<Select2OptionData>;
  GetBusinessCategories_List() {
    this.ShowCategory = false;
    var pData = {
      Task: this._HelperService.AppConfig.Api.Product.getcategories,
      Offset: 0,
      Limit: 1000,
    };

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Op, pData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          if (_Response.Result.Data != undefined) {
            var _Data = _Response.Result.Data as any[];
            if (_Data.length > 0) {
              var finalCat = [];
              this.ShowState = false;
              _Data.forEach(element => {
                var Item = {
                  id: element.ReferenceId,
                  text: element.Name,
                  ReferenceKey: element.ReferenceKey, 
                  ReferenceId: element.ReferenceId
                };
                finalCat.push(Item);
              });
              this.BCategory_Option = {
                placeholder: "Select Category",
                multiple: false,
                allowClear: false,
              };
              this._S2Categories_Data = finalCat;
              this.ShowCategory = true;             
            }
            else {
              this.BCategory_Option = {
                placeholder: "Select Category",
                multiple: false,
                allowClear: false,
              };
              setTimeout(() => {
                this.ShowCategory = true;
              }, 500);
            }
          }
          else {
            this.BCategory_Option = {
              placeholder: "Select Category",
              multiple: false,
              allowClear: false,
            };
            setTimeout(() => {
              this.ShowCategory = true;
            }, 500);
          }
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
      this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      this._HelperService.ToggleField = false;
    });
  }

  GetBusinessCategories_Selected(event: any) {
    // console.log("event", event.data[0]);
    this.selectedCategoryId = event.data[0].ReferenceId
    this.selectedCategoryKey = event.data[0].ReferenceKey
    this.documentForm.patchValue({ "selectCategory": event.data[0].text })
  }

  HCXLocManager_OpenUpdateManager_Clear() {
    this.IsAddressSet = false;
    this.data =
    {
      Latitude: 0,
      Longitude: 0,

      Address: null,
      MapAddress: null,

      CountryId: 0,
      CountryCode: null,
      CountryName: null,

      StateId: 0,
      StateName: null,
      StateCode: null,

      CityId: 0,
      CityCode: null,
      CityName: null,

      CityAreaId: 0,
      CityAreaName: null,
      CityAreaCode: null,

      PostalCode: null,
      ReferralCode: null,
      draggable: true
    }
  }
  HCXLocManager_AddressChange(address: Address) {

    this.data.Latitude = address.geometry.location.lat();
    this.data.Longitude = address.geometry.location.lng();
    this.data.MapAddress = address.formatted_address;
    this.data.Address = address.formatted_address;
    this.documentForm.patchValue({ "lat": this.data.Latitude })
    this.documentForm.patchValue({ "lng": this.data.Longitude })
    var tAddress = this._HelperService.GoogleAddressArrayToJson(address.address_components);
    // console.log("tAddress", tAddress);

    if (tAddress.postal_code != undefined && tAddress.postal_code != null && tAddress.postal_code != "") {
      this.data.PostalCode = tAddress.postal_code;
    }
    if (tAddress.country != undefined && tAddress.country != null && tAddress.country != "") {
      this.data.CountryName = tAddress.country;
    }
    if (tAddress.administrative_area_level_1 != undefined && tAddress.administrative_area_level_1 != null && tAddress.administrative_area_level_1 != "") {
      this.data.StateName = tAddress.administrative_area_level_1;
    }
    if (tAddress.locality != undefined && tAddress.locality != null && tAddress.locality != "") {
      this.data.CityName = tAddress.locality;
    }
    if (tAddress.administrative_area_level_2 != undefined && tAddress.administrative_area_level_2 != null && tAddress.administrative_area_level_2 != "") {
      this.data.CityAreaName = tAddress.administrative_area_level_2;
    }
    if (tAddress.administrative_area_level_2 != undefined && tAddress.administrative_area_level_2 != null && tAddress.administrative_area_level_2 != "") {
      this.data.CityAreaName = tAddress.administrative_area_level_2;
    }

    if (this.data.CountryName != "Nigeria") {
      this.displayMap = false
      this._HelperService.NotifyError('Currently we’re not serving in this area');

      setTimeout(() => {
        this.HCXLocManager_OpenUpdateManager_Clear();
      }, 1);
    }
    else {
      this.displayMap = true
      this.data.CountryId = 1;
      this.data.CountryCode = "nigeria";
      this.data.CountryName = "Nigeria";
      this.documentForm.patchValue({ "streetAddress": this.data.MapAddress })
      this.HCXLoc_Manager_GetStates();
    }
  }

  public HCXLocManager_S2States_Data: Array<Select2OptionData>;
  HCXLoc_Manager_GetStates() {
    this._HelperService.IsFormProcessing = true;
    this.ShowState = false;
    var PData =
    {
      Task: this._HelperService.AppConfig.Api.Core.getstates,
      ReferenceKey: this.data.CountryCode,
      ReferenceId: this.data.CountryId,
      Offset: 0,
      Limit: 1000,
    }
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.State, PData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          if (_Response.Result.Data != undefined) {
            var _Data = _Response.Result.Data as any[];
            if (_Data.length > 0) {
              var finalCat = [];
              this.ShowState = false;
              _Data.forEach(element => {
                var Item = {
                  id: element.ReferenceId,
                  key: element.ReferenceKey,
                  text: element.Name,
                  additional: element,
                };
                if (this.data.StateName != undefined || this.data.StateName != null || this.data.StateName != '') {
                  if (element.Name == this.data.StateName) {
                    this.data.StateId = Item.id;
                    this.data.StateCode = Item.key;
                    this.data.StateName = Item.text;
                    this.documentForm.patchValue({ "state": this.data.StateName })
                    this.ShowCity = false;
                    this.HCXLoc_Manager_City_Load();
                    setTimeout(() => {
                      this.ShowCity = true;
                    }, 1);
                  }
                }
                finalCat.push(Item);
              });
              if (this.data.StateId > 0) {
                this.HCXLocManager_S2States_Option = {
                  placeholder: this.data.StateName,
                  multiple: false,
                  allowClear: false,
                };
                setTimeout(() => {
                  this.ShowState = true;
                }, 500);
              }
              else {
                this.HCXLocManager_S2States_Option = {
                  placeholder: "Select state",
                  multiple: false,
                  allowClear: false,
                };
                setTimeout(() => {
                  this.ShowState = true;
                }, 500);
              }
              this.HCXLocManager_S2States_Data = finalCat;
              this.ShowState = true;
              if (this.data.CityName != undefined && this.data.CityName != null && this.data.CityName != "") {
                this.HCXLoc_Manager_GetStateCity(this.data.CityName);
              }
            }
            else {
              this.HCXLocManager_S2States_Option = {
                placeholder: "Select state",
                multiple: false,
                allowClear: false,
              };
              setTimeout(() => {
                this.ShowState = true;
              }, 500);
            }
          }
          else {
            this.HCXLocManager_S2States_Option = {
              placeholder: "Select state",
              multiple: false,
              allowClear: false,
            };
            setTimeout(() => {
              this.ShowState = true;
            }, 500);
          }
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
        this._HelperService.ToggleField = false;

      });
  }
  HCXLoc_Manager_StateSelected(Items) {
    if(this.data.MapAddress)
    {
       this.documentForm.patchValue({ "streetAddress": null})
      this.documentForm.controls['streetAddress'].markAsUntouched()
    }
    if (Items != undefined && Items.value != undefined && Items.data.length > 0 && Items.data[0].selected) {
      this.displayMap = false
      this.data.StateId = Items.data[0].id;
      this.data.StateCode = Items.data[0].key;
      this.data.StateName = Items.data[0].text;
      this.documentForm.patchValue({ "state": this.data.StateName })
      this.data.CityId = null;
      this.data.CityCode = null;
      this.data.CityName = null;
      this.data.Address = null;
      this.data.MapAddress = null;
      this.data.CityAreaName = null
      this.data.Latitude = 0;
      this.data.Longitude = 0;
      this.ShowCity = false;     
      this.documentForm.patchValue({ "city": this.data.CityName })

      this.HCXLoc_Manager_City_Load();
      setTimeout(() => {
        this.ShowCity = true;
      }, 1);
    }
  }
  HCXLoc_Manager_GetStateCity(CityName) {
    this._HelperService.IsFormProcessing = true;
    var PData =
    {
      Task: this._HelperService.AppConfig.Api.Core.getstates,
      ReferenceKey: this.data.CountryCode,
      ReferenceId: this.data.CountryId,
      SearchCondition: this._HelperService.GetSearchConditionStrict('', 'Name', 'text', CityName, '='),
      Offset: 0,
      Limit: 1,
    }
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.State, PData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          if (_Response.Result.Data != undefined) {
            var _Result = _Response.Result.Data;
            if (_Result != undefined && _Result != null && _Result.length > 0) {
              var Item = _Result[0];
              this.data.CityId = Item.ReferenceId;
              this.data.CityCode = Item.ReferenceKey;
              this.data.CityName = Item.Name;
              this.documentForm.patchValue({ "city": this.data.CityName })
              // this.ShowCity = false;
              this.HCXLoc_Manager_City_Load();
              setTimeout(() => {
                this.ShowCity = true;
              }, 1);
            }
          }
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
        this._HelperService.ToggleField = false;

      });
  }

  public HCXLoc_Manager_City_Option: Select2Options;
  HCXLoc_Manager_City_Load() {
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.Core.getcities,
      Location: this._HelperService.AppConfig.NetworkLocation.V3.City,
      ReferenceId: this.data.StateId,
      ReferenceKey: this.data.StateCode,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },
        {
          SystemName: "Name",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true,
        },
      ],
    };
    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    if (this.data.CityName != undefined || this.data.CityName != null && this.data.CityName != '') {
      this.documentForm.patchValue({ "city": this.data.CityName })
      this.HCXLoc_Manager_City_Option = {
        placeholder: this.data.CityName,
        ajax: _Transport,
        multiple: false,
        allowClear: false,
      };
    }
    else {
      this.HCXLoc_Manager_City_Option = {
        placeholder: "Select City",
        ajax: _Transport,
        multiple: false,
        allowClear: false,
      };
    }

  }
  HCXLoc_Manager_City_Change(Items: any) {
    if (Items != undefined && Items.value != undefined && Items.data.length > 0) {
      this.displayMap = false
      this.data.CityId = Items.data[0].ReferenceId;
      this.data.CityCode = Items.data[0].ReferenceKey;
      this.data.CityName = Items.data[0].Name;
      this.documentForm.patchValue({ "city": this.data.CityName })
    }
  }
  HCXLoc_Manager_Validator() {
    this._BankDetails.AccountName = this.documentForm.value.accountName
    this._BankDetails.AccountNumber = this.documentForm.value.accountNumber
    if (this.documentForm.value.bank == null) {
      this._HelperService.NotifyError("Please select bank")
    }
    else if (this.documentForm.value.selectCategory == null || this.documentForm.value.selectCategory == "Select category") {
      this._HelperService.NotifyError("Please select category")
    }
    else if (this.documentForm.value.cpFirstName == null) {
      this._HelperService.NotifyError("Please enter contact person's first name")
    }
    else if (this.documentForm.value.cpLastName == null) {
      this._HelperService.NotifyError("Please enter contact person's last name")
    }
    else if (this.documentForm.value.cpMobileNumber == null) {
      this._HelperService.NotifyError("Please enter contact person's mobile number")
    }
    else if (this.documentForm.value.cpEmailAddress == null) {
      this._HelperService.NotifyError("Please enter contact person's email address")
    }
    else if (this.documentForm.value.streetAddress == null) {
      this._HelperService.NotifyError("Please enter address")
    }
    else if (this.documentForm.value.city == null) {
      this._HelperService.NotifyError("Please select city")
    }
    else if (this.documentForm.value.state == null) {
      this._HelperService.NotifyError("Please select state")
    }
    else if (this.documentForm.value.detailAddress == null) {
      this._HelperService.NotifyError("Please enter Building/Floor number")
    }
    else if (this.documentForm.value.docFile == null) {
      this._HelperService.NotifyError("Please upload identification document")
    }
    else if (this.documentForm.invalid) {
      this._HelperService.NotifyError("Please fill all the details")
    }
    else if (this.documentForm.valid) {
      var _PostData = {
        Task: "ob_merchant_requestupdatebusinessDetails",
        ReferenceId: this.accountId,
        ReferenceKey: this.accountKey,
        BusinessName: this.documentForm.value.businessName,
        BusinessEmail: this.documentForm.value.bussinessEmail,
        Category: {
          ReferenceId: this.selectedCategoryId,
          ReferenceKey: this.selectedCategoryKey
        },
        Bankdetails: this._BankDetails,
        ContactPerson:{	
          FirstName: this.documentForm.value.cpFirstName,
          LastName: this.documentForm.value.cpLastName,
          MobileNumber: this.documentForm.value.cpMobileNumber,
          EmailAddress: this.documentForm.value.cpEmailAddress,
          countryIsd: this._HelperService.CountryCode.split('+')[1]
        },
        Address: {
          Latitude: this.documentForm.value.lat,
          Longitude: this.documentForm.value.lng,
          Address: this.documentForm.value.detailAddress + " "+ this.documentForm.value.streetAddress,
          MapAddress: this.data.MapAddress,
          CountryId: this.data.CountryId,
          CountryCode: this.data.CountryCode,
          CountryName: this.data.CountryName,
          StateId: this.data.StateId,
          StateName: this.documentForm.value.state,
          StateCode: this.data.StateCode,
          CityId: this.data.CityId,
          CityCode: this.data.CityCode,
          CityName: this.data.CityName,
          CityAreaId: 0,
          CityAreaName: this.data.CityAreaName,
          CityAreaCode: 0,
          PostalCode: this.data.PostalCode
        },
        ImageContent: {
          Name: this.fileName,
          Size: this.fileSize,
          Extension: this.fileType,
          Content: this.documentForm.value.docFile.split("base64,")[1],
          DocumentType: this.documentForm.value.docType,
          IsDefault: 0
        }
      }

      this._HelperService.IsFormProcessing = true;
      let _OResponse: Observable<OResponse>;
      _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.MOnBoardV2, _PostData);
      _OResponse.subscribe(
        _Response => {
          this._HelperService.IsFormProcessing = false;
          if (_Response.Status == this._HelperService.StatusSuccess) {
            this._HelperService.NotifySuccess('Business Details added successfully');
            this.backHome()
          }
          else {
            this._HelperService.NotifyError(_Response.Message);
            this.backHome()
          }
        },
        _Error => {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.HandleException(_Error);
          this.backHome()
        });
    }
  }
}
