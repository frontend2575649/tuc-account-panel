import { AfterViewInit, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import * as Feather from 'feather-icons';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { DataHelperService, HelperService, OResponse, OUserDetails, OSelect } from "../../service/service";
import { Observable } from 'rxjs/internal/Observable';
import { LocationStrategy } from '@angular/common';
declare var $: any;
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, AfterViewInit {
  accountId
  accountKey
  constructor(
    private router: Router,
    private activeRoute: ActivatedRoute,
    public _HelperService: HelperService,
    public _ChangeDetectorRef: ChangeDetectorRef,
    private location: LocationStrategy) { }

  queryParams: any;
  isLoaded: boolean = true;
  public MerchantAddress: any = {};
  public MerchantContactPerson: any = {};
  public _MerchantDetails: any =
    {
      ReferenceId: null,
      ReferenceKey: null,
      TypeCode: null,
      TypeName: null,
      SubTypeCode: null,
      SubTypeName: null,
      UserAccountKey: null,
      UserAccountDisplayName: null,
      Name: null,
      Description: null,
      StartDate: null,
      StartDateS: null,
      EndDate: null,
      EndDateS: null,
      SubTypeValue: null,
      MinimumInvoiceAmount: null,
      MaximumInvoiceAmount: null,
      MinimumRewardAmount: null,
      MaximumRewardAmount: null,
      ManagerKey: null,
      ManagerDisplayName: null,
      SmsText: null,
      Comment: null,
      CreateDate: null,
      CreatedByKey: null,
      CreatedByDisplayName: null,
      ModifyDate: null,
      ModifyByKey: null,
      ModifyByDisplayName: null,
      StatusId: null,
      StatusCode: null,
      StatusName: null,
      Latitude: null,
      Longitude: null,
      CreateDateS: null,
      ModifyDateS: null,
      StatusI: null,
      StatusB: null,
      StatusC: null,

    }


  ngOnInit() {
    this.accountId = this._HelperService.GetStorage("hca").UserAccount.AccountId
    this.accountKey = this._HelperService.GetStorage("hca").UserAccount.AccountKey
    const box = document.getElementById('modal');
    $("#modal").modal("show");
    if (!this._HelperService.isDocComplete) {
      this._HelperService.isDocComplete = location.href.includes('isComplete=true')
    }
    if (!this._HelperService.isAccTypeComplete) {
      this._HelperService.isAccTypeComplete = location.href.includes('isAccTypeComplete=true')
    }
    setTimeout(() => {
      this.GetMerchantDetails();
    }, 250);
  }

  // goToDashboard() {
  //   $("#modal").modal("hide");
  //   window.location.href = this._HelperService.AppConfig.Pages.ThankUCash.Dashboard.Merchant;
  // }

  goToDashboard() {
    $("#modal").modal("hide");
    var TAccessKey = this._HelperService.AccessKey;
    var TPublicKey = this._HelperService.PublicKey;
    var Key = btoa(TAccessKey + "|" + TPublicKey);

    window.location.href = this._HelperService.AppConfig.MerchantPanelURL + '/account/auth/' + Key, '_blank';
  }

  ngAfterViewInit() {
    Feather.replace();
  }
  accountType() {
    $("#modal").modal("hide");
    this.router.navigate(['m/home/acctype']);
  }

  businessDetails() {
    $("#modal").modal("hide");
    this.router.navigate(['m/home/business']);

  }

  GetMerchantDetails() {
    var pData = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetMerchantDetails,
      ReferenceId: this.accountId,
      ReferenceKey: this.accountKey,
    }
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.MOnBoardV2, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this.isLoaded = true;
          this._MerchantDetails = _Response.Result;
          this.MerchantContactPerson = _Response.Result.ContactPerson;
          this._HelperService.SaveStorage("merchantDetails", this._MerchantDetails)
          this._MerchantDetails.StatusI = this._HelperService.GetStatusIcon(this._MerchantDetails.StatusCode);
          this._MerchantDetails.StatusB = this._HelperService.GetStatusBadge(this._MerchantDetails.StatusCode);
          this._MerchantDetails.StatusC = this._HelperService.GetStatusColor(this._MerchantDetails.StatusCode);
          if (this._MerchantDetails.AddressComponent != undefined && this._MerchantDetails.AddressComponent != null) {
            this.MerchantAddress = this._MerchantDetails.AddressComponent;
          }
          this._ChangeDetectorRef.detectChanges();
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      }
    );
  }
}

enum HostType {
  Live,
  Test,
  Tech,
  Dev
}