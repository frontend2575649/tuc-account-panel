import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RegisterDashboardComponent } from './register-dashboard.component';
import { AccountComponent } from './account/account.component';
import { BussinessComponent } from './home/bussiness/bussiness.component';
import { HomeComponent } from './home/home.component';
import { RouterModule, Routes } from "@angular/router";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Select2Module } from 'ng2-select2';
import { GooglePlaceModule } from "ngx-google-places-autocomplete";
import { AgmCoreModule } from '@agm/core';
import { AgmOverlays } from "agm-overlays"

const routes: Routes = [
  {
      path: "home",
      component: RegisterDashboardComponent,
      children: [
        { path: 'setting', component: HomeComponent },
        { path: 'business', component: BussinessComponent },
        { path: 'acctype', component: AccountComponent },
      ]
  }
];

@NgModule({
  declarations: [
    RegisterDashboardComponent,
    HomeComponent,
    BussinessComponent,
    AccountComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    Select2Module,
    GooglePlaceModule,
    AgmOverlays,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyB9DMDX78ll840bMxjGL4WUfjAYXfi8vZo'
    }),
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class RegisterDashboardModule { }
