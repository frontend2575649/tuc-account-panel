import { Component, OnInit } from '@angular/core';
import * as Feather from 'feather-icons';
import { HelperService } from '../service/helper.service';

@Component({
  selector: 'app-register-dashboard',
  templateUrl: './register-dashboard.component.html',
  styleUrls: ['./register-dashboard.component.css']
})
export class RegisterDashboardComponent implements OnInit {
  constructor(
    public _HelperService: HelperService,
  ) { }

  ngOnInit() {
    Feather.replace();
  }

}
