import { AfterViewInit, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import * as Feather from 'feather-icons';
declare var $: any;
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Observable } from 'rxjs/internal/Observable';
import { DataHelperService, HelperService, OResponse, OSelect } from '../../service/service';
import {Location} from '@angular/common';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css']
})
export class AccountComponent implements OnInit, AfterViewInit {
  deals: boolean = false;
  loyalty: boolean = false;
  rewards: boolean = undefined;
  loyaltyPercentage = null;
  dealsRadio: boolean = false;
  percentVal = null
  percentError:boolean = false
  accountTypes = []
  loyaltyModel
  dealCategory
  accountId
  accountKey
  isLoaded: boolean = true;
  public MerchantAddress: any = {};
  public MerchantContactPerson: any = {};
  public _MerchantDetails: any =
    {
      ReferenceId: null,
      ReferenceKey: null,
      TypeCode: null,
      TypeName: null,
      SubTypeCode: null,
      SubTypeName: null,
      UserAccountKey: null,
      UserAccountDisplayName: null,
      Name: null,
      Description: null,
      StartDate: null,
      StartDateS: null,
      EndDate: null,
      EndDateS: null,
      SubTypeValue: null,
      MinimumInvoiceAmount: null,
      MaximumInvoiceAmount: null,
      MinimumRewardAmount: null,
      MaximumRewardAmount: null,
      ManagerKey: null,
      ManagerDisplayName: null,
      SmsText: null,
      Comment: null,
      CreateDate: null,
      CreatedByKey: null,
      CreatedByDisplayName: null,
      ModifyDate: null,
      ModifyByKey: null,
      ModifyByDisplayName: null,
      StatusId: null,
      StatusCode: null,
      StatusName: null,
      Latitude: null,
      Longitude: null,
      CreateDateS: null,
      ModifyDateS: null,
      StatusI: null,
      StatusB: null,
      StatusC: null,

    }

  constructor(
    private router: Router,
    public _HelperService: HelperService,
    public _ChangeDetectorRef: ChangeDetectorRef,
    private _location: Location) { }

  ngOnInit() {
    this._MerchantDetails = this._HelperService.GetStorage("merchantDetails")
    this.MerchantAddress = this._MerchantDetails.AddressComponent;
    this.MerchantContactPerson = this._HelperService.GetStorage("merchantDetails").ContactPerson;
    this.accountId = this._HelperService.GetStorage("hca").UserAccount.AccountId
    this.accountKey = this._HelperService.GetStorage("hca").UserAccount.AccountKey
    $("#modal").modal("show");
  }

  ngAfterViewInit() {
    Feather.replace();
  }
  selectSolution(event, data) {
    if (this.loyalty == false && data == 'tuLoyalty') {
      const box = document.getElementById('loyaltyCard');
      if (box != null) {
        box.classList.add('loyaltyCard');
      }
      this.loyalty = true;
    } else if (this.loyalty == true && data == 'tuLoyalty') {
      const box = document.getElementById('loyaltyCard');

      if (box != null) {
        box.classList.remove('loyaltyCard');
      }
      this.rewards = undefined;
      this.loyalty = false;
    } else if (this.deals == false && data == 'tuDeals') {
      const box = document.getElementById('dealsCard');

      if (box != null) {
        box.classList.add('dealsCard');
      }
      this.deals = true;
    } else if (this.deals == true && data == 'tuDeals') {
      const box = document.getElementById('dealsCard');

      if (box != null) {
        box.classList.remove('dealsCard');
      }
      this.dealsRadio = false;
      this.deals = false;
    }
  }

  rewardSelect(event) {
    if (event.target.id == 'tuLoyaltyRadio1') {
      this.rewards = true;
      this.loyaltyModel = "openloyaltymodel"
      this.loyaltyPercentage = null
      this.percentError = false
    } else if (event.target.id == 'tuLoyaltyRadio2') {
      this.rewards = false;
      this.loyaltyModel = "closedloyaltymodel"
      this.loyaltyPercentage = null
      this.percentError = false
    }
  }

  onClick() {
    if (this.deals && this.loyalty) {
      if (this.percentVal == null) {
        this._HelperService.NotifyError("Please add loyalty percentage value")
      }
      else if (this.percentVal > 100 || this.percentVal < 0) {
        this._HelperService.NotifyError("Please add value between 0-100")
      } else {
        this.accountTypes.push(
          { AccountTypeCodes: "thankucashloyalty", ConfigurationValue: this.loyaltyModel, Value: this.percentVal },
          { AccountTypeCodes: "thankucashdeals", value: this.dealCategory }
        )
      }
    }
    else if (this.deals) {
      this.accountTypes.push({ AccountTypeCodes: "thankucashdeals", Value: this.dealCategory })
    }
    else if (this.loyalty) {
      if (this.percentVal == null) {
        this._HelperService.NotifyError("Please add loyalty percentage value")
      }
      else if (this.percentVal > 100 || this.percentVal < 0) {
        this._HelperService.NotifyError("Please add value between 0-100")
      } else {
        this.accountTypes.push(
          { AccountTypeCodes: "thankucashloyalty", ConfigurationValue: this.loyaltyModel, Value: this.percentVal }
        )
      }
    }

    // console.log("loyalty", this.accountTypes);
    var _PostData = {
      Task: "ob_merchant_requestaccountconfiguration",
      ReferenceId: this.accountId ,
      ReferenceKey: this.accountKey,
      AccountTypes: this.accountTypes
    }

    // console.log("_PostData", _PostData);

    if (this.accountTypes.length > 0) {
      this._HelperService.IsFormProcessing = true;
      let _OResponse: Observable<OResponse>;
      _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.MOnBoardV2, _PostData);
      _OResponse.subscribe(
        _Response => {
          this.accountTypes = []
          this._HelperService.IsFormProcessing = false;
          if (_Response.Status == this._HelperService.StatusSuccess) {
            this._HelperService.NotifySuccess('Account Details added successfully');

          }
          else {
            this._HelperService.NotifyError(_Response.Message);
          }
        },
        _Error => {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.HandleException(_Error);
        });

      $("#modal").modal("hide");
      this.router.navigate(['/m/home/setting'], { queryParams: { isAccTypeComplete: true } });
    }
  }
  viewPricing() {
    $("#modal").modal("hide");
    $("#doneModal").modal("show");
  }

  backFromPricing() {
    $("#doneModal").modal("hide");
    $("#modal").modal("show");
  }

  goToDashboard() {
    $("#onBoardingModal").modal("hide");
    $("#doneModal").modal("hide");
    $("#modal").modal("hide");
    window.location.href = this._HelperService.AppConfig.Pages.ThankUCash.Dashboard.Merchant;
  }

  NavigateHome() {
    $("#modal").modal("hide");
    this._location.back()
  }
  percentage(percentage) {   
    this.percentError = percentage <= 0 || percentage >=100
    this.percentVal = percentage
    
  }
  dealsChange(event) {
    if (event.target.id == 'tuDealsRadio1') {
      this.dealCategory = "0"
    } else if (event.target.id == 'tuDealsRadio2') {
      this.dealCategory = "1"

    } else if (event.target.id == 'tuDealsRadio3') {
      this.dealCategory = "2"
    }
    this.dealsRadio = true;
  }
}
