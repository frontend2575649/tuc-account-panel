import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';

import { TranslateModule } from '@ngx-translate/core';
import { Select2Module } from 'ng2-select2';
import { NgxPaginationModule } from 'ngx-pagination';
import { Daterangepicker } from 'ng2-daterangepicker';
import { Ng2FileInputModule } from 'ng2-file-input';
import { GooglePlaceModule } from "ngx-google-places-autocomplete";
import { AgmCoreModule } from '@agm/core';
import { HCProfileComponent } from './hcprofile.component';
import { ImageCropperModule } from 'ngx-image-cropper';
import { InputFileConfig, InputFileModule } from 'ngx-input-file';
import { HCXAddressManagerModule } from '../../component/hcxaddressmanager/hcxaddressmanager.component';

const config: InputFileConfig = {
    fileAccept: '*',
    fileLimit: 1,
};

const routes: Routes = [
    { path: '', component: HCProfileComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class HCProfileRoutingModule { }

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        TranslateModule,
        HCProfileRoutingModule,
        Select2Module,
        NgxPaginationModule,
        Daterangepicker,
        Ng2FileInputModule,
        GooglePlaceModule,
        ImageCropperModule,
        HCXAddressManagerModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyB9DMDX78ll840bMxjGL4WUfjAYXfi8vZo'
        }),
        InputFileModule.forRoot(config),
    ],
    declarations: [HCProfileComponent]
})
export class HCProfileModule { }