import { Component, OnInit, ViewChild, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { ActivatedRoute, Router, Params } from '@angular/router';
import swal from 'sweetalert2';
import { OSelect, OList, DataHelperService, HelperService, OResponse, OStorageContent, OCoreParameter, OCoreCommon, OUserDetails } from '../../service/service';
import { GooglePlaceDirective } from 'ngx-google-places-autocomplete';
import { Address } from 'ngx-google-places-autocomplete/objects/address';
import { request } from 'http';
import { InputFileComponent, InputFile } from 'ngx-input-file';
import { HCXAddress, HCXAddressConfig, locationType } from '../../component/hcxaddressmanager/hcxaddressmanager.component';

@Component({
    selector: 'hc-profile',
    templateUrl: './hcprofile.component.html',
})
export class HCProfileComponent implements OnInit, OnDestroy {
    ngOnDestroy(): void {
        this._HelperService._FileSelect_Icon_Reset();
        this._HelperService.Icon_Crop_Clear();
    }
    @ViewChild(InputFileComponent)
    private InputFileComponent: InputFileComponent;
    CurrentImagesCount: number = 0;

    public ShowCategorySelector: boolean = true;
    public _AddressShow = true;
    public _Address: HCXAddress = {};
    public _AddressConfig: HCXAddressConfig =
        {
            locationType: locationType.form
        };

    AddressChange(Address) {
        this._Address = Address;
    }

    public mapheight = '290px'
    constructor(
        public _Router: Router,
        public _ActivatedRoute: ActivatedRoute,
        public _FormBuilder: FormBuilder,
        public _HelperService: HelperService,
        public _DataHelperService: DataHelperService,
        public _ChangeDetectorRef: ChangeDetectorRef
    ) {
    }
    ngOnInit() {
        this._HelperService.FullContainer = true;
        this._HelperService.setTheme();
        this.Form_UpdateUser_Load();
        this.Get_UserAccountDetails();
        this.Form_EditPassword_Load();
        this.Form_UpdateContactUser_Load();
        this.Form_Edit_Load();
        this.Form_UpdateCredentialUser_Load();
        this.GetBusinessCategories();
        this.Form_Security_Load();
        // this._HelperService.Icon_Crop_Clear();
        this.GetMerchantDetails();
        this.GetMerchantSecurity();
        if (this.InputFileComponent != undefined) {
            this._HelperService._InputFileComponent = this.InputFileComponent;
            this.InputFileComponent.onChange = (files: Array<InputFile>): void => {
                if (files.length >= this.CurrentImagesCount) {
                    this._HelperService._SetFirstImageOrNone(this.InputFileComponent.files);
                }
                this.CurrentImagesCount = files.length;
            }
        }
    }

    OShowPassword: boolean = true;
    OToogleShowHidePassword(): void {
        this.OShowPassword = !this.OShowPassword;
    }

    NShowPassword: boolean = true;
    NToogleShowHidePassword(): void {
        this.NShowPassword = !this.NShowPassword;
    }


    private InitImagePicker(previewurl?: string) {
        if (this.InputFileComponent != undefined) {
            this.CurrentImagesCount = 0;
            if (previewurl) {
                this.InputFileComponent.files[0] = {};
                this.InputFileComponent.files[0].preview = previewurl;
            }
            this._HelperService._InputFileComponent = this.InputFileComponent;
            this.InputFileComponent.onChange = (files: Array<InputFile>): void => {
                if (files.length >= this.CurrentImagesCount) {
                    this._HelperService._SetFirstImageOrNone(this.InputFileComponent.files);
                }
                this.CurrentImagesCount = files.length;
            };
        }
    }

    resetClose() {
        this._HelperService._InputFileComponent.files[0] = {};
        this._HelperService._InputFileComponent.files[0].preview = this._MerchantDetails.IconUrl        
        // this.InputFileComponent.files.pop();
        this._HelperService.Icon_Crop_Clear();
    }

    public _MerchantDetails: any =
        {
            ReferenceId: null,
            ReferenceKey: null,
            TypeCode: null,
            TypeName: null,
            SubTypeCode: null,
            SubTypeName: null,
            UserAccountKey: null,
            UserAccountDisplayName: null,
            Name: null,
            Description: null,
            StartDate: null,
            StartDateS: null,
            EndDate: null,
            EndDateS: null,
            SubTypeValue: null,
            MinimumInvoiceAmount: null,
            MaximumInvoiceAmount: null,
            MinimumRewardAmount: null,
            MaximumRewardAmount: null,
            ManagerKey: null,
            ManagerDisplayName: null,
            SmsText: null,
            Comment: null,
            CreateDate: null,
            CreatedByKey: null,
            CreatedByDisplayName: null,
            ModifyDate: null,
            ModifyByKey: null,
            ModifyByDisplayName: null,
            StatusId: null,
            StatusCode: null,
            StatusName: null,
            Latitude: null,
            Longitude: null,
            CreateDateS: null,
            ModifyDateS: null,
            StatusI: null,
            StatusB: null,
            StatusC: null,

        }
    public isReadOnly = true;
    EditPassword() {
        if (this.isReadOnly == true) {
            this.isReadOnly = false;
        }
        else {
            this.isReadOnly = true;
        }
    }
    public Get_UserAccountDetails() {
        this._HelperService.IsFormProcessing = true;
        this._AddressShow = false;
        var pData = {
            Task: this._HelperService.AppConfig.Api.Core.GetUserAccount,
            Reference: this._HelperService.GetSearchConditionStrict('', 'ReferenceKey', this._HelperService.AppConfig.DataType.Text, this._HelperService.AppConfig.ActiveOwnerKey, '='),
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.System, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._HelperService._UserAccount = _Response.Result as OUserDetails;
                    this._HelperService._UserAccount.CreateDateS = this._HelperService.GetDateTimeS(this._HelperService._UserAccount.CreateDate);
                    this._HelperService._UserAccount.ModifyDateS = this._HelperService.GetDateTimeS(this._HelperService._UserAccount.ModifyDate);
                    this._HelperService._UserAccount.StatusI = this._HelperService.GetStatusIcon(this._HelperService._UserAccount.StatusCode);
                    this._HelperService.IsFormProcessing = false;
                    if (this._HelperService._UserAccount != undefined && this._HelperService._UserAccount.MobileNumber != undefined && this._HelperService._UserAccount.MobileNumber != null) {
                        if (this._HelperService._UserAccount.MobileNumber.startsWith("234")) {
                            this._HelperService._UserAccount.MobileNumber = this._HelperService._UserAccount.MobileNumber.substring(2, this._HelperService._UserAccount.MobileNumber.length);
                        }
                    }
                    //#region Patch Profile
                    if (this._HelperService._UserAccount.AddressComponent != undefined && this._HelperService._UserAccount.AddressComponent != null) {
                        this._Address = this._HelperService._UserAccount.AddressComponent;
                        // console.log(this._Address);
                    }
                    this._AddressShow = true;
                    //#endregion
                }
                else {
                    this._HelperService.IsFormProcessing = false;

                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {

                this._HelperService.HandleException(_Error);
            });
    }

    //#region User
    Form_UpdateUser: FormGroup;
    Form_UpdateUser_Show() {
    }
    Form_UpdateUser_Close() {
        this._Router.navigate([this._HelperService.AppConfig.Pages.System.CoreUsers]);
    }
    Form_UpdateUser_Load() {
        this._HelperService._FileSelect_Icon_Data.Width = 128;
        this._HelperService._FileSelect_Icon_Data.Height = 128;

        this._HelperService._FileSelect_Poster_Data.Width = 800;
        this._HelperService._FileSelect_Poster_Data.Height = 400;

        this.Form_UpdateUser = this._FormBuilder.group({
            Task: this._HelperService.AppConfig.Api.Core.updatemerchantdetails,
            Name: [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(256)])],
            OwnerName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(256)])],
            DisplayName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(30)])],
            ContactNumber: [null, Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(14)])],
            EmailAddress: [null, Validators.compose([Validators.required, Validators.email, Validators.minLength(2)])],
            GenderCode: this._HelperService.AppConfig.Gender.Male,
            DateOfBirth: null,
            ReferralCode: null,
            WebsiteUrl: [null],
            Description: null,
            // ReferralCode: null,
            // ReferralUrl: null,

        });
    }
    Form_UpdateUser_Clear() {
        this._HelperService._FileSelect_Icon_Reset();
        this._HelperService._FileSelect_Poster_Reset();
        this.Form_UpdateUser_Load();
    }

    DoesCatAlreadyExist(Id: number): boolean {
        for (let index = 0; index < this._MerchantDetails.UpdateCategories.length; index++) {
            const element = this._MerchantDetails.UpdateCategories[index];
            if (element.ReferenceId == Id) {
                return true;
            }
        }
        return false;
    }

    removeCategory(cat: any): void {
        this._MerchantDetails.UpdateCategories.splice(cat, 1);
    }

    Form_UpdateUser_Process(_FormValue: any) {
        swal({
            position: 'top',
            title: this._HelperService.AppConfig.CommonResource.UpdateTitle,
            // text: this._HelperService.AppConfig.CommonResource.UpdateHelp,
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Blue,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
        }).then((result) => {
            if (result.value) {
                // _FormValue.AuthPin = result.value;

                _FormValue.Categories = [];
                if (this.SelectedBusinessCategories != undefined && this.SelectedBusinessCategories.length > 0) {
                    this.SelectedBusinessCategories.forEach(element => {
                        if (!this.DoesCatAlreadyExist(element)) {
                            _FormValue.Categories.push(
                                {
                                    // ReferenceKey: element.key,
                                    ReferenceId: element
                                }
                            )
                        }
                    });


                }

                _FormValue.Categories = _FormValue.Categories.concat(this._MerchantDetails.UpdateCategories);


                var Request = this.CreateRequestJson();
                this._HelperService.IsFormProcessing = true;
                let _OResponse: Observable<OResponse>;
                _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Account, Request);
                _OResponse.subscribe(
                    _Response => {
                        this._HelperService.IsFormProcessing = false;
                        if (_Response.Status == this._HelperService.StatusSuccess) {
                            this.GetBusinessCategories()
                            this._HelperService.emitDetailsChangeEvent();
                            this._HelperService.FlashSwalSuccess('Changes Updated', 'Done! Changes Updated Successfully');
                            this.GetMerchantDetails();
                            this._HelperService.Icon_Crop_Clear();
                        }
                        else {
                            this._HelperService.NotifyError('Unable to update account details');
                        }
                    },
                    _Error => {
                        this._HelperService.IsFormProcessing = false;
                        this._HelperService.HandleException(_Error);
                    });
            }
        });

    }
    public BusinessCategories = [];
    public S2BusinessCategories = [];

    GetBusinessCategories() {

        this._HelperService.ToggleField = true;
        var PData =
        {
            Task: this._HelperService.AppConfig.Api.Core.getcategories,
            SearchCondition: this._HelperService.GetSearchConditionStrict('', 'StatusCode', this._HelperService.AppConfig.DataType.Text, 'default.active', '='),
            SortExpression: 'Name asc',
            Offset: 0,
            Limit: 1000,
        }
        PData.SearchCondition = this._HelperService.GetSearchConditionStrict(
            PData.SearchCondition,
            "TypeCode",
            this._HelperService.AppConfig.DataType.Text,
            this._HelperService.AppConfig.HelperTypes.MerchantCategories,
            "="
        );
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Op, PData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    if (_Response.Result.Data != undefined) {
                        this.BusinessCategories = _Response.Result.Data;

                        this.ShowCategorySelector = false;
                        this._ChangeDetectorRef.detectChanges();
                        for (let index = 0; index < this.BusinessCategories.length; index++) {
                            const element = this.BusinessCategories[index];
                            this.S2BusinessCategories.push(
                                {
                                    id: element.ReferenceId,
                                    key: element.ReferenceKey,
                                    text: element.Name
                                }
                            );
                        }
                        this.ShowCategorySelector = true;
                        this._HelperService.setTheme();
                        this._ChangeDetectorRef.detectChanges();

                        this._HelperService.ToggleField = false;

                    }
                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);
                this._HelperService.ToggleField = false;

            });
    }
    public SelectedBusinessCategories = [];
    CategoriesSelected(Items) {
        // console.log(Items);
        if (Items != undefined && Items.value != undefined && Items.value.length > 0) {
            this.SelectedBusinessCategories = Items.value;
        }
        else {
            this.SelectedBusinessCategories = [];
        }

    }
    SaveMerchantBusinessCategory(item) {
        if (item != '0') {
            var Setup =
            {
                Task: this._HelperService.AppConfig.Api.Core.SaveUserParameter,
                TypeCode: this._HelperService.AppConfig.HelperTypes.MerchantCategories,
                // UserAccountKey: this._UserAccount.ReferenceKey,
                CommonKey: item,
                StatusCode: 'default.active'
            };
            let _OResponse: Observable<OResponse>;
            _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.System, Setup);
            _OResponse.subscribe(
                _Response => {
                    if (_Response.Status == this._HelperService.StatusSuccess) {
                        this._HelperService.NotifySuccess('Business category assigned to merchant');
                        // this.BusinessCategories = [];
                        // this.GetMerchantBusinessCategories();
                    }
                    else {
                        this._HelperService.NotifyError(_Response.Message);
                    }
                },
                _Error => {
                    this._HelperService.IsFormProcessing = false;
                    this._HelperService.HandleException(_Error);
                });
        }
    }
    //#endregion

    //#region Credential
    Form_UpdateCredentialUser: FormGroup;
    Form_UpdateCredentialUser_Show() {
    }
    Form_UpdateCredentialUser_Close() {
        this._Router.navigate([this._HelperService.AppConfig.Pages.System.CoreUsers]);
    }
    Form_UpdateCredentialUser_Load() {
        this._HelperService._FileSelect_Icon_Data.Width = 128;
        this._HelperService._FileSelect_Icon_Data.Height = 128;

        this._HelperService._FileSelect_Poster_Data.Width = 800;
        this._HelperService._FileSelect_Poster_Data.Height = 400;

        this.Form_UpdateCredentialUser = this._FormBuilder.group({
            OperationType: 'new',
            Task: this._HelperService.AppConfig.Api.Core.UpdateUserAccount,
            // AccountTypeCode: this._HelperService.AppConfig.AccountType.Admin,
            // AccountOperationTypeCode: this._HelperService.AppConfig.AccountOperationType.Online,
            // RegistrationSourceCode: this._HelperService.AppConfig.RegistrationSource.System,
            OwnerKey: null,
            RoleKey: null,
            UserName: [null, Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(256)])],
            Description: null,
            ReferralCode: null,
            ReferralUrl: null,

            CountValue: 0,
            AverageValue: 0,

            ApplicationStatusCode: null,

            EmailVerificationStatus: 0,
            EmailVerificationStatusDate: null,

            NumberVerificationStatus: 0,
            NumberVerificationStatusDate: null,

            CountryKey: null,
            RegionKey: null,
            RegionAreaKey: null,
            CityKey: null,
            CityAreaKey: null,

            IconContent: this._HelperService._FileSelect_Icon_Data,
            PosterContent: this._HelperService._FileSelect_Poster_Data,

            Owners: [],
            Configuration: [],
        });
    }
    Form_UpdateCredentialUser_Clear() {
        this._HelperService._FileSelect_Icon_Reset();
        this._HelperService._FileSelect_Poster_Reset();
        this.Form_UpdateCredentialUser_Load();
    }
    Form_UpdateCredentialUser_Process(_FormValue: any) {

        this._HelperService.IsFormProcessing = true;
        let _OResponse: Observable<OResponse>;
        var pData = {
            Task: "requestotpforusernameupdate",
            Type: 2,
            CountryIsd: this._HelperService.GetStorage("hca").UserCountry.CountryIsd,
            EmailMessage:"Username verification code",
            BussinessName:this._HelperService.GetStorage("hca").UserAccount.DisplayName,
            EmailAddress:this._HelperService.GetStorage("hca").User.EmailAddress
        }
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.System, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsFormProcessing = false;
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._HelperService.NotifySuccess(_Response.Message);
                    let response_reqotp = _Response.Result;
                    swal({
                        title: "Enter Verification code received in mail",
                        text: "You must enter code to update username",
                        showCancelButton: true,
                        position: this._HelperService.AppConfig.Alert_Position,
                        animation: this._HelperService.AppConfig.Alert_AllowAnimation,
                        customClass: this._HelperService.AppConfig.Alert_Animation,
                        allowOutsideClick: this._HelperService.AppConfig.Alert_AllowOutsideClick,
                        allowEscapeKey: this._HelperService.AppConfig.Alert_AllowEscapeKey,
                        confirmButtonColor: this._HelperService.AppConfig.Color_Red,
                        cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
                        confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
                        cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
                        input: 'text',
                        inputClass: 'swalText',
                        inputPlaceholder: "Enter verification code",
                        inputAttributes: {
                            // autocapitalize: 'off',
                            // autocorrect: 'off',
                            maxLength: "6",
                            // minLength: "6"
                        },
                        preConfirm: (result) => {
                            if(result === null || result.length < 6  || result === undefined){
                                swal.showValidationMessage(`Verification code must be of 6 digits`);
                                swal.enableConfirmButton();
                            }else{
                                swal.resetValidationMessage();
                            }
                         },
                    }).then((result) => {
                        if (result.value) {
                            var pData = {
                                Task: "verifyotpforusernameupdate",
                                RequestToken: response_reqotp.RequestToken,
                                AccessCode: result.value,
                                AccessKey: response_reqotp.Accesskey
                            }
                            this._HelperService.IsFormProcessing = true;
                            let _OResponse: Observable<OResponse>;
                            _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.System, pData);
                            _OResponse.subscribe(
                                _Response => {
                                    this._HelperService.IsFormProcessing = false;
                                    if (_Response.Status == this._HelperService.StatusSuccess) {
                                        _FormValue.ReferenceKey = this._HelperService.AppConfig.ActiveOwnerKey;
                                        this._HelperService.IsFormProcessing = true;
                                        let _OResponse: Observable<OResponse>;
                                        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.System, _FormValue);
                                        _OResponse.subscribe(
                                            _Response => {
                                                this._HelperService.IsFormProcessing = false;
                                                if (_Response.Status == this._HelperService.StatusSuccess) {
                                                    this._HelperService.emitDetailsChangeEvent();
                                                    this._HelperService.NotifySuccess('Account details updated successfully');
                                                }
                                                else {
                                                    this._HelperService.NotifyError('Unable to udpate account details');
                                                }
                                            },
                                            _Error => {
                                                this._HelperService.IsFormProcessing = false;
                                                this._HelperService.HandleException(_Error);
                                            });
                                    }
                                    else {
                                        this._HelperService.NotifyError('Invalid verification code');
                                    }
                                },
                                _Error => {
                                    this._HelperService.IsFormProcessing = false;
                                    this._HelperService.HandleException(_Error);
                                });
                        }
                    });
                }
                else {
                    this._HelperService.NotifyError('Unable to send verifiaction code in mail');
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);
            });

    }
    //#endregion

    //#region Contact
    Form_UpdateContactUser: FormGroup;
    Form_UpdateContactUser_Show() {
    }
    Form_UpdateContactUser_Close() {
        this._Router.navigate([this._HelperService.AppConfig.Pages.System.CoreUsers]);
    }
    Form_UpdateContactUser_Load() {
        this._HelperService._FileSelect_Icon_Data.Width = 128;
        this._HelperService._FileSelect_Icon_Data.Height = 128;

        this._HelperService._FileSelect_Poster_Data.Width = 800;
        this._HelperService._FileSelect_Poster_Data.Height = 400;

        this.Form_UpdateContactUser = this._FormBuilder.group({
            // Task: this._HelperService.AppConfig.Api.Core.updatemerchantdetails,
            FirstName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(128)])],
            LastName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(128)])],
            MobileNumber: [null, Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(14)])],
            EmailAddress: [null, Validators.compose([Validators.required, Validators.email, Validators.minLength(2)])],
        });
    }
    Form_UpdateContactUser_Clear() {
        this._HelperService._FileSelect_Icon_Reset();
        this._HelperService._FileSelect_Poster_Reset();
        this.Form_UpdateContactUser_Load();
    }
    Form_UpdateContactUser_Process(_FormValue: any) {
        swal({
            position: 'top',
            title: this._HelperService.AppConfig.CommonResource.UpdateTitle,
            // text: this._HelperService.AppConfig.CommonResource.UpdateHelp,
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Blue,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
        }).then((result) => {
            if (result.value) {
                // _FormValue.AuthPin = result.value;

                var Request = this.CreateRequestJson();

                this._HelperService.IsFormProcessing = true;
                let _OResponse: Observable<OResponse>;
                _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Account, Request);
                _OResponse.subscribe(
                    _Response => {
                        this._HelperService.IsFormProcessing = false;
                        if (_Response.Status == this._HelperService.StatusSuccess) {
                            this._HelperService.emitDetailsChangeEvent();
                            this._HelperService.FlashSwalSuccess('Changes Updated', 'Done! Changes Updated Successfully');
                        }
                        else {
                            this._HelperService.NotifyError('Unable to udpate account details');
                        }
                    },
                    _Error => {
                        this._HelperService.IsFormProcessing = false;
                        this._HelperService.HandleException(_Error);
                    });
            }
        });

    }
    //#endregion 

    //#region Map
    // _CurrentAddress: any = {};
    // _CurrentAddressStore: any = {};
    //#endregion

    //#region Address
    Form_UpdateAddressUser_Process() {
       
        if(this._Address.CityName == "Pune" || this._Address.StateId == 43){
            if(this._Address.CityName == "Pune") 
            this._HelperService.NotifyError("Please select the Valid City ")
            else{
                this._HelperService.NotifyError("Please select the Valid State ")
            }
         }
        else if (this._Address.CityId < 1) {
            this._HelperService.NotifyError("Please select city");
         }
        else {

            swal({
                position: 'top',
                title: this._HelperService.AppConfig.CommonResource.UpdateTitle,
                // text: this._HelperService.AppConfig.CommonResource.UpdateHelp,
                animation: false,
                customClass: this._HelperService.AppConfig.Alert_Animation,
                showCancelButton: true,
                confirmButtonColor: this._HelperService.AppConfig.Color_Blue,
                cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
                confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
                cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
            }).then((result) => {
                if (result.value) {
                    // _FormValue.AuthPin = result.value;
                    var Request = this.CreateRequestJson();
                    this._HelperService.IsFormProcessing = true;
                    let _OResponse: Observable<OResponse>;
                    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Account, Request);
                    _OResponse.subscribe(
                        _Response => {
                            this._HelperService.IsFormProcessing = false;
                            if (_Response.Status == this._HelperService.StatusSuccess) {
                                this._HelperService.emitDetailsChangeEvent();
                                this._HelperService.FlashSwalSuccess('Changes Updated', 'Done! Changes Updated Successfully');
                            }
                            else {
                                this._HelperService.NotifyError('Unable to udpate account details');
                            }
                        },
                        _Error => {
                            this._HelperService.IsFormProcessing = false;
                            this._HelperService.HandleException(_Error);
                        });
                }
            });
        }

    }
    //#endregion


    //#region Security
    Emails: string[] = [];
    Email: string;
    EmailIds = [];

    KeyUP(): void {
        this.Emails = this.Configuration.EmailAddress.split(';');

    }
    AddEmail() {
        for (let index = 0; index < this.Emails.length; index++) {
            const element = this.Emails[index];
            if (element.trim().length > 1) {
                this.EmailIds.push({
                    securitynotificationemails: element,
                    securitymaximuminvoiceamountpertransactioncap: this.Form_Security.controls['securitymaximuminvoiceamountpertransactioncap'].value,
                    securitymaximumtransactionperdaycap: this.Form_Security.controls['securitymaximumtransactionperdaycap'].value,
                    securitymaximuminvoiceamountperdaycap: this.Form_Security.controls['securitymaximuminvoiceamountperdaycap'].value
                });
            }
            else {
                // this._HelperService.NotifyError('Enter Terminal Id')
            }

        }
        this.Emails = [];
        this.Email = null;
    }

    Form_Security: FormGroup;
    Form_Security_Show() {
    }
    Form_Security_Close() {
        this._Router.navigate([this._HelperService.AppConfig.Pages.System.CoreUsers]);
    }
    Form_Security_Load() {
        this._HelperService._FileSelect_Icon_Data.Width = 128;
        this._HelperService._FileSelect_Icon_Data.Height = 128;

        this._HelperService._FileSelect_Poster_Data.Width = 800;
        this._HelperService._FileSelect_Poster_Data.Height = 400;

        this.Form_Security = this._FormBuilder.group({
            Task: this._HelperService.AppConfig.Api.Core.updatemerchantdetails,
            securitymaximuminvoiceamountpertransactioncap: [null, Validators.required],
            securitymaximumtransactionperdaycap: [null, Validators.required],
            securitymaximuminvoiceamountperdaycap: [null, Validators.required],

        });
    }
    Form_Security_Clear() {
        this._HelperService._FileSelect_Icon_Reset();
        this._HelperService._FileSelect_Poster_Reset();
        this.Form_Security_Load();
    }
    Form_Security_Process(_FormValue: any) {
        swal({
            position: 'top',
            title: this._HelperService.AppConfig.CommonResource.UpdateTitle,
            // text: this._HelperService.AppConfig.CommonResource.UpdateHelp,
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Blue,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
        }).then((result) => {
            if (result.value) {
                // _FormValue.AuthPin = result.value;

                var BulkEmails = {
                    Task: this._HelperService.AppConfig.Api.Core.saveconfigurations,
                    AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
                    AccountId: this._HelperService.AppConfig.ActiveOwnerId,
                    Items: this.EmailIds,
                }

                this._HelperService.IsFormProcessing = true;
                let _OResponse: Observable<OResponse>;

                _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Operations, BulkEmails);
                _OResponse.subscribe(
                    _Response => {
                        this._HelperService.IsFormProcessing = false;
                        if (_Response.Status == this._HelperService.StatusSuccess) {
                            this._HelperService.FlashSwalSuccess('Changes Updated', 'Done! Changes Updated Successfully');
                        }
                        else {
                            this._HelperService.NotifyError('Unable to udpate account details');
                        }
                    },
                    _Error => {
                        this._HelperService.IsFormProcessing = false;
                        this._HelperService.HandleException(_Error);
                    });
            }
        });

    }


    CreateRequestJsonSecurtiy(): any {
        var AccountDetailSecurtiy = this.Form_Security.value;
        AccountDetailSecurtiy.Task = this._HelperService.AppConfig.Api.Core.saveconfigurations,
            AccountDetailSecurtiy.AccountKey = this._HelperService.AppConfig.ActiveOwnerKey;
        AccountDetailSecurtiy.AccountId = this._HelperService.AppConfig.ActiveOwnerId;
        AccountDetailSecurtiy.Items = this.EmailIds

        return AccountDetailSecurtiy;


    }


    //#endregion



    //#region password
    Form_EditPassword: FormGroup;
    Form_EditPassword_Load() {
        this.Form_EditPassword = this._FormBuilder.group({
            OperationType: 'new',
            ReferenceKey: this._HelperService.AppConfig.ActiveOwnerKey,
            Task: this._HelperService.AppConfig.Api.Core.UpdateUserAccountPassword,
            OldPassword: [null, Validators.required],
            Password: [null, Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(20), Validators.pattern(this._HelperService.AppConfig.ValidatorRegex.Password)])],
        });
    }
    Form_EditPassword_Clear() {
        this.Form_EditPassword.reset();
    }
    Form_EditPassword_Process(_FormValue: any) {
        swal({
            position: 'top',
            title: 'Update password ?',
            // text: this._HelperService.AppConfig.CommonResource.UpdateHelp,
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Blue,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
        }).then((result) => {
            if (result.value) {
                _FormValue.ReferenceKey = this._HelperService.AppConfig.ActiveOwnerKey;
                // _FormValue.AuthPin = result.value;
                this._HelperService.IsFormProcessing = true;
                let _OResponse: Observable<OResponse>;
                if (_FormValue.OldPassword != _FormValue.Password) {
                    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.System, _FormValue);
                    _OResponse.subscribe(
                        _Response => {
                            this._HelperService.IsFormProcessing = false;
                            if (_Response.Status == this._HelperService.StatusSuccess) {
                                this._HelperService.NotifySuccess('Password updated');
                                this.Form_EditPassword_Clear();
                                this.Form_EditPassword.controls.ReferenceKey.setValue(this._HelperService.AppConfig.ActiveOwnerKey);
                                this.Form_EditPassword.controls.Task.setValue(this._HelperService.AppConfig.Api.Core.UpdateUserAccountPassword);
                                
            
                            }
                            else {
                                this._HelperService.NotifyError('Enter valid old password');
                            }
                        },
                        _Error => {
                            this._HelperService.IsFormProcessing = false;
                            this._HelperService.HandleException(_Error);
                        });
                }
                else {
                    this._HelperService.NotifyError('Old Password and New Password should not be same');
                    this._HelperService.IsFormProcessing = false;
                }


            }
        });

    }
    //#endregion


    CreateRequestJson(): any {
        var AccountDetail = this.Form_UpdateUser.value;
        AccountDetail.Address = this._Address.Address;
        AccountDetail.AddressComponent = this._Address;
        var ContactPersonDetail = this.Form_UpdateContactUser.value;
        AccountDetail.ContactPerson = ContactPersonDetail;

        var IconContent: any = undefined;
        if (this._HelperService._Icon_Cropper_Data.Content != null) {
            IconContent = this._HelperService._Icon_Cropper_Data;
        }
        AccountDetail.IconContent = IconContent;
        AccountDetail.AccountKey = this._HelperService.AppConfig.ActiveOwnerKey;
        AccountDetail.AccountId = this._HelperService.AppConfig.ActiveOwnerId;
        return AccountDetail;
    }

    Form_Edit: FormGroup;
    Form_Edit_Load() {
        this.Form_Edit = this._FormBuilder.group({
            OperationType: 'new',
            ReferenceKey: this._HelperService.AppConfig.ActiveOwnerKey,
            Task: this._HelperService.AppConfig.Api.Core.UpdateUserAccountAccessPin,
            OldAccessPin: [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(4)])],
            AccessPin: [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(4)])],
        });
    }
    Form_Edit_Clear() {
        this.Form_Edit.reset();
    }
    Form_Edit_Process(_FormValue: any) {
        swal({
            position: 'top',
            title: this._HelperService.AppConfig.CommonResource.UpdateTitle,
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Blue,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
        }).then((result) => {
            if (result.value) {
                _FormValue.ReferenceKey = this._HelperService.AppConfig.ActiveOwnerKey;
                // _FormValue.AuthPin = result.value;
                this._HelperService.IsFormProcessing = true;
                let _OResponse: Observable<OResponse>;
                _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.System, _FormValue);
                _OResponse.subscribe(
                    _Response => {
                        this._HelperService.IsFormProcessing = false;
                        if (_Response.Status == this._HelperService.StatusSuccess) {
                            this._HelperService.NotifySuccess('Password updated');
                            this.Form_Edit_Clear();
                        }
                        else {
                            this._HelperService.NotifyError('Enter valid old password');
                        }
                    },
                    _Error => {
                        this._HelperService.IsFormProcessing = false;
                        this._HelperService.HandleException(_Error);
                    });
            }
        });

    }

    ShowSuccessModal(): void {
        this._HelperService.OpenModal('');
    }
    toogleIsFormProcessing(value: boolean): void {
        this._HelperService.IsFormProcessing = value;
        //    this._ChangeDetectorRef.detectChanges();
    }
    MerchantContact: any = {};
    GetMerchantDetails() {
        this._HelperService.IsFormProcessing = true;
        var pData = {
            Task: this._HelperService.AppConfig.Api.ThankUCash.GetMerchant,
            AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
            AccountId: this._HelperService.AppConfig.ActiveOwnerId,
        }
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Account, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this.toogleIsFormProcessing(false);
                    this._MerchantDetails = _Response.Result;
                    this.InitImagePicker(this._MerchantDetails.IconUrl);
                    this._HelperService.UserAccount.IconUrl = this._MerchantDetails.IconUrl
                    this._HelperService.UserAccount.DisplayName = this._MerchantDetails.DisplayName
                    this.MerchantContact = this._MerchantDetails.ContactPerson;
                    if (this.MerchantContact != undefined && this.MerchantContact.MobileNumber != undefined && this.MerchantContact.MobileNumber != null) {
                        if (this.MerchantContact.MobileNumber.startsWith("234")) {
                            this.MerchantContact.MobileNumber = this.MerchantContact.MobileNumber.substring(3, this.MerchantContact.MobileNumber.length);
                        }
                    }
                    this._MerchantDetails.UpdateCategories = [];

                    for (let index = 0; index < this._MerchantDetails.Categories.length; index++) {
                        const element = this._MerchantDetails.Categories[index];
                        this._MerchantDetails.UpdateCategories.push({
                            Name: element.Name,
                            ReferenceKey: element.ReferenceKey,
                            ReferenceId: element.ReferenceId
                        });

                    }


                    //#region RelocateMarker 

                    if (_Response.Result.Latitude != undefined && _Response.Result.Longitude != undefined) {
                        this._HelperService._UserAccount.Latitude = _Response.Result.Latitude;
                        this._HelperService._UserAccount.Longitude = _Response.Result.Longitude;
                    } else {
                        this._HelperService._UserAccount.Longitude = this._HelperService._UserAccount.Latitude;
                        this._HelperService._UserAccount.Longitude = this._HelperService._UserAccount.Longitude;
                    }


                    //#endregion

                    //#region DatesAndStatusInit 

                    this._MerchantDetails.StartDateS = this._HelperService.GetDateS(
                        this._MerchantDetails.StartDate
                    );
                    this._MerchantDetails.EndDateS = this._HelperService.GetDateS(
                        this._MerchantDetails.EndDate
                    );
                    this._MerchantDetails.CreateDateS = this._HelperService.GetDateTimeS(
                        this._MerchantDetails.CreateDate
                    );
                    this._MerchantDetails.ModifyDateS = this._HelperService.GetDateTimeS(
                        this._MerchantDetails.ModifyDate
                    );
                    this._MerchantDetails.StatusI = this._HelperService.GetStatusIcon(
                        this._MerchantDetails.StatusCode
                    );
                    this._MerchantDetails.StatusB = this._HelperService.GetStatusBadge(
                        this._MerchantDetails.StatusCode
                    );
                    this._MerchantDetails.StatusC = this._HelperService.GetStatusColor(
                        this._MerchantDetails.StatusCode
                    );


                    //#endregion

                    this._ChangeDetectorRef.detectChanges();
                }
                else {
                    this.toogleIsFormProcessing(false);
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            }
        );
    }



    MerchantSecurity: any = {};
    GetMerchantSecurity() {
        this._HelperService.IsFormProcessing = true;
        var pData = {
            Task: 'getconfigurations',
            AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
            AccountId: this._HelperService.AppConfig.ActiveOwnerId,
            "Limit": 100,
            "RefreshCount": true,
        }
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Operations, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this.toogleIsFormProcessing(false);
                    this.MerchantSecurity = _Response.Result.Data;

                    // console.log("this.MerchantSecurity", this.MerchantSecurity)

                    if (this.MerchantSecurity) {


                        var maxamount: any = this.MerchantSecurity.find(x => x['ConfigurationSystemName'] == 'securitymaximuminvoiceamountpertransactioncap');
                        if (maxamount) {
                            this.Configuration.MaxAmount = maxamount.Value;

                        }


                        var MaxTransaction: any = this.MerchantSecurity.find(x => x['ConfigurationSystemName'] == 'securitymaximumtransactionperdaycap');
                        if (MaxTransaction) {

                            this.Configuration.MaxTransaction = MaxTransaction.Value;
                        }

                        var maxamountpercustomer: any = this.MerchantSecurity.find(x => x['ConfigurationSystemName'] == 'securitymaximuminvoiceamountperdaycap');
                        if (maxamountpercustomer) {
                            this.Configuration.MaxAmountPerCustomer = maxamountpercustomer.Value;

                        }

                        var NotificationEmail: any = this.MerchantSecurity.find(x => x['ConfigurationSystemName'] == 'securitynotificationemails');
                        if (NotificationEmail) {
                            this.Configuration.EmailAddress = NotificationEmail.Value;

                        }


                    }


                    this._ChangeDetectorRef.detectChanges();
                }
                else {
                    this.toogleIsFormProcessing(false);
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            }
        );
    }


    Configuration: any = {
        MaxAmount: null,
        MaxTransaction: null,
        MaxAmountPerCustomer: null,
        EmailAddress: null


    }
    Security_Update(ConfigurationKey, value) {
        var data = []
        data.push({
            ConfigurationKey: ConfigurationKey,
            Value: value
        })
        if (value == null || value == undefined || value == "") {
            this._HelperService.NotifyError(
                "Please Enter Configuration Value"
            );
        }
        else {
            swal({
                title: this._HelperService.AppConfig.CommonResource.UpdateTitle,
                // text: this._HelperService.AppConfig.CommonResource.UpdateHelp,
                position: this._HelperService.AppConfig.Alert_Position,
                animation: this._HelperService.AppConfig.Alert_AllowAnimation,
                customClass: this._HelperService.AppConfig.Alert_Animation,
                allowOutsideClick: this._HelperService.AppConfig.Alert_AllowOutsideClick,
                allowEscapeKey: this._HelperService.AppConfig.Alert_AllowEscapeKey,
                confirmButtonColor: this._HelperService.AppConfig.Color_Blue,
                cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
                confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
                cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
                showCancelButton: true,
                // input: 'password',
                // inputPlaceholder: this._HelperService.AppConfig.CommonResource.AccessPin,
                // inputAttributes: {
                //     autocapitalize: 'off',
                //     autocorrect: 'off',
                //     maxLength: "4",
                //     minLength: "4"
                // },
            }).then((result) => {
                if (result.value) {
                    this._HelperService.IsFormProcessing = true;
                    var PostData = {
                        Task: "saveconfigurations",
                        AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
                        AccountId: this._HelperService.AppConfig.ActiveOwnerId,
                        Items: data
                    };

                    let _OResponse: Observable<OResponse>;

                    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Operations, PostData);
                    _OResponse.subscribe(
                        _Response => {
                            this._HelperService.IsFormProcessing = false;
                            if (_Response.Status == this._HelperService.StatusSuccess) {
                                this._HelperService.NotifySuccess("Configuration Updated");
                                value = null;
                                this.Emails = [];
                                this.Configuration.MaxAmount = null
                                this.Configuration.MaxAmountPerCustomer = null
                                this.Configuration.MaxTransaction = null
                                this.Configuration.EmailAddress = null
                                this.GetMerchantSecurity();
                            } else {
                                this._HelperService.NotifySuccess(_Response.Message);
                            }

                        },
                        _Error => {
                            this._HelperService.IsFormProcessing = false;
                            this._HelperService.HandleException(_Error);
                        }
                    );
                }
            });
        }
    }
}

