import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { Observable } from 'rxjs';
import { HelperService } from 'src/app/service/helper.service';
import { OResponse } from 'src/app/service/object.service';
import { Location } from '@angular/common'

@Component({
  selector: 'app-change-number',
  templateUrl: './change-number.component.html',
  styleUrls: ['../../../../assets/css/auth.css']
})
export class ChangeNumberComponent implements OnInit {

 
  i = 1;
  interval
  backgroundImage = [
      { img: require("src/assets/images/onboarding/backgroundImg1.png"), text: `<p class="in-left" style="letter-spacing: 1px;"><span style="color:#ffc141">Grow</span> your <span style="color:#ffc141">business</span> with <br/>ease and confidence<br/> from anywhere</p>` },
      { img: require("src/assets/images/onboarding/backgroundImg2.png"), text: `<p class="in-left" style="letter-spacing: 1px;">A marketing tool <br/> for <span style="color:#ffc141">customer loyalty</span><br/><span style="visibility: hidden;">loyalty</span></p>` },
      { img: require("src/assets/images/onboarding/backgroundImg3.png"), text: `<p class="in-left" style="letter-spacing: 1px;">Get real<span style="color:#ffc141"> results</span> using <br/> our <span style="color:#ffc141">loyalty</span> customer<br/> marketing solution</p>` }
  ];

  Countries = [
    {
        ReferenceId: 1,
        ReferenceKey: "nigeria",
        Name: "Nigeria",
        IconUrl: "../../../../assets/images/countries/nigeria_icon.png",
        Isd: "234",
        Iso: "ng",
        CurrencyName: "Naira",
        CurrencyNotation: "NGN",
        CurrencySymbol: "₦",
        CurrencyHex: "&#8358;",
        NumberLength: 10
    },
    {
        ReferenceId: 87,
        ReferenceKey: "ghana",
        Name: "Ghana",
        IconUrl: "../../../../assets/images/countries/ghana_icon.png",
        Isd: "233",
        Iso: "gh",
        CurrencyName: "Cedi",
        CurrencyNotation: "GHS",
        CurrencySymbol: "₵",
        CurrencyHex: "&#8373;",
        NumberLength: 9
    },
    {
        ReferenceId: 118,
        ReferenceKey: "kenya",
        Name: "Kenya",
        IconUrl: "../../../../assets/images/countries/kenya_icon.png",
        Isd: "254",
        Iso: "key",
        CurrencyName: "Shilling",
        CurrencyNotation: "KHS",
        CurrencySymbol: "KSh",
        CurrencyHex: "KSh",
        NumberLength: 9
    }
]
  imageText: any = "Grow your business with ease and confidence from anywhere";

  ngAfterViewInit() {
     this.interval = setInterval(() => {
          let imageHead = document.getElementById("auth-container");
          let content = document.getElementById("conent")
          
          imageHead.style.backgroundImage = "url(" + this.backgroundImage[this.i].img + ")";
          content.innerHTML  = this.backgroundImage[this.i].text;
          this.i = this.i + 1;
          if (this.i == this.backgroundImage.length) {
              this.i = 0;
          }
      }, 3000);
  }
  Form_AddUser_Processing = false;
  Form_AddUser: FormGroup;
  constructor(
      public _FormBuilder: FormBuilder,
      public _TranslateService: TranslateService,
      public _Router: Router,
      public _HelperService: HelperService,
      private location: Location
  ) {
      this.Form_AddUser = _FormBuilder.group({
          Task: "ob_merchant_updatemobile",
          MobileNumber: [null, Validators.compose([Validators.required, Validators.minLength(11), Validators.maxLength(14), Validators.pattern("^((\\+243-?)|0)?[0-9]{10,14}$")])], Source: 'Merchant',
      })
  }
  activeRegistration =
      {
          reference: "",
          businessName: "",
          businessEmailAddress: "",
          name: "",
          password: "",
          source: "",
          countryIsd: "",
          mobileNumber: "",
          subscriptionKey: "",
          host: "",
          token: ""
      }
  ngOnInit() {
      var activeRegistration = this._HelperService.GetStorage(this._HelperService.AppConfig.Storage.DataReference);
      if (activeRegistration != null) {
          this.activeRegistration = activeRegistration;
      }
      else {
          this._Router.navigate([this._HelperService.AppConfig.Pages.System.register]);
      }
  }
  SelectedCountry = this.Countries[0];
    CountryClick(Item) {
        this.SelectedCountry = Item;
        this.Form_AddUser.patchValue({
            CountryIsd: Item.Isd,
        })
    }


  goBack(){
      this.location.back()
  }

  Form_AddUser_Process(_FormValue: any) {
    // console.log("_FormValue",_FormValue);
    this.activeRegistration.mobileNumber =  _FormValue.MobileNumber
      var _Request =
      {
          Task: 'ob_merchant_updatemobile',
          reference: this.activeRegistration.reference,
          businessEmailAddress: this.activeRegistration.businessEmailAddress,
          mobileNumber:_FormValue.MobileNumber, 
      }
      
      this.Form_AddUser_Processing = true;
      let _OResponse: Observable<OResponse>;
      _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.MOnBoardV2, _Request);
      _OResponse.subscribe(
          _Response => {
              this.Form_AddUser_Processing = false;
              if (_Response.Status == this._HelperService.StatusSuccess) {
                  this._HelperService.NotifySuccess(_Response.Message);
                  this._HelperService.MerchantToken = _Response.Result.Reference;
                  this.activeRegistration.token = _Response.Result.token;
                  this._HelperService.SaveStorage(this._HelperService.AppConfig.Storage.DataReference, this.activeRegistration);
                  this._Router.navigate([this._HelperService.AppConfig.Pages.System.verifynumber]);
              }
              else {
                  this._HelperService.NotifyError(_Response.Message);
              }
          },
          _Error => {
              this.Form_AddUser_Processing = false;
              this._HelperService.HandleException(_Error);
          });
  }



  ngOnDestroy(){
      clearInterval(this.interval);        
  }

}


