import { AfterViewInit, Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { HelperService, OResponse } from '../../../service/service';
import * as cloneDeep from 'lodash/cloneDeep';
import { Location } from '@angular/common'

@Component({
    selector: 'verifyemail',
    templateUrl: './verifyemail.component.html',
    styleUrls: ['../../../../assets/css/auth.css']
})
export class HCX_Reg_VerifyEmailComponent implements OnInit, AfterViewInit {
    i = 1;
    interval
    backgroundImage = [
        { img: require("src/assets/images/onboarding/backgroundImg1.png"), text: `<p class="in-left" style="letter-spacing: 1px;"><span style="color:#ffc141">Grow</span> your <span style="color:#ffc141">business</span> with <br/>ease and confidence<br/> from anywhere</p>` },
        { img: require("src/assets/images/onboarding/backgroundImg2.png"), text: `<p class="in-left" style="letter-spacing: 1px;">A marketing tool <br/> for <span style="color:#ffc141">customer loyalty</span><br/><span style="visibility: hidden;">loyalty</span></p>` },
        { img: require("src/assets/images/onboarding/backgroundImg3.png"), text: `<p class="in-left" style="letter-spacing: 1px;">Get real<span style="color:#ffc141"> results</span> using <br/> our <span style="color:#ffc141">loyalty</span> customer<br/> marketing solution</p>` }
    ];
    imageText: any = "Grow your business with ease and confidence from anywhere";

    ngAfterViewInit() {
       this.interval = setInterval(() => {
            let imageHead = document.getElementById("auth-container");
            let content = document.getElementById("conent")
            
            imageHead.style.backgroundImage = "url(" + this.backgroundImage[this.i].img + ")";
            content.innerHTML  = this.backgroundImage[this.i].text;
            this.i = this.i + 1;
            if (this.i == this.backgroundImage.length) {
                this.i = 0;
            }
        }, 3000);
    }
    Form_AddUser_Processing = false;
    Form_AddUser: FormGroup;
    constructor(
        public _FormBuilder: FormBuilder,
        public _TranslateService: TranslateService,
        public _Router: Router,
        public _HelperService: HelperService,
        private location: Location
    ) {
        this.Form_AddUser = _FormBuilder.group({
            Task: "createaccount",
            otp: [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(4)])], Source: 'Merchant',
        })
    }
    activeRegistration =
        {
            reference: "",
            businessName: "",
            businessEmailAddress: "",
            name: "",
            password: "",
            source: "",
            countryIsd: "",
            mobileNumber: "",
            subscriptionKey: "",
            host: "",
            token: "",
            accountId:"",
            accountKey:""
        }
    ngOnInit() {
        var activeRegistration = this._HelperService.GetStorage(this._HelperService.AppConfig.Storage.DataReference);
        if (activeRegistration != null) {
            this.activeRegistration = activeRegistration;
            // console.log(activeRegistration);
        }
        else {
            this._Router.navigate([this._HelperService.AppConfig.Pages.System.register]);
        }
    }


    onOtpChange(value) {
        // console.log(value);
        this.Form_AddUser.patchValue(
            {
                otp: value
            }
        )
    }

    resolved(Item) {
        this.capt = Item;
    }
    ShowPassword: boolean = true;
    ToogleShowHidePassword(): void {
        this.ShowPassword = !this.ShowPassword;
    }

    goBack(){
        this.location.back()
    }

    changeEmail()
    {
        this._Router.navigateByUrl("account/changeemail")
    }
    
    public capt = undefined;
    Form_AddUser_Process(_FormValue: any) {
        var _Request =
        {
            Task: 'ob_merchant_emailverify',
            reference: this.activeRegistration.reference,
            businessEmailAddress: this.activeRegistration.businessEmailAddress,
            code: _FormValue.otp,
            host: this._HelperService.AppConfig.Host,
            CellAuth: false,
            IsPolicy: 1,
            IsNewsLetter: 1
        }
        this.Form_AddUser_Processing = true;
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.MOnBoardV2, _Request);
        _OResponse.subscribe(
            _Response => {
                this.Form_AddUser_Processing = false;
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._HelperService.NotifySuccess("Email verified successfully");
                    this._HelperService.MerchantToken = _Response.Result.Reference;
                    this.activeRegistration.token = _Response.Result.AuthToken;
                    this.activeRegistration.accountId =_Response.Result.ReferenceId;
                    this.activeRegistration.accountKey =_Response.Result.ReferenceKey;
                    this._HelperService.SaveStorage(this._HelperService.AppConfig.Storage.DataReference, this.activeRegistration);
                    // this._Router.navigate([this._HelperService.AppConfig.Pages.System.verifynumber]);
                    this._Router.navigate([this._HelperService.AppConfig.Pages.System.accountTypeSelction]);
                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this.Form_AddUser_Processing = false;
                this._HelperService.HandleException(_Error);
            });
    }


    ResendCode() {
        var _Request =
        {
            Task: 'ob_merchant_resendemail',
            reference: this.activeRegistration.reference,
            businessEmailAddress: this.activeRegistration.businessEmailAddress,
            host: this._HelperService.AppConfig.Host,
        }
        this.Form_AddUser_Processing = true;
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.MOnBoardV2, _Request);
        _OResponse.subscribe(
            _Response => {
                this.Form_AddUser_Processing = false;
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._HelperService.NotifySuccess(_Response.Message);
                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this.Form_AddUser_Processing = false;
                this._HelperService.HandleException(_Error);
            });
        return false;
    }

    ngOnDestroy(){
        clearInterval(this.interval);        
    }

}


// }
