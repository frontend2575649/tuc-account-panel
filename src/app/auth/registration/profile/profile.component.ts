import { AfterViewInit, Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { HelperService, OResponse } from '../../../service/service';
import * as cloneDeep from 'lodash/cloneDeep';
import { Location } from '@angular/common'
import { Select2OptionData } from 'ng2-select2';

@Component({
    selector: 'profile',
    templateUrl: './profile.component.html',
    styleUrls: ['../../../../assets/css/auth.css']
})
export class HCX_Reg_ProfileComponent implements OnInit, AfterViewInit {
    interval
    Countries = [
        {
            ReferenceId: 1,
            ReferenceKey: "nigeria",
            Name: "Nigeria",
            IconUrl: "../../../../assets/images/countries/nigeria_icon.png",
            Isd: "234",
            Iso: "ng",
            CurrencyName: "Naira",
            CurrencyNotation: "NGN",
            CurrencySymbol: "₦",
            CurrencyHex: "&#8358;",
            NumberLength: 10
        },
        {
            ReferenceId: 87,
            ReferenceKey: "ghana",
            Name: "Ghana",
            IconUrl: "../../../../assets/images/countries/ghana_icon.png",
            Isd: "233",
            Iso: "gh",
            CurrencyName: "Cedi",
            CurrencyNotation: "GHS",
            CurrencySymbol: "₵",
            CurrencyHex: "&#8373;",
            NumberLength: 9
        },
        {
            ReferenceId: 118,
            ReferenceKey: "kenya",
            Name: "Kenya",
            IconUrl: "../../../../assets/images/countries/kenya_icon.png",
            Isd: "254",
            Iso: "key",
            CurrencyName: "Shilling",
            CurrencyNotation: "KHS",
            CurrencySymbol: "KSh",
            CurrencyHex: "KSh",
            NumberLength: 9
        }
    ]
    i = 1;

    countryData = [{ id: '0', text: 'Nigeria', ISD: '234' },
    { id: '1', text: 'Ghana', ISD: '233' },
    { id: '2', text: 'Kenya', ISD: '254' }]
    // temp = require("src/assets/images/onboarding/backgroundImg1.png")
    backgroundImage = [
        { img: require("src/assets/images/onboarding/backgroundImg1.png"), text: `<p class="in-left" style="letter-spacing: 1px;"><span style="color:#ffc141">Grow</span> your <span style="color:#ffc141">business</span> with <br/>ease and confidence<br/> from anywhere</p>` },
        { img: require("src/assets/images/onboarding/backgroundImg2.png"), text: `<p class="in-left" style="letter-spacing: 1px;">A marketing tool <br/> for <span style="color:#ffc141">customer loyalty</span><br/><span style="visibility: hidden;">loyalty</span></p>` },
        { img: require("src/assets/images/onboarding/backgroundImg3.png"), text: `<p class="in-left" style="letter-spacing: 1px;">Get real<span style="color:#ffc141"> results</span> using <br/> our <span style="color:#ffc141">loyalty</span> customer<br/> marketing solution</p>` }
    ];
    imageText: any = "Grow your business with ease and confidence from anywhere";
    ispwdContainsNum = true
    ispwdContainsUC = true
    ispwdContainsLC = true
    IsminLength = true
    isPwdContainsSC = true
    ngAfterViewInit() {
        this.interval = setInterval(() => {
            let imageHead = document.getElementById("auth-container");
            let content = document.getElementById("conent")

            imageHead.style.backgroundImage = "url(" + this.backgroundImage[this.i].img + ")";
            content.innerHTML = this.backgroundImage[this.i].text;
            this.i = this.i + 1;
            if (this.i == this.backgroundImage.length) {
                this.i = 0;
            }
        }, 3000);
    }

    pwdValueChange(val) {
        this.ispwdContainsNum = /\d/.test(val)
        this.ispwdContainsUC = /[A-Z]/.test(val)
        this.ispwdContainsLC = /[a-z]/.test(val)
        this.IsminLength = val.length >= 8
        this.isPwdContainsSC = /(?=.*?[#?!@$%^&*-])/.test(val)
    }
    Form_AddUser_Processing = false;
    Form_AddUser: FormGroup;
    public _S2Country_Data: Array<Select2OptionData>;
    public _S2Country_Option: Select2Options;
    countryValue = null

    constructor(
        public _FormBuilder: FormBuilder,
        public _TranslateService: TranslateService,
        public _Router: Router,
        public _HelperService: HelperService,
        private location: Location
    ) {
        this.Form_AddUser = _FormBuilder.group({
            Task: "createaccount",
            AccountTypeCode: this._HelperService.AppConfig.AccountType.Merchant,
            BusinessName: [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(128)])],
            FullName: [null, [Validators.required, Validators.minLength(4), Validators.maxLength(256), Validators.pattern("^[a-zA-Z _-]*$"), this.fullnameValidator]],
            Password: [null, Validators.required],
            EmailAddress: [null, Validators.compose([Validators.required, Validators.email, Validators.minLength(2), Validators.pattern('^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}$')])],
            MobileNumber: [null, Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(14), Validators.pattern("^[0-9]{8,14}$")])], Source: 'Merchant',
            CountryIsd: [null, Validators.required],
            Policy: [true, Validators.requiredTrue],
            newsletters: [true],
            Host: null,
            // captcha: [null]
            captcha: [null, Validators.required]
        })

        this._S2Country_Option = {
            placeholder: "Select Country",
            multiple: false,
            allowClear: false,
        };
    }
    get fullname() {
        return this.Form_AddUser.controls['FullName'] as FormControl;
    }

    GetCountry_Selected(event: any) {
        if(event.data[0].ISD == '234'){
            this.SelectedCountry = this.Countries[0];
        }else if(event.data[0].ISD == '233'){
            this.SelectedCountry = this.Countries[1];
        }else{
            this.SelectedCountry = this.Countries[2];
        }
        this.Form_AddUser.patchValue({ "CountryIsd": event.data[0].ISD })
    }

    fullnameValidator(control: FormControl) {
        let fullname = control.value;
        let error = { fullname: { twoworld: false, requiredLastname: false, minLengthFirstname: false, minLengthLastname: false } }
        if (fullname !== null) {
            fullname = fullname.trim();
            if (fullname.split(' ')[0].length < 3) {
                error.fullname.minLengthFirstname = true;
            }
            else if (fullname.split(' ').length == 1) {
                error.fullname.requiredLastname = true;
            }
            else if (fullname.split(' ')[1].length < 3) {
                error.fullname.minLengthLastname = true;
            }
            else if (fullname.split(' ').length > 2) {
                error.fullname.twoworld = true;
            }
            else {
                error = null;
            }
        }
        return error;
    }
    ngOnInit() {
        this._HelperService.DeleteStorage(this._HelperService.AppConfig.Storage.Account);
        this._HelperService.DeleteStorage(this._HelperService.AppConfig.Storage.Device);
        this._HelperService.DeleteStorage(this._HelperService.AppConfig.Storage.Location);
        this._HelperService.DeleteStorage(this._HelperService.AppConfig.Storage.OReqH);
        this._HelperService.DeleteStorage("merchantDetails");
        this._HelperService.SetNetwork();

        this._S2Country_Data = this.countryData

    }
    SelectedCountry = this.Countries[0];
    CountryClick(Item) {
        this.SelectedCountry = Item;
        this.Form_AddUser.patchValue({
            CountryIsd: Item.Isd,
        })
    }
    resolved(Item) {
        this.capt = Item;
    }
    ShowPassword: boolean = true;
    ToogleShowHidePassword(): void {
        this.ShowPassword = !this.ShowPassword;
    }

    goBack(){
        this.location.back()
    }
    public capt = undefined;
    Form_AddUser_Process(_FormValue: any) {
        // this.capt = "das";
        // if (this.capt == undefined) {
        //     this._HelperService.NotifyError("Verify captcha")
        // }
        // else {
        // console.log("_FormValue.MobileNumber",_FormValue.MobileNumber);

        let str = _FormValue.EmailAddress;
        let email_lowercase = "";
        for (let chars of str) {
            //Get the ascii value of character
            let value = chars.charCodeAt();

            //If the character is in uppercase
            if (value >= 65 && value <= 90) {
                //convert it to lowercase
                email_lowercase += String.fromCharCode(value + 32);
            } else {
                //else add the original character
                email_lowercase += chars;
            }
        }

        var _Request =
        {
            Task: 'ob_merchant_request',
            businessName: _FormValue.BusinessName,
            businessEmailAddress: email_lowercase,
            name: _FormValue.FullName,
            password: _FormValue.Password,
            source: 'regsource.tucwebsite',
            countryIsd: _FormValue.CountryIsd,
            mobileNumber: _FormValue.MobileNumber,
            subscriptionKey: null,
            host: this._HelperService.AppConfig.Host,
        }
        // let checkBox = {
        //     IsPolicy: _FormValue.Policy == true ? "1" : "0",
        //     IsNewsLetter: _FormValue.newsletters == true ? "1" : "0"
        // }

        // this._HelperService.SaveStorage('t&c', checkBox);
        this.Form_AddUser_Processing = true;
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.MOnBoardV2, _Request);
        _OResponse.subscribe(
            _Response => {
                this.Form_AddUser_Processing = false;
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._HelperService.SaveStorage(this._HelperService.AppConfig.Storage.DataReference, _Response.Result);
                    this._HelperService.MerchantToken = _Response.Result.Reference;
                    this._Router.navigate([this._HelperService.AppConfig.Pages.System.verifymail]);
                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this.Form_AddUser_Processing = false;
                this._HelperService.HandleException(_Error);
            });
        // }
    }

    ngOnDestroy() {
        clearInterval(this.interval);
    }
}