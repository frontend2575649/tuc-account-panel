import { AfterViewInit, Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
// import { HelperService, OResponse } from '../../../service/service';
import * as cloneDeep from 'lodash/cloneDeep';
import { Location } from '@angular/common'
import { HelperService } from 'src/app/service/helper.service';
import { OResponse } from 'src/app/service/object.service';

@Component({
    selector: 'selectaccounttype',
    templateUrl: './selectaccounttype.component.html',
    styleUrls: ['./selectaccounttype.component.css']
})
export class HCX_Reg_SelectAccountTypeComponent implements OnInit {
    deals: boolean = false;
    loyalty: boolean = false;
    rewards: boolean = undefined;
    dealsRadio: boolean = false;
    accountTypes: any = [];
    loyaltyModel = "closedloyaltymodel";
    activeRegistration
    dealCategory
    constructor(
        private _HelperService: HelperService,
        private _router: Router,
    ) { }

    ngOnInit(): void {
        this.activeRegistration = this._HelperService.GetStorage(this._HelperService.AppConfig.Storage.DataReference);
        if (this.activeRegistration == null || this.activeRegistration == undefined || this.activeRegistration == "") {
            window.localStorage.clear();
            this._router.navigate(['/account/register']);
        }
    }
    selectSolution(event, data) {

        if (data == 'tuLoyalty') {
            this.deals = false;
        }
        if (data == 'tuDeals') {
            this.loyalty = false;

        }
        // if (this.loyalty == false && data == 'tuLoyalty') {
        //     const box = document.getElementById('loyaltyCard');
        //     if (box != null) {
        //         box.classList.add('loyaltyCard');
        //     }
        //     this.loyalty = true;
        //     const box1 = document.getElementById('dealsCard');

        //     if (box1 != null) {
        //         box1.classList.remove('dealsCard');
        //     }
        //     this.deals = false;
        // } else if (this.loyalty == true && data == 'tuLoyalty') {
        //     const box = document.getElementById('loyaltyCard');

        //     if (box != null) { 
        //         box.classList.remove('loyaltyCard');
        //     }
        //     this.rewards = undefined;
        //     this.loyalty = false;
        //     const box1 = document.getElementById('dealsCard');

        //     if (box1 != null) {
        //         box1.classList.remove('dealsCard');
        //     }
        //     this.deals = false;
        // } else if (this.deals == false && data == 'tuDeals') {
        //     const box = document.getElementById('dealsCard');

        //     if (box != null) {
        //         box.classList.add('dealsCard');
        //     }
        //     this.deals = true;
        //     this.loyalty = false;
        //     const box1 = document.getElementById('loyaltyCard');

        //     if (box1 != null) { 
        //         box1.classList.remove('loyaltyCard');
        //     }
        // } else if (this.deals == true && data == 'tuDeals') {
        //     const box = document.getElementById('dealsCard');

        //     if (box != null) {
        //         box.classList.remove('dealsCard');
        //     }

        //     const box1 = document.getElementById('loyaltyCard');

        //     if (box1 != null) { 
        //         box1.classList.remove('loyaltyCard');
        //     }
        //     this.dealsRadio = false;
        //     this.deals = false;
        //     this.loyalty = false;
        // }
    }

    dealsChange(event) {
        if (event.target.id == 'tuDealsRadio1') {
          this.dealCategory = "0"
        } else if (event.target.id == 'tuDealsRadio2') {
          this.dealCategory = "1"
    
        } else if (event.target.id == 'tuDealsRadio3') {
          this.dealCategory = "2"
        }
        this.dealsRadio = true;
      }

    submitAcc() {

        var _PostData = {
            Task: "ob_merchant_requestaccountconfiguration",
            ReferenceId: this.activeRegistration.accountId,
            ReferenceKey: this.activeRegistration.accountKey,
            AccountTypes: this.accountTypes
        }
        if (this.accountTypes.length > 0) {
            this._HelperService.IsFormProcessing = true;
            let _OResponse: Observable<OResponse>;
            _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.MOnBoardV2, _PostData);
            _OResponse.subscribe(
                _Response => {
                    this.accountTypes = [];
                    this._HelperService.IsFormProcessing = false;
                    if (_Response.Status == this._HelperService.StatusSuccess) {
                        this._HelperService.NotifySuccess('Account Details added successfully');
                        // this._router.navigate(['/m/home/setting'], { queryParams: { isAccTypeComplete: true } });
                        this.goToMerchantDashboard();
                    }
                    else {
                        this._HelperService.NotifyError(_Response.Message);
                    }
                },
                _Error => {
                    this._HelperService.IsFormProcessing = false;
                    this._HelperService.HandleException(_Error);
                });
        }
    }
    referenceLogin() {
        if (this.loyalty) {
            this.accountTypes.push({
                AccountTypeCodes: "thankucashloyalty",
                ConfigurationValue: this.loyaltyModel,
                Value: null
            })
        }
        if (this.deals) {
            this.accountTypes.push({
                AccountTypeCodes: "thankucashdeals",
                Value: this.dealCategory
            })
        }

        let store = this._HelperService.GetStorage("hcreference");
        // console.log("store", store);
        var pData = {
            Task: 'login',
            UserName: store.businessEmailAddress,
            Password: store.password,
            PlatformCode: 'web',
        };
        this._HelperService.SaveStorage(
            this._HelperService.AppConfig.Storage.Credentials,
            {
                UserName: pData.UserName,
                Password: pData.Password,
                PlatformCode: 'web',
            }
        );
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V1.System, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._HelperService.SaveStorage(this._HelperService.AppConfig.Storage.Account, _Response.Result);
                    var _StorageReqH = this._HelperService.GetStorage(this._HelperService.AppConfig.Storage.OReqH);
                    _StorageReqH.hcuak = _Response.Result['AccessKey'];
                    _StorageReqH.hcupk = btoa(_Response.Result['PublicKey']);
                    this._HelperService.SaveStorage(this._HelperService.AppConfig.Storage.OReqH, _StorageReqH);
                    this._HelperService.RefreshHelper();
                    // window.location.href = this._HelperService.AppConfig.Pages.ThankUCash.Dashboard.MerchantSetting;
                    this.submitAcc();
                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });

    }
    goToMerchantDashboard() {
        var TAccessKey = this._HelperService.AccessKey;
        var TPublicKey = this._HelperService.PublicKey;
        var Key = btoa(TAccessKey + "|" + TPublicKey);

        window.location.href = this._HelperService.AppConfig.MerchantPanelURL + '/account/auth/' + Key, '_blank';
    }
}
enum HostType {
    Live,
    Test,
    Tech,
    Dev
}
