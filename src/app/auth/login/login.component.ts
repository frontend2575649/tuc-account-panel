import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { HelperService, OResponse } from '../../service/service';
import { Location } from '@angular/common'

@Component({
    selector: 'login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
    ShowPassword: boolean = true;
    _Form_Login_Processing = false;
    _Form_Login: FormGroup;
    i = 1;
    interval
    // temp = require("src/assets/images/onboarding/backgroundImg1.png")
    backgroundImage = [
        { img: require("src/assets/images/onboarding/backgroundImg1.png"), text: `<p class="in-left" style="letter-spacing: 1px;"><span style="color:#ffc141">Grow</span> your <span style="color:#ffc141">business</span> with <br/>ease and confidence<br/> from anywhere</p>` },
        { img: require("src/assets/images/onboarding/backgroundImg2.png"), text: `<p class="in-left" style="letter-spacing: 1px;">A marketing tool <br/> for <span style="color:#ffc141">customer loyalty</span><br/><span style="visibility: hidden;">loyalty</span></p>` },
        { img: require("src/assets/images/onboarding/backgroundImg3.png"), text: `<p class="in-left" style="letter-spacing: 1px;">Get real<span style="color:#ffc141"> results</span> using <br/> our <span style="color:#ffc141">loyalty</span> customer<br/> marketing solution</p>` }
    ];
    imageText: any = "Grow your business with ease and confidence from anywhere";
    constructor(
        public _FormBuilder: FormBuilder,
        public _TranslateService: TranslateService,
        public _Router: Router,
        public _HelperService: HelperService,
        private location: Location
    ) {
        this._Form_Login = _FormBuilder.group({
            'username': ['', Validators.required],
            'password': ['', Validators.required],
        });

    }

    ngAfterViewInit() {
        this.interval = setInterval(() => {
            let imageHead = document.getElementById("auth-container");
            let content = document.getElementById("conent")

            imageHead.style.backgroundImage = "url(" + this.backgroundImage[this.i].img + ")";
            content.innerHTML = this.backgroundImage[this.i].text;
            this.i = this.i + 1;
            if (this.i == this.backgroundImage.length) {
                this.i = 0;
            }
        }, 3000);
    }
    ngOnInit() {
        this._HelperService.DeleteStorage(this._HelperService.AppConfig.Storage.Account);
        this._HelperService.DeleteStorage(this._HelperService.AppConfig.Storage.Device);
        this._HelperService.DeleteStorage(this._HelperService.AppConfig.Storage.Location);
        this._HelperService.DeleteStorage(this._HelperService.AppConfig.Storage.OReqH);
        this._HelperService.SetNetwork();
    }

    goBack(){
        this.location.back()
    }
    _Form_Login_Process(value: any) {
        this._Form_Login_Processing = true;
        var pData = {
            Task: 'login',
            UserName: value.username,
            Password: value.password,
            PlatformCode: 'web',
        };
        this._HelperService.SaveStorage(
            this._HelperService.AppConfig.Storage.Credentials,
            {
                UserName: pData.UserName,
                Password: pData.Password,
                PlatformCode: 'web',
            }
        );
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V1.System, pData);
        _OResponse.subscribe(
            _Response => {
                this._Form_Login_Processing = false;
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._HelperService.SaveStorage(this._HelperService.AppConfig.Storage.Account, _Response.Result);
                    var _StorageReqH = this._HelperService.GetStorage(this._HelperService.AppConfig.Storage.OReqH);
                    _StorageReqH.hcuak = _Response.Result['AccessKey'];
                    _StorageReqH.hcupk = btoa(_Response.Result['PublicKey']);
                    this._HelperService.SaveStorage(this._HelperService.AppConfig.Storage.OReqH, _StorageReqH);
                    this._HelperService.NotifySuccess(_Response.Message);
                    this._Form_Login.reset();

                    if (_Response.Result.UserAccount.AccountTypeCode == this._HelperService.AppConfig.AccountType.Merchant ||
                        _Response.Result.UserAccount.AccountTypeCode == this._HelperService.AppConfig.AccountType.SubAccount) {
                        var TAccessKey = _Response.Result['AccessKey'];
                        var TPublicKey = _Response.Result['PublicKey'];
                        var Key = btoa(TAccessKey + "|" + TPublicKey);

                        window.location.href = this._HelperService.AppConfig.MerchantPanelURL + '/account/auth/' + Key, '_blank';
                    }
                    else {
                        this._HelperService.NotifyError('Invalid account. Please contact Support');
                    }
                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._Form_Login_Processing = false;
                this._HelperService.HandleException(_Error);
            });
    }
    ToogleShowHidePassword(): void {
        this.ShowPassword = !this.ShowPassword;
    }
    ngOnDestroy() {
        clearInterval(this.interval);
    }
}
enum HostType {
    Live,
    Test,
    Tech,
    Dev
  }