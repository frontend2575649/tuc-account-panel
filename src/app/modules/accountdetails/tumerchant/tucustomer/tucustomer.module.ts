import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";
import { TranslateModule } from "@ngx-translate/core";
import { Select2Module } from "ng2-select2";
import { NgxPaginationModule } from "ngx-pagination";
import { Daterangepicker } from "ng2-daterangepicker";
import { Ng2FileInputModule } from "ng2-file-input";
import { AgmCoreModule } from '@agm/core';
import { GooglePlaceModule } from "ngx-google-places-autocomplete";
import { TUCustomerComponent } from "./tucustomer.component";

const routes: Routes = [
    {
        path: "",
        component: TUCustomerComponent,
        children: [
            { path: "", data: { permission: "customer", menuoperations: "ManageMerchant", accounttypecode: "customer" }, loadChildren: "../../../dashboards/customer/dashboard.module#TUDashboardModule" },
            { path: "dashboard/:referencekey/:referenceid", data: { permission: "customer", menuoperations: "ManageMerchant", PageName: 'System.Menu.Customer', accounttypecode: "customer" }, loadChildren: "../../../dashboards/customer/dashboard.module#TUDashboardModule" },
            { path: 'sales/salehistory/:referencekey/:referenceid', data: { 'permission': 'salehistory', PageName: 'System.Menu.Customer' }, loadChildren: '../../../../modules/transactions/tusale/customer/merchant/tusale.module#TUSaleModule' },
            { path: 'sales/rewardhistory/:referencekey/:referenceid', data: { 'permission': 'salehistory', PageName: 'System.Menu.Customer' }, loadChildren: '../../../../modules/transactions/tusale/customer/rewardhistory/tusale.module#TUSaleModule' },
            { path: 'sales/redeemhistory/:referencekey/:referenceid', data: { 'permission': 'salehistory', PageName: 'System.Menu.Customer' }, loadChildren: '../../../../modules/transactions/tusale/customer/redeemhistory/tusale.module#TUSaleModule' },
            { path: 'sales/rewardclaimhistory/:referencekey/:referenceid', data: { 'permission': 'salehistory', PageName: 'System.Menu.Customer' }, loadChildren: '../../../../modules/transactions/tusale/customer/turewardclaim/turewardclaim.module#TURewardsClaimModule' },
           
            // { path: 'terminals/:referencekey/:referenceid', data: { 'permission': 'terminals', PageName: 'System.Menu.Store' }, loadChildren: '../../../../modules/accounts/tuterminals/store/tuterminals.module#TUTerminalsModule' },
            { path: "sales/dealhistory", data: { permission: "getcustomer", menuoperations: "ManageCustomer", accounttypecode: "customer" }, loadChildren: "../../../transactions/tusale/customer/tudealhistory/dealtransaction/dealtransaction.module#DealTransactionModule" },
       
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TUCustomerRoutingModule { }

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        TranslateModule,
        Select2Module,
        NgxPaginationModule,
        Daterangepicker,
        Ng2FileInputModule,
        TUCustomerRoutingModule,
        GooglePlaceModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyB9DMDX78ll840bMxjGL4WUfjAYXfi8vZo'
        }),
    ],
    declarations: [TUCustomerComponent]
})
export class TUCustomerModule { }
