import { ChangeDetectorRef, Component, ElementRef, OnDestroy, OnInit, ViewChild } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import * as Feather from 'feather-icons';
import { GooglePlaceDirective } from 'ngx-google-places-autocomplete';
import { Address } from 'ngx-google-places-autocomplete/objects/address';
import { Observable, Subscription } from "rxjs";
import { DataHelperService, HelperService, OResponse, OSelect, OUserDetails, FilterHelperService } from "../../../../service/service";
declare let $: any;
import swal from 'sweetalert2';

@Component({
  selector: "tu-customer",
  templateUrl: "./tucustomer.component.html"
})
export class TUCustomerComponent implements OnInit, OnDestroy {
  isLoaded: boolean = true;

  public _CustomerAddress: any = {};

  public ShowCategorySelector: boolean = true;


  //#region subscriptions 

  subscription: Subscription;
  ReloadSubscription: Subscription;

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
    this.ReloadSubscription.unsubscribe();
  }

  //#endregion

  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef,
    public _FilterHelperService: FilterHelperService
  ) {

  }

  //#region ToogleDetailView 

  HideStoreDetail() {
    var element = document.getElementById("StoresHide");
    element.classList.add("Hm-HideDiv");
    element.classList.remove("Hm-ShowStoreDetail");
  }

  ShowStoreDetail() {
    var element = document.getElementById("StoresHide");
    element.classList.add("Hm-ShowStoreDetail");
    element.classList.remove("Hm-HideDiv");
  }

  //#endregion

  BackDropInit(): void {
    var backdrop: HTMLElement = document.getElementById("backdrop");
    backdrop.onclick = () => {
      $(this.divView.nativeElement).removeClass('show');
      backdrop.classList.remove("show");
    };
  }

  ngOnInit() {
    //#region UIInit 
    Feather.replace();
    this._HelperService.ContainerHeight = window.innerHeight;
    this._HelperService.AppConfig.ShowHeader = true;
    this.BackDropInit();
    //#endregion
    //#region Subscriptions 
    this.subscription = this._HelperService.isprocessingtoogle
      .subscribe((item) => {
        this._ChangeDetectorRef.detectChanges();
      });

    this.ReloadSubscription = this._HelperService.ReloadEventEmit.subscribe((number) => {

    });

    //#endregion

    var StorageDetails = this._HelperService.GetStorage(this._HelperService.AppConfig.Storage.ActiveCustomer);
    if (StorageDetails != null) {
      this._HelperService.AppConfig.ActiveReferenceKey = StorageDetails.ReferenceKey;
      this._HelperService.AppConfig.ActiveReferenceId = StorageDetails.ReferenceId;
      this._HelperService.AppConfig.ActiveReferenceDisplayName = StorageDetails.DisplayName;
      this._HelperService.AppConfig.ActiveReferenceAccountTypeCode = StorageDetails.AccountTypeCode;
    }

    // this._HelperService.Get_UserAccountDetails(false);
    this.GetCustomersDetails();


  }

  //#region OffCanvasNBackdrop 
  @ViewChild("offCanvas") divView: ElementRef;

  ShowOffCanvas() {
    $(this.divView.nativeElement).addClass('show');
    var backdrop: HTMLElement = document.getElementById("backdrop");
    backdrop.classList.add("show");
  }
  RemoveOffCanvas() {
    $(this.divView.nativeElement).removeClass('show');
    var backdrop: HTMLElement = document.getElementById("backdrop");
    backdrop.classList.remove("show");
  }
  //#endregion

  //#region CustomerDetails 

  public _CustomerDetails: any =
    {
      ManagerName: null,
      BranchName: null,
      ReferenceId: null,
      ReferenceKey: null,
      TypeCode: null,
      TypeName: null,
      SubTypeCode: null,
      SubTypeName: null,
      UserAccountKey: null,
      UserAccountDisplayName: null,
      Name: null,
      Description: null,
      StartDate: null,
      StartDateS: null,
      EndDate: null,
      EndDateS: null,
      SubTypeValue: null,
      MinimumInvoiceAmount: null,
      MaximumInvoiceAmount: null,
      MinimumRewardAmount: null,
      MaximumRewardAmount: null,
      ManagerKey: null,
      ManagerDisplayName: null,
      SmsText: null,
      Comment: null,
      CreateDate: null,
      CreatedByKey: null,
      CreatedByDisplayName: null,
      ModifyDate: null,
      ModifyByKey: null,
      ModifyByDisplayName: null,
      StatusId: null,
      StatusCode: null,
      StatusName: null,
      CreateDateS: null,
      ModifyDateS: null,
      StatusI: null,
      StatusB: null,
      StatusC: null,
    }
  GetCustomersDetails() {

    this._HelperService.IsFormProcessing = true;
    this.isLoaded = false;

    var pData = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetCustomer,
      // AccountId: this._HelperService.UserAccount.AccountId,
      // AccountKey: this._HelperService.UserAccount.AccountKey,
      AccountKey: this._HelperService.AppConfig.ActiveReferenceKey,
      AccountId: this._HelperService.AppConfig.ActiveReferenceId,
      MerchantKey: this._HelperService.AppConfig.ActiveOwnerKey,
      MerchantId: this._HelperService.AppConfig.ActiveOwnerId,
    }
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Account, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.IsFormProcessing = false;
          this.isLoaded = true;

          this._CustomerDetails = _Response.Result;
          this._CustomerDetails.FirstTransactionDate = this._HelperService.GetDateS(this._CustomerDetails.FirstTransactionDate);
          this._CustomerDetails.LastTransactionDate = this._HelperService.GetDateS(this._CustomerDetails.LastTransactionDate);
          this._CustomerDetails.DateOfBirth = this._HelperService.GetDateSByFormat(this._CustomerDetails.DateOfBirth, 'DD MMMM YYYY');

          this._CustomerDetails.StatusI = this._HelperService.GetStatusIcon(this._CustomerDetails.StatusCode);
          this._CustomerDetails.StatusB = this._HelperService.GetStatusBadge(this._CustomerDetails.StatusCode);
          this._CustomerDetails.StatusC = this._HelperService.GetStatusColor(this._CustomerDetails.StatusCode);
          this._CustomerAddress = this._CustomerDetails.Address;
          this._ChangeDetectorRef.detectChanges();
          //#region ResponseInit 


          this._CustomerDetails.EndDateS = this._HelperService.GetDateS(
            this._CustomerDetails.EndDate
          );
          this._CustomerDetails.CreateDateS = this._HelperService.GetDateTimeS(
            this._CustomerDetails.CreateDate
          );
          this._CustomerDetails.ModifyDateS = this._HelperService.GetDateTimeS(
            this._CustomerDetails.ModifyDate
          );
          this._CustomerDetails.StatusI = this._HelperService.GetStatusIcon(
            this._CustomerDetails.StatusCode
          );
          this._CustomerDetails.StatusB = this._HelperService.GetStatusBadge(
            this._CustomerDetails.StatusCode
          );
          this._CustomerDetails.StatusC = this._HelperService.GetStatusColor(
            this._CustomerDetails.StatusCode
          );
        }
        else {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      }
    );
  }





  toogleIsFormProcessing(value: boolean): void {
    this._HelperService.IsFormProcessing = value;
  }





  unclick() {
    $(this.divView.nativeElement).removeClass('show');
    var backdrop: HTMLElement = document.getElementById("backdrop");
    backdrop.classList.remove("show");
  }

}

export class OAccountOverview {
  public Merchants: number;
  public Stores: number;
  public ActiveMerchants: number;
  public ActiveMerchantsDiff: number;
  public Terminals: number;
  public ActiveTerminals: number;
  public ActiveTerminalsDiff: number;
  public Transactions: number;
  public TransactionsDiff: number;
  public PurchaseAmount: number;
  public PurchaseAmountDiff: number;
  public CashRewardPurchaseAmount: number;
  public CashRewardPurchaseAmountDiff: number;
  public CardRewardPurchaseAmount: number;
  public CardRewardPurchaseAmountDiff: number;
}
