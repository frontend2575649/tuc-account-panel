import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";

import { TranslateModule } from "@ngx-translate/core";
import { Select2Module } from "ng2-select2";
import { NgxPaginationModule } from "ngx-pagination";
import { Daterangepicker } from "ng2-daterangepicker";
import { Ng2FileInputModule } from "ng2-file-input";

import { ImageCropperModule } from 'ngx-image-cropper';
import { TUSettingsComponent } from "./tusettings.component";
import { AgmCoreModule } from '@agm/core';
import { GooglePlaceModule } from "ngx-google-places-autocomplete";
import { MainPipe } from '../../../../service/main-pipe.module'
import { InputFileConfig, InputFileModule } from 'ngx-input-file';
import { HCXAddressManagerModule } from "../../../../component/hcxaddressmanager/hcxaddressmanager.component";

const config: InputFileConfig = {
    fileAccept: '*',
    fileLimit: 1,
};

const routes: Routes = [
    {
        path: "",
        component: TUSettingsComponent,
        children: [
            { path: '', data: {PageName: 'System.Menu.Settings' }},
            // { path: '', data: { permission: "getmerchantdetails", menuoperations: "ManageMerchant", accounttypecode: "merchant" }, loadChildren: '../../../accounts/tumerchants/stores/tustores.module#TUStoresModule' },
            { path: 'cashiers', data: { 'permission': 'cashiers', PageName: 'System.Menu.Cashiers' }, loadChildren: '../../../accounts/tumerchants/cashiers/tucashiers.module#TUCashiersModule' },
            { path: 'stores', data: { 'permission': 'stores', PageName: 'System.Menu.Stores' }, loadChildren: '../../../accounts/tumerchants/stores/tustores.module#TUStoresModule' },
            { path: 'terminals', data: { 'permission': 'cashiers', PageName: 'System.Menu.Terminals' }, loadChildren: '../../../accounts/tumerchants/tuterminals/merchant/tuterminals.module#TUTerminalsModule' },
            { path: 'rewardpercentage', data: { 'permission': 'cashiers', PageName: 'System.Menu.Rewardpercentage' }, loadChildren: '../../../accounts/tumerchants/rewardpercentage/turewardpercentage.module#TURewardPercentageModule' },
            { path: 'subaccounts', data: { 'permission': 'cashiers', PageName: 'System.Menu.SubAccounts' }, loadChildren: '../../../accounts/tumerchants/subaccounts/tusubaccounts.module#TUSubAccountsModule' },
            { path: 'topup', data: { 'permission': 'topup', PageName: 'System.Menu.TopUPSubscriptions' }, loadChildren: '../../../accounts/tumerchants/topup/topup.module#TUTopUpModule' },
            { path: 'cashouts', data: { permission: "terminals", PageName: "System.Menu.CashOuts" }, loadChildren: '../../../accounts/tumerchants/tucashouts/tucashouts.module#TUCashoutsModule' },
            { path: 'banks', data: { permission: "terminals", PageName: "System.Menu.PaymentMethods" }, loadChildren: '../../../accounts/tumerchants/bankmanager/banks/banks.module#TUBanksModule' },
            { path: 'editprofile', data: { permission: "customers", PageName: "System.Menu.Profile"}, loadChildren: '../../../../pages/hcprofile/hcprofile.module#HCProfileModule' },
            { path: 'apps', data: { 'permission': 'administration', PageName: 'System.Menu.Administration' }, loadChildren: '../../../accounts/tumerchants/hcapps/tuapplist/tuapplist.module#TUAppListModule' },
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TUSettingRoutingModule { }

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        TranslateModule,
        Select2Module,
        NgxPaginationModule,
        Daterangepicker,
        Ng2FileInputModule,
        TUSettingRoutingModule,
        GooglePlaceModule,
        MainPipe,
        HCXAddressManagerModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyB9DMDX78ll840bMxjGL4WUfjAYXfi8vZo'
        }),
        ImageCropperModule,
        InputFileModule.forRoot(config),

    ],
    declarations: [TUSettingsComponent]
})
export class TUSettingsModule { }
