import { ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild, ViewChildren, OnDestroy } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import * as Feather from 'feather-icons';
import { GooglePlaceDirective } from 'ngx-google-places-autocomplete';
import { Address } from 'ngx-google-places-autocomplete/objects/address';
import { Observable } from "rxjs";
import { DataHelperService, HelperService, OResponse, OUserDetails, OSelect } from "../../../../service/service";
import { InputFileComponent, InputFile } from 'ngx-input-file';
import { QueryList } from '@angular/core/src/render3';
import { HCXAddress, HCXAddressConfig, locationType } from "../../../../component/hcxaddressmanager/hcxaddressmanager.component";
import { debug } from "console";
import { Select2OptionData } from "ng2-select2";
declare let $: any;
declare var moment: any;

const HelpImageType: any = {
  Receipt: 'receipt',
  Terminal: 'terminal',
  Serial: 'serial'
}

@Component({
  selector: "tu-settings",
  templateUrl: "./tusettings.component.html"
})
export class TUSettingsComponent implements OnInit, OnDestroy {
  isLoaded: boolean = true;
  maintainAspectRatioPOS: boolean = true
  subscription: any;
  ShowManager = false;
  public StoreManager_Data: Array<Select2OptionData>;
  public S2StoreManager_Option: Select2Options;

  ngOnDestroy(): void {
    this._HelperService._FileSelect_Icon_Reset();
    this._HelperService.Icon_Crop_Clear();
    this.subscription.unsubscribe();
  }

  selectedRole = null
  public _Roles = [
    {
      'id': 10,
      'text': 'Admin',
      'apival': 'merchantadmin'
    },
    {
      'id': 11,
      'text': 'Store Manager',
      'apival': 'merchantstoremanager'
    },

  ];

  public ShowCategorySelector: boolean = false;
  public MerchantAddress: any = {};
  public MerchantContactPerson: any = {};
  CurrentFileInput: string = "stores";
  FileInputOptions: any = {
    stores: "stores",
    cashiers: "cashiers"
  }

  defaultToDarkFilter = ["grayscale: 100%", "invert: 100%"];
  Form_AddStorevalue;

  @ViewChild("terminal")
  private InputFileComponent_Term: InputFileComponent;

  @ViewChild("cashier")
  private InputFileComponent_Cashier: InputFileComponent;

  @ViewChild("subaccount")
  private InputFileComponent_Subaccount: InputFileComponent;
  CurrentImagesCount: number = 0;
  isStoreManager:boolean = false
  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef
  ) {
    this.CurrentFileInput = this.FileInputOptions.stores;
    this._HelperService.ResetDateRange();
  }

  //#region DetailShowHIde 

  // HideStoreDetail() {
  //   var element = document.getElementById("StoresDetails");
  //   element.classList.add("Hm-HideDiv");
  //   element.classList.remove("Hm-ShowStoreDetail");
  // }

  // ShowStoreDetail() {
  //   var element = document.getElementById("StoresDetails");
  //   element.classList.add("Hm-ShowStoreDetail");
  //   element.classList.remove("Hm-HideDiv");
  // }

  //#endregion

  InitBackDropClickEvent(): void {
    var backdrop: HTMLElement = document.getElementById("backdrop");

    backdrop.onclick = () => {
      $(this.divView.nativeElement).removeClass('show');
      backdrop.classList.remove("show");
    };
  }
  public _AddressShow = true;
  public _Address: HCXAddress = {};
  public _AddressConfig: HCXAddressConfig =
    {
      locationType: locationType.form
    };
  
  public mapheight = '203px'

  AddressChange(Address) {
    this._Address = Address;
  }
  ngOnInit() {

    //#region GetStorageData 
    let roleKey = this._HelperService.GetStorage("hca").UserAccount.RoleKey
    this.isStoreManager = roleKey ? roleKey == "merchantstoremanager" : false
    var StorageDetails = this._HelperService.GetStorage(this._HelperService.AppConfig.Storage.ActiveMerchant);
    if (StorageDetails != null) {
      this._HelperService.AppConfig.ActiveReferenceKey = StorageDetails.ReferenceKey;
      this._HelperService.AppConfig.ActiveReferenceId = StorageDetails.ReferenceId;
      this._HelperService.AppConfig.ActiveReferenceDisplayName = StorageDetails.DisplayName;
      this._HelperService.AppConfig.ActiveReferenceAccountTypeCode = StorageDetails.AccountTypeCode;
    }

    this.subscription = this._HelperService.getDetailsChangeEmitter()
    .subscribe(item => {
        this.GetMerchantDetails()
        this.GetStoreManagerList()
      });
    //#endregion

    //#region UIInitialize 

    Feather.replace();
    this._HelperService.AppConfig.ShowHeader = true;
    this._HelperService.ContainerHeight = window.innerHeight;
    this.InitBackDropClickEvent();
    this.GetMerchantDetails();
    // this.HideStoreDetail();
    this.Form_AddUser_Load();
    this.TUTr_Filter_Stores_Load();
    this.TUTr_Filter_Providers_Load();
    this.TUTr_Filter_Banks_Load();
    this.Form_AddStore_Load();
    this.Form_AddCashier_Load();
    this.GetBusinessCategories();
    this.TUTr_Filter_Cashiers_Load();
    this.Form_AddSubAccount_Load();
    this.getroles_list();
    this.GetStoreManagerList()
      this._HelperService.setTheme();
    this._HelperService.GetAccountCount(this._HelperService.AppConfig.ActiveOwnerKey, this._HelperService.AppConfig.ActiveOwnerId, this._HelperService.AppConfig.DefaultStartTimeAll, this._HelperService.AppConfig.DefaultEndTimeToday)
  }

  public SelectedStoreManager = ''
  GetStoreManagerList() {
    this.ShowManager = false;
    var PData =
    {
        Task: 'getstoremanagers',
        AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
        AccountId: this._HelperService.AppConfig.ActiveOwnerId,
    }
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Account, PData);
    _OResponse.subscribe(
        _Response => {
            if (_Response.Status == this._HelperService.StatusSuccess) {
                if (_Response.Result.Data != undefined) {
                    var _Data = _Response.Result.Data as any[];
                    if (_Data.length > 0) {
                        var finalCat = [];
                        this.ShowManager = false;
                        _Data.forEach(element => {
                            var Item = {
                                id: element.ReferenceId,
                                key: element.ReferenceKey,
                                text: element.DisplayName,
                                additional: element,
                            };
                            finalCat.push(Item);
                        });
                        this.S2StoreManager_Option = {
                          placeholder: "Select Store Manager",
                          multiple: false,
                          allowClear: false,
                      };
                        this.StoreManager_Data = finalCat;
                        this.ShowManager = true;
                        this._HelperService.setTheme();
                      
                    }
                    else {
                        this.S2StoreManager_Option = {
                            placeholder: "Select Store Manager",
                            multiple: false,
                            allowClear: false,
                        };
                        setTimeout(() => {
                            this.ShowManager = true;
                            this._HelperService.setTheme();
                        }, 500);
                    }
                }
                else {
                    this.S2StoreManager_Option = {
                        placeholder: "Select Store Manager",
                        multiple: false,
                        allowClear: false,
                    };
                    setTimeout(() => {
                        this.ShowManager = true;
                        this._HelperService.setTheme();
                    }, 500);
                }
            }
            else {
                this._HelperService.NotifyError(_Response.Message);
            }
        },
        _Error => {
            this._HelperService.IsFormProcessing = false;
            this._HelperService.HandleException(_Error);
            this._HelperService.ToggleField = false;

        });
  }

  StoreManagerSelected(Items) {
    console.log("Item",Items);
    this.Form_AddStore.controls["StoreManagerId"].patchValue(Items.data[0].additional.ReferenceId)
    this.Form_AddStore.controls["MEmailAddress"].patchValue(Items.data[0].additional.EmailAddress)
    this.Form_AddStore.controls["MobileNumber"].patchValue(Items.data[0].additional.ContactNumber)
}
  public GetRoles_Option: Select2Options;
  public GetRoles_Transport: any;
  getroles_list() {
    this.GetRoles_Option = {
      placeholder: 'Select Role',
      ajax: this.GetRoles_Transport,
      multiple: false,
    };
  }
  GetRoles_ListChange(event: any) {
    this.Form_AddSubAccount.patchValue(
      {
        RoleKey: event.data[0].apival,
        RoleId: event.value

      }
    );
    this.ToggleStoreSelect = false;
    this.SelectedStore =true ;   // to disable the save button untill store selected

    if (this.Form_AddSubAccount.controls['RoleKey'].value == 'merchantstoremanager') {
      // this.TUTr_Filter_Stores_Load();
      // setTimeout(() => {
      //   this.ToggleStoreSelect = true;
      //   this.SelectedStore =false ; //to disable the save button untill store selected
      //   this.TUTr_Filter_Stores_Load();
      // }, 500);
    }
    else {
      this.Form_AddSubAccount.patchValue({
        StoreReferenceKey: this._HelperService.AppConfig.ActiveOwnerKey,
        StoreReferenceId: this._HelperService.AppConfig.ActiveOwnerId

      })
    }



  }
  AddBulkTerminal() {
    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.BulkTerminal,
    ]);
  }


  //#region MerchantDetails 

  // public _UserAccount: OUserDetails = {
  //   ContactNumber: null,
  //   SecondaryEmailAddress: null,
  //   ReferenceId: null,
  //   BankDisplayName: null,
  //   BankKey: null,
  //   SubOwnerAddress: null,
  //   SubOwnerLatitude: null,
  //   SubOwnerDisplayName: null,
  //   SubOwnerKey: null,
  //   SubOwnerLongitude: null,
  //   AccessPin: null,
  //   LastLoginDateS: null,
  //   AppKey: null,
  //   AppName: null,
  //   AppVersionKey: null,
  //   CreateDate: null,
  //   CreateDateS: null,
  //   CreatedByDisplayName: null,
  //   CreatedByIconUrl: null,
  //   CreatedByKey: null,
  //   Description: null,
  //   IconUrl: null,
  //   ModifyByDisplayName: null,
  //   ModifyByIconUrl: null,
  //   ModifyByKey: null,
  //   ModifyDate: null,
  //   ModifyDateS: null,
  //   PosterUrl: null,
  //   ReferenceKey: null,
  //   StatusCode: null,
  //   StatusI: null,
  //   StatusId: null,
  //   StatusName: null,
  //   AccountCode: null,
  //   AccountOperationTypeCode: null,
  //   AccountOperationTypeName: null,
  //   AccountTypeCode: null,
  //   AccountTypeName: null,
  //   Address: null,
  //   AppVersionName: null,
  //   ApplicationStatusCode: null,
  //   ApplicationStatusName: null,
  //   AverageValue: null,
  //   CityAreaKey: null,
  //   CityAreaName: null,
  //   CityKey: null,
  //   CityName: null,
  //   CountValue: null,
  //   CountryKey: null,
  //   CountryName: null,
  //   DateOfBirth: null,
  //   DisplayName: null,
  //   EmailAddress: null,
  //   EmailVerificationStatus: null,
  //   EmailVerificationStatusDate: null,
  //   FirstName: null,
  //   GenderCode: null,
  //   GenderName: null,
  //   LastLoginDate: null,
  //   LastName: null,
  //   Latitude: null,
  //   Longitude: null,
  //   MobileNumber: null,
  //   Name: null,
  //   NumberVerificationStatus: null,
  //   NumberVerificationStatusDate: null,
  //   OwnerDisplayName: null,
  //   OwnerKey: null,
  //   Password: null,
  //   Reference: null,
  //   ReferralCode: null,
  //   ReferralUrl: null,
  //   RegionAreaKey: null,
  //   RegionAreaName: null,
  //   RegionKey: null,
  //   RegionName: null,
  //   RegistrationSourceCode: null,
  //   RegistrationSourceName: null,
  //   RequestKey: null,
  //   RoleKey: null,
  //   RoleName: null,
  //   SecondaryPassword: null,
  //   SystemPassword: null,
  //   UserName: null,
  //   WebsiteUrl: null
  // };

  public _MerchantDetails: any =
    {
      ReferenceId: null,
      ReferenceKey: null,
      TypeCode: null,
      TypeName: null,
      SubTypeCode: null,
      SubTypeName: null,
      UserAccountKey: null,
      UserAccountDisplayName: null,
      Name: null,
      Description: null,
      StartDate: null,
      StartDateS: null,
      EndDate: null,
      EndDateS: null,
      SubTypeValue: null,
      MinimumInvoiceAmount: null,
      MaximumInvoiceAmount: null,
      MinimumRewardAmount: null,
      MaximumRewardAmount: null,
      ManagerKey: null,
      ManagerDisplayName: null,
      SmsText: null,
      Comment: null,
      CreateDate: null,
      CreatedByKey: null,
      CreatedByDisplayName: null,
      ModifyDate: null,
      ModifyByKey: null,
      ModifyByDisplayName: null,
      StatusId: null,
      StatusCode: null,
      StatusName: null,
      Latitude: null,
      Longitude: null,
      CreateDateS: null,
      ModifyDateS: null,
      StatusI: null,
      StatusB: null,
      StatusC: null,

    }

  private InitImagePicker(InputFileComponent: InputFileComponent) {
    if (InputFileComponent != undefined) {
      this.CurrentImagesCount = 0;
      this._HelperService._InputFileComponent = InputFileComponent;
      InputFileComponent.onChange = (files: Array<InputFile>): void => {
        if (files.length >= this.CurrentImagesCount) {
          this._HelperService._SetFirstImageOrNone(InputFileComponent.files);
        }
        this.CurrentImagesCount = files.length;
      };
    }
  }
  GetMerchantDetails() {
    this._HelperService.IsFormProcessing = true;
    this.isLoaded = false;
    var pData = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetMerchant,
      AccountKey: this._HelperService.UserAccount.AccountKey,
      AccountId: this._HelperService.UserAccount.AccountId,
    }
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Account, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this.toogleIsFormProcessing(false);
          this.isLoaded = true;

          this._MerchantDetails = _Response.Result;
          this._MerchantDetails.StatusI = this._HelperService.GetStatusIcon(this._MerchantDetails.StatusCode);
          this._MerchantDetails.StatusB = this._HelperService.GetStatusBadge(this._MerchantDetails.StatusCode);
          this._MerchantDetails.StatusC = this._HelperService.GetStatusColor(this._MerchantDetails.StatusCode);
          // this.MerchantAddress = this._MerchantDetails.Address;
          this.MerchantContactPerson = this._MerchantDetails.ContactPerson;
          if (this._MerchantDetails.AddressComponent != undefined && this._MerchantDetails.AddressComponent != null) {
            this.MerchantAddress = this._MerchantDetails.AddressComponent;
          }
          
          //#region RelocateMarker
          if (_Response.Result.Latitude != undefined && _Response.Result.Longitude != undefined) {
            this._HelperService._UserAccount.Latitude = _Response.Result.Latitude;
            this._HelperService._UserAccount.Longitude = _Response.Result.Longitude;
          } else {
            this._HelperService._UserAccount.Longitude = this._HelperService._UserAccount.Latitude;
            this._HelperService._UserAccount.Longitude = this._HelperService._UserAccount.Longitude;
          }
          // this.FormC_EditUser_Latitude = this._MerchantDetails.Latitude;
          // this.FormC_EditUser_Longitude = this._MerchantDetails.Longitude;
          this.FormC_EditUser_Latitude = _Response.Result.Latitude;
          this.FormC_EditUser_Longitude = _Response.Result.Longitude;
          // this._HelperService._ReLocate();

          //#endregion

          //#region DatesAndStatusInit 

          this._MerchantDetails.StartDateS = this._HelperService.GetDateS(
            this._MerchantDetails.StartDate
          );
          this._MerchantDetails.EndDateS = this._HelperService.GetDateS(
            this._MerchantDetails.EndDate
          );
          this._MerchantDetails.CreateDateS = this._HelperService.GetDateTimeS(
            this._MerchantDetails.CreateDate
          );
          this._MerchantDetails.ModifyDateS = this._HelperService.GetDateTimeS(
            this._MerchantDetails.ModifyDate
          );
          this._MerchantDetails.StatusI = this._HelperService.GetStatusIcon(
            this._MerchantDetails.StatusCode
          );
          this._MerchantDetails.StatusB = this._HelperService.GetStatusBadge(
            this._MerchantDetails.StatusCode
          );
          this._MerchantDetails.StatusC = this._HelperService.GetStatusColor(
            this._MerchantDetails.StatusCode
          );
          //#endregion
          this._ChangeDetectorRef.detectChanges();
        }
        else {
          this.toogleIsFormProcessing(false);
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      }
    );
  }

  //#endregion

  //#region EditUser

  //#region EditUserFormA 

  FormA_EditUser: FormGroup;

  FormA_EditUser_Show() {
    this._HelperService.OpenModal("FormA_EditUser_Content");
  }
  FormA_EditUser_Close() {
    var backdrop: HTMLElement = document.getElementById("backdrop");
    $(this.divView.nativeElement).removeClass('show');
    backdrop.classList.remove("show");
  }
  FormA_EditUser_Load() {
    this._HelperService._FileSelect_Icon_Data.Width = 128;
    this._HelperService._FileSelect_Icon_Data.Height = 128;

    this._HelperService._FileSelect_Poster_Data.Width = 800;
    this._HelperService._FileSelect_Poster_Data.Height = 400;

    this.FormA_EditUser = this._FormBuilder.group({
      OperationType: 'new',
      ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
      ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
      Task: this._HelperService.AppConfig.Api.Core.UpdateUserAccount,
      AccountTypeCode: this._HelperService.AppConfig.AccountType.Merchant,
      AccountOperationTypeCode: this._HelperService.AppConfig.AccountOperationType.Offline,
      RegistrationSourceCode: this._HelperService.AppConfig.RegistrationSource.System,
      OwnerKey: this._HelperService.AppConfig.ActiveOwnerKey,
      OwnerName: [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(256)])],
      DisplayName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(25)])],
      Name: [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(256)])],
      ContactNumber: [null, Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(14)])],
      EmailAddress: [null, Validators.compose([Validators.required, Validators.email, Validators.minLength(2)])],
      RewardPercentage: [null, Validators.compose([Validators.required, Validators.min(0), Validators.max(100)])]
    });
  }

  FormA_EditUser_Process(_FormValue: any) {
    this._HelperService.IsFormProcessing = true;
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(
      this._HelperService.AppConfig.NetworkLocation.V2.System,
      _FormValue
    );
    _OResponse.subscribe(
      _Response => {
        this.toogleIsFormProcessing(false);
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.NotifySuccess("Account updated successfully");
          this.Forms_EditUser_Close();
        } else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this.toogleIsFormProcessing(false);
        this._HelperService.HandleException(_Error);
      }
    );
  }

  //#endregion

  //#region EditUserFormB 

  FormB_EditUser: FormGroup;
  FormB_EditUser_Address: string = null;
  FormB_EditUser_Latitude: number = 0;
  FormB_EditUser_Longitude: number = 0;


  FormB_EditUser_Show() {
    this._HelperService.OpenModal("FormB_EditUser_Content");
  }

  FormB_EditUser_Close() {
    var backdrop: HTMLElement = document.getElementById("backdrop");
    $(this.divView.nativeElement).removeClass('show');
    backdrop.classList.remove("show");
  }
  FormB_EditUser_Load() {
    this._HelperService._FileSelect_Icon_Data.Width = 128;
    this._HelperService._FileSelect_Icon_Data.Height = 128;

    this._HelperService._FileSelect_Poster_Data.Width = 800;
    this._HelperService._FileSelect_Poster_Data.Height = 400;

    this.FormB_EditUser = this._FormBuilder.group({
      OperationType: 'new',
      ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
      ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
      Task: this._HelperService.AppConfig.Api.Core.UpdateUserAccount,
      AccountTypeCode: this._HelperService.AppConfig.AccountType.Merchant,
      AccountOperationTypeCode: this._HelperService.AppConfig.AccountOperationType.Offline,
      RegistrationSourceCode: this._HelperService.AppConfig.RegistrationSource.System,
      OwnerKey: this._HelperService.AppConfig.ActiveOwnerKey,

      MobileNumber: [null, Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(14)])],
      FirstName: [null, Validators.compose([Validators.required, Validators.minLength(2)])],
      // LastName: [null, Validators.compose([Validators.required, Validators.email, Validators.minLength(2)])],
      SecondaryEmailAddress: [null, Validators.compose([Validators.required, Validators.email, Validators.minLength(2)])],
    });
  }

  FormB_EditUser_Process(_FormValue: any) {
    // _FormValue.DisplayName = _FormValue.FirstName;
    // _FormValue.Name = _FormValue.FirstName + " " + _FormValue.LastName;
    this.toogleIsFormProcessing(true);
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(
      this._HelperService.AppConfig.NetworkLocation.V2.System,
      _FormValue
    );
    _OResponse.subscribe(
      _Response => {
        this.toogleIsFormProcessing(false);
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.NotifySuccess("Account updated successfully");
          if (_FormValue.OperationType == "edit") {
            this.Forms_EditUser_Close();
          }
        } else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this.toogleIsFormProcessing(false);
        this._HelperService.HandleException(_Error);
      }
    );
  }

  //#endregion

  //#region EditUserFormC 

  FormC_EditUser: FormGroup;
  FormC_EditUser_Address: string = null;
  FormC_EditUser_Latitude: number = 0;
  FormC_EditUser_Longitude: number = 0;


  @ViewChild('placesStore') placesStore: GooglePlaceDirective;

  FormC_EditUser_PlaceMarkerClick(event) {
    this.FormC_EditUser_Latitude = event.coords.lat;
    this.FormC_EditUser_Longitude = event.coords.lng;

  }
  public FormC_EditUser_AddressChange(address: Address) {
    this.FormC_EditUser_Latitude = address.geometry.location.lat();
    this.FormC_EditUser_Longitude = address.geometry.location.lng();
    this.FormC_EditUser_Address = address.formatted_address;
  }

  FormC_EditUser_Show() {
    this._HelperService.OpenModal("FormC_EditUser_Content");
  }

  FormC_EditUser_Load() {
    this._HelperService._FileSelect_Icon_Data.Width = 128;
    this._HelperService._FileSelect_Icon_Data.Height = 128;

    this._HelperService._FileSelect_Poster_Data.Width = 800;
    this._HelperService._FileSelect_Poster_Data.Height = 400;

    this.FormC_EditUser = this._FormBuilder.group({
      OperationType: 'new',
      ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
      ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
      Task: this._HelperService.AppConfig.Api.Core.UpdateUserAccount,
      AccountTypeCode: this._HelperService.AppConfig.AccountType.Merchant,
      AccountOperationTypeCode: this._HelperService.AppConfig.AccountOperationType.Offline,
      RegistrationSourceCode: this._HelperService.AppConfig.RegistrationSource.System,
      OwnerKey: this._HelperService.AppConfig.ActiveOwnerKey,
      Latitude: 0,
      Longitude: 0,
      Address: [null, Validators.compose([Validators.required, Validators.maxLength(256), Validators.minLength(2)])],
      Configuration: [],
    });
  }

  FormC_EditUser_Process(_FormValue: any) {
    // _FormValue.DisplayName = _FormValue.FirstName;
    // _FormValue.Name = _FormValue.FirstName + " " + _FormValue.LastName;
    _FormValue.Longitude = this.FormC_EditUser_Longitude
    _FormValue.Latitude = this.FormC_EditUser_Latitude

    this.toogleIsFormProcessing(true);
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(
      this._HelperService.AppConfig.NetworkLocation.V2.System,
      _FormValue
    );
    _OResponse.subscribe(
      _Response => {
        this.toogleIsFormProcessing(false);
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.NotifySuccess("Account updated successfully");
          if (_FormValue.OperationType == "edit") {
            this.Forms_EditUser_Close();
          }
        } else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this.toogleIsFormProcessing(false);
        this._HelperService.HandleException(_Error);
      }
    );
  }

  //#endregion
  Forms_EditUser_Close() {
    this.GetMerchantDetails();
    var backdrop: HTMLElement = document.getElementById("backdrop");
    $(this.divView.nativeElement).removeClass('show');
    backdrop.classList.remove("show");
  }
  //#endregion

  //#region BackdropDismiss 

  @ViewChild("offCanvas") divView: ElementRef;

  clicked() {

    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.Accounts.EditProfile
    ]);

  }


  //#endregion

  toogleIsFormProcessing(value: boolean): void {
    this._HelperService.IsFormProcessing = value;
    //    this._ChangeDetectorRef.detectChanges();
  }

  //#region Add Terminal 

  Form_AddUser: FormGroup;
  Form_AddUser_Show() {
    this.maintainAspectRatioPOS = false
    this._HelperService.Icon_Crop_Clear();
    this.InitImagePicker(this.InputFileComponent_Term);
    this._HelperService.OpenModal("Form_AddUser_Content");
  }
  Form_AddUser_Close() {
    this.maintainAspectRatioPOS = true
    this._HelperService._FileSelect_Icon_Reset();
    this.InputFileComponent_Term.files.pop();
    this._HelperService.CloseModal("Form_AddUser_Content");
  }
  Form_AddUser_Load() {
    this._HelperService._FileSelect_Icon_Data.Width = 128;
    this._HelperService._FileSelect_Icon_Data.Height = 128;

    this._HelperService._FileSelect_Poster_Data.Width = 800;
    this._HelperService._FileSelect_Poster_Data.Height = 400;

    this.Form_AddUser = this._FormBuilder.group({
      OperationType: "new",
      Task: this._HelperService.AppConfig.Api.ThankUCash.saveterminal,
      AccountId: this._HelperService.UserAccount.AccountId,
      AccountKey: this._HelperService.UserAccount.AccountKey,
      MerchantReferenceKey: this._HelperService.AppConfig.ActiveOwnerKey,
      MerchantReferenceId: this._HelperService.AppConfig.ActiveOwnerId,
      StoreReferenceId: [null, Validators.required],
      StoreReferenceKey: [null, Validators.required],
      ProviderReferenceId: [null, Validators.required],
      ProviderReferenceKey: [null, Validators.required],
      AcquirerReferenceId: [null, Validators.required],
      AcquirerReferenceKey: [null, Validators.required],
      // CashierReferenceId: [null, Validators.required],
      // CashierReferenceKey: [null, Validators.required],
      StatusCode: this._HelperService.AppConfig.Status.Active,
      TypeCode:this._HelperService.AppConfig.Type.Pos,
      TerminalId: [null, Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(25)])],
      SerialNumber: null,


    });
  }
  Form_AddUser_Clear() {
    this.Form_AddUser.reset();
    this._HelperService._FileSelect_Icon_Reset();
    this._HelperService._FileSelect_Poster_Reset();
    this.Form_AddUser_Load();
    this._HelperService.GetRandomNumber();
    this._HelperService.GeneratePassoword();
  }
  Form_AddUser_Process(_FormValue: any) {
    this.ResetFilterUI();
    this._HelperService.IsFormProcessing = true;
    let _OResponse: Observable<OResponse>;



    if (this._HelperService.AppConfig.TerminalsCount == 0) {
      _OResponse = this._HelperService.PostData(
        this._HelperService.AppConfig.NetworkLocation.V3.Account,
        _FormValue
      );
      _OResponse.subscribe(
        _Response => {
          this._HelperService.IsFormProcessing = false;
          if (_Response.Status == this._HelperService.StatusSuccess) {
            this._HelperService.FlashSwalSuccess("New POS terminal has been added successfully",
              "You have successfully created new POS terminal");
            this._HelperService.ObjectCreated.next(true);
            this.Form_AddUser_Clear();
            this.Form_AddUser_Close();
            if (_FormValue.OperationType == "close") {
              this.Form_AddUser_Close();
            }
            this._HelperService.GetAccountCount(this._HelperService.AppConfig.ActiveOwnerKey, this._HelperService.AppConfig.ActiveOwnerId, this._HelperService.AppConfig.DefaultStartTimeAll, this._HelperService.AppConfig.DefaultEndTimeToday)

          } else {
            this._HelperService.NotifyError(_Response.Message);
          }
        },
        _Error => {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.HandleException(_Error);
        }
      );
      return;
    }


    else if (this._HelperService.AppConfig.TerminalPermission) {
      if (this._HelperService.AppConfig.TerminalPermission.MinimumLimit <= this._HelperService.AppConfig.TerminalsCount && this._HelperService.AppConfig.TerminalPermission.MaximumLimit > this._HelperService.AppConfig.TerminalsCount) {
        _OResponse = this._HelperService.PostData(
          this._HelperService.AppConfig.NetworkLocation.V3.Account,
          _FormValue
        );
        _OResponse.subscribe(
          _Response => {
            this._HelperService.IsFormProcessing = false;
            if (_Response.Status == this._HelperService.StatusSuccess) {
              this._HelperService.FlashSwalSuccess("New POS terminal has been added successfully",
                "You have successfully created new POS terminal");
              this._HelperService.ObjectCreated.next(true);
              this.Form_AddUser_Clear();
              this.Form_AddUser_Close();
              if (_FormValue.OperationType == "close") {
                this.Form_AddUser_Close();
              }
              this._HelperService.GetAccountCount(this._HelperService.AppConfig.ActiveOwnerKey, this._HelperService.AppConfig.ActiveOwnerId, this._HelperService.AppConfig.DefaultStartTimeAll, this._HelperService.AppConfig.DefaultEndTimeToday)

            } else {
              this._HelperService.NotifyError(_Response.Message);
            }
          },
          _Error => {
            this._HelperService.IsFormProcessing = false;
            this._HelperService.HandleException(_Error);
          }
        );
      } else {
        this._HelperService.NotifyError('Upgrade Your Subscription')
        this.Form_AddUser_Close();
        this._Router.navigate(['m' + '/' + this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.Upgrade]);
        return;
      }



    } else {
      this._HelperService.IsFormProcessing = false;
      this._HelperService.NotifyError('Terminal is disabled');
      return;
    }


  }
  //#endregion

  //#region Add Cashier 

  Form_AddCashier: FormGroup;
  Form_AddCashier_Show() {
    this._HelperService.Icon_Crop_Clear();
    this.InitImagePicker(this.InputFileComponent_Cashier);
    this._HelperService.OpenModal("Form_AddCashier_Content");
  }
  Form_AddCashier_Close() {
    this._HelperService._FileSelect_Icon_Reset();
    this.InputFileComponent_Cashier.files.pop();
    this._HelperService.CloseModal("Form_AddCashier_Content");
  }
  Form_AddCashier_Load() {
    this._HelperService._FileSelect_Icon_Data.Width = 128;
    this._HelperService._FileSelect_Icon_Data.Height = 128;

    this._HelperService._FileSelect_Poster_Data.Width = 800;
    this._HelperService._FileSelect_Poster_Data.Height = 400;

    this.Form_AddCashier = this._FormBuilder.group({
      OperationType: "new",
      Task: this._HelperService.AppConfig.Api.ThankUCash.SaveCashier,
      AccountKey: this._HelperService.UserAccount.AccountKey,
      AccountId: this._HelperService.UserAccount.AccountId,
      StoreId: [null, Validators.required],
      StoreKey: [null, Validators.required],
      StatusCode: this._HelperService.AppConfig.Status.Active,
      FirstName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(128),Validators.pattern("^[a-zA-Z _-]*$")])],
      LastName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(128),Validators.pattern("^[a-zA-Z _-]*$")])],
      ContactNumber: [null, Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(14)])],
      EmailAddress: [null, Validators.compose([Validators.required, Validators.email, Validators.minLength(2)])],
      EmployeeCode: [null],
      GenderCode: this._HelperService.AppConfig.Gender.Male,
    });
  }
  Form_AddCashier_Clear() {
    this.Form_AddCashier.reset();
    this._HelperService._FileSelect_Icon_Reset();
    this._HelperService._FileSelect_Poster_Reset();
    this.Form_AddCashier_Load();
    this._HelperService.GetRandomNumber();
    this._HelperService.GeneratePassoword();
  }
  Form_AddCashier_Process(_FormValue: any) {
    this.ResetFilterUI(); this._HelperService.StopClickPropogation();
    this._HelperService.IsFormProcessing = true;
    var Request = this.CreateCashierRequest(_FormValue);

    let _OResponse: Observable<OResponse>;
    if (this._HelperService.AppConfig.CashiersCount == 0) {
      _OResponse = this._HelperService.PostData(
        this._HelperService.AppConfig.NetworkLocation.V3.Account,
        Request
      );
      _OResponse.subscribe(
        _Response => {
          this._HelperService.IsFormProcessing = false;
          if (_Response.Status == this._HelperService.StatusSuccess) {
            this._HelperService.FlashSwalSuccess("New cashier has been added successfully",
              "Done! You have successfully created new Cashier");
            setTimeout(() => {
              this.ToggleStoreSelect = true;

            }, 500);
            this._HelperService.ObjectCreated.next(true);
            this.Form_AddCashier_Clear();
            this.Form_AddCashier_Close();
            if (_FormValue.OperationType == "close") {
              this.Form_AddCashier_Close();
            }
            this._HelperService.GetAccountCount(this._HelperService.AppConfig.ActiveOwnerKey, this._HelperService.AppConfig.ActiveOwnerId, this._HelperService.AppConfig.DefaultStartTimeAll, this._HelperService.AppConfig.DefaultEndTimeToday)
          } else {
            this._HelperService.NotifyError(_Response.Message);
          }
        },
        _Error => {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.HandleException(_Error);
        }
      );

      return;
    }
    else if (this._HelperService.AppConfig.CashiersPermission) {
      if (this._HelperService.AppConfig.CashiersPermission.MinimumLimit <= this._HelperService.AppConfig.CashiersCount && this._HelperService.AppConfig.CashiersPermission.MaximumLimit > this._HelperService.AppConfig.CashiersCount) {
        _OResponse = this._HelperService.PostData(
          this._HelperService.AppConfig.NetworkLocation.V3.Account,
          Request
        );
        _OResponse.subscribe(
          _Response => {
            this._HelperService.IsFormProcessing = false;
            if (_Response.Status == this._HelperService.StatusSuccess) {
              this._HelperService.FlashSwalSuccess("New cashier has been added successfully",
                "Done! You have successfully created new Cashier");
              this._HelperService.ObjectCreated.next(true);
              this.Form_AddCashier_Clear();
              this.Form_AddCashier_Close();
              if (_FormValue.OperationType == "close") {
                this.Form_AddCashier_Close();
              }
            } else {
              this._HelperService.NotifyError(_Response.Message);
            }
          },
          _Error => {
            this._HelperService.IsFormProcessing = false;
            this._HelperService.HandleException(_Error);
          }
        );
      } else {
        this._HelperService.NotifyError('Upgrade Your Subscription')
        this.Form_AddCashier_Close();
        this._Router.navigate(['m' + '/' + this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.Upgrade]);
        return;
      }
    } else {
      this._HelperService.IsFormProcessing = false;
      this._HelperService.NotifyError('Cashier is disabled');
      return;
    }


  }

  //#endregion

  _StoreInfoCopy: boolean = false
  _ToogleStoreCopy(): void {
    this._StoreInfoCopy = !this._StoreInfoCopy;

    // if (this._StoreInfoCopy) {
    if (this._StoreInfoCopy == true) {
      console.log(this.MerchantAddress);
      this.Form_AddStore.controls['DisplayName'].setValue(this._MerchantDetails.DisplayName);
      this.Form_AddStore.controls['Name'].setValue(this._MerchantDetails.DisplayName);
      this.Form_AddStore.controls['ContactNumber'].setValue(this._MerchantDetails.ContactNumber);
      this.Form_AddStore.controls['EmailAddress'].setValue(this._MerchantDetails.EmailAddress);
      this.Form_AddStore_Longitude = this.MerchantAddress.Latitude;
      this.Form_AddStore_Longitude = this.MerchantAddress.Longitude;
      this.Form_AddStore.controls['MapAddress'].setValue(this.MerchantAddress.Address);
      this.Form_AddStore.controls['Latitude'].setValue(this.MerchantAddress.Latitude);
      this.Form_AddStore.controls['Longitude'].setValue(this.MerchantAddress.Longitude);


      // this.Form_AddStore.controls['EmailAddress'].setValue(this._MerchantDetails.EmailAddress);

    } else {

      this.Form_AddStore.controls['DisplayName'].setValue('');
      this.Form_AddStore.controls['Name'].setValue('');
      this.Form_AddStore.controls['ContactNumber'].setValue('');
      this.Form_AddStore.controls['EmailAddress'].setValue('');
      this.Form_AddStore_Longitude = null;
      this.Form_AddStore_Latitude = null;

      this.Form_AddStore.controls['MapAddress'].setValue('');



      // this.F_AddStore.controls['ContactNumber'].setValue(null);
      // this.F_AddStore.controls['DisplayName'].setValue(null);
      // this.F_AddStore.controls['EmailAddress'].setValue(null);
      // this.F_AddStore.controls['Address'].setValue(null);
      // this.F_AddStore.controls['Latitude'].setValue(null);
      // this.F_AddStore.controls['Longitude'].setValue(null);

      // this.F_AddStore_Latitude = 0;
      // this.F_AddStore_Longitude = 0;
      // this.F_AddStore_Address = null;

    }
  }



  //#region Add SubAccount 

  Form_AddSubAccount: FormGroup;
  Form_AddSubAccount_Show() {
    this._HelperService.Icon_Crop_Clear();
    this.InitImagePicker(this.InputFileComponent_Subaccount);
    this._HelperService.OpenModal("Form_AddSubAccount_Content");
  }
  Form_AddSubAccount_Close() {
    this._HelperService._FileSelect_Icon_Reset();
    this.InputFileComponent_Subaccount.files.pop();
    this._HelperService.CloseModal("Form_AddSubAccount_Content");
  }
  Form_AddSubAccount_Load() {
    this._HelperService._FileSelect_Icon_Data.Width = 128;
    this._HelperService._FileSelect_Icon_Data.Height = 128;

    this._HelperService._FileSelect_Poster_Data.Width = 800;
    this._HelperService._FileSelect_Poster_Data.Height = 400;

    this.Form_AddSubAccount = this._FormBuilder.group({
      OperationType: "new",
      Task: this._HelperService.AppConfig.Api.ThankUCash.SaveSubAccount,
      AccountKey: this._HelperService.UserAccount.AccountKey,
      AccountId: this._HelperService.UserAccount.AccountId,
      StoreReferenceId: [0],
      StoreReferenceKey: [0],
      RoleId: [null, Validators.required],
      RoleKey: [null, Validators.required],
      StatusCode: this._HelperService.AppConfig.Status.Active,
      FirstName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(256), Validators.pattern("^[a-zA-Z _-]*$")])],
      LastName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(128), Validators.pattern("^[a-zA-Z _-]*$")])],
      ContactNumber: [null, Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(14)])],
      EmailAddress: [null, Validators.compose([Validators.required, Validators.email, Validators.minLength(2), Validators.pattern('^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}$')])],
      GenderCode: this._HelperService.AppConfig.Gender.Male,
    });
  }
  Form_AddSubAccount_Clear() {
    this.Form_AddSubAccount.reset();
    this._HelperService._FileSelect_Icon_Reset();
    this._HelperService._FileSelect_Poster_Reset();
    this.Form_AddSubAccount_Load();
    this.getroles_list();
    this._HelperService.GetRandomNumber();
    this._HelperService.GeneratePassoword();
  }
  Form_AddSubAccount_Process(_FormValue: any) {
    // this.ResetFilterUI();
    if (_FormValue.RoleId == undefined || _FormValue.RoleId == null || _FormValue.RoleId < 1) {
      this._HelperService.NotifyError("Please select role");
    }
    else {

      // this._HelperService.IsFormProcessing = true;

      let emailaddress=_FormValue.EmailAddress.toLowerCase();

      let new__FormValue = {};
      Object.assign(new__FormValue, _FormValue);
      new__FormValue['EmailAddress'] = emailaddress;
      // console.log("Request",new__FormValue);
      var Request = this.CreateSubAccountRequest(new__FormValue);

      let _OResponse: Observable<OResponse>;
      // console.log('SubAccount', this._HelperService.AppConfig.SubAccountPermission)
      if (this._HelperService.AppConfig.SubAccountCount == 0) {
        _OResponse = this._HelperService.PostData(
          this._HelperService.AppConfig.NetworkLocation.V3.Account,
          Request
        );
        _OResponse.subscribe(
          _Response => {
            this._HelperService.IsFormProcessing = false;
            if (_Response.Status == this._HelperService.StatusSuccess) {
              this._HelperService.FlashSwalSuccess("New SubAccount has been added successfully",
                "Done! You have successfully created new SubAccount");
              this._HelperService.ObjectCreated.next(true);
              this._HelperService.IsFormProcessing = true;
              this.Form_AddSubAccount_Clear();
              this.Form_AddSubAccount_Close();
              this.ResetFilterUI();
              if (_FormValue.OperationType == "close") {
                this.Form_AddSubAccount_Close();
              }
              this._HelperService.GetAccountCount(this._HelperService.AppConfig.ActiveOwnerKey, this._HelperService.AppConfig.ActiveOwnerId, this._HelperService.AppConfig.DefaultStartTimeAll, this._HelperService.AppConfig.DefaultEndTimeToday)

            } else {
              this._HelperService.NotifyError(_Response.Message);
            }
          },
          _Error => {
            this._HelperService.IsFormProcessing = false;
            this._HelperService.HandleException(_Error);
          }
        );
        return;
      }
      else if (this._HelperService.AppConfig.SubAccountPermission) {
        if (this._HelperService.AppConfig.SubAccountPermission.MinimumLimit <= this._HelperService.AppConfig.SubAccountCount && this._HelperService.AppConfig.SubAccountPermission.MaximumLimit > this._HelperService.AppConfig.SubAccountCount) {
          _OResponse = this._HelperService.PostData(
            this._HelperService.AppConfig.NetworkLocation.V3.Account,
            Request
          );
          _OResponse.subscribe(
            _Response => {
              this._HelperService.IsFormProcessing = false;
              if (_Response.Status == this._HelperService.StatusSuccess) {
                this._HelperService.FlashSwalSuccess("New SubAccount has been added successfully",
                  "Done! You have successfully created new SubAccount");
                this._HelperService.ObjectCreated.next(true);
                this.Form_AddSubAccount_Clear();
                this.Form_AddSubAccount_Close();
                if (_FormValue.OperationType == "close") {
                  this.Form_AddSubAccount_Close();
                }
                this.TUTr_Filter_Stores_Load();
                this._HelperService.GetAccountCount(this._HelperService.AppConfig.ActiveOwnerKey, this._HelperService.AppConfig.ActiveOwnerId, this._HelperService.AppConfig.DefaultStartTimeAll, this._HelperService.AppConfig.DefaultEndTimeToday)



              } else {
                this._HelperService.NotifyError(_Response.Message);
              }
            },
            _Error => {
              this._HelperService.IsFormProcessing = false;
              this._HelperService.HandleException(_Error);
            }
          );
        } else {
          this._HelperService.NotifyError('Upgrade Your Subscription')
          this.Form_AddSubAccount_Close();
          this._Router.navigate(['m' + '/' + this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.Upgrade]);
          return;
        }
      } else {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.NotifyError('SubAccount is disabled');
        return;
      }
    }
  }
  //#endregion
  public TerminalId: string = null;
  public SelectedStore:Boolean  ///To make save button disable untill store selected in subaccount.
  public Select_StoreNumber:number = 0;
  public ToggleStoreSelect: boolean = false;
  public TUTr_Filter_Store_Option: Select2Options;
  TUTr_Filter_Stores_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
      Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceKey",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: true,
          Text: false,
        },
        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        },
        {
          SystemName: "AccountTypeCode",
          Type: this._HelperService.AppConfig.DataType.Text,
          SearchCondition: "=",
          SearchValue: this._HelperService.AppConfig.AccountType.Store
        }
      ]
    };

    _Select.SearchCondition = this._HelperService.GetSearchConditionStrictOr('', 'OwnerId', this._HelperService.AppConfig.DataType.Number, this._HelperService.UserAccount.AccountId, '=')
    if (this._HelperService.UserAccount.AccountTypeCode != this._HelperService.AppConfig.AccountType.Merchant) {
      _Select.SearchCondition = this._HelperService.GetSearchConditionStrictOr(_Select.SearchCondition, 'ParentOwnerId', this._HelperService.AppConfig.DataType.Number,  this._HelperService.UserOwner.AccountId, '=');      
    }else{
      _Select.SearchCondition = this._HelperService.GetSearchConditionStrictOr(_Select.SearchCondition, 'ParentOwnerId', this._HelperService.AppConfig.DataType.Number,  this._HelperService.UserAccount.AccountId, '=');      
    }
    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.TUTr_Filter_Store_Option = {
      placeholder: 'Select Store',
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }
  TUTr_Filter_Stores_Change(event: any) {
    this.Form_AddCashier.patchValue(
      {
        StoreId: event.data[0].ReferenceId,
        StoreKey: event.data[0].ReferenceKey,
      }


    );
    this.Form_AddUser.patchValue(
      {
        StoreReferenceId: event.data[0].ReferenceId,
        StoreReferenceKey: event.data[0].ReferenceKey,
      }


    );

    this.Form_AddSubAccount.patchValue(
      {
        StoreReferenceId: event.data[0].ReferenceId,
        StoreReferenceKey: event.data[0].ReferenceKey,
      }


    );
    //if condition help us to disable the save button 
    if (event.data[0].ReferenceId != this.Select_StoreNumber){
      this.Select_StoreNumber = event.data[0].ReferenceId
      this.SelectedStore = true;
    }
    else{
      this.SelectedStore = false;
      this.Select_StoreNumber = 0
    }


  }
  public TUTr_Filter_Bank_Option: Select2Options;
  TUTr_Filter_Banks_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
      Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceKey",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: true,
          Text: false,
        },
        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        },
        {
          SystemName: "AccountTypeCode",
          Type: this._HelperService.AppConfig.DataType.Text,
          SearchCondition: "=",
          SearchValue: this._HelperService.AppConfig.AccountType.Acquirer
        }]
    };
    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.TUTr_Filter_Bank_Option = {
      placeholder: 'Select Bank (Acquirer)',
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }
  TUTr_Filter_Banks_Change(event: any) {
    this.Form_AddUser.patchValue(
      {
        AcquirerReferenceId: event.data[0].ReferenceId,
        AcquirerReferenceKey: event.data[0].ReferenceKey,
      });
  }
  public TUTr_Filter_Provider_Option: Select2Options;
  TUTr_Filter_Providers_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
      Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceKey",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: true,
          Text: false,
        },
        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        },
        {
          SystemName: "AccountTypeCode",
          Type: this._HelperService.AppConfig.DataType.Text,
          SearchCondition: "=",
          SearchValue: this._HelperService.AppConfig.AccountType.PosAccount
        }]
    };


    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.TUTr_Filter_Provider_Option = {
      placeholder: 'Select  Provider (PTSP)',
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }
  TUTr_Filter_Providers_Change(event: any) {
    this.Form_AddUser.patchValue(
      {
        ProviderReferenceId: event.data[0].ReferenceId,
        ProviderReferenceKey: event.data[0].ReferenceKey,
      });
  }
  public TUTr_Filter_Cashier_Option: Select2Options;
  TUTr_Filter_Cashiers_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetCashiers,
      Location: this._HelperService.AppConfig.NetworkLocation.V3.Account,
      AccountId: this._HelperService.AppConfig.ActiveOwnerId,
      AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceKey",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: true,
          Text: false,
        },
        {
          SystemName: "Name",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        }
        // {
        //   SystemName: "AccountTypeCode",
        //   Type: this._HelperService.AppConfig.DataType.Text,
        //   SearchCondition: "=",
        //   SearchValue: this._HelperService.AppConfig.AccountType.PosAccount
        // }
      ]
    };


    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.TUTr_Filter_Cashier_Option = {
      placeholder: 'Select  Cashier',
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }
  TUTr_Filter_Cashiers_Change(event: any) {
    this.Form_AddUser.patchValue(
      {
        CashierReferenceId: event.data[0].ReferenceId,
        CashierReferenceKey: event.data[0].ReferenceKey,
      });
  }

  OpenModel() {
    this._HelperService.CloseModal('Form_AddHelp_Content');

    this._HelperService.OpenModal('Form_AddUser_Content');


  }

  HelpImageUrl: string;
  ShowHelpImage(HelpImage: string): void {
    this._HelperService.CloseModal('Form_AddUser_Content');
    switch (HelpImage) {
      case HelpImageType.Receipt: {
        this.HelpImageUrl = '../../../../../assets/img/receipt.png';
        this._HelperService.OpenModal("Form_AddHelp_Content");
      }

        break;

      case HelpImageType.Terminal: {
        this.HelpImageUrl = '../../../../../assets/img/terminalid.png';
        this._HelperService.OpenModal('Form_AddHelp_Content');
      }

        break;

      case HelpImageType.Serial: {
        this.HelpImageUrl = '../../../../../assets/img/serial.png';
        this._HelperService.OpenModal('Form_AddHelp_Content');
      }

        break;

      default:
        break;
    }
  }



  //#region Add Store 

  _CurrentAddress: any = {};
  Form_AddStore_Address: string = null;
  CustomAddress: string = null;

  Form_AddStore_Latitude: number = 0;
  Form_AddStore_Longitude: number = 0;
  @ViewChild('places') places: GooglePlaceDirective;
  Form_AddStore_PlaceMarkerClick(event) {
    this.Form_AddStore_Latitude = event.coords.lat;
    this.Form_AddStore_Longitude = event.coords.lng;
  }
  public Form_AddStore_AddressChange(address: Address) {

    this.Form_AddStore_Latitude = address.geometry.location.lat();
    this.Form_AddStore_Longitude = address.geometry.location.lng();
    this.Form_AddStore_Address = address.formatted_address;

    this.Form_AddStore.controls['Latitude'].setValue(this.Form_AddStore_Latitude);
    this.Form_AddStore.controls['Longitude'].setValue(this.Form_AddStore_Longitude);
    this._CurrentAddress = this._HelperService.GoogleAddressArrayToJson(address.address_components);

  }

  Form_AddStore: FormGroup;
  Form_AddStore_Show() {
    this.ShowCategorySelector = false;
    setTimeout(() => {
      this.ShowCategorySelector = true;
      this._HelperService.setTheme();
    }, 300);
    this._HelperService.OpenModal("Form_AddStore_Content");
  }
  Form_AddStore_Close() {
    this._HelperService.CloseModal("Form_AddStore_Content");
  }
  Form_AddStore_Load() {
    this._HelperService._FileSelect_Icon_Data.Width = 128;
    this._HelperService._FileSelect_Icon_Data.Height = 128;

    this._HelperService._FileSelect_Poster_Data.Width = 800;
    this._HelperService._FileSelect_Poster_Data.Height = 400;

    this.Form_AddStore = this._FormBuilder.group({
      OperationType: "new",
      Task: this._HelperService.AppConfig.Api.ThankUCash.savestore,
      AccountId: this._HelperService.UserAccount.AccountId,
      AccountKey: this._HelperService.UserAccount.AccountKey,
      DisplayName: [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(256)])],
      Name: [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(256)])],
      StoreManagerId: [null],
      // FirstName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(128),Validators.pattern("^[a-zA-Z _-]*$")])],
      // LastName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(128),Validators.pattern("^[a-zA-Z _-]*$")])],
      ContactNumber: [null, Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(14)])],

      MobileNumber: [null],
      EmailAddress: [null, Validators.compose([Validators.required, Validators.email, Validators.minLength(2)])],
      MEmailAddress: [null],
      // BusinessCategory:[null, Validators.required],

      // MapAddress: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(256)])],
      // Latitude: [null, Validators.compose([Validators.required])],
      // Longitude: [null, Validators.compose([Validators.required])],
      StatusCode: this._HelperService.AppConfig.Status.Active,
    });
  }
  Form_AddStore_Clear() {
    this.Form_AddStore.reset();
    this._HelperService._FileSelect_Icon_Reset();
    this._HelperService._FileSelect_Poster_Reset();
    this.Form_AddStore_Load();
    this._HelperService.GetRandomNumber();
    this._HelperService.GeneratePassoword();
    this.Form_AddStore_Latitude = 0;
    this.Form_AddStore_Longitude = 0;
  }

  Form_AddStore_Process(_FormValue: any) {
    //This if condition act as validator for address field
    if (!this._Address.Address == undefined || this._Address.Address == null || this._Address.Address == "" || this._Address.CityId < 1) {
      this._HelperService.NotifyError("Please select store location");
    }
    else {
      //This if condition act as validator for Business Categories
      if (this.SelectedBusinessCategories.length == 0){
        this._HelperService.NotifyError("Please select Business Categories");
      }
      else if (this.SelectedBusinessCategories != undefined && this.SelectedBusinessCategories.length > 0) {
        _FormValue.Categories = []
        this.SelectedBusinessCategories.forEach(element => {
          _FormValue.Categories.push(
            {
              ReferenceId: element
            }
          )
        });
      
      this.ResetFilterUI();
      this._HelperService.IsFormProcessing = true;
      let _OResponse: Observable<OResponse>;
      if (this._HelperService.AppConfig.StorePermission) {
        if (this._HelperService.AppConfig.StorePermission.MinimumLimit <= this._HelperService.AppConfig.StoresCount && this._HelperService.AppConfig.StorePermission.MaximumLimit > this._HelperService.AppConfig.StoresCount) {
          var Request = this.CreateStoreRequest(_FormValue);
          _OResponse = this._HelperService.PostData(
            this._HelperService.AppConfig.NetworkLocation.V3.Account,
            Request
          );
          _OResponse.subscribe(
            _Response => {
              this._HelperService.IsFormProcessing = false;
              if (_Response.Status == this._HelperService.StatusSuccess) {
                this._HelperService.FlashSwalSuccess("Store Added successfully", "Done! New Store Has Been Added");
                this._HelperService.ObjectCreated.next(true);
                this.Form_AddStore_Clear();
                this.Form_AddStore_Close();
                if (_FormValue.OperationType == "close") {
                  this.Form_AddStore_Close();

                }
                this.S2BusinessCategories = [];
                this.SelectedBusinessCategories = []
                this.GetBusinessCategories()
                this._HelperService.GetAccountCount(this._HelperService.AppConfig.ActiveOwnerKey, this._HelperService.AppConfig.ActiveOwnerId, this._HelperService.AppConfig.DefaultStartTimeAll, this._HelperService.AppConfig.DefaultEndTimeToday)

              } else {
                this._HelperService.NotifyError(_Response.Message);
              }
            },
            _Error => {
              this._HelperService.IsFormProcessing = false;
              this._HelperService.HandleException(_Error);
            }
          );
        } else {
          this._HelperService.NotifyError('Upgrade Your Subscription')
          this.Form_AddStore_Close();
          this._Router.navigate(['m' + '/' + this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.Upgrade]);
          return;
        }
      }
      else {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.NotifyError('Store is disabled');
        return;
      }
    }
 
  }
}
  //#endregion
  CreateStoreRequest(_FormValue: any): void {
    _FormValue.ContactPerson = {
      // FirstName: _FormValue.FirstName,
      // LastName: _FormValue.LastName,
      MobileNumber: _FormValue.MobileNumber,
      EmailAddress: _FormValue.MEmailAddress
    }
    _FormValue.Address = this._Address.Address;
    _FormValue.AddressComponent = this._Address;
    _FormValue.FirstName = undefined;
    _FormValue.LastName = undefined;
    _FormValue.MobileNumber = undefined;
    // _FormValue.EmailAddress = undefined;

    _FormValue.Latitude = undefined;
    _FormValue.Longitude = undefined;
    _FormValue.MapAddress = undefined;

    return _FormValue;
  }

  CreateCashierRequest(_FormValue: any): void {

    // _FormValue.Address = {
    //   Latitude: this.Form_AddStore_Latitude,
    //   Longitude: this.Form_AddStore_Longitude,
    //   Address: this.Form_AddStore_Address,
    //   CityName: this._CurrentAddress.sublocality_level_2,
    //   StateName: this._CurrentAddress.sublocality_level_1,
    //   CountryName: this._CurrentAddress.country
    // }

    // _FormValue.Latitude = undefined;
    // _FormValue.Longitude = undefined;
    // _FormValue.MapAddress = undefined;

    var IconContent: any = undefined;

    if (this._HelperService._Icon_Cropper_Data.Content != null) {
      IconContent = this._HelperService._Icon_Cropper_Data;
    }

    _FormValue.IconContent = IconContent;

    return _FormValue;
  }

  CreateSubAccountRequest(_FormValue: any): void {

    // _FormValue.Address = {
    //   Latitude: this.Form_AddStore_Latitude,
    //   Longitude: this.Form_AddStore_Longitude,
    //   Address: this.Form_AddStore_Address,
    //   CityName: this._CurrentAddress.sublocality_level_2,
    //   StateName: this._CurrentAddress.sublocality_level_1,
    //   CountryName: this._CurrentAddress.country
    // }

    // _FormValue.Latitude = undefined;
    // _FormValue.Longitude = undefined;
    // _FormValue.MapAddress = undefined;

    var IconContent: any = undefined;

    if (this._HelperService._Icon_Cropper_Data.Content != null) {
      IconContent = this._HelperService._Icon_Cropper_Data;
    }

    _FormValue.IconContent = IconContent;

    return _FormValue;
  }

  public BusinessCategories = [];
  public S2BusinessCategories = [];
  public Select2Options_Multiple :Select2Options;
  GetBusinessCategories() {
    this.ShowCategorySelector  = false;
    this.Select2Options_Multiple = {
      multiple: true,
      placeholder: "Select category",
    };
    this._HelperService.ToggleField = true;
    var PData =
    {
      Task: this._HelperService.AppConfig.Api.Core.getcategories,
      SearchCondition: this._HelperService.GetSearchConditionStrict('', 'StatusCode', this._HelperService.AppConfig.DataType.Text, 'default.active', '='),
      SortExpression: 'Name asc',
      Offset: 0,
      Limit: 1000,
    }
    PData.SearchCondition = this._HelperService.GetSearchConditionStrict(
      PData.SearchCondition,
      "TypeCode",
      this._HelperService.AppConfig.DataType.Text,
      this._HelperService.AppConfig.HelperTypes.MerchantCategories,
      "="
    );
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Op, PData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          if (_Response.Result.Data != undefined) {
            this.BusinessCategories = _Response.Result.Data;

            this.ShowCategorySelector = false;
            this._ChangeDetectorRef.detectChanges();
            for (let index = 0; index < this.BusinessCategories.length; index++) {
              const element = this.BusinessCategories[index];
              this.S2BusinessCategories.push(
                {
                  id: element.ReferenceId,
                  key: element.ReferenceKey,
                  text: element.Name
                }
              );
            }
            this.ShowCategorySelector = true;
            this._HelperService.setTheme();
            this._ChangeDetectorRef.detectChanges();

            this._HelperService.ToggleField = false;

          }
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
        this._HelperService.ToggleField = false;

      });
  }
  public SelectedBusinessCategories = [];
  CategoriesSelected(Items) {

    if (Items != undefined && Items.value != undefined && Items.value.length > 0) {
      this.SelectedBusinessCategories = Items.value;
    }
    else {
      this.SelectedBusinessCategories = [];
    }


  }
  SaveMerchantBusinessCategory(item) {
    if (item != '0') {
      var Setup =
      {
        Task: this._HelperService.AppConfig.Api.Core.SaveUserParameter,
        TypeCode: this._HelperService.AppConfig.HelperTypes.MerchantCategories,
        // UserAccountKey: this._UserAccount.ReferenceKey,
        CommonKey: item,
        StatusCode: 'default.active'
      };
      let _OResponse: Observable<OResponse>;
      _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.System, Setup);
      _OResponse.subscribe(
        _Response => {
          if (_Response.Status == this._HelperService.StatusSuccess) {
            this._HelperService.NotifySuccess('Business category assigned to merchant');
            // this.BusinessCategories = [];
            // this.GetMerchantBusinessCategories();
          }
          else {
            this._HelperService.NotifyError(_Response.Message);
          }
        },
        _Error => {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.HandleException(_Error);
        });
    }
  }

  ExpandedView: any = false;

  ToogleExpandedView(): void {
    this.ExpandedView = !this.ExpandedView;
  }
  public ResetFilterControls: boolean = true;
  ResetFilterUI(): void {
    setTimeout(() => {
      this.ResetFilterControls = false;
      this._ChangeDetectorRef.detectChanges();
      this.Form_AddCashier_Load();
      this.Form_AddStore_Load();
      this.Form_AddUser_Load();
      this.Form_AddSubAccount_Load()
      this.ResetFilterControls = true;
      this._ChangeDetectorRef.detectChanges();
    }, 500);

  }
  imgProfileID: any;
  setprofileId(value) {
    this.imgProfileID = value;
  }
  resetClose() {
    if(this._HelperService.isUpdateSubaccountImage){      
      this._HelperService._InputFileComponent.files[0] = {};
      this._HelperService._InputFileComponent.files[0].preview = this._HelperService.subaccountImage
    }    
    if (this.imgProfileID == 'cashier') {
      this.InputFileComponent_Cashier.files.pop();
    } else if (this.imgProfileID == 'terminal') {
      this.InputFileComponent_Term.files.pop();
    }else if (this.imgProfileID == 'subaccount') {
      this.InputFileComponent_Subaccount.files.pop();
    }
    this._HelperService.Icon_Crop_Clear();
  }
}

export class OAccountOverview {
  public Merchants: number;
  public Stores: number;
  public ActiveMerchants: number;
  public ActiveMerchantsDiff: number;
  public Terminals: number;
  public ActiveTerminals: number;
  public ActiveTerminalsDiff: number;
  public Transactions: number;
  public TransactionsDiff: number;
  public PurchaseAmount: number;
  public PurchaseAmountDiff: number;
  public CashRewardPurchaseAmount: number;
  public CashRewardPurchaseAmountDiff: number;
  public CardRewardPurchaseAmount: number;
  public CardRewardPurchaseAmountDiff: number;
}
