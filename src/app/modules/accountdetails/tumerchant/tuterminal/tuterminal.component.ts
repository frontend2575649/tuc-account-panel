import { ChangeDetectorRef, Component, ElementRef, OnDestroy, OnInit, ViewChild } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import * as Feather from 'feather-icons';
import { GooglePlaceDirective } from 'ngx-google-places-autocomplete';
import { Address } from 'ngx-google-places-autocomplete/objects/address';
import { Observable, Subscription } from "rxjs";
import { DataHelperService, HelperService, OResponse, OSelect, OUserDetails, FilterHelperService } from "../../../../service/service";
declare let $: any;

@Component({
  selector: "tu-terminal",
  templateUrl: "./tuterminal.component.html"
})
export class TUTerminalComponent implements OnInit, OnDestroy {
  isLoaded: boolean = true;

  //#region subscriptions 

  subscription: Subscription;
  ReloadSubscription: Subscription;

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
    this.ReloadSubscription.unsubscribe();
  }

  //#endregion

  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef,
    public _FilterHelperService: FilterHelperService
  ) { }

  //#region MapCorrection 

  slideOpen: any = false;
  changeSlide(): void {
    this.slideOpen = !this.slideOpen;
    if (this.slideOpen) {

    }
  }

  //#endregion

  //#region ToogleDetailView 

  HideStoreDetail() {
    var element = document.getElementById("StoresHide");
    element.classList.add("Hm-HideDiv");
    element.classList.remove("Hm-ShowStoreDetail");
  }

  ShowStoreDetail() {
    var element = document.getElementById("StoresHide");
    element.classList.add("Hm-ShowStoreDetail");
    element.classList.remove("Hm-HideDiv");
  }

  //#endregion


  ngOnInit() {
    //#region UIInit 
    Feather.replace();
    this._HelperService.setTheme()

    this._HelperService.ContainerHeight = window.innerHeight;
    this._HelperService.AppConfig.ShowHeader = true;
    //#endregion
    //#region Subscriptions 
    this.subscription = this._HelperService.isprocessingtoogle
      .subscribe((item) => {
        this._ChangeDetectorRef.detectChanges();
      });
    this.ReloadSubscription = this._HelperService.ReloadEventEmit.subscribe((number) => {
    });
    //#endregion
    var StorageDetails = this._HelperService.GetStorage(this._HelperService.AppConfig.Storage.ActiveTerminal);
    if (StorageDetails != null) {
      this._HelperService.AppConfig.ActiveReferenceKey = StorageDetails.ReferenceKey;
      this._HelperService.AppConfig.ActiveReferenceId = StorageDetails.ReferenceId;
      this._HelperService.AppConfig.ActiveReferenceDisplayName = StorageDetails.DisplayName;
      this._HelperService.AppConfig.ActiveReferenceAccountTypeCode = StorageDetails.AccountTypeCode;
    }
    //#region DropdownInit 
    this.GetTerminalDetails();


    //#endregion

  }

  //#endregion

  //#region StoreDetails 
  public _TerminalDetails: any =
    {
      ManagerName: null,
      BranchName: null,
      ReferenceId: null,
      ReferenceKey: null,
      TypeCode: null,
      TypeName: null,
      SubTypeCode: null,
      SubTypeName: null,
      UserAccountKey: null,
      UserAccountDisplayName: null,
      Name: null,
      Description: null,
      StartDate: null,
      StartDateS: null,
      EndDate: null,
      EndDateS: null,
      SubTypeValue: null,
      MinimumInvoiceAmount: null,
      MaximumInvoiceAmount: null,
      MinimumRewardAmount: null,
      MaximumRewardAmount: null,
      ManagerKey: null,
      ManagerDisplayName: null,
      SmsText: null,
      Comment: null,
      CreateDate: null,
      CreatedByKey: null,
      CreatedByDisplayName: null,
      ModifyDate: null,
      ModifyByKey: null,
      ModifyByDisplayName: null,
      StatusId: null,
      StatusCode: null,
      StatusName: null,

      CreateDateS: null,
      ModifyDateS: null,
      StatusI: null,
      StatusB: null,
      StatusC: null,
      SupportContactName: null,
      SupportContactContactNumber: null

    }
  GetTerminalDetails() {

    this._HelperService.IsFormProcessing = true;
    this.isLoaded = false;

    var pData = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetTerminal,
      AccountKey: this._HelperService.AppConfig.ActiveReferenceKey,
      AccountId: this._HelperService.AppConfig.ActiveReferenceId
    }
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Account, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.IsFormProcessing = false;
          this.isLoaded = true;

          this._TerminalDetails = _Response.Result;
          this._ChangeDetectorRef.detectChanges();
          //#region ResponseInit 
          this._TerminalDetails.EndDateS = this._HelperService.GetDateS(
            this._TerminalDetails.EndDate
          );
          this._TerminalDetails.CreateDateS = this._HelperService.GetDateTimeS(
            this._TerminalDetails.CreateDate
          );
          this._TerminalDetails.ModifyDateS = this._HelperService.GetDateTimeS(
            this._TerminalDetails.ModifyDate
          );
          this._TerminalDetails.StatusI = this._HelperService.GetStatusIcon(
            this._TerminalDetails.StatusCode
          );
          this._TerminalDetails.StatusB = this._HelperService.GetStatusBadge(
            this._TerminalDetails.StatusCode
          );
          this._TerminalDetails.StatusC = this._HelperService.GetStatusColor(
            this._TerminalDetails.StatusCode
          );

          //#endregion

        }
        else {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      }
    );
  }

  //#endregion
  Form_EditUser_Block() {

  }
  //#region PosBlocking 
  BlockPos() {
    this._HelperService.OpenModal("BlockPos");
  }
  public Block(): void {
    this._HelperService.IsFormProcessing = true;
    this._HelperService.AppConfig.ShowHeader = true;
    var pData = {
      Task: "disableterminal",
      AccountId: this._TerminalDetails.ReferenceId,
      AccountKey: this._TerminalDetails.ReferenceKey,
      StatusCode: "default.blocked",
    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Account, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.NotifySuccess(_Response.Message);
          this._HelperService.CloseModal("BlockPos");
          this.GetTerminalDetails();

        } else {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      }
    );
  }
  public UnBlock(): void {
    this._HelperService.IsFormProcessing = true;
    this._HelperService.AppConfig.ShowHeader = true;
    var pData = {
      Task: "enableterminal",
      AccountId: this._TerminalDetails.ReferenceId,
      AccountKey: this._TerminalDetails.ReferenceKey,
      StatusCode: "default.blocked",
    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Account, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.NotifySuccess(_Response.Message);
          this._HelperService.CloseModal("BlockPos");
          this.GetTerminalDetails();

        } else {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      }
    );
  }
  //#endregion

}

export class OAccountOverview {
  public Merchants: number;
  public Stores: number;
  public ActiveMerchants: number;
  public ActiveMerchantsDiff: number;
  public Terminals: number;
  public ActiveTerminals: number;
  public ActiveTerminalsDiff: number;
  public Transactions: number;
  public TransactionsDiff: number;
  public PurchaseAmount: number;
  public PurchaseAmountDiff: number;
  public CashRewardPurchaseAmount: number;
  public CashRewardPurchaseAmountDiff: number;
  public CardRewardPurchaseAmount: number;
  public CardRewardPurchaseAmountDiff: number;
}
