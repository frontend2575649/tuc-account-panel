import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { OSelect, OList, DataHelperService, HelperService, OResponse, OStorageContent, OCoreParameter, OCoreCommon, FilterHelperService } from '../../../../service/service';
import swal from 'sweetalert2';
import { Options, LabelType, ChangeContext } from 'ng5-slider';
import * as Feather from 'feather-icons';
declare var moment: any;
declare var $: any;

@Component({
    selector: 'tu-sale',
    templateUrl: './tusale.component.html',
})
export class TUSaleComponent implements OnInit {
    TodayStartTime = null;
    TodayEndTime = null;
    constructor(
        public _Router: Router,
        public _ActivatedRoute: ActivatedRoute,
        public _FormBuilder: FormBuilder,
        public _HelperService: HelperService,
        public _DataHelperService: DataHelperService,
        public _ChangeDetectorRef: ChangeDetectorRef,
        public _FilterHelperService: FilterHelperService
    ) {
        this._HelperService.ShowDateRange = false;
    }
    ngOnInit() {
        Feather.replace();
        this._HelperService.StopClickPropogation();
        this._ActivatedRoute.params.subscribe((params: Params) => {
            this._HelperService.AppConfig.ActiveReferenceKey = params["referencekey"];
            this._HelperService.AppConfig.ActiveReferenceId = params['referenceid'];
            if (this._HelperService.AppConfig.ActiveReferenceKey == null || this._HelperService.AppConfig.ActiveReferenceId == null) {
                this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotFound]);
            } else {
                //#region DropdownInit 
                this.TodayStartTime = new Date(2017, 0, 1, 0, 0, 0, 0), moment();
                this.TodayEndTime = moment();
                this.TUTr_Filter_UserAccounts_Load();
                this.TUTr_Filter_Banks_Load();
                this.TUTr_Filter_Providers_Load();
                this.TUTr_Filter_Issuers_Load();
                this.TUTr_Filter_CardBrands_Load();
                this.TUTr_Filter_TransactionTypes_Load();
                this.TUTr_Filter_CardBanks_Load();

                //#endregion
                this.TUTr_Setup();
                this._HelperService.GetSubAccountOverviewLite(moment(this.TodayStartTime), moment(this.TodayEndTime));

            }
        });
    }

    //#region Transactions 

    public TUTr_Config: OList;
    TUTr_Setup() {
        this.TUTr_Config =
        {
            Id: null,
            Task: this._HelperService.AppConfig.Api.ThankUCash.getsaletransactions,
            Location: this._HelperService.AppConfig.NetworkLocation.V3.Transaction,
            Title: 'Sales History',
            StatusType: 'transaction',
            SearchBaseCondition: this._HelperService.GetSearchConditionStrict('', 'StoreReferenceId', this._HelperService.AppConfig.DataType.Number, this._HelperService.AppConfig.ActiveReferenceId, '='),
            AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
            AccountId: this._HelperService.AppConfig.ActiveOwnerId,
            Status: this._HelperService.AppConfig.StatusList.transactiondefaultitem,
            StatusName: 'Success',
            Type: this._HelperService.AppConfig.ListType.All,
            Sort:
            {
                SortDefaultName: null,
                SortDefaultColumn: 'TransactionDate',
                SortName: null,
                SortColumn: null,
                SortOrder: 'desc',
                SortOptions: [],
            },
            TableFields: [
                {
                    DisplayName: '#',
                    SystemName: 'ReferenceId',
                    DataType: this._HelperService.AppConfig.DataType.Number,
                    Show: true,
                    Search: false,
                    Sort: false,
                },
                {
                    DisplayName: 'Date',
                    SystemName: 'TransactionDate',
                    DataType: this._HelperService.AppConfig.DataType.Date,
                    Show: true,
                    Search: true,
                    Sort: true,
                    IsDateSearchField: true,
                },
                {
                    DisplayName: 'Transaction Status',
                    SystemName: 'StatusName',
                    DataType: this._HelperService.AppConfig.DataType.Date,
                    Show: true,
                    Search: false,
                    Sort: false,
                },
                {
                    DisplayName: 'Customer',
                    SystemName: 'UserAccountId',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Show: true,
                    Search: true,
                    Sort: true,
                },
                {
                    DisplayName: 'User Mobile Number',
                    SystemName: 'UserMobileNumber',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Show: true,
                    Search: true,
                    Sort: false,
                },
                {
                    DisplayName: 'Type',
                    SystemName: 'TypeName',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Show: true,
                    Search: false,
                    Sort: false,
                },
                {
                    DisplayName: 'Invoice Amount',
                    SystemName: 'InvoiceAmount',
                    DataType: this._HelperService.AppConfig.DataType.Decimal,
                    Show: true,
                    Search: false,
                    Sort: true,
                },
                {
                    DisplayName: 'Reward Amount',
                    SystemName: 'RewardAmount',
                    DataType: this._HelperService.AppConfig.DataType.Decimal,
                    Class: 'text-grey',
                    Search: false,
                    Sort: false,
                },
                {
                    DisplayName: 'User Reward Amount',
                    SystemName: 'UserAmount',
                    DataType: this._HelperService.AppConfig.DataType.Decimal,
                    Show: true,
                    Search: false,
                    Sort: false,
                },
                {
                    DisplayName: 'Commission Amount',
                    SystemName: 'CommissionAmount',
                    DataType: this._HelperService.AppConfig.DataType.Decimal,
                    Show: true,
                    Search: false,
                    Sort: false,
                },

                {
                    DisplayName: 'Card Number',
                    SystemName: 'AccountNumber',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Show: true,
                    Search: false,
                    Sort: false,
                },
                {
                    DisplayName: 'Card Type',
                    SystemName: 'CardBrandName',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Show: true,
                    Search: false,
                    Sort: false,
                },
                {
                    DisplayName: 'Card Bank Provider',
                    SystemName: 'CardBankName',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Show: true,
                    Search: false,
                    Sort: false,
                },
                {
                    DisplayName: 'TUC Card Number',
                    SystemName: 'TUCCardNumber',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Show: true,
                    Search: false,
                    Sort: false,
                },
                {
                    DisplayName: 'Merchant',
                    SystemName: 'ParentDisplayName',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Show: true,
                    Search: false,
                    Sort: false,
                },
                {
                    DisplayName: 'Store',
                    SystemName: 'SubParentDisplayName',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Show: true,
                    Search: false,
                    Sort: false,
                },
                {
                    DisplayName: 'Acquirer / Bank',
                    SystemName: 'AcquirerDisplayName',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Show: true,
                    Search: false,
                    Sort: false,
                },
                {
                    DisplayName: 'Terminal Provider',
                    SystemName: 'ProviderDisplayName',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Show: true,
                    Search: false,
                    Sort: false,
                },
                {
                    DisplayName: 'Transaction Issuer (Done by)',
                    SystemName: 'CreatedByDisplayName',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Show: true,
                    Search: false,
                    Sort: false,
                },
                {
                    DisplayName: 'Transaction Reference',
                    SystemName: 'ReferenceNumber',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: '',
                    Show: true,
                    Search: true,
                    Sort: false,
                    ResourceId: null,
                },


            ]
        }
        this.TUTr_Config = this._DataHelperService.List_Initialize(this.TUTr_Config);
        this._HelperService.Active_FilterInit(
            this._HelperService.AppConfig.FilterTypeOption.MerchantSales,
            this.TUTr_Config
        );
        this.TUTr_GetData();
    }
    // TUTr_ToggleOption_Date(event: any, Type: any) {
    //     this.TUTr_ToggleOption(event, Type);
    // }
    TUTr_ToggleOption(event: any, Type: any) {
        if (Type == "date") {
            this._HelperService.AppConfig.DateRangeOptions.startDate = event.start;
            this._HelperService.AppConfig.DateRangeOptions.endDate = event.end;
        }

        this._HelperService.Update_CurrentFilterSnap(event, Type, this.TUTr_Config);

        this.TUTr_Config = this._DataHelperService.List_Operations(this.TUTr_Config, event, Type);
        //this._DataHelperService.List_OperationsForTransCounts(this.Data, event, Type);

        this.TodayStartTime = this.TUTr_Config.StartTime;
        this.TodayEndTime = this.TUTr_Config.EndTime;
        if (event != null) {
            for (let index = 0; index < this.TUTr_Config.Sort.SortOptions.length; index++) {
                const element = this.TUTr_Config.Sort.SortOptions[index];
                if (event.SystemName == element.SystemName) {

                    element.SystemActive = true;



                }

                else {
                    element.SystemActive = false;

                }

            }
        }


        if (
            (this.TUTr_Config.RefreshData == true)
            && this._HelperService.DataReloadEligibility(Type)
        ) {
            this.TUTr_GetData();
        }
    }

    public OverviewData: any = {
        Transactions: 0,
        InvoiceAmount: 0.0
    };
    TUTr_GetData() {
        this.GetOverviews(this.TUTr_Config, this._HelperService.AppConfig.Api.ThankUCash.TransactionOverviews.getsaletransactionsoverview);
        var TConfig = this._DataHelperService.List_GetDataTur(this.TUTr_Config);
        this.TUTr_Config = TConfig;
    }
    TUTr_RowSelected(ReferenceData) {
        var ReferenceKey = ReferenceData.ReferenceKey;
        this._HelperService.AppConfig.ActiveReferenceKey = ReferenceKey;
    }

    //#endregion

    SetOtherFilters(): void {
        this.TUTr_Config.SearchBaseConditions = [];
        var CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.MerchantSales.Merchant));
        if (CurrentIndex != -1) {
            this.TUTr_Filter_Merchant_Selected = 0;
            this.MerchantEventProcessing(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
        }

        var CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.MerchantSales.Provider));
        if (CurrentIndex != -1) {
            this.TUTr_Filter_Provider_Selected = 0;
            this.ProvidersEventProcessing(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
        }

        CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.MerchantSales.Store));
        if (CurrentIndex != -1) {
            this.TUTr_Filter_Store_Selected = 0;
            this.StoresEventProcessing(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
        }

        CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.MerchantSales.TransactionType));
        if (CurrentIndex != -1) {
            this.TUTr_Filter_TransactionType_Selected = 0;
            this.TransTypeEventProcessing(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
        }

        CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.MerchantSales.CardBank));
        if (CurrentIndex != -1) {
            this.TUTr_Filter_CardBank_Selected = 0;
            this.CardBankEventProcessing(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
        }

        CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.MerchantSales.CardBrand));
        if (CurrentIndex != -1) {
            this.TUTr_Filter_CardBrand_Selected = 0;
            this.CardBrandEventProcessing(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
        }
    }

    //#region dropdowns 

    //#region merchant 

    public TUTr_Filter_Merchant_Option: Select2Options;
    public TUTr_Filter_Merchant_Selected = 0;
    TUTr_Filter_Merchants_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.ThankUCash.GetStores,
            Location: this._HelperService.AppConfig.NetworkLocation.V3.Account,
            ReferenceKey: this._HelperService.AppConfig.ActiveOwnerKey,
            ReferenceId: this._HelperService.AppConfig.ActiveOwnerId,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "DisplayName",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                // {
                //     SystemName: "AccountTypeCode",
                //     Type: this._HelperService.AppConfig.DataType.Text,
                //     SearchCondition: "=",
                //     SearchValue: this._HelperService.AppConfig.AccountType.Merchant
                // }
            ]
        };
        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.TUTr_Filter_Merchant_Option = {
            placeholder: 'Search By Merchant',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    }
    TUTr_Filter_Merchants_Change(event: any) {
        this._HelperService.Update_CurrentFilterSnap(
            event,
            this._HelperService.AppConfig.ListToggleOption.Other,
            this.TUTr_Config,
            this._HelperService.AppConfig.OtherFilters.MerchantSales.Merchant
        );
        this.MerchantEventProcessing(event);
    }

    MerchantEventProcessing(event: any): void {
        if (event.value == this.TUTr_Filter_Merchant_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'ParentId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Merchant_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_Merchant_Selected = 0;
        }
        else if (event.value != this.TUTr_Filter_Merchant_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'ParentId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Merchant_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_Merchant_Selected = event.value;
            this.TUTr_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'ParentId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Merchant_Selected, '='));
        }
        this.TUTr_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
        this._HelperService.ToggleField = true;
        this.TUTr_Filter_Store_Toggle = true;

        this.TUTr_Filter_Stores_Load();
        this.TUTr_Filter_Issuers_Load();
        setTimeout(() => {
            this._HelperService.ToggleField = false;
            this.TUTr_Filter_Store_Toggle = false;
        }, 500);
    }

    //#endregion

    //#region stores 

    public TUTr_Filter_Store_Option: Select2Options;
    public TUTr_Filter_Store_Toggle = false;
    public TUTr_Filter_Store_Selected = 0;
    TUTr_Filter_Stores_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.ThankUCash.GetStores,
            Location: this._HelperService.AppConfig.NetworkLocation.V3.Account,
            AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
            AccountId: this._HelperService.AppConfig.ActiveOwnerId,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "DisplayName",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                }

            ]
        };


        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.TUTr_Filter_Store_Option = {
            placeholder: 'Search by Store',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    }

    TUTr_Filter_Stores_Change(event: any) {
        this._HelperService.Update_CurrentFilterSnap(
            event,
            this._HelperService.AppConfig.ListToggleOption.Other,
            this.TUTr_Config,
            this._HelperService.AppConfig.OtherFilters.MerchantSales.Store
        );
        this.StoresEventProcessing(event);
    }

    StoresEventProcessing(event: any): void {
        if (event.value == this.TUTr_Filter_Store_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'StoreReferenceId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Store_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_Store_Selected = 0;
        }
        else if (event.value != this.TUTr_Filter_Store_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'StoreReferenceId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Store_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_Store_Selected = event.value;
            this.TUTr_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'StoreReferenceId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Store_Selected, '='));
        }
        this.TUTr_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
        this._HelperService.ToggleField = true;
        this.TUTr_Filter_Issuers_Load();
        setTimeout(() => {
            this._HelperService.ToggleField = false;
        }, 500);
    }

    //#endregion

    //#region Cashiers 

    public TUTr_Filter_Cashier_Option: Select2Options;
    public TUTr_Filter_Cashier_Toggle = false;
    public TUTr_Filter_Cashier_Selected = 0;
    TUTr_Filter_Cashiers_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.ThankUCash.GetCashiers,
            Location: this._HelperService.AppConfig.NetworkLocation.V3.Account,
            AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
            AccountId: this._HelperService.AppConfig.ActiveOwnerId,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "Name",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                }

            ]
        };


        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.TUTr_Filter_Cashier_Option = {
            placeholder: 'Search by Cashier',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    }

    TUTr_Filter_Cashiers_Change(event: any) {
        this._HelperService.Update_CurrentFilterSnap(
            event,
            this._HelperService.AppConfig.ListToggleOption.Other,
            this.TUTr_Config,
            this._HelperService.AppConfig.OtherFilters.MerchantSales.Cashier
        );
        this.CashiersEventProcessing(event);
    }

    CashiersEventProcessing(event: any): void {
        if (event.value == this.TUTr_Filter_Cashier_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'CashierId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Cashier_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_Cashier_Selected = 0;
        }
        else if (event.value != this.TUTr_Filter_Cashier_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'CashierId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Cashier_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_Cashier_Selected = event.value;
            this.TUTr_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'CashierId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Cashier_Selected, '='));
        }
        this.TUTr_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
        this._HelperService.ToggleField = true;
        this.TUTr_Filter_Issuers_Load();
        setTimeout(() => {
            this._HelperService.ToggleField = false;
        }, 500);
    }

    //#endregion


    //#region Terminal Filter
    public TUTr_Filter_Terminal_Option: Select2Options;
    public TUTr_Filter_Terminal_Toggle = false;
    public TUTr_Filter_Terminal_Selected = 0;
    TUTr_Filter_Terminals_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.ThankUCash.GetTerminals,
            Location: this._HelperService.AppConfig.NetworkLocation.V3.Account,
            AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
            AccountId: this._HelperService.AppConfig.ActiveOwnerId,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "TerminalId",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                }

            ]
        };


        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.TUTr_Filter_Terminal_Option = {
            placeholder: 'Search by Terminal',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    }

    TUTr_Filter_Terminals_Change(event: any) {
        this._HelperService.Update_CurrentFilterSnap(
            event,
            this._HelperService.AppConfig.ListToggleOption.Other,
            this.TUTr_Config,
            this._HelperService.AppConfig.OtherFilters.MerchantSales.Terminal
        );
        this.TerminalsEventProcessing(event);
    }

    TerminalsEventProcessing(event: any): void {
        if (event.value == this.TUTr_Filter_Terminal_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'CreatedById', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Terminal_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_Terminal_Selected = 0;
        }
        else if (event.value != this.TUTr_Filter_Terminal_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'CreatedById', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Terminal_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_Terminal_Selected = event.value;
            this.TUTr_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'CreatedById', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Terminal_Selected, '='));
        }
        this.TUTr_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
        this._HelperService.ToggleField = true;
        this.TUTr_Filter_Issuers_Load();
        setTimeout(() => {
            this._HelperService.ToggleField = false;
        }, 500);
    }
    //#endregion End


    //#region useraccount 

    public TUTr_Filter_UserAccount_Option: Select2Options;
    public TUTr_Filter_UserAccount_Selected = 0;
    TUTr_Filter_UserAccounts_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "DisplayName",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                {
                    SystemName: "MobileNumber",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                {
                    SystemName: "AccountTypeCode",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: "=",
                    SearchValue: this._HelperService.AppConfig.AccountType.AppUser
                }
            ]
        };
        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.TUTr_Filter_UserAccount_Option = {
            placeholder: 'Search Customer',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    }
    TUTr_Filter_UserAccounts_Change(event: any) {
        if (event.value == this.TUTr_Filter_UserAccount_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'UserAccountId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_UserAccount_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_UserAccount_Selected = 0;
        }
        else if (event.value != this.TUTr_Filter_UserAccount_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'UserAccountId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_UserAccount_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_UserAccount_Selected = event.value;
            this.TUTr_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'UserAccountId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_UserAccount_Selected, '='));
        }
        this.TUTr_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
    }

    //#endregion

    //#region bank 

    public TUTr_Filter_Bank_Option: Select2Options;
    public TUTr_Filter_Bank_Selected = 0;
    TUTr_Filter_Banks_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "DisplayName",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                {
                    SystemName: "AccountTypeCode",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: "=",
                    SearchValue: this._HelperService.AppConfig.AccountType.Acquirer
                }]
        };
        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.TUTr_Filter_Bank_Option = {
            placeholder: 'Search By Bank / Acquirer',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    }

    TUTr_Filter_Banks_Change(event: any) {

        this._HelperService.Update_CurrentFilterSnap(
            event,
            this._HelperService.AppConfig.ListToggleOption.Other,
            this.TUTr_Config,
            this._HelperService.AppConfig.OtherFilters.MerchantSales.Bank
        );

        this.BanksEventProcessing(event);
    }

    BanksEventProcessing(event: any): void {
        if (event.value == this.TUTr_Filter_Bank_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'AcquirerId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Bank_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_Bank_Selected = 0;
        }
        else if (event.value != this.TUTr_Filter_Bank_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'AcquirerId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Bank_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_Bank_Selected = event.value;
            this.TUTr_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'AcquirerId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Bank_Selected, '='));
        }
        this.TUTr_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
        this._HelperService.ToggleField = true;
        this.TUTr_Filter_Issuers_Load();
        setTimeout(() => {
            this._HelperService.ToggleField = false;
        }, 500);
    }

    //#endregion

    //#region provider 

    public TUTr_Filter_Provider_Option: Select2Options;
    public TUTr_Filter_Provider_Selected = 0;
    TUTr_Filter_Providers_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "DisplayName",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                {
                    SystemName: "AccountTypeCode",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: "=",
                    SearchValue: this._HelperService.AppConfig.AccountType.PosAccount
                }]
        };


        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.TUTr_Filter_Provider_Option = {
            placeholder: 'Search By Provider',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    }

    TUTr_Filter_Providers_Change(event: any) {
        this._HelperService.Update_CurrentFilterSnap(
            event,
            this._HelperService.AppConfig.ListToggleOption.Other,
            this.TUTr_Config,
            this._HelperService.AppConfig.OtherFilters.MerchantSales.Provider
        );

        this.ProvidersEventProcessing(event);
    }

    ProvidersEventProcessing(event: any): void {
        if (event.value == this.TUTr_Filter_Provider_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'ProviderId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Provider_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_Provider_Selected = 0;
        }
        else if (event.value != this.TUTr_Filter_Provider_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'ProviderId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Provider_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_Provider_Selected = event.value;
            this.TUTr_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'ProviderId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Provider_Selected, '='));
        }
        this.TUTr_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
        this._HelperService.ToggleField = true;
        this.TUTr_Filter_Issuers_Load();
        setTimeout(() => {
            this._HelperService.ToggleField = false;
        }, 500);
    }

    //#endregion

    //#region issuers 

    public TUTr_Filter_Issuer_Option: Select2Options;
    public TUTr_Filter_Issuer_Selected = 0;
    TUTr_Filter_Issuers_Load() {
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "DisplayName",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                }]
        };


        _Select.SearchCondition = this._HelperService.GetSearchConditionStrictFromArray('', 'AccountTypeCode', this._HelperService.AppConfig.DataType.Text,
            [
                this._HelperService.AppConfig.AccountType.PosTerminal,
                this._HelperService.AppConfig.AccountType.PGAccount,
                this._HelperService.AppConfig.AccountType.Cashier,
            ], "=");

        if (this.TUTr_Filter_Provider_Selected != 0) {
            _Select.SearchCondition = this._HelperService.GetSearchConditionStrict(_Select.SearchCondition, 'OwnerId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Provider_Selected, '=');
        }
        if (this.TUTr_Filter_Bank_Selected != 0) {
            _Select.SearchCondition = this._HelperService.GetSearchConditionStrict(_Select.SearchCondition, 'BankId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Bank_Selected, '=');
        }
        _Select.SearchCondition = this._HelperService.GetSearchConditionStrict(_Select.SearchCondition, 'SubOwnerId', this._HelperService.AppConfig.DataType.Number, this._HelperService.AppConfig.ActiveReferenceId, '=');
        _Select.SearchCondition = this._HelperService.GetSearchConditionStrictOr(_Select.SearchCondition, 'OwnerId', this._HelperService.AppConfig.DataType.Number, this._HelperService.AppConfig.ActiveReferenceId, '=');
        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.TUTr_Filter_Issuer_Option = {
            placeholder: 'Search By Issuer',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    }

    TUTr_Filter_Issuers_Change(event: any) {
        if (event.value == this.TUTr_Filter_Issuer_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'CreatedById', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Issuer_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_Issuer_Selected = 0;
        }
        else if (event.value != this.TUTr_Filter_Issuer_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'CreatedById', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Issuer_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_Issuer_Selected = event.value;
            this.TUTr_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'CreatedById', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Issuer_Selected, '='));
        }
        this.TUTr_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
    }

    //#endregion

    //#region transactionType 

    public TUTr_Filter_TransactionType_Option: Select2Options;
    public TUTr_Filter_TransactionType_Selected = 0;
    TUTr_Filter_TransactionTypes_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.Core.GetCoreHelpersLite,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "Name",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                {
                    SystemName: "ParentCode",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: "=",
                    SearchValue: this._HelperService.AppConfig.HelperTypes.TransactionType
                },
                {
                    SystemName: "SubParentCode",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: "=",
                    SearchValue: this._HelperService.AppConfig.HelperTypes.TransactionTypeReward
                }]
        };


        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.TUTr_Filter_TransactionType_Option = {
            placeholder: 'Search By Tran. Type',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    }
    TUTr_Filter_TransactionTypes_Change(event: any) {
        this._HelperService.Update_CurrentFilterSnap(
            event,
            this._HelperService.AppConfig.ListToggleOption.Other,
            this.TUTr_Config,
            this._HelperService.AppConfig.OtherFilters.MerchantSales.TransactionType
        );

        this.TransTypeEventProcessing(event);
    }

    TransTypeEventProcessing(event: any): void {
        if (event.value == this.TUTr_Filter_TransactionType_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'TypeId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_TransactionType_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_TransactionType_Selected = 0;
        }
        else if (event.value != this.TUTr_Filter_TransactionType_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'TypeId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_TransactionType_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_TransactionType_Selected = event.value;
            this.TUTr_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'TypeId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_TransactionType_Selected, '='));
        }
        this.TUTr_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
    }

    //#endregion

    //#region cardbrands 

    public TUTr_Filter_CardBrand_Option: Select2Options;
    public TUTr_Filter_CardBrand_Selected = 0;
    TUTr_Filter_CardBrands_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.Core.GetCoreCommonsLite,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "Name",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                {
                    SystemName: "TypeCode",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: "=",
                    SearchValue: this._HelperService.AppConfig.HelperTypes.CardBrand
                }]
        };


        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.TUTr_Filter_CardBrand_Option = {
            placeholder: 'Search By Card Brand',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    }

    TUTr_Filter_CardBrands_Change(event: any) {

        this._HelperService.Update_CurrentFilterSnap(
            event,
            this._HelperService.AppConfig.ListToggleOption.Other,
            this.TUTr_Config,
            this._HelperService.AppConfig.OtherFilters.MerchantSales.CardBrand
        );
        this.CardBrandEventProcessing(event);
    }

    CardBrandEventProcessing(event: any): void {
        if (event.value == this.TUTr_Filter_CardBrand_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'CardBrandId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_CardBrand_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_CardBrand_Selected = 0;
        }
        else if (event.value != this.TUTr_Filter_CardBrand_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'CardBrandId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_CardBrand_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_CardBrand_Selected = event.value;
            this.TUTr_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'CardBrandId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_CardBrand_Selected, '='));
        }
        this.TUTr_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
    }

    //#endregion

    //#region cardbank 

    public TUTr_Filter_CardBank_Option: Select2Options;
    public TUTr_Filter_CardBank_Selected = 0;
    TUTr_Filter_CardBanks_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.Core.GetCoreCommonsLite,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: "",
            SortCondition: [],
            Fields:
                [
                    {
                        SystemName: "ReferenceId",
                        Type: this._HelperService.AppConfig.DataType.Number,
                        Id: true,
                        Text: false,
                    },
                    {
                        SystemName: "Name",
                        Type: this._HelperService.AppConfig.DataType.Text,
                        Id: false,
                        Text: true
                    },
                    {
                        SystemName: "TypeCode",
                        Type: this._HelperService.AppConfig.DataType.Text,
                        SearchCondition: "=",
                        SearchValue: this._HelperService.AppConfig.HelperTypes.CardBank
                    }
                ]
        };
        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.TUTr_Filter_CardBank_Option = {
            placeholder: 'Search By Card Bank',
            ajax: _Transport,
            multiple: false,
            allowClear: true,

        };
    }

    TUTr_Filter_CardBanks_Change(event: any) {
        this._HelperService.Update_CurrentFilterSnap(
            event,
            this._HelperService.AppConfig.ListToggleOption.Other,
            this.TUTr_Config,
            this._HelperService.AppConfig.OtherFilters.MerchantSales.CardBank
        );

        this.CardBankEventProcessing(event);
    }

    CardBankEventProcessing(event: any): void {
        if (event.value == this.TUTr_Filter_CardBank_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'CardBankId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_CardBank_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_CardBank_Selected = 0;
        }
        else if (event.value != this.TUTr_Filter_CardBank_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'CardBankId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_CardBank_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_CardBank_Selected = event.value;
            this.TUTr_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'CardBankId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_CardBank_Selected, '='));
        }
        this.TUTr_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
    }

    //#endregion

    //#endregion

    public ResetFilterControls: boolean = true;
    ResetFilterUI(): void {
        this.ResetFilterControls = false;
        this._ChangeDetectorRef.detectChanges();

        this.TUTr_Filter_UserAccounts_Load();
        this.TUTr_Filter_Providers_Load();
        this.TUTr_Filter_Issuers_Load();
        this.TUTr_Filter_CardBrands_Load();
        this.TUTr_Filter_TransactionTypes_Load();
        this.TUTr_Filter_CardBanks_Load();

        this.ResetFilterControls = true;
        this._ChangeDetectorRef.detectChanges();

    }

    //#region filterOperations

    Active_FilterValueChanged(event: any) {
        this._HelperService.Active_FilterValueChanged(event);
        this._FilterHelperService.SetMerchantSalesConfig(this.TUTr_Config);

        //#region setOtherFilters
        this.SetOtherFilters();
        //#endregion

        this.TUTr_GetData();
    }

    RemoveFilterComponent(Type: string, index?: number): void {
        this._FilterHelperService._RemoveFilter_MerchantSales(Type, index);
        this._FilterHelperService.SetMerchantSalesConfig(this.TUTr_Config);

        if (Type == 'Time') {
            this._HelperService.AppConfig.DateRangeOptions.startDate= new Date(2017, 0, 1, 0, 0, 0, 0);
            this._HelperService.AppConfig.DateRangeOptions.endDate=moment().endOf("day");
        }
        //#region setOtherFilters
        this.SetOtherFilters();
        //#endregion

        this.TUTr_GetData();
    }

    Save_NewFilter() {
        swal({
            position: "center",
            title: this._HelperService.AppConfig.CommonResource.SaveFilterTitle,
            text: this._HelperService.AppConfig.CommonResource.SaveFilterHelp,
            input: "text",
            inputPlaceholder: this._HelperService.AppConfig.CommonResource.FilterName,
            inputAttributes: {
                autocapitalize: "off",
                autocorrect: "off",
                maxLength: "32",
                minLength: "4",
            },
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Green,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: "Save",
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
        }).then((result) => {
            if (result.value) {
                this._HelperService._RefreshUI = false;
                this._ChangeDetectorRef.detectChanges();

                this._FilterHelperService._BuildFilterName_MerchantSales(result.value);
                this._HelperService.Save_NewFilter(
                    this._HelperService.AppConfig.FilterTypeOption.MerchantSales
                );

                this._HelperService._RefreshUI = true;
                this._ChangeDetectorRef.detectChanges();
            }
        });
    }

    Delete_Filter() {

        swal({
            position: "center",
            title: this._HelperService.AppConfig.CommonResource.DeleteTitle,
            text: this._HelperService.AppConfig.CommonResource.DeleteHelp,
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Red,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel

        }).then((result) => {
            if (result.value) {
                this._HelperService._RefreshUI = false;
                this._ChangeDetectorRef.detectChanges();

                this._HelperService.Delete_Filter(
                    this._HelperService.AppConfig.FilterTypeOption.MerchantSales
                );
                this._FilterHelperService.SetMerchantSalesConfig(this.TUTr_Config);
                this.TUTr_GetData();

                this._HelperService._RefreshUI = true;
                this._ChangeDetectorRef.detectChanges();
            }
        });
    }

    ApplyFilters(event: any, Type: any, ButtonType: any): void {
        this._HelperService.MakeFilterSnapPermanent();
        this.TUTr_GetData();

        this.ResetFilterUI(); this._HelperService.StopClickPropogation();

        if (ButtonType == 'Sort') {
            $("#TUTr_sdropdown").dropdown('toggle');
        } else if (ButtonType == 'Other') {
            $("#TUTr_fdropdown").dropdown('toggle');
        }
    }

    ResetFilters(event: any, Type: any): void {
        this._HelperService.ResetFilterSnap();
        this._FilterHelperService.SetMerchantSalesConfig(this.TUTr_Config);

        //#region setOtherFilters
        this.SetOtherFilters();
        //#endregion
        this.TUTr_GetData();

        this.ResetFilterUI(); this._HelperService.StopClickPropogation();
    }

    //#endregion

    //#region SalesOverview 

    GetOverviews(ListOptions: any, Task: string): any {
        this._HelperService.IsFormProcessing = true;

        ListOptions.SearchCondition = '';
        ListOptions = this._DataHelperService.List_GetSearchConditionTur(ListOptions);
        if (ListOptions.ActivePage == 1) {
            ListOptions.RefreshCount = true;
        }
        var SortExpression = ListOptions.Sort.SortDefaultColumn + ' ' + ListOptions.Sort.SortDefaultOrder;;

        if (ListOptions.Sort.SortDefaultName) {
            ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName.replace(' desc', '');
            ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName.replace(' asc', '');
            ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName + ' ' + ListOptions.Sort.SortDefaultOrder;
        }

        if (ListOptions.Sort.SortColumn != undefined && ListOptions.Sort.SortColumn != null && ListOptions.Sort.SortColumn != '') {
            if (ListOptions.Sort.SortOrder != undefined && ListOptions.Sort.SortOrder != null && ListOptions.Sort.SortOrder != '') {
                SortExpression = ListOptions.Sort.SortColumn + ' ' + ListOptions.Sort.SortOrder;
            }
            else {
                SortExpression = ListOptions.Sort.SortColumn + ' desc';
            }
        }


        var pData = {
            Task: Task,
            TotalRecords: ListOptions.TotalRecords,
            Offset: (ListOptions.ActivePage - 1) * ListOptions.PageRecordLimit,
            Limit: ListOptions.PageRecordLimit,
            RefreshCount: ListOptions.RefreshCount,
            SearchCondition: ListOptions.SearchCondition,
            SortExpression: SortExpression,
            Type: ListOptions.Type,
            ReferenceKey: ListOptions.ReferenceKey,
            StartDate: ListOptions.StartDate,
            EndDate: ListOptions.EndDate,
            ReferenceId: ListOptions.ReferenceId,
            SubReferenceId: ListOptions.SubReferenceId,
            SubReferenceKey: ListOptions.SubReferenceKey,
            AccountId: ListOptions.AccountId,
            AccountKey: ListOptions.AccountKey,
            ListType: ListOptions.ListType,
            IsDownload: false,
        };

        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Transaction, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsFormProcessing = false;
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this.OverviewData = _Response.Result as any;
                    console.log(this.OverviewData);
                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);

            });


    }

    //#endregion


}