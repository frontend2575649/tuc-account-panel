import { ChangeDetectorRef, Component, OnInit, ViewChildren, OnDestroy } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { BaseChartDirective, Label, Color } from 'ng2-charts';
import { Observable, Subscription } from 'rxjs';
import { DataHelperService, HelperService, OList, OResponse, OSelect, OSalesTrend, OSalesTrendData, OSalesTrendDataHourly, OLoyalityHistoryData, OLoyalityHistory } from '../../../service/service';
declare var moment: any;
declare var $: any;
declare var d3: any;
import * as cloneDeep from 'lodash/cloneDeep';
import * as lodash from 'lodash';
import * as pluginEmptyOverlay from "chartjs-plugin-empty-overlay";
import * as Feather from "feather-icons";
import { ChartDataSets } from 'chart.js';
import { timeInterval } from 'rxjs/operators';
@Component({
  selector: 'dashboard',
  templateUrl: './dashboard.component.html',
  styles: [`
    agm-map {
      height: 300px;
    }
`]
})
export class TUDashboardComponent implements OnInit, OnDestroy {

  public _DateSubscription: Subscription = null;
  ngOnDestroy(): void {
    this._HelperService.ShowDateRange = false;
    if (this._DateSubscription != undefined && this._DateSubscription != null) {
      this._DateSubscription.unsubscribe();
    }
  }

  @ViewChildren(BaseChartDirective) components: BaseChartDirective[];

  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef

  ) {
    this._HelperService.ShowDateRange = true;
  }

  public BarChartOptions: any = {
    cornerRadius: 20,
    responsive: true,
    legend: {
      display: false,
      position: 'right',
    },
    ticks: {
      autoSkip: false
    },
    scales: {
      xAxes: [
        {
          gridLines: {
            stacked: true,
            display: false
          },
          ticks: {
            autoSkip: false,
            fontSize: 11
          }
        }
      ],
      yAxes: [
        {

          gridLines: {
            stacked: true,
            display: true
          },
          ticks: {
            beginAtZero: true,
            fontSize: 11
          }
        }
      ]
    },
    annotation: {
      annotations: [{
        type: 'line',
        mode: 'horizontal',
        scaleID: 'y-axis-0',
        // value: 20,
        borderColor: 'rgb(75, 192, 192)',
        borderWidth: 4,
        label: {
          enabled: false,
          content: 'Test label'
        }
      }]
    },
    plugins: {
      datalabels: {
        backgroundColor: "#ffffff47",
        color: "#798086",
        borderRadius: "2",
        borderWidth: "1",
        borderColor: "transparent",
        anchor: "end",
        align: "end",
        padding: 2,
        font: {
          size: 10,
          weight: 500
        },
        formatter: (value, ctx) => {
          const label = ctx.chart.data.labels[ctx.dataIndex];
          if (label != undefined) {
            return value;
          } else {
            return value;
          }
        }
      },
    },
    emptyOverlay: {
      fillStyle: 'rgba(255,0,0,0.4)',
      fontColor: 'rgba(255,255,255,1.0)',
      fontStrokeWidth: 0,
      enabled: true
    }
  }
  public barChartLabels: Label[] = [];
  public barChartColors = [{ backgroundColor: ['#00CCCC', '#00CCCC', '#00CCCC', '#00CCCC', '#00CCCC', '#00CCCC', '#00CCCC'] }, { backgroundColor: ['#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A'] }, { backgroundColor: ['#F10875', '#F10875', '#F10875', '#F10875', '#F10875', '#F10875', '#F10875'] }, { backgroundColor: ['#0168FA', '#0168FA', '#0168FA', '#0168FA', '#0168FA', '#0168FA', '#0168FA'] }];
  public barChartType = 'bar';
  public barChartLegend = true;
  public barChartData = [
    { data: [0, 0, 0, 0, 0, 0, 0], label: 'Active' },
    { data: [0, 0, 0, 0, 0, 0, 0], label: 'Idle' },
    { data: [0, 0, 0, 0, 0, 0, 0], label: 'Dead' },
    { data: [0, 0, 0, 0, 0, 0, 0], label: 'Inactive' }
  ];

  ngOnInit() {


    // this.StartDate = moment().startOf('day');
    // this.EndDate = moment().startOf('day');
    // this.MonthStartTime = moment().startOf('month').startOf('day');
    // this.MonthEndTime = moment().startOf('day');
    // this._HelperService.DateRangeStart = moment(this.MonthStartTime);
    // this._HelperService.DateRangeEnd = moment(this.MonthEndTime);
    // this._HelperService.DateRangeStartO = moment(this.MonthStartTime);
    // this._HelperService.DateRangeEndO = moment(this.MonthEndTime);
    Feather.replace();

    this.InitializeDates();

    this._ActivatedRoute.params.subscribe((params: Params) => {

      this._HelperService.AppConfig.ActiveReferenceKey = params['referencekey'];
      this._HelperService.AppConfig.ActiveReferenceId = params['referenceid'];


      this.pLoyalityData = {
        Task: 'getloyaltyvisithistory',
        StartDate: null,
        EndDate: null,
        AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
        AccountId: this._HelperService.AppConfig.ActiveOwnerId,
        CustomerReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
        CustomerReferenceIdKey: this._HelperService.AppConfig.ActiveReferenceKey,
        StoreReferenceId: 0,
        StoreReferenceKey: null,
        Type: null
      };

    });

    this.GetLoyalityReport(moment(this._HelperService.DateRangeStart), moment(this._HelperService.DateRangeEnd),
      this.LoyalityData);
    this.GetCustomerOverviewLite();
    this.GetSalesOverviewLite();

    this._DateSubscription = this._HelperService.RangeAltered.subscribe(value => {
      this.DateRangeAltered();
    });
  }

  InitializeDates(): void {
    //#region Monthly Dates 

    // this._Monthly.ActualStartDate = moment().startOf('month').startOf('day');
    // this._Monthly.ActualEndDate = moment().endOf('month').endOf('day');

    // this._Monthly.CompareStartDate = moment().subtract(1, 'months').startOf('month').startOf('day');
    // this._Monthly.CompareEndDate = moment().subtract(1, 'months').endOf('month').endOf('day');


    this._HelperService.ResetDateRange();

    this.MonthlybarChartLabels = this._HelperService.CalculateIntermediateDate(moment(this._HelperService.DateRangeStart), moment(this._HelperService.DateRangeEnd));

    //#endregion

  }

  DateRangeAltered(): void {
    this.MonthlybarChartLabels = this._HelperService.CalculateIntermediateDate(moment(this._HelperService.DateRangeStart), moment(this._HelperService.DateRangeEnd));
    this.GetLoyalityReport(moment(this._HelperService.DateRangeStart), moment(this._HelperService.DateRangeEnd),
      this.LoyalityData);
    this.GetCustomerOverviewLite();
    this.GetSalesOverviewLite();
  }

  // StartDate = null;
  // EndDate = null;
  // TodayStartTime = null;
  // TodayEndTime = null;
  // public MonthStartTime = null;
  // public MonthEndTime = null;
  GetCustomerOverviewLite() {
    this._HelperService.IsFormProcessing = true;
    var Data = {
      Task: 'getloyaltyoverview',
      StartDate: this._HelperService.DateInUTC(this._HelperService.DateRangeStart), // new Date(2017, 0, 1, 0, 0, 0, 0),
      EndDate: this._HelperService.DateInUTC(this._HelperService.DateRangeEnd), // moment().add(2, 'days'),
      AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
      AccountId: this._HelperService.AppConfig.ActiveOwnerId,
      CustomerReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
      CustomerReferenceIdKey: this._HelperService.AppConfig.ActiveReferenceKey

    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Analytics, Data);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._AccountCustomerOverview = _Response.Result as OAccountCustomerOverview;
          this._AccountCustomerOverview.AvgSpentVisit = this._AccountCustomerOverview.TransactionInvoiceAmount / this._AccountCustomerOverview.Transaction;


          this.components.forEach(a => {
            try {
              if (a.chart) a.chart.update();
            } catch (error) {
              console.log('chartjs error');
            }
          });

        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }

  public _AccountSalesOverview: any =
    {

      CardInvoiceAmount: 0,
      CardTransaction: 0,
      CashInvoiceAmount: 0,
      CashTransaction: 0,
      InvoiceAmount: 0,
      Transactions: 0


    }
  GetSalesOverviewLite() {
    this._HelperService.IsFormProcessing = true;
    var Data = {
      Task: 'getsalesoverview',
      StartDate: this._HelperService.DateInUTC(this._HelperService.DateRangeStart), // new Date(2017, 0, 1, 0, 0, 0, 0),
      EndDate: this._HelperService.DateInUTC(this._HelperService.DateRangeEnd), // moment().add(2, 'days'),
      AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
      AccountId: this._HelperService.AppConfig.ActiveOwnerId,
      CustomerReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
      CustomerReferenceIdKey: this._HelperService.AppConfig.ActiveReferenceKey

    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Analytics, Data);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._AccountSalesOverview = _Response.Result as any;


          this.components.forEach(a => {
            try {
              if (a.chart) a.chart.update();
            } catch (error) {
              console.log('chartjs error');
            }
          });

        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }

  public _AccountCustomerOverview: OAccountCustomerOverview =
    {

      RewardAmount: 0,
      RedeemAmount: 0,
      NewCustomers: 0,
      RepeatingCustomers: 0,
      TransactionInvoiceAmount: 0,
      Transaction: 0,
      RedeemTransaction: 0,
      RedeemInvoiceAmount: 0,
      AvgSpentVisit: 0,
      AvgVisitCustomer: 0,
      VisitsByRepeatingCustomers: 0,
      TotalCustomer: 0,
      RewardInvoiceAmount: 0

    }

  public lineChartColors: Color[] = [
    {
      borderColor: 'yellow',
      backgroundColor: '#0168fa',
    },
    {
      borderColor: '#20ab68',
      backgroundColor: 'rgba(1, 104, 250, 0.10)',
    },

    {
      backgroundColor: 'rgba(0, 204, 204, 1)',
    }

  ]

  public MonthlybarChartLabels = [];

  public MonthlylineChartData: ChartDataSets[] = [
    { data: [22, 44, 44, 77], label: 'Series A' },
    { data: [33, 55, 77, 44], borderDash: [10, 5], label: 'Series B' },
    { data: [33, 55, 77, 99], type: 'bar', label: 'Series C' },
    { data: [33, 55, 77, 88], type: 'bar', label: 'Series D' }

  ];

  public MonthlylineChartColors: Color[] = [
    {
      borderColor: '#0168fa',
      backgroundColor: 'rgba(1, 104, 250, 0.10)',
    },
    {
      borderColor: '#FFC20A',
      backgroundColor: 'rgba(255, 194, 10, 00)',
    },
    {
      backgroundColor: 'red',
    },
    {
      backgroundColor: 'rgba(255, 194, 10, 00)',
    }
  ]

  //#region Monthly Sales Report 

  showMonthlyChart = true;

  public _Monthly: OSalesTrend = {
    ActualStartDate: moment(),
    ActualEndDate: moment(),
    ActualData: null,

    CompareStartDate: moment(),
    CompareEndDate: moment(),
    CompareData: null,

    ActualSalesAmount: 0,
    CompareSalesAmount: 0,
    SalesAmountDifference: 0
  }

  MonthlyDateChanged(event: any, Type: any): void {
    var ev: any = cloneDeep(event);
    this._Monthly.CompareStartDate = moment(ev.start).startOf("month").startOf("day");
    this._Monthly.CompareEndDate = moment(ev.end).endOf("month").endOf("day");

    this.MonthlybarChartLabels = this._HelperService.CalculateIntermediateDate(moment(this._Monthly.ActualStartDate), moment(this._Monthly.ActualEndDate).endOf('month').endOf('day'));

    this._MonthlySalesReportGetActualData();
  }

  public _MonthlySalesReportReset(): void {

  }

  public _MonthlySalesReportGetActualData(): void {

    this.MonthlylineChartData[0].data = [];
    this.MonthlylineChartData[1].data = [];
    this.MonthlylineChartData[2].data = [];
    this.MonthlylineChartData[3].data = [];


    //this._Monthly.ActualData = this.GetSalesReport(this._Monthly.ActualStartDate, this._Monthly.ActualEndDate, this._Monthly.ActualData, this.Types.month, 'actual');

  }


  //#endregion

  Toogle_Dataset_Visibility(dataset_label: string): void {
    for (let index = 0; index < this.ChartData.length; index++) {
      const element = this.ChartData[index];

      if (element.label == dataset_label) {
        //#region Reload_UI 

        this.showLoalityChart = false;
        this._ChangeDetectorRef.detectChanges();

        element.hidden = !element.hidden;

        this.showLoalityChart = true;
        this._ChangeDetectorRef.detectChanges();

        //#endregion
        break;
      }
    }
  }

  //#region loyality History 

  //#region Chart Data 
  public ChartData: ChartDataSets[] = [
    { data: [], type: 'bar', borderDash: [10, 5], label: 'Total Sale', hidden: false },
    { data: [], label: 'Returning Sales', hidden: false },
    // { data: [], type: 'bar', label: 'Returning Visits', hidden: false }

  ];
  public ChartColors: Color[] = [
    {
      borderColor: '#0168fa',
      backgroundColor: 'rgba(1, 104, 250, 0.10)',
    },
    {
      borderColor: '#FFC20A',
      backgroundColor: '#FFC20A',
    },
    {
      backgroundColor: 'rgba(0, 204, 204, 1)',
    },
    {
      backgroundColor: 'rgba(0, 204, 204, .10)',
    },
  ];
  public ChartLabels = [];
  //#endregion

  public _OLoyalityHistory: OLoyalityHistory = {
    NewCustomerInvoiceAmount_Sum: 0,
    NewCustomer_Sum: 0,
    RepeatingCustomerSaleAmount_Sum: 0,
    RepeatingCustomer_Sum: 0,
    TotalCustomer_Sum: 0,
    TotalInvoiceAmount_Sum: 0,
    RepeatingCustomerInvoiceAmount: 0
  };

  public pLoyalityData;
  LoyalityData: OLoyalityHistoryData[];
  showLoalityChart: boolean = true;
  ChartArrayLength:any = 0;
  GetLoyalityReport(StartDateTime, EndDateTime, Data: OLoyalityHistoryData[]) {

    this._OLoyalityHistory = {
      NewCustomerInvoiceAmount_Sum: 0,
      NewCustomer_Sum: 0,
      RepeatingCustomerSaleAmount_Sum: 0,
      RepeatingCustomer_Sum: 0,
      TotalCustomer_Sum: 0,
      TotalInvoiceAmount_Sum: 0,
      RepeatingCustomerInvoiceAmount: 0
    };

    this._HelperService.IsFormProcessing = true;

    this.pLoyalityData.StartDate = this._HelperService.DateInUTC(StartDateTime);
    this.pLoyalityData.EndDate = this._HelperService.DateInUTC(EndDateTime);

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Analytics, this.pLoyalityData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          Data = _Response.Result as OLoyalityHistoryData[];
          this.ChartArrayLength =  _Response.Result;
          var ReturningSales = [];
          var NewSales = [];
          var ReturningVisits = [];
          var TotalInvoiceAmount = [];

          var ChartLabels: any[] = [];
          //#region temprary region 

          {
            this.CalculateDateRangeType(cloneDeep(StartDateTime), cloneDeep(EndDateTime));

            if (this.SelectedDateRangeType == this._HelperService.AppConfig.GraphTypes.week) {

              for (let index = 0; index < this.Intermediate.length; index++) {
                const element = this.Intermediate[index];
                ChartLabels.push(element);
                var RData: any = Data.find(x => moment(x.Date).format('DD MMM') == element);
                if (RData != undefined && RData != null) {

                  const element: OLoyalityHistoryData = Data[index];

                  //#region Calculate Totals 

                  this._OLoyalityHistory.NewCustomer_Sum = this._OLoyalityHistory.NewCustomer_Sum + RData.NewCustomer;
                  this._OLoyalityHistory.NewCustomerInvoiceAmount_Sum = this._OLoyalityHistory.NewCustomerInvoiceAmount_Sum + RData.NewCustomerInvoiceAmount;
                  this._OLoyalityHistory.RepeatingCustomer_Sum = this._OLoyalityHistory.RepeatingCustomer_Sum + RData.RepeatingCustomer;
                  this._OLoyalityHistory.RepeatingCustomerSaleAmount_Sum = this._OLoyalityHistory.RepeatingCustomerSaleAmount_Sum + RData.RepeatingCustomerSaleAmount;
                  this._OLoyalityHistory.TotalCustomer_Sum = this._OLoyalityHistory.TotalCustomer_Sum + RData.NewCustomer + RData.RepeatingCustomer;
                  this._OLoyalityHistory.TotalInvoiceAmount_Sum = this._OLoyalityHistory.TotalInvoiceAmount_Sum + RData.NewCustomerInvoiceAmount + RData.RepeatingCustomerSaleAmount;

                  //#endregion

                  TotalInvoiceAmount.push(RData.TotalInvoiceAmount);

                }
                else {
                  TotalInvoiceAmount.push(0.0);
                }
              }

            } else if (this.SelectedDateRangeType == this._HelperService.AppConfig.GraphTypes.month) {

              for (let index = 0; index < this.Intermediate.length; index++) {
                const element = this.Intermediate[index];
                ChartLabels.push(element);
                var RData: any = Data.find(x => moment(x.Date).format('DD MMM') == element);
                if (RData != undefined && RData != null) {
                  const element: OLoyalityHistoryData = Data[index];

                  //#region Calculate Totals 

                  this._OLoyalityHistory.NewCustomer_Sum = this._OLoyalityHistory.NewCustomer_Sum + RData.NewCustomer;
                  this._OLoyalityHistory.NewCustomerInvoiceAmount_Sum = this._OLoyalityHistory.NewCustomerInvoiceAmount_Sum + RData.NewCustomerInvoiceAmount;
                  this._OLoyalityHistory.RepeatingCustomer_Sum = this._OLoyalityHistory.RepeatingCustomer_Sum + RData.RepeatingCustomer;
                  this._OLoyalityHistory.RepeatingCustomerSaleAmount_Sum = this._OLoyalityHistory.RepeatingCustomerSaleAmount_Sum + RData.RepeatingCustomerSaleAmount;
                  this._OLoyalityHistory.TotalCustomer_Sum = this._OLoyalityHistory.TotalCustomer_Sum + RData.NewCustomer + RData.RepeatingCustomer;
                  this._OLoyalityHistory.TotalInvoiceAmount_Sum = this._OLoyalityHistory.TotalInvoiceAmount_Sum + RData.NewCustomerInvoiceAmount + RData.RepeatingCustomerSaleAmount;

                  //#endregion

                  TotalInvoiceAmount.push(RData.TotalInvoiceAmount);

                }
                else {
                  TotalInvoiceAmount.push(0.0);
                }
              }

            } else if (this.SelectedDateRangeType == this._HelperService.AppConfig.GraphTypes.year) {

              for (let index = 0; index < this.Intermediate.length; index++) {
                const element = this.Intermediate[index];
              }

            }


          }

          this.MonthlybarChartLabels = ChartLabels;

          this.showLoalityChart = false;
          this._ChangeDetectorRef.detectChanges();
          this.ChartData[0].data = TotalInvoiceAmount;

          this.showLoalityChart = true;
          this._ChangeDetectorRef.detectChanges();
          this._HelperService.IsFormProcessing = false;

          return Data;

          //#endregion



          for (let index = 0; index < Data.length; index++) {
            const element: OLoyalityHistoryData = Data[index];

            //#region Calculate Totals 

            this._OLoyalityHistory.NewCustomer_Sum = this._OLoyalityHistory.NewCustomer_Sum + element.NewCustomer;
            this._OLoyalityHistory.NewCustomerInvoiceAmount_Sum = this._OLoyalityHistory.NewCustomerInvoiceAmount_Sum + element.NewCustomerInvoiceAmount;
            this._OLoyalityHistory.RepeatingCustomer_Sum = this._OLoyalityHistory.RepeatingCustomer_Sum + element.RepeatingCustomer;
            this._OLoyalityHistory.RepeatingCustomerSaleAmount_Sum = this._OLoyalityHistory.RepeatingCustomerSaleAmount_Sum + element.RepeatingCustomerSaleAmount;
            this._OLoyalityHistory.TotalCustomer_Sum = this._OLoyalityHistory.TotalCustomer_Sum + element.NewCustomer + element.RepeatingCustomer;
            this._OLoyalityHistory.TotalInvoiceAmount_Sum = this._OLoyalityHistory.TotalInvoiceAmount_Sum + element.NewCustomerInvoiceAmount + element.RepeatingCustomerSaleAmount;

            //#endregion

            TotalInvoiceAmount.push(element.TotalInvoiceAmount);

          }


          this.showLoalityChart = false;
          this._ChangeDetectorRef.detectChanges();
          this.ChartData[0].data = TotalInvoiceAmount;

          this.showLoalityChart = true;
          this._ChangeDetectorRef.detectChanges();
          this._HelperService.IsFormProcessing = false;

          return Data;
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
          return Data;
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
        return Data;
      });
  }

  //#endregion

  TitleIsHour(title: string): boolean {
    console.log('title', title);
    return (title.includes('AM') || title.includes('PM'));
  }

  SelectedDateRangeType: string = this._HelperService.AppConfig.GraphTypes.week;
  Intermediate: any[];

  CalculateDateRangeType(start, end): void {

    this.Intermediate = this._HelperService.CalculateIntermediateDate(start, end);

    if (this.Intermediate.length <= 7) {
      this.SelectedDateRangeType = this._HelperService.AppConfig.GraphTypes.week;
    } else if (this.Intermediate.length > 7 && this.Intermediate.length < 32) {
      this.SelectedDateRangeType = this._HelperService.AppConfig.GraphTypes.month;
    } else if (this.Intermediate.length > 32) {
      this.SelectedDateRangeType = this._HelperService.AppConfig.GraphTypes.year;
    }

  }

}
export class OAccountCustomerOverview {
  public RewardAmount: any;
  public RewardInvoiceAmount: any;

  public RedeemAmount: any;
  public NewCustomers: any;
  public RepeatingCustomers: any;
  public TransactionInvoiceAmount: any;
  public Transaction: any;
  public RedeemTransaction: any;
  public RedeemInvoiceAmount: any;
  public AvgSpentVisit: any;
  public AvgVisitCustomer: any;
  public VisitsByRepeatingCustomers: any;
  public TotalCustomer: any;

}
