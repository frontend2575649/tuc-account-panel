import { ChangeDetectorRef, Component, OnInit } from "@angular/core";
import { FormBuilder, Validators, FormGroup } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import * as Feather from "feather-icons";
import swal from "sweetalert2";
declare const window: any;
declare var $: any;

import {
  DataHelperService,
  HelperService,
  OList,
  OSelect,
  FilterHelperService,
  OResponse,
} from "../../../../service/service";
import { Observable } from 'rxjs';
import { stringify } from "@angular/core/src/util";

@Component({
  selector: "tu-topup",
  templateUrl: "./topup.component.html",
})
export class TUTopupComponent implements OnInit {
  public ResetFilterControls: boolean = true;
  CurrentPlan: any = {
    Name: null,
    TypeName: null,
    SellingPrice: null,
    Policy: null,
    EndDate: null


  };
  FreePlan: any = {
    Name: null,
    TypeName: null,
    SellingPrice: null,
    Policy: null,


  };
  BasicPlan: any = {
    Name: null,
    TypeName: null,
    SellingPrice: null,
    Policy: null,
  };
  PremiumPlan: any = {
    Name: null,
    TypeName: null,
    SellingPrice: null,
    Policy: null,
  };


  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef,
    public _FilterHelperService: FilterHelperService
  ) {
    this._HelperService.showAddNewPosBtn = false;
    this._HelperService.showAddNewStoreBtn = false;
    this._HelperService.showAddNewCashierBtn = false;
    this._HelperService.showAddNewSubAccBtn = false;


  }

  ngOnInit() {
    Feather.replace();
    this.TopUpHistoryList_Setup();
    this.GetBalance();
    this.GetAccountSubscription();
    // this.GetSubscription();

  }
  public _InterSwitchAmount = 0;
  public PaymentId = 0;
  public nameValue2: any = 0;
  public RandomNumber: number = null;

  interswitch() {
    this.RandomNumber = this._HelperService.Get6DigitRandomNumber();
    this._InterSwitchAmount = null;
    this.PaymentId = this._HelperService.GetRandomNumber();
    this._HelperService.OpenModal('Form_AddUser_InterSwitch');
  }
  timeout = null;
// showModalPopUp : boolean = false;
// InvoiceData:any = {};
// InvoiceView(ReferenceData){
//   this.showModalPopUp = true;
//   this.InvoiceData = Object.assign(ReferenceData)
//   this._HelperService.OpenModal("Invoice_view")

// }

  startTop() {
    // this._InterSwitchAmount = 0;
    console.log("test", this.RandomNumber)
    var submitForm = document.getElementById("submit-form");
    submitForm.addEventListener("submit", (event) => {
      event.preventDefault();
      console.log("test2", this.RandomNumber)
      var merchantCode = this._HelperService.AppConfig.MerchantCode
      var itemId = this._HelperService.AppConfig.MerchantPayMentId;
      var transRef = String(this.RandomNumber);
      var customerName = this._HelperService.AppConfig.ActiveOwnerDisplayName;
      var nameValue: any = document.getElementById("param-amount");
      this.nameValue2 = Number(nameValue.value) * 100;
      var customerId = this._HelperService.AppConfig.ActiveOwnerId;
      var mode = this._HelperService.AppConfig.MerchantPayMode;
      var redirectUrl = location.href;
      var paymentRequest = {
        merchant_code: merchantCode,
        pay_item_id: itemId,
        txn_ref: transRef,
        amount: this.nameValue2.toString(),
        currency: 566,
        cust_id: customerId,
        cust_name: customerName,
        site_redirect_url: redirectUrl,
        onComplete: (resp) => {
          // console.log(resp.resp);
          if (resp.resp != undefined && resp.resp != null && resp.resp != '') {
            if (resp.resp == "00") {
              this.CreditInterSwitchAmount();
            }
            else {
              this._HelperService.NotifyError('Payment failed');
              // this._HelperService.CloseModal('Form_AddUser_InterSwitch');  
            location.reload();

            }
          }
          else {
            this._HelperService.NotifyError('Payment failed');
            // this._HelperService.CloseModal('Form_AddUser_InterSwitch');
            location.reload();

          }

        },
        mode: mode
      };
      if (customerName != "") {
        paymentRequest.cust_name = customerName;
      }
      if (customerId != "") {
        paymentRequest.cust_id = customerId;
      }
      //  console.log("paymentRequest",paymentRequest)
        window.webpayCheckout(paymentRequest);
    });

  }
  public CreditInterSwitchAmount() {
    this._HelperService.IsFormProcessing = true;
    var PostData = {
      Task: "topupaccount",
      AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
      AccountId: this._HelperService.AppConfig.ActiveOwnerId,
      Amount: null,
      merchant_code: this._HelperService.AppConfig.MerchantCode,
      pay_item_id: this._HelperService.AppConfig.MerchantPayMentId,
      txn_ref: null,
      cust_name: this._HelperService.AppConfig.ActiveOwnerDisplayName,
      cust_id: this._HelperService.AppConfig.ActiveOwnerId,
      PaymentReference: "quickteller",
      PaymentSource: "quickteller",
      TransactionId: this.PaymentId
    };
    let _OResponse: Observable<OResponse>;
    var nameValue: any = document.getElementById("param-amount");
    PostData.Amount = nameValue.value;
    PostData.txn_ref = this._HelperService.GetRandomNumber();
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Subscription, PostData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._AccountBalanceCreditAmount = 0;
          this._TransactionReference = null;
          this._HelperService.NotifySuccess("Account credited");
          this._HelperService.CloseModal('Form_AddUser_InterSwitch');
          this.GetBalance();


        } else {
          this._HelperService.NotifySuccess(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      }
    );
  }
  public SubscriptionId: number;
  public SubscriptionKey: string;
  selectPlan(plan: string) {
    if (plan == 'basic') {
      this.SubscriptionId = this.BasicPlan.ReferenceId
      this.SubscriptionKey = this.BasicPlan.ReferenceKey
      console.log('key', this.SubscriptionId)
    }
    if (plan == 'premium') {
      this.SubscriptionId = this.PremiumPlan.ReferenceId
      this.SubscriptionKey = this.PremiumPlan.ReferenceKey
    }
    if (plan == 'free') {
      this.SubscriptionId = this.FreePlan.ReferenceId
      this.SubscriptionKey = this.FreePlan.ReferenceKey
    }
    this._HelperService.CloseModal('Plans');
  }
  closePlanDetail() {
    this._HelperService.OpenModal('Plans');
  }
  closePremiumPlanDetail() {
    this._HelperService.OpenModal('Plans');
  }



  public _TransactionReference = null;
  public _AccountBalanceCreditAmount = 0;
  generateRandomNumber() {
    var TrRef = Math.floor(100000 + Math.random() * (999999 + 1 - 100000));
    return TrRef;
  }
  OpenPaymentOptions() {
    this._AccountBalanceCreditAmount = 0;
    var Ref = this.generateRandomNumber();
    this._TransactionReference = 'Tp' + '_' + this._HelperService.UserAccount.AccountId + '_' + Ref;
    this._HelperService.OpenModal('Form_AddUser_Content');
  }
  paymentDone(ref: any) {
    this.TransactionId = ref.trans
    if (ref != undefined && ref != null && ref != '') {
      if (ref.status == 'success') {
        this.CreditAmount();
      }
      else {
        this._HelperService.NotifyError('Payment failed');
      }
    }
    else {
      this._HelperService.NotifyError('Payment failed');
    }
    // this.title = 'Payment successfull';
    // console.log(this.title, ref);
  }
  public TransactionId
  public CreditAmount() {
    this._HelperService.IsFormProcessing = true;
    var PostData = {
      Task: "topupaccount",
      AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
      AccountId: this._HelperService.AppConfig.ActiveOwnerId,
      Amount: this._AccountBalanceCreditAmount,
      PaymentReference: this._TransactionReference,
      TransactionId: this.TransactionId
    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Subscription, PostData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._AccountBalanceCreditAmount = 0;
          this._TransactionReference = null;
          this._HelperService.NotifySuccess("Account credited");
          this.GetBalance();


        } else {
          this._HelperService.NotifySuccess(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      }
    );
  }
  paymentCancel() {
    console.log('payment failed');
  }

  //SUbscription Payment

  public _SubscriptionTransactionReference = null;
  SubscriptionDone(ref: any, SubscriptionId, SubscriptionKey) {
    this.TransactionId = ref.trans
    if (ref != undefined && ref != null && ref != '') {
      if (ref.status == 'success') {
        this.Subscription(SubscriptionId, SubscriptionKey);
      }
      else {
        this._HelperService.NotifyError('Payment failed');
      }
    }
    else {
      this._HelperService.NotifyError('Payment failed');
    }
    // this.title = 'Payment successfull';
    // console.log(this.title, ref);
  }



  public SubscriptionAccountInformation: any;
  public Subscription(SubscriptionId, SubscriptionKey) {
    this._HelperService.IsFormProcessing = true;
    var PostData = {
      Task: "updatesubscription",
      AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
      AccountId: this._HelperService.AppConfig.ActiveOwnerId,
      SubscriptionId: this.SubscriptionId,
      SubscriptionKey: this.SubscriptionKey,
      PaymentReference: this._SubscriptionTransactionReference,
    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.AccountSubscription, PostData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.NotifySuccess(_Response.Message);
          this.SubscriptionAccountInformation = _Response.Result

          this.GetAccountSubscription();

          var UserAccountInformation = this._HelperService.GetStorage(
            this._HelperService.AppConfig.Storage.Account
          );

          UserAccountInformation.Features = this.SubscriptionAccountInformation.Features;
          UserAccountInformation.Permissions = this.SubscriptionAccountInformation.Permissions;

          this._HelperService.SaveStorage(
            this._HelperService.AppConfig.Storage.Account, UserAccountInformation
          );

          // this._HelperService.UserAccount.SystemNamePermissions = [];
          // this._HelperService.UserAccount.Permissions = [];

          if (this.SubscriptionAccountInformation.Permissions) {
            this._HelperService.UserAccount.SystemNamePermissions = this.SubscriptionAccountInformation.Permissions;

          } else {
            this._HelperService.UserAccount.SystemNamePermissions = [];
          }
          var rewardpercentage: any = this.SubscriptionAccountInformation.Permissions.find(x => x['SystemName'] == 'rewardpercentage');
          this._HelperService.AppConfig.RewardPercentagePermission = rewardpercentage;

          var storepermission: any = this.SubscriptionAccountInformation.Permissions.find(x => x['SystemName'] == 'stores');
          this._HelperService.AppConfig.StorePermission = storepermission;

          var terminalpermission: any = this.SubscriptionAccountInformation.Permissions.find(x => x['SystemName'] == 'terminals');
          this._HelperService.AppConfig.TerminalPermission = terminalpermission;

          var cashierpermission: any = this.SubscriptionAccountInformation.Permissions.find(x => x['SystemName'] == 'cashiers');
          this._HelperService.AppConfig.CashiersPermission = cashierpermission;

          var subaccountpermission: any = this.SubscriptionAccountInformation.Permissions.find(x => x['SystemName'] == 'subaccounts');
          this._HelperService.AppConfig.SubAccountPermission = subaccountpermission;

          if (this.SubscriptionAccountInformation.Features) {
            this._HelperService.UserAccount.Permissions = this.SubscriptionAccountInformation.Features;
          } else {
            this._HelperService.UserAccount.Permissions = [];
          }
          var RData: any = this._HelperService.UserAccount.Permissions.find(x => x['SystemName'] == 'loyalty');
          var RData1: any = this._HelperService.UserAccount.Permissions.find(x => x['SystemName'] == 'salesanalytics');
          this._HelperService.AppConfig.salesanalytics = RData1;
          if (RData1) {
            this._HelperService.AppConfig.salesanalytics = true;
          }
          else {
            this._HelperService.AppConfig.salesanalytics = false;
          }
          this._HelperService.AppConfig.Loyality = RData;
          location.reload();

          // console.log("this.SubscriptionAccountInformation",this.SubscriptionAccountInformation);


        } else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      }
    );
  }

  FreePlanActivate() {
    this._HelperService.IsFormProcessing = true;
    var PostData = {
      Task: "updatesubscription",
      AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
      AccountId: this._HelperService.AppConfig.ActiveOwnerId,
      SubscriptionId: 1,
      SubscriptionKey: 'thankucashmerchantfree',
      PaymentReference: this._SubscriptionTransactionReference,
    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.AccountSubscription, PostData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.NotifySuccess(_Response.Message);
          this.SubscriptionAccountInformation = _Response.Result

          this._HelperService.CloseModal('FreePlans');
          this.GetAccountSubscription();

          var UserAccountInformation = this._HelperService.GetStorage(
            this._HelperService.AppConfig.Storage.Account
          );

          UserAccountInformation.Features = this.SubscriptionAccountInformation.Features;
          UserAccountInformation.Permissions = this.SubscriptionAccountInformation.Permissions;

          this._HelperService.SaveStorage(
            this._HelperService.AppConfig.Storage.Account, UserAccountInformation
          );

          if (this.SubscriptionAccountInformation.Permissions) {
            this._HelperService.UserAccount.SystemNamePermissions = this.SubscriptionAccountInformation.Permissions;

          } else {
            this._HelperService.UserAccount.SystemNamePermissions = [];
          }
          var rewardpercentage: any = this.SubscriptionAccountInformation.Permissions.find(x => x['SystemName'] == 'rewardpercentage');
          this._HelperService.AppConfig.RewardPercentagePermission = rewardpercentage;

          var storepermission: any = this.SubscriptionAccountInformation.Permissions.find(x => x['SystemName'] == 'stores');
          this._HelperService.AppConfig.StorePermission = storepermission;

          var terminalpermission: any = this.SubscriptionAccountInformation.Permissions.find(x => x['SystemName'] == 'terminals');
          this._HelperService.AppConfig.TerminalPermission = terminalpermission;

          var cashierpermission: any = this.SubscriptionAccountInformation.Permissions.find(x => x['SystemName'] == 'cashiers');
          this._HelperService.AppConfig.CashiersPermission = cashierpermission;

          var subaccountpermission: any = this.SubscriptionAccountInformation.Permissions.find(x => x['SystemName'] == 'subaccounts');
          this._HelperService.AppConfig.SubAccountPermission = subaccountpermission;

          if (this.SubscriptionAccountInformation.Features) {
            this._HelperService.UserAccount.Permissions = this.SubscriptionAccountInformation.Features;
          } else {
            this._HelperService.UserAccount.Permissions = [];
          }
          var RData: any = this._HelperService.UserAccount.Permissions.find(x => x['SystemName'] == 'loyalty');
          var RData1: any = this._HelperService.UserAccount.Permissions.find(x => x['SystemName'] == 'salesanalytics');
          this._HelperService.AppConfig.salesanalytics = RData1;
          if (RData1) {
            this._HelperService.AppConfig.salesanalytics = true;
          }
          else {
            this._HelperService.AppConfig.salesanalytics = false;
          }
          this._HelperService.AppConfig.Loyality = RData;
          location.reload();
          // console.log("this.SubscriptionAccountInformation",this.SubscriptionAccountInformation);


        } else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      }
    );
  }
  //end Subscription

  SubscriptionpaymentCancel() {
    console.log('payment failed');

  }
  public _AccountBalance =
    {
      Credit: 0,
      Debit: 0,
      Balance: 0
    }
  public GetBalance() {
    this._HelperService.IsFormProcessing = true;
    var pData = {
      Task: 'getaccountbalance',
      AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
      AccountId: this._HelperService.AppConfig.ActiveOwnerId,
      Source: this._HelperService.AppConfig.TransactionSource.Merchant
    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Subscription, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
  
          this._AccountBalance = _Response.Result;
          // this._AccountBalance.Balance = _Response.Result.Balance / 100;
          // this._AccountBalance.Credit = _Response.Result.Credit / 100;
          // this._AccountBalance.Debit = _Response.Result.Debit / 100;
        } else {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      }
    );
  }

  OpenTopUpHistory() {
    this._HelperService.OpenModal('TopupHistory')
    this.TopUpHistoryList_Setup();
  }
  public TopUpHistoryList_Config: OList;
  TopUpHistoryList_Setup() {
    this.TopUpHistoryList_Config = {
      Id: null,
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetTopupHistory,
      Location: this._HelperService.AppConfig.NetworkLocation.V3.Subscription,
      Title: "Available Top History",
      StatusType: "default",
      AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
      AccountId: this._HelperService.AppConfig.ActiveOwnerId,
      Sort:
      {
        SortDefaultName: null,
        SortDefaultColumn: 'TransactionDate',
        SortName: null,
        SortColumn: null,
        SortOrder: 'desc',
        SortOptions: [],
      },
      TableFields: [
        {
          DisplayName: '#',
          SystemName: 'ReferenceId',
          DataType: this._HelperService.AppConfig.DataType.Number,
          Show: false,
          Search: false,
          Sort: false,
        },
        {
          DisplayName: "Reference Number",
          SystemName: "PaymentReference",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: true,
          Sort: false,
          ResourceId: null,
        },

        {
          DisplayName: 'Amount',
          SystemName: 'Amount',
          DataType: this._HelperService.AppConfig.DataType.Decimal,
          Show: true,
          Search: false,
          Sort: true,
        },

        {
          DisplayName: 'Date',
          SystemName: 'TransactionDate',
          DataType: this._HelperService.AppConfig.DataType.Date,
          Show: true,
          Search: true,
          Sort: true,
          IsDateSearchField: true,
        },

      ],
    };
    this.TopUpHistoryList_Config = this._DataHelperService.List_Initialize(
      this.TopUpHistoryList_Config
    );

    this._HelperService.Active_FilterInit(
      this._HelperService.AppConfig.FilterTypeOption.Merchant,
      this.TopUpHistoryList_Config
    );

    this.TopUpHistoryList_GetData();
  }
  TopUpHistoryList_ToggleOption(event: any, Type: any) {

    if (event != null) {
      for (let index = 0; index < this.TopUpHistoryList_Config.Sort.SortOptions.length; index++) {
        const element = this.TopUpHistoryList_Config.Sort.SortOptions[index];
        if (event.SystemName == element.SystemName) {
          element.SystemActive = true;
        }
        else {
          element.SystemActive = false;
        }
      }
    }

    this._HelperService.Update_CurrentFilterSnap(
      event,
      Type,
      this.TopUpHistoryList_Config


    );

    this.TopUpHistoryList_Config = this._DataHelperService.List_Operations(
      this.TopUpHistoryList_Config,
      event,
      Type
    );

    if (
      (this.TopUpHistoryList_Config.RefreshData == true)
      && this._HelperService.DataReloadEligibility(Type)
    ) {
      this.TopUpHistoryList_GetData();
    }

  }
  TopUpHistoryList_GetData() {
    var TConfig = this._DataHelperService.List_GetData(
      this.TopUpHistoryList_Config
    );
    this.TopUpHistoryList_Config = TConfig;
  }
  TopUpHistoryList_RowSelected(ReferenceData) {
    this._HelperService.SaveStorage(
      this._HelperService.AppConfig.Storage.ActiveStore,
      {
        ReferenceKey: ReferenceData.ReferenceKey,
        ReferenceId: ReferenceData.ReferenceId,
        DisplayName: ReferenceData.DisplayName,
        AccountTypeCode: this._HelperService.AppConfig.AccountType.Store,
      }
    );

    this._HelperService.AppConfig.ActiveReferenceKey =
      ReferenceData.ReferenceKey;
    this._HelperService.AppConfig.ActiveReferenceId = ReferenceData.ReferenceId;




  }
  ApplyFilters(event: any, Type: any, ButtonType: any): void {
    this._HelperService.MakeFilterSnapPermanent();
    this.TopUpHistoryList_GetData();

    if (ButtonType == 'Sort') {
      $("#TopUpHistoryList_sdropdown").dropdown('toggle');
    } else if (ButtonType == 'Other') {
      $("#TopUpHistoryList_fdropdown").dropdown('toggle');
    }

    this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  }

  ResetFilters(event: any, Type: any): void {
    this._HelperService.ResetFilterSnap();
    this._FilterHelperService.SetMerchantConfig(this.TopUpHistoryList_Config);

    this.TopUpHistoryList_GetData();

    this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  }

  //#endregion

  ResetFilterUI(): void {
    this.ResetFilterControls = false;
    this._ChangeDetectorRef.detectChanges();
    this.ResetFilterControls = true;
    this._ChangeDetectorRef.detectChanges();
  }

  //#endregion
  ViewPlans() {
    this.GetSubscription();
    var Ref = this.generateRandomNumber();
    this._SubscriptionTransactionReference = 'Sc' + '_' + this._HelperService.UserAccount.AccountId + '_' + Ref;

  }

  public _AccountSubscription = {

    Data: []
  }

  public GetSubscription() {
    this._HelperService.IsFormProcessing = true;
    var pData = {
      Task: 'getsubscriptions',
      AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
      AccountId: this._HelperService.AppConfig.ActiveOwnerId,
      AccountTypeCode: this._HelperService.AppConfig.ActiveOwnerAccountCode,
      Source: this._HelperService.AppConfig.TransactionSource.Merchant
    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.AccountSubscription, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._AccountSubscription = _Response.Result;
          this.GetAccountSubscription();

          for (let index = 0; index < this._AccountSubscription.Data.length; index++) {
            const element: any = this._AccountSubscription.Data[index];
            if (element.ReferenceId == 1) {
              this.FreePlan = element
            }
            else if (element.ReferenceId == 2) {
              this.BasicPlan = element
              // document.getElementById("BasicPlanPolicy").innerHTML = element.Policy

            }
            else {
              this.PremiumPlan = element

            }
            for (let index = 0; index < element.Features.length; index++) {
              const features = element.Features[index];

            }


          }
          // this._AccountBalance.Balance = _Response.Result.Balance / 100;
          // this._AccountBalance.Credit = _Response.Result.Credit / 100;
          // this._AccountBalance.Debit = _Response.Result.Debit / 100;
        } else {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      }
    );
  }
  public _GetAccountSubscription = {
    EndDate: null,
    Data: []
  }

  public GetAccountSubscription() {
    this._HelperService.IsFormProcessing = true;
    var pData = {
      Task: 'getaccountsubscriptions',
      AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
      AccountId: this._HelperService.AppConfig.ActiveOwnerId,
      Offset: 0,
      Limit: 3,
      // Source: this._HelperService.AppConfig.TransactionSource.Merchant
    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.AccountSubscription, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._GetAccountSubscription = _Response.Result;
          for (let index = 0; index < this._GetAccountSubscription.Data.length; index++) {
            const element: any = this._GetAccountSubscription.Data[index];

            if (this._GetAccountSubscription.Data[0]) {
              this.CurrentPlan = this._GetAccountSubscription.Data[0]


            }

            // for (let index = 0; index < this._GetAccountSubscription.Data.length; index++) {
            //   const element = this._GetAccountSubscription.Data[index];


            // }


            for (let index = 0; index < element.Features.length; index++) {
              const features = element.Features[index];

            }


          }
          // this._AccountBalance.Balance = _Response.Result.Balance / 100;
          // this._AccountBalance.Credit = _Response.Result.Credit / 100;
          // this._AccountBalance.Debit = _Response.Result.Debit / 100;
        } else {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      }
    );
  }


}
