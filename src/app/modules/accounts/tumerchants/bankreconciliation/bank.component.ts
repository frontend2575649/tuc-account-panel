import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { OSelect, OList, DataHelperService, HelperService, OResponse, OStorageContent, OCoreParameter, OCoreCommon } from '../../../../service/service';
import swal from 'sweetalert2';
declare var moment: any;
import { DaterangePickerComponent } from 'ng2-daterangepicker';

@Component({
    selector: 'tu-bank',
    templateUrl: './bank.component.html',
})
export class TUBankComponent implements OnInit {
    @ViewChild(DaterangePickerComponent)
    private picker: DaterangePickerComponent;

    constructor(
        public _Router: Router,
        public _ActivatedRoute: ActivatedRoute,
        public _FormBuilder: FormBuilder,
        public _HelperService: HelperService,
        public _DataHelperService: DataHelperService,
    ) {

    }
    public StartDate = null;
    ngOnInit() {
        this.StartDate = moment().startOf('day').subtract(1, 'days');
        this.StartDate = this.StartDate.startOf('day').add(1, 'days');
        this.GetOverview();
    }

    GetOverviewNext() {
        this.StartDate = this.StartDate.startOf('day').add(1, 'days');
        this.picker.datePicker.setStartDate(this.StartDate.startOf('day'));
        this.picker.datePicker.setEndDate(this.StartDate.startOf('day'));
        this.StartDate = this.StartDate.startOf('day').add(1, 'days');
        this.GetOverview();
    }
    GetOverviewPrevious() {
        this.StartDate = this.StartDate.startOf('day').subtract(1, 'days');
        this.picker.datePicker.setStartDate(this.StartDate.startOf('day').subtract(1, 'days'));
        this.picker.datePicker.setEndDate(this.StartDate.startOf('day').subtract(1, 'days'));
        this.StartDate = this.StartDate.startOf('day').add(1, 'days');
        this.GetOverview();
    }
    StoreSalesListDate_ToggleOption(event: any) {
        this.StartDate = event.start;
        this.picker.datePicker.setStartDate(event.start);
        this.picker.datePicker.setEndDate(event.end);
        this.StartDate = event.start;
        this.StartDate = this.StartDate.startOf('day').add(1, 'days');
        this.GetOverview();
    }

    public BankCollectionData :any  ={
      Terminals: 0,
      InvoiceAmount: 0,
      NibsAmount: 0,
      ExpectedAmount: 0,
      ReceivedAmount: 0,
      AmountDifference: 0,
      Stores :[]

    } ;
    GetOverview() {
        var Data = {
            Task: 'getstorebankcollection',
            StartDate: this.StartDate,
            ReferenceKey: this._HelperService.AppConfig.ActiveOwnerKey,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Analytics, Data);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsFormProcessing = false;
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this.BankCollectionData = _Response.Result;
                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);
            });
    }

    AmountReceived: number = 0;
    public SelectedItem: any = null;
    Acquirer_Click(Item) {
        this.SelectedItem = Item;
        this._HelperService.OpenModal('ModalUpdateReceivedAmount');
    }

    UpdateAmount() {
        if (isNaN(this.AmountReceived) == true) {
            this._HelperService.NotifyError('Enter valid amount');
        }
        else if (this.AmountReceived < 0) {
            this._HelperService.NotifyError('Amount must be greater than 0');
        }
        else {
            this._HelperService.IsFormProcessing = true;
            var Data = {
                Task: 'updatereceivedamount',
                ReceivedAmount: this.AmountReceived,
                ReferenceKey: this.SelectedItem.ReferenceKey,
            };
            let _OResponse: Observable<OResponse>;
            _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Analytics, Data);
            _OResponse.subscribe(
                _Response => {
                    this._HelperService.IsFormProcessing = false;
                    if (_Response.Status == this._HelperService.StatusSuccess) {
                        this._HelperService.NotifySuccess(_Response.Message);
                        this.GetOverview();
                        this._HelperService.CloseModal('ModalUpdateReceivedAmount');
                    }
                    else {
                        this._HelperService.NotifyError(_Response.Message);
                    }
                },
                _Error => {
                    this._HelperService.IsFormProcessing = false;
                    this._HelperService.HandleException(_Error);
                });
        }
    }
}