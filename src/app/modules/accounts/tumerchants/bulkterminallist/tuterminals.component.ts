import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { GooglePlaceDirective } from 'ngx-google-places-autocomplete';
import { Address } from 'ngx-google-places-autocomplete/objects/address';

import { OSelect, OList, DataHelperService, HelperService, OResponse, OStorageContent, FilterHelperService } from '../../../../service/service';
import swal from 'sweetalert2';
import * as XLSX from 'xlsx';
declare var $: any;

type AOA = any[][];
import * as Feather from "feather-icons";

@Component({
  selector: "tu-tuterminals",
  templateUrl: "./tuterminals.component.html",
})
export class TUTerminalsComponent implements OnInit {
  public ResetFilterControls: boolean = true;

  title = 'XlsRead'
  file: File;
  arrayBuffer: any;
  filelist: any;
  public PostItem = {

    OperationType: 'new',
    Task: this._HelperService.AppConfig.Api.Core.SaveUserAccount,
    AccountId: this._HelperService.UserAccount.AccountId,
    AccountKey: this._HelperService.UserAccount.AccountKey,
    Data: [],
    StatusCode: this._HelperService.AppConfig.Status.Inactive,


  };

  public SelectedDay = null;
  public SelectedMonth = null;
  public SelectedYear = null;
  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef,
    public _FilterHelperService: FilterHelperService
  ) {


  }
  ngOnInit() {
    Feather.replace();
    this.TerminalsList_Setup();
    this.TerminalsList_Filter_Owners_Load();
    this.TUTr_Filter_Stores_Load();
  }
  IsUploading = false;
  UploadCount = 0;
  TotalCount = 0;
  CustomersList: CustomerImport[] = [];
  data: AOA = [];
  wopts: XLSX.WritingOptions = { bookType: 'xlsx', type: 'array' };
  fileName: string = 'SheetJS.xlsx';
  onFileChange(evt: any) {
    this.UploadCount = 0;
    this.TotalCount = 0;
    this.CustomersList = [];
    this.IsUploading = false;
    const target: DataTransfer = <DataTransfer>(evt.target);
    if (target.files.length !== 1) throw new Error('Cannot use multiple files');
    const reader: FileReader = new FileReader();
    reader.onload = (e: any) => {
      const bstr: string = e.target.result;
      const wb: XLSX.WorkBook = XLSX.read(bstr, { type: 'binary' });
      const wsname: string = wb.SheetNames[0];
      const ws: XLSX.WorkSheet = wb.Sheets[wsname];
      this.data = <AOA>(XLSX.utils.sheet_to_json(ws, { header: 1 }));
      for (let index = 1; index < this.data.length; index++) {
        const CustomerInfo = this.data[index];
        var DisplayName = CustomerInfo[0];
        var CompanyName = CustomerInfo[1];
        var BusinessContactNumber = CustomerInfo[2];
        var BusinessEmailAddress = CustomerInfo[2];
        var BusinessType = CustomerInfo[2];
        var BusinessAddress = CustomerInfo[2];
        var ContactFirstName = CustomerInfo[2];
        var ContactLastName = CustomerInfo[2];
        var ContactMobileNumber = CustomerInfo[2];
        var RewardPercentage = CustomerInfo[2];
        if (BusinessType == "online") {
          BusinessType = this._HelperService.AppConfig.AccountOperationType.Online
        }
        else if (BusinessType == "offline") {
          BusinessType = this._HelperService.AppConfig.AccountOperationType.Offline
        }
        else {
          BusinessType = this._HelperService.AppConfig.AccountOperationType.OnlineAndOffline
        }
        if (DisplayName != undefined && DisplayName != undefined) {
          var AppUserInfo: CustomerImport =
          {
            BusinessAddress: BusinessAddress,
            BusinessContactNumber: BusinessContactNumber,
            BusinessEmailAddress: BusinessEmailAddress,
            CompanyName: CompanyName,
            BusinessType: BusinessType,
            ContactFirstName: ContactFirstName,
            ContactLastName: ContactLastName,
            ContactMobileNumber: ContactMobileNumber,
            DisplayName: DisplayName,
            RewardPercentage: RewardPercentage,
            Status: 'pending',
            Message: '',
          };
          this.CustomersList.push(AppUserInfo);
          this.TotalCount = this.TotalCount + 1;
        }
      }
    };
    reader.readAsBinaryString(target.files[0]);
  }




  export(): void {
    var Cus = this.CustomersList as any;
    const ws: XLSX.WorkSheet = XLSX.utils.aoa_to_sheet(Cus);
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'ImportReport');
    XLSX.writeFile(wb, this.fileName);
  }
  onUpload() {
    this.StartMerchantUpload();

  }

  // StartCustomerUpload() {
  //     if (this.CustomersList.length > 0) {
  //         this.IsUploading = true;
  //         for (let index = 0; index < this.CustomersList.length; index++) {
  //             const element = this.CustomersList[index];
  //             this.CustomersList[index].Status = "processing";
  //             this.CustomersList[index].Message = "sending data";
  //             var PostItem = {
  //                 OperationType: 'new',
  //                 Task: this._HelperService.AppConfig.Api.Core.SaveUserAccount,
  //                 AccountTypeCode: this._HelperService.AppConfig.AccountType.Merchant,
  //                 AccountOperationTypeCode: this._HelperService.AppConfig.AccountOperationType.Offline,
  //                 RegistrationSourceCode: this._HelperService.AppConfig.RegistrationSource.System,
  //                 OwnerKey: this._HelperService.AppConfig.ActiveOwnerKey,
  //                 DisplayName: element.DisplayName,
  //                 Name: element.CompanyName,
  //                 FirstName: element.ContactFirstName,
  //                 LastName: element.ContactLastName,
  //                 MobileNumber: element.ContactMobileNumber,
  //                 ContactNumber: element.BusinessContactNumber,
  //                 EmailAddress: element.BusinessEmailAddress,
  //                 Address: element.BusinessAddress,
  //                 Latitude: 0,
  //                 Longitude: 0,
  //                 RegionKey: '',
  //                 RegionAreaKey: '',
  //                 CityKey: '',
  //                 CityAreaKey: '',
  //                 PostalCodeKey: '',
  //                 CountValue: 0,
  //                 AverageValue: 0,
  //                 StatusCode: this._HelperService.AppConfig.Status.Inactive,
  //                 Configuration: [{
  //                     SystemName: 'rewardpercentage',
  //                     Value: element.RewardPercentage,
  //                 }],
  //             };
  //             this._HelperService.IsFormProcessing = true;
  //             let _OResponse: Observable<OResponse>;
  //             _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.System, PostItem);
  //             _OResponse.subscribe(
  //                 _Response => {
  //                     this._HelperService.IsFormProcessing = false;
  //                     this.UploadCount = this.UploadCount + 1;
  //                     if (_Response.Status == this._HelperService.StatusSuccess) {
  //                         this.CustomersList[index].Status = "success";
  //                         this.CustomersList[index].Message = "merchant added";
  //                     }
  //                     else {
  //                         this.CustomersList[index].Status = "failed";
  //                         this.CustomersList[index].Message = "merchant already present";
  //                     }
  //                     if (index == (this.CustomersList.length - 1)) {
  //                         this.IsUploading = false;
  //                         this._HelperService.NotifySuccess('Merchants import successfull');
  //                     }


  //                 },
  //                 _Error => {
  //                     this.UploadCount = this.UploadCount + 1;
  //                     if (index == (this.CustomersList.length - 1)) {
  //                         this.IsUploading = false;
  //                         this._HelperService.NotifySuccess('Merchants import successfull');
  //                     }
  //                     this._HelperService.IsFormProcessing = false;
  //                     this._HelperService.HandleException(_Error);
  //                 });
  //         }
  //     }
  //     else {
  //         this._HelperService.NotifyError('Select file to upload merchants');
  //     }

  // }
  StartMerchantUpload() {
    if (this.filelist.length > 0 && this.filelist.length <= 1000) {

      this.IsUploading = true;

      // this.filelist[index].Status = "processing";
      // this.filelist[index].Message = "sending data";
      var PostItem = {
        OperationType: 'new',
        Task: this._HelperService.AppConfig.Api.Core.onboardmerchants,
        AccountId: this._HelperService.UserAccount.AccountId,
        AccountKey: this._HelperService.UserAccount.AccountKey,
        Data: this.filelist,
      };
      this._HelperService.IsFormProcessing = true;
      let _OResponse: Observable<OResponse>;
      _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.OnBoard, PostItem);
      _OResponse.subscribe(
        _Response => {
          this._HelperService.IsFormProcessing = false;
          this.UploadCount = this.UploadCount + 1;
          if (_Response.Status == this._HelperService.StatusSuccess) {
            this.filelist.Status = "success";
            this.filelist.Message = "merchant added";
            this._HelperService.NotifySuccess('Merchants import successfull');
            this._HelperService.CloseModal('SampleSheet');
            this.TerminalsList_Setup();
          }
          else {
            this.filelist.Status = "failed";
            this.filelist.Message = "merchant already present";
          }
          if (this.filelist == (this.filelist.length - 1)) {
            this.IsUploading = false;
            this._HelperService.NotifySuccess('Merchants import successfull');
          }


        },
        _Error => {
          this.UploadCount = this.UploadCount + 1;
          if (this.filelist == (this.filelist.length - 1)) {
            this.IsUploading = false;
            this._HelperService.NotifySuccess('Merchants import successfull');
          }
          this._HelperService.IsFormProcessing = false;
          this._HelperService.HandleException(_Error);
        });
    }
    else {
      this._HelperService.NotifyError('Select file to upload merchants');
    }


  }

  addfile(event) {
    this.file = event.target.files[0];
    let fileReader = new FileReader();
    fileReader.readAsArrayBuffer(this.file);
    fileReader.onload = (e) => {
      this.arrayBuffer = fileReader.result;
      var data = new Uint8Array(this.arrayBuffer);
      var arr = new Array();
      for (var i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
      var bstr = arr.join("");
      var workbook = XLSX.read(bstr, { type: "binary" });
      var first_sheet_name = workbook.SheetNames[0];
      var worksheet = workbook.Sheets[first_sheet_name];
      console.log(XLSX.utils.sheet_to_json(worksheet, { raw: true }));
      this.filelist = XLSX.utils.sheet_to_json(worksheet, { raw: true });
      // this.filelist = [];    
      // this.PostItem.Data =  ;
      console.log(this.filelist)

    }
  }


  public TerminalsList_Config: OList;
  TerminalsList_Setup() {
    this.TerminalsList_Config = {
      Id: null,
      Sort: null,
      Task: this._HelperService.AppConfig.Api.ThankUCash.getuploadedterminals,
      Location: this._HelperService.AppConfig.NetworkLocation.V3.OnBoard,
      Title: "Upload History",
      StatusType: "Bulk",
      // Type: this._HelperService.AppConfig.ListType.SubOwner,
      ReferenceKey: this._HelperService.AppConfig.ActiveOwnerKey,
      ReferenceId: this._HelperService.AppConfig.ActiveOwnerId,
      DefaultSortExpression: "CreateDate desc",
      // SearchBaseCondition: this._HelperService.GetSearchConditionStrict('', 'StatusId', this._HelperService.AppConfig.DataType.Number, 2, '=='),
      TableFields: [
        {
          DisplayName: "Merchant Name",
          SystemName: "DisplayName",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey",

        },
        {
          DisplayName: "Contact No",
          SystemName: "ContactNumber",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "text-center",
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
          DefaultValue: "ThankUCash",
          NavigateField: "ReferenceKey",

        },
        {
          DisplayName: "Email Address",
          SystemName: "EmailAddress",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: true,
          Sort: false,
          ResourceId: null,
          NavigateField: "ReferenceKey",

        },
        {
          DisplayName: "Category Name",
          SystemName: "CategoryName",
          DataType: this._HelperService.AppConfig.DataType.Number,
          Class: "text-right",
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey",

        },
        {
          DisplayName: "Contact Person Name",
          SystemName: "ContactPersonName",
          DataType: this._HelperService.AppConfig.DataType.Number,
          Class: "text-right",
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey",

        },

        {
          DisplayName: this._HelperService.AppConfig.CommonResource.CreateDate,
          SystemName: "CreateDate",
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: "td-date text-right",
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey",

        },
      ]
    };
    this.TerminalsList_Config = this._DataHelperService.List_Initialize(
      this.TerminalsList_Config
    );

    this._HelperService.Active_FilterInit(
      this._HelperService.AppConfig.FilterTypeOption.Merchant,
      this.TerminalsList_Config
    );

    this.TerminalsList_GetData();
  }
  TerminalsList_ToggleOption(event: any, Type: any) {

    for (let index = 0; index < this.TerminalsList_Config.Sort.SortOptions.length; index++) {
      const element = this.TerminalsList_Config.Sort.SortOptions[index];
      if (event.SystemName == element.SystemName) {

        element.SystemActive = true;

      }
      else {
        element.SystemActive = false;

      }

    }


    this._HelperService.Update_CurrentFilterSnap(
      event,
      Type,
      this.TerminalsList_Config
    );

    this.TerminalsList_Config = this._DataHelperService.List_Operations(
      this.TerminalsList_Config,
      event,
      Type
    );

    if (
      (this.TerminalsList_Config.RefreshData == true)
      && this._HelperService.DataReloadEligibility(Type)
    ) {
      this.TerminalsList_GetData();
    }

  }
  TerminalsList_GetData() {
    var TConfig = this._DataHelperService.List_GetDataTur(
      this.TerminalsList_Config
    );
    this.TerminalsList_Config = TConfig;
  }

  CopyStoresToAll(): void {
    if (this.TerminalsList_Config.Data[0].StoreId != undefined
      && this.TerminalsList_Config.Data[0].StoreId != null) {

      var FirstTerminalStoreId = this.TerminalsList_Config.Data[0].StoreId;
      var FirstTerminalStoreKey = this.TerminalsList_Config.Data[0].StoreKey;

      for (let index = 0; index < this.TerminalsList_Config.Data.length; index++) {
        const element = this.TerminalsList_Config.Data[index];
        element.StoreId = FirstTerminalStoreId;
        element.StoreKey = FirstTerminalStoreKey;
      }
    }

  }

  SelectStore(Store: any, TerminalIndex: number): void {
    this.TerminalsList_Config.Data[TerminalIndex].StoreId = Store.ReferenceId;
    this.TerminalsList_Config.Data[TerminalIndex].StoreKey = Store.ReferenceKey;
  }


  public TerminalsList_Filter_Owners_Option: Select2Options;
  public TerminalsList_Filter_Owners_Selected = null;
  TerminalsList_Filter_Owners_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetMerchants,
      Location: this._HelperService.AppConfig.NetworkLocation.V3.Account,
      ReferenceKey: this._HelperService.UserAccount.AccountKey,
      ReferenceId: this._HelperService.UserAccount.AccountId,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },
        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true,
        },
      ],
    };
    // _Select.SearchCondition = this._HelperService.GetSearchConditionStrictFromArray('', 'AccountTypeCode', this._HelperService.AppConfig.DataType.Text,
    //   [
    //     this._HelperService.AppConfig.AccountType.Merchant,
    //     this._HelperService.AppConfig.AccountType.Acquirer,
    //     this._HelperService.AppConfig.AccountType.PGAccount,
    //     this._HelperService.AppConfig.AccountType.PosAccount
    //   ]
    //   , '=');
    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.TerminalsList_Filter_Owners_Option = {
      placeholder: "Sort by Referrer",
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }
  TerminalsList_Filter_Owners_Change(event: any) {
    this._HelperService.Update_CurrentFilterSnap(
      event,
      this._HelperService.AppConfig.ListToggleOption.Other,
      this.TerminalsList_Config,
      this._HelperService.AppConfig.OtherFilters.Merchant.Owner
    );

    this.OwnerEventProcessing(event);

  }

  OwnerEventProcessing(event: any): void {
    if (event.value == this.TerminalsList_Filter_Owners_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict(
        "",
        "ReferenceKey",
        this._HelperService.AppConfig.DataType.Text,
        this.TerminalsList_Filter_Owners_Selected,
        "="
      );
      this.TerminalsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(
        SearchCase,
        this.TerminalsList_Config.SearchBaseConditions
      );
      this.TerminalsList_Filter_Owners_Selected = null;
    } else if (event.value != this.TerminalsList_Filter_Owners_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict(
        "",
        "ReferenceKey",
        this._HelperService.AppConfig.DataType.Text,
        this.TerminalsList_Filter_Owners_Selected,
        "="
      );
      this.TerminalsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(
        SearchCase,
        this.TerminalsList_Config.SearchBaseConditions
      );
      this.TerminalsList_Filter_Owners_Selected = event.data[0].ReferenceKey;
      this.TerminalsList_Config.SearchBaseConditions.push(
        this._HelperService.GetSearchConditionStrict(
          "",
          "ReferenceKey",
          this._HelperService.AppConfig.DataType.Text,
          this.TerminalsList_Filter_Owners_Selected,
          "="
        )
      );
    }

    this.TerminalsList_ToggleOption(
      null,
      this._HelperService.AppConfig.ListToggleOption.ResetOffset
    );
  }

  //#endregion

  SetOtherFilters(): void {
    this.TerminalsList_Config.SearchBaseConditions = [];
    this.TerminalsList_Config.SearchBaseCondition = null;

    var CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.Merchant.Owner));
    if (CurrentIndex != -1) {
      this.TerminalsList_Filter_Owners_Selected = null;
      this.OwnerEventProcessing(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
    }
  }

  //#region filterOperations

  Active_FilterValueChanged(event: any) {
    this._HelperService.Active_FilterValueChanged(event);
    this._FilterHelperService.SetMerchantConfig(this.TerminalsList_Config);

    //#region setOtherFilters
    this.SetOtherFilters();
    //#endregion

    this.TerminalsList_GetData();
  }

  RemoveFilterComponent(Type: string, index?: number): void {
    this._FilterHelperService._RemoveFilter_Merchant(Type, index);
    this._FilterHelperService.SetMerchantConfig(this.TerminalsList_Config);

    this.SetOtherFilters();

    this.TerminalsList_GetData();
  }

  Save_NewFilter() {
    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.SaveFilterTitle,
      text: this._HelperService.AppConfig.CommonResource.SaveFilterHelp,
      input: "text",
      inputPlaceholder: this._HelperService.AppConfig.CommonResource.FilterName,
      inputAttributes: {
        autocapitalize: "off",
        autocorrect: "off",
        //maxLength: "4",
        minLength: "4",
      },
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Green,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: "Save",
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
    }).then((result) => {
      if (result.value) {
        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._FilterHelperService._BuildFilterName_Merchant(result.value);
        this._HelperService.Save_NewFilter(
          this._HelperService.AppConfig.FilterTypeOption.Merchant
        );

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });
  }

  Delete_Filter() {

    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.DeleteTitle,
      text: this._HelperService.AppConfig.CommonResource.DeleteHelp,
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel

    }).then((result) => {
      if (result.value) {
        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._HelperService.Delete_Filter(
          this._HelperService.AppConfig.FilterTypeOption.Merchant
        );
        this._FilterHelperService.SetMerchantConfig(this.TerminalsList_Config);
        this.TerminalsList_GetData();

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });

  }

  ApplyFilters(event: any, Type: any, ButtonType: any): void {
    this._HelperService.MakeFilterSnapPermanent();
    this.TerminalsList_GetData();

    if (ButtonType == 'Sort') {
      $("#TerminalsList_sdropdown").dropdown('toggle');
    } else if (ButtonType == 'Other') {
      $("#TerminalsList_fdropdown").dropdown('toggle');
    }

    this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  }

  ResetFilters(event: any, Type: any): void {
    this._HelperService.ResetFilterSnap();
    this._FilterHelperService.SetMerchantConfig(this.TerminalsList_Config);
    this.SetOtherFilters();

    this.TerminalsList_GetData();

    this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  }

  //#endregion

  ResetFilterUI(): void {
    this.ResetFilterControls = false;
    this._ChangeDetectorRef.detectChanges();

    this.TerminalsList_Filter_Owners_Load();

    this.ResetFilterControls = true;
    this._ChangeDetectorRef.detectChanges();
  }


  //#region stores 

  public TUTr_Filter_Store_Option: Select2Options;
  public TUTr_Filter_Store_Toggle = false;
  public TUTr_Filter_Store_Selected = 0;
  TUTr_Filter_Stores_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetStores,
      Location: this._HelperService.AppConfig.NetworkLocation.V3.Account,
      AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
      AccountId: this._HelperService.AppConfig.ActiveOwnerId,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },
        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        }

      ]
    };


    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.TUTr_Filter_Store_Option = {
      placeholder: 'Search by Store',
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }

  TUTr_Filter_Stores_Change(event: any) {
    this._HelperService.Update_CurrentFilterSnap(
      event,
      this._HelperService.AppConfig.ListToggleOption.Other,
      this.TerminalsList_Config,
      this._HelperService.AppConfig.OtherFilters.MerchantSales.Store
    );
    this.StoresEventProcessing(event);
  }

  StoresEventProcessing(event: any): void {
    if (event.value == this.TUTr_Filter_Store_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict('', 'StoreReferenceId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Store_Selected, '=');
      this.TerminalsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TerminalsList_Config.SearchBaseConditions);
      this.TUTr_Filter_Store_Selected = 0;
    }
    else if (event.value != this.TUTr_Filter_Store_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict('', 'StoreReferenceId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Store_Selected, '=');
      this.TerminalsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TerminalsList_Config.SearchBaseConditions);
      this.TUTr_Filter_Store_Selected = event.value;
      this.TerminalsList_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'StoreReferenceId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Store_Selected, '='));
    }
    this.TerminalsList_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
    this._HelperService.ToggleField = true;
    //  this.TUTr_Filter_Issuers_Load();
    setTimeout(() => {
      this._HelperService.ToggleField = false;
    }, 500);
  }

  //#endregion
}
export class CustomerImport {
  public DisplayName: string | null;
  public CompanyName: string | null;
  public BusinessContactNumber: string | null;
  public BusinessEmailAddress: string | null;
  public BusinessAddress: string | null;
  public BusinessType: string | null;
  public ContactFirstName: string | null;
  public ContactLastName: string | null;
  public ContactMobileNumber: string | null;
  public RewardPercentage: string | null;
  public Status: string | null;
  public Message: string | null;
}