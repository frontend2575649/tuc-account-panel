import { ChangeDetectorRef, Component, OnInit, OnDestroy } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import * as Feather from "feather-icons";
import swal from "sweetalert2";
declare var $: any;

import {
  DataHelperService,
  HelperService,
  OList,
  OSelect,
  FilterHelperService,
  OResponse,
} from "../../../../service/service";
import { Observable, Subscription } from 'rxjs';
import { OTerminalList } from "src/app/modules/dashboards/cashier/dashboard.component";

@Component({
  selector: "tu-bankmanager",
  templateUrl: "./bankmanager.component.html",
})
export class TUBankManagerComponent implements OnInit, OnDestroy {
  public ResetFilterControls: boolean = true;
  public _ObjectSubscription: Subscription = null;

  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef,
    public _FilterHelperService: FilterHelperService
  ) {
    this._HelperService.showAddNewPosBtn = true;
    this._HelperService.showAddNewStoreBtn = false;
    this._HelperService.showAddNewCashierBtn = false;
    this._HelperService.showAddNewSubAccBtn = false;


  }

  ngOnInit() {
    this._HelperService.StopClickPropogation();
    Feather.replace();

    this.TerminalsList_Setup();
    this.TerminalsList_Filter_Stores_Load();
    this.TerminalsList_Filter_Providers_Load();
    this.InitColConfig();
    this.TerminalsList_Filter_Banks_Load();
    this.Form_AddUser_Load();
    this.TUTr_Filter_Banks_Load();
    this.GetBankCodeList();
    this._ObjectSubscription = this._HelperService.ObjectCreated.subscribe(value => {
      this.TerminalsList_GetData();
    });
  }

  ngOnDestroy(): void {
    try {
      this._ObjectSubscription.unsubscribe();
    } catch (error) {
    }
  }

  Form_AddUser_Open() {
    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer
        .MerchantOnboarding,
    ]);
  }

  public _BankCodeList = []
  GetBankCodeList() {

    this._HelperService.IsFormProcessing = true;
    var Data = {
        Task: 'getbankcodes',
        // StartDate:this._HelperService.AppConfig.DefaultStartTimeAll,
        // EndDate:this._HelperService.AppConfig.DefaultEndTimeToday,
        AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
        AccountId: this._HelperService.AppConfig.ActiveOwnerId,
    
    };

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Bank, Data);
    _OResponse.subscribe(
        _Response => {



            if (_Response.Status == this._HelperService.StatusSuccess) {
                this._BankCodeList = _Response.Result;

                // Open To See Dummy Data

                // this._LoyalityOverview = {
                //     NewCustomers: 0,
                //     RepeatingCustomers: 0,
                //     VisitsByRepeatingCustomers: 0,
                //     NewCustomerInvoiceAmount: 0.0,
                //     RepeatingCustomerInvoiceAmount: 0.0,
                //     Transaction: 520.0,
                //     TransactionInvoiceAmount: 0.0,

                //     RewardTransaction: 210.0,
                //     RewardAmount: 0.0,
                //     NonRewardTransaction: 0.0,

                //     TucRewardTransaction: 510.0,
                //     TucPlusRewardTransaction: 0,
                //     TucPlusRewardClaimTransaction: 0,

                //     RedeemTransaction: 0,

                //     RewardTransactionPerc: 210.0,
                //     NonRewardTransactionPerc: 0.0,
                // }

          

            }
            else {
                this._HelperService.NotifyError(_Response.Message);
            }
            //#endregion

           

        },
        _Error => {
            this._HelperService.IsFormProcessing = false;
            this._HelperService.HandleException(_Error);
        });
}

  //#region columnConfig

  TempColumnConfig: any = [
    {
      Name: "Status",
      Value: true,
    },
    {
      Name: "City",
      Value: true,
    },
    {
      Name: "Contact",
      Value: true,
    },
    {
      Name: "Stores",
      Value: true,
    },
    {
      Name: "Reward",
      Value: true,
    },
    {
      Name: "POS",
      Value: true,
    },
    {
      Name: "ActivePOS",
      Value: true,
    },
    {
      Name: "RM",
      Value: true,
    },
    {
      Name: "Added",
      Value: true,
    },
  ];

  ColumnConfig: any = [
    {
      Name: "Status",
      Value: true,
    },
    {
      Name: "City",
      Value: true,
    },
    {
      Name: "Contact",
      Value: true,
    },
    {
      Name: "Stores",
      Value: true,
    },
    {
      Name: "Reward",
      Value: true,
    },
    {
      Name: "POS",
      Value: true,
    },
    {
      Name: "ActivePOS",
      Value: true,
    },
    {
      Name: "RM",
      Value: true,
    },
    {
      Name: "Added",
      Value: true,
    },
  ];

  InitColConfig() {
    var MerchantTableConfig = this._HelperService.GetStorage("BMerchantTable");
    var ColConfigExist: boolean =
      MerchantTableConfig != undefined && MerchantTableConfig != null;
    if (ColConfigExist) {
      this.ColumnConfig = MerchantTableConfig.config;
      this.TempColumnConfig = this._HelperService.CloneJson(
        MerchantTableConfig.config
      );
    }
  }

  OpenEditColModal() {
    this._HelperService.OpenModal("EditCol");
  }

  SaveEditCol() {
    this.ColumnConfig = this._HelperService.CloneJson(this.TempColumnConfig);
    this._HelperService.SaveStorage("BMerchantTable", {
      config: this.ColumnConfig,
    });
    this._HelperService.CloseModal("EditCol");
  }

  //#endregion

  //#region merchantlist

  public TerminalsList_Config: OList;
  TerminalsList_Setup() {
    this.TerminalsList_Config = {
      Id: null,
      Sort: null,
      Task: this._HelperService.AppConfig.Api.ThankUCash.getbankcodes,
      Location: this._HelperService.AppConfig.NetworkLocation.V3.Bank,
      Title: "Available Stores",
      StatusType: "default",
      Type: this._HelperService.AppConfig.TerminalTypes.all,
      AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
      AccountId: this._HelperService.AppConfig.ActiveOwnerId,
      DefaultSortExpression: "CreateDate desc",
      TableFields: [
        {
          DisplayName: "Code",
          SystemName: "Code",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: true,
          Sort: false,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "Name",
          SystemName: "Name",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "text-center",
          Show: true,
          Search: true,
          Sort: false,
          ResourceId: null,
          DefaultValue: "ThankUCash",
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "Country",
          SystemName: "Country",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "text-center",
          Show: true,
          Search: true,
          Sort: false,
          ResourceId: null,
          DefaultValue: "ThankUCash",
          NavigateField: "ReferenceKey"
        },
  
      ]
    };
    this.TerminalsList_Config = this._DataHelperService.List_Initialize(
      this.TerminalsList_Config
    );

    this._HelperService.Active_FilterInit(
      this._HelperService.AppConfig.FilterTypeOption.Terminal,
      this.TerminalsList_Config
    );

    this.TerminalsList_GetData();
  }
  TerminalsList_ToggleOption(event: any, Type: any) {

    if (event != null) {
      for (let index = 0; index < this.TerminalsList_Config.Sort.SortOptions.length; index++) {
        const element = this.TerminalsList_Config.Sort.SortOptions[index];
        if (event.SystemName == element.SystemName) {
          element.SystemActive = true;
        }
        else {
          element.SystemActive = false;
        }
      }
    }

    this._HelperService.Update_CurrentFilterSnap(
      event,
      Type,
      this.TerminalsList_Config


    );

    this.TerminalsList_Config = this._DataHelperService.List_Operations(
      this.TerminalsList_Config,
      event,
      Type
    );

    if (
      (this.TerminalsList_Config.RefreshData == true)
      && this._HelperService.DataReloadEligibility(Type)
    ) {
      this.TerminalsList_GetData();
    }

  }

  timeout = null;
  TerminalsList_ToggleOptionSearch(event: any, Type: any) {

    clearTimeout(this.timeout);

    this.timeout = setTimeout(() => {
      if (event != null) {
        for (let index = 0; index < this.TerminalsList_Config.Sort.SortOptions.length; index++) {
          const element = this.TerminalsList_Config.Sort.SortOptions[index];
          if (event.SystemName == element.SystemName) {
            element.SystemActive = true;
          }
          else {
            element.SystemActive = false;
          }
        }
      }
  
      this._HelperService.Update_CurrentFilterSnap(
        event,
        Type,
        this.TerminalsList_Config
  
  
      );
  
      this.TerminalsList_Config = this._DataHelperService.List_Operations(
        this.TerminalsList_Config,
        event,
        Type
      );
  
      if (
        (this.TerminalsList_Config.RefreshData == true)
        && this._HelperService.DataReloadEligibility(Type)
      ) {
        this.TerminalsList_GetData();
      }
    }, this._HelperService.AppConfig.SearchInputDelay);


  }

  public OverviewData: any = {
    Total: 0,
    Active: 0,
    Idle: 0,
    Dead: 0,
    Unused: 0
  };

  TerminalsList_GetData() {
    // this.GetOverviews(this.TerminalsList_Config, this._HelperService.AppConfig.Api.ThankUCash.TransactionOverviews.getterminalsoverview);
    var TConfig = this._DataHelperService.List_GetData(
      this.TerminalsList_Config
    );
    this.TerminalsList_Config = TConfig;
  }
  TerminalsList_RowSelected(ReferenceData) {
    this._HelperService.SaveStorage(
      this._HelperService.AppConfig.Storage.ActiveBankCode,
      {
        BankCode: ReferenceData.Code,
       
      }
    );

    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.AddBanks,
      ReferenceData.Code,

    ]);


  }

  //#endregion

  //#region Store

  public TerminalsList_Filter_Store_Option: Select2Options;
  public TerminalsList_Filter_Store_Selected = 0;
  TerminalsList_Filter_Stores_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetStores,
      Location: this._HelperService.AppConfig.NetworkLocation.V3.Account,
      AccountId: this._HelperService.UserAccount.AccountId,
      AccountKey: this._HelperService.UserAccount.AccountKey,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },
        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true,
        },
        // {
        //   SystemName: "AccountTypeCode",
        //   Type: this._HelperService.AppConfig.DataType.Text,
        //   SearchCondition: "=",
        //   SearchValue: this._HelperService.AppConfig.AccountType.Store
        // }
      ],
    };
    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.TerminalsList_Filter_Store_Option = {
      placeholder: "Filter by Store",
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }
  TerminalsList_Filter_Stores_Change(event: any) {

    this._HelperService.Update_CurrentFilterSnap(
      event,
      this._HelperService.AppConfig.ListToggleOption.Other,
      this.TerminalsList_Config,
      this._HelperService.AppConfig.OtherFilters.Terminal.Store
    );

    this.StoreEventProcessing(event);

  }

  StoreEventProcessing(event: any): void {
    if (event.value == this.TerminalsList_Filter_Store_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict(
        "",
        "StoreReferenceId",
        this._HelperService.AppConfig.DataType.Number,
        this.TerminalsList_Filter_Store_Selected,
        "="
      );
      this.TerminalsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(
        SearchCase,
        this.TerminalsList_Config.SearchBaseConditions
      );
      this.TerminalsList_Filter_Store_Selected = 0;
    } else if (event.value != this.TerminalsList_Filter_Store_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict(
        "",
        "StoreReferenceId",
        this._HelperService.AppConfig.DataType.Number,
        this.TerminalsList_Filter_Store_Selected,
        "="
      );
      this.TerminalsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(
        SearchCase,
        this.TerminalsList_Config.SearchBaseConditions
      );
      this.TerminalsList_Filter_Store_Selected = event.value;
      this.TerminalsList_Config.SearchBaseConditions.push(
        this._HelperService.GetSearchConditionStrict(
          "",
          "StoreReferenceId",
          this._HelperService.AppConfig.DataType.Number,
          this.TerminalsList_Filter_Store_Selected,
          "="
        )
      );
    }
    this.TerminalsList_ToggleOption(
      null,
      this._HelperService.AppConfig.ListToggleOption.ResetOffset
    );
  }

  //#endregion

  //#region Provider

  public TerminalsList_Filter_Provider_Option: Select2Options;
  public TerminalsList_Filter_Provider_Selected = 0;
  TerminalsList_Filter_Providers_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
      Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },
        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true,
        },
        {
          SystemName: "AccountTypeCode",
          Type: this._HelperService.AppConfig.DataType.Text,
          SearchCondition: "=",
          SearchValue: this._HelperService.AppConfig.AccountType.PosAccount,
        },
      ],
    };
    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.TerminalsList_Filter_Provider_Option = {
      placeholder: "Filter by PTSP",
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }
  TerminalsList_Filter_Providers_Change(event: any) {

    this._HelperService.Update_CurrentFilterSnap(
      event,
      this._HelperService.AppConfig.ListToggleOption.Other,
      this.TerminalsList_Config,
      this._HelperService.AppConfig.OtherFilters.Terminal.Provider
    );

    this.ProvderEventProcessing(event);

  }

  ProvderEventProcessing(event): void {
    if (event.value == this.TerminalsList_Filter_Provider_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict(
        "",
        "ProviderReferenceId",
        this._HelperService.AppConfig.DataType.Number,
        this.TerminalsList_Filter_Provider_Selected,
        "="
      );
      this.TerminalsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(
        SearchCase,
        this.TerminalsList_Config.SearchBaseConditions
      );
      this.TerminalsList_Filter_Provider_Selected = 0;
    } else if (event.value != this.TerminalsList_Filter_Provider_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict(
        "",
        "ProviderReferenceId",
        this._HelperService.AppConfig.DataType.Number,
        this.TerminalsList_Filter_Provider_Selected,
        "="
      );
      this.TerminalsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(
        SearchCase,
        this.TerminalsList_Config.SearchBaseConditions
      );
      this.TerminalsList_Filter_Provider_Selected = event.value;
      this.TerminalsList_Config.SearchBaseConditions.push(
        this._HelperService.GetSearchConditionStrict(
          "",
          "ProviderReferenceId",
          this._HelperService.AppConfig.DataType.Number,
          this.TerminalsList_Filter_Provider_Selected,
          "="
        )
      );
    }
    this.TerminalsList_ToggleOption(
      null,
      this._HelperService.AppConfig.ListToggleOption.ResetOffset
    );
  }

  //#endregion

  //#region Bank 

  public TerminalsList_Filter_Bank_Option: Select2Options;
  public TerminalsList_Filter_Bank_Selected = 0;
  TerminalsList_Filter_Banks_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
      Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },
        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        },
        {
          SystemName: "AccountTypeCode",
          Type: this._HelperService.AppConfig.DataType.Text,
          SearchCondition: "=",
          SearchValue: this._HelperService.AppConfig.AccountType.Acquirer
        }
      ]
    };
    // S2Data.SearchBaseCondition = this._HelperService.GetSearchConditionStrict('', 'TypeCode', this._HelperService.AppConfig.DataType.Text, Condition, '=');
    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.TerminalsList_Filter_Bank_Option = {
      placeholder: 'Filter by Bank',
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }
  TerminalsList_Filter_Banks_Change(event: any) {
    this._HelperService.Update_CurrentFilterSnap(
      event,
      this._HelperService.AppConfig.ListToggleOption.Other,
      this.TerminalsList_Config,
      this._HelperService.AppConfig.OtherFilters.Terminal.Bank
    );

    this.BankEventProcessing(event);
  }

  BankEventProcessing(event): void {
    if (event.value == this.TerminalsList_Filter_Bank_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict('', 'AcquierReferenceId', this._HelperService.AppConfig.DataType.Number, this.TerminalsList_Filter_Bank_Selected, '=');
      this.TerminalsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TerminalsList_Config.SearchBaseConditions);
      this.TerminalsList_Filter_Bank_Selected = 0;
    }
    else if (event.value != this.TerminalsList_Filter_Bank_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict('', 'AcquierReferenceId', this._HelperService.AppConfig.DataType.Number, this.TerminalsList_Filter_Bank_Selected, '=');
      this.TerminalsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TerminalsList_Config.SearchBaseConditions);
      this.TerminalsList_Filter_Bank_Selected = event.value;
      this.TerminalsList_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'AcquierReferenceId', this._HelperService.AppConfig.DataType.Number, this.TerminalsList_Filter_Bank_Selected, '='));
    }
    this.TerminalsList_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
  }

  //#endregion

  SetOtherFilters(): void {
    this.TerminalsList_Config.SearchBaseConditions = [];
    this.TerminalsList_Config.SearchBaseCondition = null;

    var CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.Merchant.Owner));
    if (CurrentIndex != -1) {
      // this.TerminalsList_Filter_Owners_Selected = null;
      // this.OwnerEventProcessing(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
    }
  }

  ToogleType(type: string): void {
    this.TerminalsList_Config.Type = type;
    this.TerminalsList_GetData();
  }

  // ToogleStatusType(type: any): void {
  //   console.log(type.value);
  //   switch (type.value) {
  //     case '0': this.TerminalsList_Config.Type = this._HelperService.AppConfig.TerminalTypes.all

  //       break;
  //     case '547': this.TerminalsList_Config.Type = this._HelperService.AppConfig.TerminalTypes.active

  //       break;
  //     case '548': {
  //       this.TerminalsList_Config.Type = this._HelperService.AppConfig.TerminalTypes.idle
  //     }
  //       break;
  //     case '549': this.TerminalsList_Config.Type = this._HelperService.AppConfig.TerminalTypes.dead

  //       break;
  //     case '550': this.TerminalsList_Config.Type = this._HelperService.AppConfig.TerminalTypes.unused

  //       break;

  //     default:
  //       break;
  //   }

  //   // this.TerminalsList_Config.Type = type;
  //   // this.TerminalsList_GetData();
  // }

  //#region filterOperations

  Active_FilterValueChanged(event: any) {
    this._HelperService.Active_FilterValueChanged(event);
    this._FilterHelperService.SetMerchantConfig(this.TerminalsList_Config);

    //#region setOtherFilters
    this.SetOtherFilters();
    //#endregion

    this.TerminalsList_GetData();
  }

  RemoveFilterComponent(Type: string, index?: number): void {
    this._FilterHelperService._RemoveFilter_POS(Type, index);
    this._FilterHelperService.SetPOSConfig(this.TerminalsList_Config);

    this.SetOtherFilters();

    this.TerminalsList_GetData();
  }

  Save_NewFilter() {
    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.SaveFilterTitle,
      text: this._HelperService.AppConfig.CommonResource.SaveFilterHelp,
      input: "text",
      inputPlaceholder: this._HelperService.AppConfig.CommonResource.FilterName,
      inputAttributes: {
        autocapitalize: "off",
        autocorrect: "off",
        maxLength: "32",
        minLength: "4",
      },
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Green,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: "Save",
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
    }).then((result) => {
      if (result.value) {
        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._FilterHelperService._BuildFilterName_Merchant(result.value);
        this._HelperService.Save_NewFilter(
          this._HelperService.AppConfig.FilterTypeOption.Terminal
        );

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });
  }
  Delete_Confirm() {
    swal({
      position: 'top',
      title: this._HelperService.AppConfig.CommonResource.DeleteTitle,
      text: this._HelperService.AppConfig.CommonResource.DeleteHelp,
      input: 'password',
      inputPlaceholder: this._HelperService.AppConfig.CommonResource.AccessPin,
      inputAttributes: {
        autocapitalize: 'off',
        autocorrect: 'off',
        maxLength: "4",
        minLength: "4"
      },
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
    }).then((result) => {
      if (result.value) {
        this._HelperService.IsFormProcessing = true;
        var pData = {
          Task: this._HelperService.AppConfig.Api.ThankUCash.deletebankaccount,
          ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
          AuthPin: result.value
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Bank, pData);
        _OResponse.subscribe(
          _Response => {
            this._HelperService.IsFormProcessing = false;
            if (_Response.Status == this._HelperService.StatusSuccess) {
              this._HelperService.NotifySuccess(_Response.Message);
            }
            else {
              this._HelperService.NotifyError(_Response.Message);
            }
          },
          _Error => {

            this._HelperService.HandleException(_Error);
          });
      }
    });
  }


  Delete_Filter() {

    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.DeleteTitle,
      text: this._HelperService.AppConfig.CommonResource.DeleteHelp,
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel

    }).then((result) => {
      if (result.value) {
        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._HelperService.Delete_Filter(
          this._HelperService.AppConfig.FilterTypeOption.Terminal
        );
        this._FilterHelperService.SetMerchantConfig(this.TerminalsList_Config);
        this.TerminalsList_GetData();

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });

  }

  ApplyFilters(event: any, Type: any, ButtonType: any): void {
    this._HelperService.MakeFilterSnapPermanent();
    this.TerminalsList_GetData();

    if (ButtonType == 'Sort') {
      $("#TerminalsList_sdropdown").dropdown('toggle');
    } else if (ButtonType == 'Other') {
      $("#TerminalsList_fdropdown").dropdown('toggle');
    }
    this.TerminalsList_GetData();
    this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  }

  ResetFilters(event: any, Type: any): void {
    this._HelperService.ResetFilterSnap();
    this._FilterHelperService.SetMerchantConfig(this.TerminalsList_Config);
    this.SetOtherFilters();

    this.TerminalsList_GetData();

    this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  }

  //#endregion

  ResetFilterUI(): void {
    this.ResetFilterControls = false;
    this._ChangeDetectorRef.detectChanges();

    this.ResetFilterControls = true;
    this._ChangeDetectorRef.detectChanges();
  }
  public TUTr_Filter_Bank_Option: Select2Options;
  TUTr_Filter_Banks_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetAcquirers,
      Location: this._HelperService.AppConfig.NetworkLocation.V3.ConAccount,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceKey",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: true,
          Text: false,
        },
        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        },
        // {
        //   SystemName: "AccountTypeCode",
        //   Type: this._HelperService.AppConfig.DataType.Text,
        //   SearchCondition: "=",
        //   SearchValue: this._HelperService.AppConfig.AccountType.Acquirer
        // }
      ]

    };
    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.TUTr_Filter_Bank_Option = {
      placeholder: 'Select Bank (Acquirer)',
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }
  TUTr_Filter_Banks_Change(event: any) {
    console.log("bankkk",event.data[0])
    this.Form_AddUser.patchValue(
      {
        BankId: event.data[0].ReferenceId,
        BankName: event.data[0].text,
      });
  }
  Form_AddUser: FormGroup;
  Form_AddUser_Show() {
    this._HelperService.Icon_Crop_Clear();
    this._HelperService.OpenModal("Form_AddUser_Content");
  }
  Form_AddUser_Close() {
    this._HelperService._FileSelect_Icon_Reset();
    this._HelperService.CloseModal("Form_AddUser_Content");
  }
  Form_AddUser_Load() {
    this._HelperService._FileSelect_Icon_Data.Width = 128;
    this._HelperService._FileSelect_Icon_Data.Height = 128;

    this._HelperService._FileSelect_Poster_Data.Width = 800;
    this._HelperService._FileSelect_Poster_Data.Height = 400;

    this.Form_AddUser = this._FormBuilder.group({
      OperationType: "new",
      Task: this._HelperService.AppConfig.Api.ThankUCash.savebankaccount,
      AccountId: this._HelperService.AppConfig.ActiveOwnerId,
      AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
      BankId: [null, Validators.required],
      StatusCode: this._HelperService.AppConfig.Status.Active,
      BvnNumber: [null, Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(25)])],
      AccountNumber: [null, Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(20)])],
      BankName:[null, Validators.required],

    });
  }
  Form_AddUser_Clear() {
    this.Form_AddUser.reset();
    this._HelperService._FileSelect_Icon_Reset();
    this._HelperService._FileSelect_Poster_Reset();
    this.Form_AddUser_Load();
    this._HelperService.GetRandomNumber();
    this._HelperService.GeneratePassoword();
  }
  Form_AddUser_Process(_FormValue: any) {
    this._HelperService.IsFormProcessing = true;
    let _OResponse: Observable<OResponse>;



    if (true) {
      _OResponse = this._HelperService.PostData(
        this._HelperService.AppConfig.NetworkLocation.V3.Bank,
        _FormValue
      );
      _OResponse.subscribe(
        _Response => {
          this._HelperService.IsFormProcessing = false;
          if (_Response.Status == this._HelperService.StatusSuccess) {
            this._HelperService.FlashSwalSuccess("New pos terminal has been added successfully",
              "You have successfully created new POS terminal");
            this._HelperService.ObjectCreated.next(true);
            this.Form_AddUser_Clear();
            this.Form_AddUser_Close();
            if (_FormValue.OperationType == "close") {
              this.Form_AddUser_Close();
            }
            this._HelperService.GetAccountCount(this._HelperService.AppConfig.ActiveOwnerKey, this._HelperService.AppConfig.ActiveOwnerId, this._HelperService.AppConfig.DefaultStartTimeAll, this._HelperService.AppConfig.DefaultEndTimeToday)

          } else {
            this._HelperService.NotifyError(_Response.Message);
          }
        },
        _Error => {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.HandleException(_Error);
        }
      );
      return;
    }


    else if (this._HelperService.AppConfig.TerminalPermission.MinimumLimit <= this._HelperService.AppConfig.TerminalsCount && this._HelperService.AppConfig.TerminalPermission.MaximumLimit > this._HelperService.AppConfig.TerminalsCount) {
      _OResponse = this._HelperService.PostData(
        this._HelperService.AppConfig.NetworkLocation.V3.Account,
        _FormValue
      );
      _OResponse.subscribe(
        _Response => {
          this._HelperService.IsFormProcessing = false;
          if (_Response.Status == this._HelperService.StatusSuccess) {
            this._HelperService.FlashSwalSuccess("New pos terminal has been added successfully",
              "You have successfully created new POS terminal");
            this._HelperService.ObjectCreated.next(true);
            this.Form_AddUser_Clear();
            this.Form_AddUser_Close();
            if (_FormValue.OperationType == "close") {
              this.Form_AddUser_Close();
            }
            this._HelperService.GetAccountCount(this._HelperService.AppConfig.ActiveOwnerKey, this._HelperService.AppConfig.ActiveOwnerId, this._HelperService.AppConfig.DefaultStartTimeAll, this._HelperService.AppConfig.DefaultEndTimeToday)

          } else {
            this._HelperService.NotifyError(_Response.Message);
          }
        },
        _Error => {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.HandleException(_Error);
        }
      );
    } else {
      this._HelperService.NotifyError('Upgrade Your Subscription')
      this.Form_AddUser_Close();
      this._Router.navigate(['m' + '/' + this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.Upgrade]);
      return;
    }


  }
  
  GetOverviews(ListOptions: any, Task: string): any {
    this._HelperService.IsFormProcessing = true;

    ListOptions.SearchCondition = '';
    ListOptions = this._DataHelperService.List_GetSearchCondition(ListOptions);
    if (ListOptions.ActivePage == 1) {
      ListOptions.RefreshCount = true;
    }
    var SortExpression = ListOptions.Sort.SortDefaultColumn + ' ' + ListOptions.Sort.SortDefaultOrder;;

    if (ListOptions.Sort.SortDefaultName) {
      ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName.replace(' desc', '');
      ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName.replace(' asc', '');
      ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName + ' ' + ListOptions.Sort.SortDefaultOrder;
    }

    if (ListOptions.Sort.SortColumn != undefined && ListOptions.Sort.SortColumn != null && ListOptions.Sort.SortColumn != '') {
      if (ListOptions.Sort.SortOrder != undefined && ListOptions.Sort.SortOrder != null && ListOptions.Sort.SortOrder != '') {
        SortExpression = ListOptions.Sort.SortColumn + ' ' + ListOptions.Sort.SortOrder;
      }
      else {
        SortExpression = ListOptions.Sort.SortColumn + ' desc';
      }
    }

    // var SearchCondition = '';
    // if (ListOptions.SearchCondition.includes('StatusCode')) {
    //   SearchCondition = '';
    // }else{
    //   SearchCondition = '';
    // }


    var pData = {
      Task: Task,
      TotalRecords: ListOptions.TotalRecords,
      Offset: (ListOptions.ActivePage - 1) * ListOptions.PageRecordLimit,
      Limit: ListOptions.PageRecordLimit,
      RefreshCount: ListOptions.RefreshCount,
      SearchCondition: ListOptions.SearchCondition,
      SortExpression: SortExpression,
      Type: ListOptions.Type,
      ReferenceKey: ListOptions.ReferenceKey,
      StartDate: ListOptions.StartDate,
      EndDate: ListOptions.EndDate,
      ReferenceId: ListOptions.ReferenceId,
      SubReferenceId: ListOptions.SubReferenceId,
      SubReferenceKey: ListOptions.SubReferenceKey,
      AccountId: ListOptions.AccountId,
      AccountKey: ListOptions.AccountKey,
      ListType: ListOptions.ListType,
      IsDownload: false,
    };

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Account, pData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this.OverviewData = _Response.Result.Data as any;

      
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);

      });


  }

}
