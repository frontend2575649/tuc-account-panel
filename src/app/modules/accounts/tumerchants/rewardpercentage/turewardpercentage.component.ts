import { ChangeDetectorRef, Component, OnInit } from "@angular/core";
import { FormBuilder, Validators, FormGroup } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import * as Feather from "feather-icons";
import swal from "sweetalert2";
declare var $: any;

import {
  DataHelperService,
  HelperService,
  OList,
  OSelect,
  FilterHelperService,
  OResponse,
} from "../../../../service/service";
import { Observable } from 'rxjs';

@Component({
  selector: "tu-turewardpercentage",
  templateUrl: "./turewardpercentage.component.html",
})
export class TURewardPercentageComponent implements OnInit {
  public ResetFilterControls: boolean = true;
  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef,
    public _FilterHelperService: FilterHelperService
  ) {
    this._HelperService.showAddNewPosBtn = false;
    this._HelperService.showAddNewStoreBtn = false;
    this._HelperService.showAddNewCashierBtn = false;
    this._HelperService.showAddNewSubAccBtn = false;


  }

  ngOnInit() {
    this.Form_AddUser_Load();
    this.RewardHistoryList_Setup();
    this.RewardList_GetData();
    this.GetLoyaltyConfiguration();
  }


  public _LoyaltyConfig =
    {
      IsTucPlus: false,
      IsTucGold: false,
      Reward:
      {
        IsCustomReward: false,
        TucCommissionSource: "Invoice Amount",
        RewardPercentage: 0,
        UserPercentage: 0,
        SystemPercentage: 0,
        MerchantPercentage: 0,
        PaymentMethod: "Prepay"

      },
      Redeem:
      {
        TucCommissionSource: "Redeem Amount",
        UserPercentage: 0,
        SystemPercentage: 0,
        MerchantPercentage: 0,
      }
    };

  public _LoyaltyConfigExample =
    {
      InvoiceAmount: 1000,
      UserAmount: 0,
      SystemAmount: 0,
      TotalAmount: 0,
    }

  GetLoyaltyConfiguration() {
    var pData = {
      Task: 'getloyaltyconfiguration',
      StartDate: null,
      EndDate: null,
      AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
      AccountId: this._HelperService.AppConfig.ActiveOwnerId,
      ConfigurationKey: this._HelperService.AppConfig.ConfigurationKey
    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Operations, pData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._LoyaltyConfig = _Response.Result;
          var RewardAmount = this._HelperService.GetAmountFromPercentage(this._LoyaltyConfigExample.InvoiceAmount, this._LoyaltyConfig.Reward.RewardPercentage);
          if (this._LoyaltyConfig.Reward.TucCommissionSource == "Reward Amount") {
            this._LoyaltyConfigExample.UserAmount = this._HelperService.GetAmountFromPercentage(RewardAmount, this._LoyaltyConfig.Reward.RewardPercentage);
            this._LoyaltyConfigExample.SystemAmount = (RewardAmount - this._LoyaltyConfigExample.UserAmount)
            this._LoyaltyConfigExample.TotalAmount = RewardAmount;
          }
          else {
            this._LoyaltyConfigExample.UserAmount = RewardAmount;
            this._LoyaltyConfigExample.SystemAmount = this._HelperService.GetAmountFromPercentage(this._LoyaltyConfigExample.InvoiceAmount, this._LoyaltyConfig.Reward.SystemPercentage);
            this._LoyaltyConfigExample.TotalAmount = this._LoyaltyConfigExample.UserAmount + this._LoyaltyConfigExample.SystemAmount;
          }
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });

  }

  isValidRewardpercentage:boolean=false;
  CustomerEarns(event) {
   let value=event.target.value
   
   if(value > 0 && value <100){
    this.isValidRewardpercentage=true;
   }else{
    this.isValidRewardpercentage=false;
   }
    

  }
  RewardPerc: any = null;
  RewardList_GetData() {
    var pData = {
      Task: 'getconfiguration',
      StartDate: null,
      EndDate: null,
      AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
      AccountId: this._HelperService.AppConfig.ActiveOwnerId,
      ConfigurationKey: this._HelperService.AppConfig.ConfigurationKey
    };


    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Operations, pData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this.RewardPerc = _Response.Result.Value;
          if (this.RewardPerc != 0) {
            this._HelperService.IsRewardSet = true;
          }

        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });

  }



  public RewardHistoryList_Config: OList;
  RewardHistoryList_Setup() {
    this.RewardHistoryList_Config = {
      Id: null,
      Sort: null,
      Task: this._HelperService.AppConfig.Api.ThankUCash.getconfigurationhistory,
      Location: this._HelperService.AppConfig.NetworkLocation.V3.Operations,
      Title: "Available History",
      StatusType: "default",
      // Type: this._HelperService.AppConfig.ListType.SubOwner,
      AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
      AccountId: this._HelperService.AppConfig.ActiveOwnerId,
      SubReferenceKey: "rewardpercentage",
      DefaultSortExpression: "CreateDate desc",
      // SearchBaseCondition: this._HelperService.GetSearchConditionStrict('', 'StatusId', this._HelperService.AppConfig.DataType.Number, 2, '=='),
      TableFields: [
        {
          DisplayName: "TerminalId",
          SystemName: "TerminalId",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "SerialNumber",
          SystemName: "SerialNumber",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "text-center",
          Show: true,
          Search: true,
          Sort: false,
          ResourceId: null,
          DefaultValue: "ThankUCash",
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "Email Address",
          SystemName: "EmailAddress",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: false,
          Sort: false,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "Provider",
          SystemName: "ProviderDisplayName",
          DataType: this._HelperService.AppConfig.DataType.Number,
          Class: "text-right",
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },

        {
          DisplayName: this._HelperService.AppConfig.CommonResource.CreateDate,
          SystemName: "CreateDate",
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: "td-date text-right",
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey",
        },
      ]
    };
    this.RewardHistoryList_Config = this._DataHelperService.List_InitializeRewPerc(
      this.RewardHistoryList_Config
    );

    this._HelperService.Active_FilterInit(
      this._HelperService.AppConfig.FilterTypeOption.Merchant,
      this.RewardHistoryList_Config
    );

    this.RewardHistoryList_GetData();
  }
  RewardHistoryList_ToggleOption(event: any, Type: any) {

    if (event != null) {
      for (let index = 0; index < this.RewardHistoryList_Config.Sort.SortOptions.length; index++) {
        const element = this.RewardHistoryList_Config.Sort.SortOptions[index];
        if (event.SystemName == element.SystemName) {
          element.SystemActive = true;
        }
        else {
          element.SystemActive = false;
        }
      }
    }

    this._HelperService.Update_CurrentFilterSnap(
      event,
      Type,
      this.RewardHistoryList_Config


    );

    this.RewardHistoryList_Config = this._DataHelperService.List_Operations(
      this.RewardHistoryList_Config,
      event,
      Type
    );

    if (
      (this.RewardHistoryList_Config.RefreshData == true)
      && this._HelperService.DataReloadEligibility(Type)
    ) {
      this.RewardHistoryList_GetData();
    }

  }
  RewardHistoryList_GetData() {
    var TConfig = this._DataHelperService.List_GetData(
      this.RewardHistoryList_Config
    );
    this.RewardHistoryList_Config = TConfig;
  }
  RewardHistoryList_RowSelected(ReferenceData) {
    this._HelperService.SaveStorage(
      this._HelperService.AppConfig.Storage.ActiveMerchant,
      {
        ReferenceKey: ReferenceData.ReferenceKey,
        ReferenceId: ReferenceData.ReferenceId,
        DisplayName: ReferenceData.DisplayName,
        AccountTypeCode: this._HelperService.AppConfig.AccountType.Merchant,
      }
    );

    this._HelperService.AppConfig.ActiveReferenceKey =
      ReferenceData.ReferenceKey;
    this._HelperService.AppConfig.ActiveReferenceId = ReferenceData.ReferenceId;


  }
  Form_AddUser: FormGroup;
  Form_AddUser_Show() {
    this._HelperService.OpenModal("Form_AddUser_Content");
  }
  Form_AddUser_Close() {
    // this._Router.navigate([
    //     this._HelperService.AppConfig.Pages.System.AdminUsers
    // ]);
    this._HelperService.CloseModal("Form_AddUser_Content");
  }
  Form_AddUser_Load() {
    this._HelperService._FileSelect_Icon_Data.Width = 128;
    this._HelperService._FileSelect_Icon_Data.Height = 128;

    this._HelperService._FileSelect_Poster_Data.Width = 800;
    this._HelperService._FileSelect_Poster_Data.Height = 400;

    this.Form_AddUser = this._FormBuilder.group({
      OperationType: "new",
      Task: this._HelperService.AppConfig.Api.ThankUCash.saveconfiguration,
      AccountId: this._HelperService.AppConfig.ActiveOwnerId,
      AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
      StatusCode: this._HelperService.AppConfig.Status.Active,
      ConfigurationKey: this._HelperService.AppConfig.ConfigurationKey,
      Value: [null, Validators.compose([Validators.required, Validators.min(0), Validators.max(100)])],
      Comment: [null, Validators.required],

    });

    // if(this._HelperService.AppConfig.salesanalytics){
    //   this.Form_AddUser.controls['Value'].validator
    // }
    // else{

    // }


  }
  Form_AddUser_Clear() {
    this.Form_AddUser.reset();
    this._HelperService._FileSelect_Icon_Reset();
    this._HelperService._FileSelect_Poster_Reset();
    this.Form_AddUser_Load();
    this._HelperService.GetRandomNumber();
    this._HelperService.GeneratePassoword();
  }
  Form_AddUser_Process(_FormValue: any) {
    this._HelperService.IsFormProcessing = true;
    // console.log(''_FormValue.Value)

    if (this._HelperService.AppConfig.RewardPercentagePermission) {
      this._HelperService.IsFormProcessing = false;
      if (_FormValue.Value >= this._HelperService.AppConfig.RewardPercentagePermission.MinimumValue && _FormValue.Value <= this._HelperService.AppConfig.RewardPercentagePermission.MaximumValue) {
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(
          this._HelperService.AppConfig.NetworkLocation.V3.Operations,
          _FormValue
        );
        _OResponse.subscribe(
          _Response => {
            this._HelperService.IsFormProcessing = false;
            if (_Response.Status == this._HelperService.StatusSuccess) {
              this._HelperService.FlashSwalSuccess("Reward Percentage Updated successfully", "Done! Your Reward percentage is updated");
              this.Form_AddUser_Clear();
              this.Form_AddUser_Close();
              this.RewardHistoryList_Setup();
              this.RewardList_GetData();
              this.GetLoyaltyConfiguration();
              if (_FormValue.OperationType == "close") {
                this.Form_AddUser_Close();
              }
            } else {
              this._HelperService.NotifyError(_Response.Message);
            }
          },
          _Error => {
            this._HelperService.IsFormProcessing = false;
            this._HelperService.HandleException(_Error);
          }
        );
      } else {
        this._HelperService.NotifyError('Upgrade Your Subscription')
        this.Form_AddUser_Close();
        this._Router.navigate(['m' + '/' + this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.Upgrade]);
        return;
      }
    }
    else {
      this._HelperService.IsFormProcessing = false;
      this._HelperService.NotifyError('Reward percentage is disabled');
      return;


    }



  }


}
