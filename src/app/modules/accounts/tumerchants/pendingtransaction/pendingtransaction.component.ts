import { ChangeDetectorRef, Component, OnInit, OnDestroy, ElementRef, ViewChild } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import * as Feather from "feather-icons";
import swal from "sweetalert2";
declare var $: any;

import {
  DataHelperService,
  HelperService,
  OList,
  OSelect,
  FilterHelperService,
  OResponse,
} from "../../../../service/service";
import { Observable, Subscription } from 'rxjs';

@Component({
  selector: "tu-pendingtransaction",
  templateUrl: "./pendingtransaction.component.html",
})
export class TUPendingTransactionComponent implements OnInit, OnDestroy {
  public ResetFilterControls: boolean = true;
  public _ObjectSubscription: Subscription = null;

  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef,
    public _FilterHelperService: FilterHelperService
  ) {
    this._HelperService.showAddNewPosBtn = true;
    this._HelperService.showAddNewStoreBtn = false;
    this._HelperService.showAddNewCashierBtn = false;
    this._HelperService.showAddNewSubAccBtn = false;


  }
  @ViewChild("offCanvas") divView: ElementRef;

  ngOnInit() {
    this._HelperService.StopClickPropogation();
    Feather.replace();
    // var StorageDetails = this._HelperService.GetStorage(this._HelperService.AppConfig.Storage.ActiveBankCode);
    // if (StorageDetails != null) {
    //   this._HelperService.AppConfig.ActiveBankCode = StorageDetails.BankCode;

    // }
    this.TransactionList_Setup();
    this.TransactionList_Filter_Stores_Load();
    this.TransactionList_Filter_Providers_Load();
    this.InitColConfig();
    this.TransactionList_Filter_Banks_Load();
    this.Form_AddUser_Load();
    this.TUTr_Filter_Banks_Load();
    this.InitBackDropClickEvent();
    // this.GetCashOutConfiguration(); 

    this._ObjectSubscription = this._HelperService.ObjectCreated.subscribe(value => {
      this.TransactionList_GetData();
    });
  }
  SetSearchRanges(): void {
    //#region Invoice 
    this.TransactionList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArrayByField('InvoiceAmount', this.TransactionList_Config.SearchBaseConditions);
    var SearchCase = this._HelperService.GetSearchConditionRange('', 'InvoiceAmount', this.TUTr_InvoiceRangeMinAmount, this.TUTr_InvoiceRangeMaxAmount);
    if (this.TUTr_InvoiceRangeMinAmount == this._HelperService.AppConfig.RangeInvoiceAmountMinimumLimit && this.TUTr_InvoiceRangeMaxAmount == this._HelperService.AppConfig.RangeInvoiceAmountMaximumLimit) {
      this.TransactionList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TransactionList_Config.SearchBaseConditions);
    }
    else {
      this.TransactionList_Config.SearchBaseConditions.push(SearchCase);
    }

    //#endregion      

  }

  InitBackDropClickEvent(): void {
    var backdrop: HTMLElement = document.getElementById("backdrop");

    backdrop.onclick = () => {
      $(this.divView.nativeElement).removeClass('show');
      backdrop.classList.remove("show");
    };
  }
  clicked() {
    $(this.divView.nativeElement).addClass('show');
    var backdrop: HTMLElement = document.getElementById("backdrop");
    backdrop.classList.add("show");
  }
  unclick() {
    $(this.divView.nativeElement).removeClass('show');
    var backdrop: HTMLElement = document.getElementById("backdrop");
    backdrop.classList.remove("show");
  }

  ngOnDestroy(): void {
    try {
      this._ObjectSubscription.unsubscribe();
    } catch (error) {
    }
  }

  Form_AddUser_Open() {
    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer
        .MerchantOnboarding,
    ]);
  }

  //#region columnConfig

  TempColumnConfig: any = [
    {
      Name: "Status",
      Value: true,
    },
    {
      Name: "City",
      Value: true,
    },
    {
      Name: "Contact",
      Value: true,
    },
    {
      Name: "Stores",
      Value: true,
    },
    {
      Name: "Reward",
      Value: true,
    },
    {
      Name: "POS",
      Value: true,
    },
    {
      Name: "ActivePOS",
      Value: true,
    },
    {
      Name: "RM",
      Value: true,
    },
    {
      Name: "Added",
      Value: true,
    },
  ];

  ColumnConfig: any = [
    {
      Name: "Status",
      Value: true,
    },
    {
      Name: "City",
      Value: true,
    },
    {
      Name: "Contact",
      Value: true,
    },
    {
      Name: "Stores",
      Value: true,
    },
    {
      Name: "Reward",
      Value: true,
    },
    {
      Name: "POS",
      Value: true,
    },
    {
      Name: "ActivePOS",
      Value: true,
    },
    {
      Name: "RM",
      Value: true,
    },
    {
      Name: "Added",
      Value: true,
    },
  ];

  InitColConfig() {
    var MerchantTableConfig = this._HelperService.GetStorage("BMerchantTable");
    var ColConfigExist: boolean =
      MerchantTableConfig != undefined && MerchantTableConfig != null;
    if (ColConfigExist) {
      this.ColumnConfig = MerchantTableConfig.config;
      this.TempColumnConfig = this._HelperService.CloneJson(
        MerchantTableConfig.config
      );
    }
  }

  OpenEditColModal() {
    this._HelperService.OpenModal("EditCol");
  }

  SaveEditCol() {
    this.ColumnConfig = this._HelperService.CloneJson(this.TempColumnConfig);
    this._HelperService.SaveStorage("BMerchantTable", {
      config: this.ColumnConfig,
    });
    this._HelperService.CloseModal("EditCol");
  }

  //#endregion

  //#region merchantlist
  TUTr_InvoiceRangeMinAmount: number = this._HelperService.AppConfig.RangeInvoiceAmountMinimumLimit;
  TUTr_InvoiceRangeMaxAmount: number = this._HelperService.AppConfig.RangeInvoiceAmountMaximumLimit;
  public TransactionList_Config: OList;
  TransactionList_Setup() {
    this.TransactionList_Config = {
      Id: null,
      // Sort: null,
      Task: this._HelperService.AppConfig.Api.ThankUCash.getsaletransactions,
      Location: this._HelperService.AppConfig.NetworkLocation.V3.Transaction,
      Title: "Available Transaction",
      StatusType: "CashOut",
      Type: this._HelperService.AppConfig.TerminalTypes.all,
      AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
      AccountId: this._HelperService.AppConfig.ActiveOwnerId,
      // DefaultSortExpression: "CreateDate desc",
      Status: this._HelperService.AppConfig.StatusList.OnHoldTransaction,
      StatusName: 'On Hold',

      IsDownload: true,
      Sort:
      {
        SortDefaultName: null,
        SortDefaultColumn: 'TransactionDate',
        SortName: null,
        SortColumn: null,
        SortOrder: 'desc',
        SortOptions: [],
      },
      // SearchBaseCondition: this._HelperService.GetSearchConditionStrict('', 'StatusId', this._HelperService.AppConfig.DataType.Number, 2, '=='),
      TableFields: [
        {
          DisplayName: '#',
          SystemName: 'ReferenceId',
          DataType: this._HelperService.AppConfig.DataType.Number,
          Show: true,
          Search: false,
          Sort: false,
        },
        {
          DisplayName: 'Date',
          SystemName: 'TransactionDate',
          DataType: this._HelperService.AppConfig.DataType.Date,
          Show: true,
          Search: true,
          Sort: true,
          IsDateSearchField: true,
        },
        {
          DisplayName: 'Transaction Status',
          SystemName: 'StatusName',
          DataType: this._HelperService.AppConfig.DataType.Date,
          Show: true,
          Search: false,
          Sort: false,
        },
        {
          DisplayName: 'Customer',
          SystemName: 'UserDisplayName',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Show: true,
          Search: true,
          Sort: true,
        },
        {
          DisplayName: 'User Mobile Number',
          SystemName: 'UserMobileNumber',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Show: true,
          Search: true,
          Sort: false,
        },
        {
          DisplayName: 'Type',
          SystemName: 'TypeName',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Show: true,
          Search: false,
          Sort: false,
        },
        {
          DisplayName: 'Sale Amount',
          SystemName: 'InvoiceAmount',
          DataType: this._HelperService.AppConfig.DataType.Decimal,
          Show: true,
          Search: false,
          Sort: true,
        },
        {
          DisplayName: 'Reward Amount',
          SystemName: 'RewardAmount',
          DataType: this._HelperService.AppConfig.DataType.Decimal,
          Class: 'text-grey',
          Search: false,
          Sort: false,
        },
        {
          DisplayName: 'User Reward Amount',
          SystemName: 'UserAmount',
          DataType: this._HelperService.AppConfig.DataType.Decimal,
          Show: true,
          Search: false,
          Sort: false,
        },
        {
          DisplayName: 'Commission Amount',
          SystemName: 'CommissionAmount',
          DataType: this._HelperService.AppConfig.DataType.Decimal,
          Show: false,
          Search: false,
          Sort: false,
        },
        {
          DisplayName: 'Card Number',
          SystemName: 'AccountNumber',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Show: true,
          Search: false,
          Sort: false,
        },
        {
          DisplayName: 'Card Type',
          SystemName: 'CardBrandName',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Show: true,
          Search: false,
          Sort: false,
        },
        {
          DisplayName: 'Card Bank Provider',
          SystemName: 'CardBankName',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Show: true,
          Search: false,
          Sort: false,
        },
        {
          DisplayName: 'TUC Card Number',
          SystemName: 'TUCCardNumber',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Show: true,
          Search: false,
          Sort: false,
        },
        {
          DisplayName: 'Merchant',
          SystemName: 'ParentDisplayName',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Show: true,
          Search: false,
          Sort: false,
        },
        {
          DisplayName: 'Store',
          SystemName: 'SubParentDisplayName',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Show: true,
          Search: false,
          Sort: false,
        },
        {
          DisplayName: 'Acquirer / Bank',
          SystemName: 'AcquirerDisplayName',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Show: true,
          Search: false,
          Sort: false,
        },
        {
          DisplayName: 'Terminal Provider',
          SystemName: 'ProviderDisplayName',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Show: true,
          Search: false,
          Sort: false,
        },
        {
          DisplayName: 'Transaction Issuer (Done by)',
          SystemName: 'CreatedByDisplayName',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Show: true,
          Search: false,
          Sort: false,
        },
        {
          DisplayName: 'Cashier Code',
          SystemName: 'CashierCode',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Show: true,
          Search: false,
          Sort: false,
        },
        {
          DisplayName: 'Transaction Reference',
          SystemName: 'ReferenceNumber',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: '',
          Show: true,
          Search: false,
          Sort: false,
          ResourceId: null,
        },


      ]
    };
    this.TransactionList_Config = this._DataHelperService.List_Initialize(
      this.TransactionList_Config
    );

    this._HelperService.Active_FilterInit(
      this._HelperService.AppConfig.FilterTypeOption.Terminal,
      this.TransactionList_Config
    );

    this.TransactionList_GetData();
  }
  TransactionList_ToggleOption(event: any, Type: any) {
    if (Type == this._HelperService.AppConfig.ListToggleOption.SalesRange) {
      event.data = {
        SalesMin: this.TUTr_InvoiceRangeMinAmount,
        SalesMax: this.TUTr_InvoiceRangeMaxAmount
      }
    }


    if (event != null) {
      for (let index = 0; index < this.TransactionList_Config.Sort.SortOptions.length; index++) {
        const element = this.TransactionList_Config.Sort.SortOptions[index];
        if (event.SystemName == element.SystemName) {
          element.SystemActive = true;
        }
        else {
          element.SystemActive = false;
        }
      }
    }

    this._HelperService.Update_CurrentFilterSnap(
      event,
      Type,
      this.TransactionList_Config


    );

    this.TransactionList_Config = this._DataHelperService.List_Operations(
      this.TransactionList_Config,
      event,
      Type
    );

    if (
      (this.TransactionList_Config.RefreshData == true)
      && this._HelperService.DataReloadEligibility(Type)
    ) {
      this.TransactionList_GetData();
    }

  }

  timeout = null;
  TransactionList_ToggleOptionSearch(event: any, Type: any) {

    clearTimeout(this.timeout);

    this.timeout = setTimeout(() => {
      if (event != null) {
        for (let index = 0; index < this.TransactionList_Config.Sort.SortOptions.length; index++) {
          const element = this.TransactionList_Config.Sort.SortOptions[index];
          if (event.SystemName == element.SystemName) {
            element.SystemActive = true;
          }
          else {
            element.SystemActive = false;
          }
        }
      }

      this._HelperService.Update_CurrentFilterSnap(
        event,
        Type,
        this.TransactionList_Config


      );

      this.TransactionList_Config = this._DataHelperService.List_Operations(
        this.TransactionList_Config,
        event,
        Type
      );

      if (
        (this.TransactionList_Config.RefreshData == true)
        && this._HelperService.DataReloadEligibility(Type)
      ) {
        this.TransactionList_GetData();
      }
    }, this._HelperService.AppConfig.SearchInputDelay);


  }

  public OverviewData: any = {
    Total: 0,
    Active: 0,
    Idle: 0,
    Dead: 0,
    Unused: 0
  };

  TransactionList_GetData() {
    // this.GetOverviews(this.TransactionList_Config, this._HelperService.AppConfig.Api.ThankUCash.TransactionOverviews.getterminalsoverview);
    var TConfig = this._DataHelperService.List_GetDataTur(
      this.TransactionList_Config
    );
    this.TransactionList_Config = TConfig;
  }
  CurrentData: any = {

  }
  TransactionList_RowSelected(ReferenceData) {
    this._HelperService.SaveStorage(
      this._HelperService.AppConfig.Storage.ActiveTerminal,
      {
        ReferenceKey: ReferenceData.ReferenceKey,
        ReferenceId: ReferenceData.ReferenceId,
        DisplayName: ReferenceData.DisplayName,
        AccountTypeCode: this._HelperService.AppConfig.AccountType.PosTerminal,
      }
    );

    this._HelperService.AppConfig.ActiveReferenceKey =
      ReferenceData.ReferenceKey;
    this._HelperService.AppConfig.ActiveReferenceId = ReferenceData.ReferenceId;
    this.CurrentData = ReferenceData
    this.CurrentData.StatusB = this._HelperService.GetStatusBadge(this.CurrentData.StatusCode);
    console.log("this.CurrentData", this.CurrentData)

    this.clicked()
    // this._Router.navigate([
    //   this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.Terminal
    //     .Dashboard,
    //   ReferenceData.ReferenceKey,
    //   ReferenceData.ReferenceId,
    // ]);

    // this._Router.navigate([
    //   this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.Terminal
    //     .SalesHistory,
    //   ReferenceData.ReferenceKey,
    //   ReferenceData.ReferenceId,
    // ]);


  }


  SetSalesRanges(): void {
    this.TUTr_InvoiceRangeMinAmount = this._HelperService.AppConfig.RangeInvoiceAmountMinimumLimit;
    this.TUTr_InvoiceRangeMaxAmount = this._HelperService.AppConfig.RangeInvoiceAmountMaximumLimit;
  }

  //#endregion

  //#region Store

  public TransactionList_Filter_Store_Option: Select2Options;
  public TransactionList_Filter_Store_Selected = 0;
  TransactionList_Filter_Stores_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetStores,
      Location: this._HelperService.AppConfig.NetworkLocation.V3.Account,
      AccountId: this._HelperService.UserAccount.AccountId,
      AccountKey: this._HelperService.UserAccount.AccountKey,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },
        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true,
        },
        // {
        //   SystemName: "AccountTypeCode",
        //   Type: this._HelperService.AppConfig.DataType.Text,
        //   SearchCondition: "=",
        //   SearchValue: this._HelperService.AppConfig.AccountType.Store
        // }
      ],
    };
    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.TransactionList_Filter_Store_Option = {
      placeholder: "Filter by Store",
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }
  TransactionList_Filter_Stores_Change(event: any) {

    this._HelperService.Update_CurrentFilterSnap(
      event,
      this._HelperService.AppConfig.ListToggleOption.Other,
      this.TransactionList_Config,
      this._HelperService.AppConfig.OtherFilters.Terminal.Store
    );

    this.StoreEventProcessing(event);

  }

  StoreEventProcessing(event: any): void {
    if (event.value == this.TransactionList_Filter_Store_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict(
        "",
        "StoreReferenceId",
        this._HelperService.AppConfig.DataType.Number,
        this.TransactionList_Filter_Store_Selected,
        "="
      );
      this.TransactionList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(
        SearchCase,
        this.TransactionList_Config.SearchBaseConditions
      );
      this.TransactionList_Filter_Store_Selected = 0;
    } else if (event.value != this.TransactionList_Filter_Store_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict(
        "",
        "StoreReferenceId",
        this._HelperService.AppConfig.DataType.Number,
        this.TransactionList_Filter_Store_Selected,
        "="
      );
      this.TransactionList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(
        SearchCase,
        this.TransactionList_Config.SearchBaseConditions
      );
      this.TransactionList_Filter_Store_Selected = event.value;
      this.TransactionList_Config.SearchBaseConditions.push(
        this._HelperService.GetSearchConditionStrict(
          "",
          "StoreReferenceId",
          this._HelperService.AppConfig.DataType.Number,
          this.TransactionList_Filter_Store_Selected,
          "="
        )
      );
    }
    this.TransactionList_ToggleOption(
      null,
      this._HelperService.AppConfig.ListToggleOption.ResetOffset
    );
  }

  //#endregion

  //#region Provider

  public TransactionList_Filter_Provider_Option: Select2Options;
  public TransactionList_Filter_Provider_Selected = 0;
  TransactionList_Filter_Providers_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
      Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },
        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true,
        },
        {
          SystemName: "AccountTypeCode",
          Type: this._HelperService.AppConfig.DataType.Text,
          SearchCondition: "=",
          SearchValue: this._HelperService.AppConfig.AccountType.PosAccount,
        },
      ],
    };
    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.TransactionList_Filter_Provider_Option = {
      placeholder: "Filter by PTSP",
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }
  TransactionList_Filter_Providers_Change(event: any) {

    this._HelperService.Update_CurrentFilterSnap(
      event,
      this._HelperService.AppConfig.ListToggleOption.Other,
      this.TransactionList_Config,
      this._HelperService.AppConfig.OtherFilters.Terminal.Provider
    );

    this.ProvderEventProcessing(event);

  }

  ProvderEventProcessing(event): void {
    if (event.value == this.TransactionList_Filter_Provider_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict(
        "",
        "ProviderId",
        this._HelperService.AppConfig.DataType.Number,
        this.TransactionList_Filter_Provider_Selected,
        "="
      );
      this.TransactionList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(
        SearchCase,
        this.TransactionList_Config.SearchBaseConditions
      );
      this.TransactionList_Filter_Provider_Selected = 0;
    } else if (event.value != this.TransactionList_Filter_Provider_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict(
        "",
        "ProviderId",
        this._HelperService.AppConfig.DataType.Number,
        this.TransactionList_Filter_Provider_Selected,
        "="
      );
      this.TransactionList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(
        SearchCase,
        this.TransactionList_Config.SearchBaseConditions
      );
      this.TransactionList_Filter_Provider_Selected = event.value;
      this.TransactionList_Config.SearchBaseConditions.push(
        this._HelperService.GetSearchConditionStrict(
          "",
          "ProviderId",
          this._HelperService.AppConfig.DataType.Number,
          this.TransactionList_Filter_Provider_Selected,
          "="
        )
      );
    }
    this.TransactionList_ToggleOption(
      null,
      this._HelperService.AppConfig.ListToggleOption.ResetOffset
    );
  }

  //#endregion
  //#region Bank 

  public TransactionList_Filter_Bank_Option: Select2Options;
  public TransactionList_Filter_Bank_Selected = 0;
  TransactionList_Filter_Banks_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
      Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },
        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        },
        {
          SystemName: "AccountTypeCode",
          Type: this._HelperService.AppConfig.DataType.Text,
          SearchCondition: "=",
          SearchValue: this._HelperService.AppConfig.AccountType.Acquirer
        }
      ]
    };
    // S2Data.SearchBaseCondition = this._HelperService.GetSearchConditionStrict('', 'TypeCode', this._HelperService.AppConfig.DataType.Text, Condition, '=');
    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.TransactionList_Filter_Bank_Option = {
      placeholder: 'Filter by Bank',
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }
  TransactionList_Filter_Banks_Change(event: any) {
    this._HelperService.Update_CurrentFilterSnap(
      event,
      this._HelperService.AppConfig.ListToggleOption.Other,
      this.TransactionList_Config,
      this._HelperService.AppConfig.OtherFilters.Terminal.Bank
    );

    this.BankEventProcessing(event);
  }

  BankEventProcessing(event): void {
    if (event.value == this.TransactionList_Filter_Bank_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict('', 'AcquirerId', this._HelperService.AppConfig.DataType.Number, this.TransactionList_Filter_Bank_Selected, '=');
      this.TransactionList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TransactionList_Config.SearchBaseConditions);
      this.TransactionList_Filter_Bank_Selected = 0;
    }
    else if (event.value != this.TransactionList_Filter_Bank_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict('', 'AcquirerId', this._HelperService.AppConfig.DataType.Number, this.TransactionList_Filter_Bank_Selected, '=');
      this.TransactionList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TransactionList_Config.SearchBaseConditions);
      this.TransactionList_Filter_Bank_Selected = event.value;
      this.TransactionList_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'AcquirerId', this._HelperService.AppConfig.DataType.Number, this.TransactionList_Filter_Bank_Selected, '='));
    }
    this.TransactionList_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
  }

  //#endregion

  SetOtherFilters(): void {
    this.TransactionList_Config.SearchBaseConditions = [];
    // this.TransactionList_Config.SearchBaseCondition = null;

    var CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.Merchant.Owner));
    if (CurrentIndex != -1) {
      // this.TransactionList_Filter_Owners_Selected = null;
      // this.OwnerEventProcessing(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
    }
  }
  ToogleType(type: string): void {
    this.TransactionList_Config.Type = type;
    this.TransactionList_GetData();
  }
  // ToogleStatusType(type: any): void {
  //   console.log(type.value);
  //   switch (type.value) {
  //     case '0': this.TransactionList_Config.Type = this._HelperService.AppConfig.TerminalTypes.all

  //       break;
  //     case '547': this.TransactionList_Config.Type = this._HelperService.AppConfig.TerminalTypes.active

  //       break;
  //     case '548': {
  //       this.TransactionList_Config.Type = this._HelperService.AppConfig.TerminalTypes.idle
  //     }
  //       break;
  //     case '549': this.TransactionList_Config.Type = this._HelperService.AppConfig.TerminalTypes.dead

  //       break;
  //     case '550': this.TransactionList_Config.Type = this._HelperService.AppConfig.TerminalTypes.unused

  //       break;

  //     default:
  //       break;
  //   }

  //   // this.TransactionList_Config.Type = type;
  //   // this.TransactionList_GetData();
  // }

  //#region filterOperations

  Active_FilterValueChanged(event: any) {
    this._HelperService.Active_FilterValueChanged(event);
    this._FilterHelperService.SetMerchantConfig(this.TransactionList_Config);

    //#region setOtherFilters
    this.SetOtherFilters();
    this.SetSalesRanges();
    //#endregion

    this.TransactionList_GetData();
  }

  RemoveFilterComponent(Type: string, index?: number): void {
    this._FilterHelperService._RemoveFilter_MerchantSales(Type, index);
    this._FilterHelperService.SetMerchantConfig(this.TransactionList_Config);

    this.SetOtherFilters();
    this.SetSalesRanges();

    this.TransactionList_GetData();
  }

  Save_NewFilter() {
    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.SaveFilterTitle,
      text: this._HelperService.AppConfig.CommonResource.SaveFilterHelp,
      input: "text",
      inputPlaceholder: this._HelperService.AppConfig.CommonResource.FilterName,
      inputAttributes: {
        autocapitalize: "off",
        autocorrect: "off",
        maxLength: "32",
        minLength: "4",
      },
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Green,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: "Save",
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
    }).then((result) => {
      if (result.value) {
        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._FilterHelperService._BuildFilterName_Merchant(result.value);
        this._HelperService.Save_NewFilter(
          this._HelperService.AppConfig.FilterTypeOption.Terminal
        );

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });
  }
  Delete_Confirm() {
    swal({
      position: 'top',
      title: this._HelperService.AppConfig.CommonResource.DeleteBankTitle,
      text: this._HelperService.AppConfig.CommonResource.DeleteBankHelp,
      // input: 'password',
      // inputPlaceholder: this._HelperService.AppConfig.CommonResource.AccessPin,
      // inputAttributes: {
      //   autocapitalize: 'off',
      //   autocorrect: 'off',
      //   maxLength: "4",
      //   minLength: "4"
      // },
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
    }).then((result) => {
      if (result.value) {
        this._HelperService.IsFormProcessing = true;
        var pData = {
          Task: this._HelperService.AppConfig.Api.ThankUCash.deletebankaccount,
          AccountId: this._HelperService.AppConfig.ActiveOwnerId,
          AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
          ReferenceId: this.CurrentData.ReferenceId,
          ReferenceKey: this.CurrentData.ReferenceKey,
          // AuthPin: result.value
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Bank, pData);
        _OResponse.subscribe(
          _Response => {
            this._HelperService.IsFormProcessing = false;
            if (_Response.Status == this._HelperService.StatusSuccess) {
              this._HelperService.NotifySuccess(_Response.Message);
              this.unclick();
              this.TransactionList_Setup();
            }
            else {
              this._HelperService.NotifyError(_Response.Message);
            }
          },
          _Error => {

            this._HelperService.HandleException(_Error);
          });
      }
    });
  }


  Delete_Filter() {

    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.DeleteTitle,
      text: this._HelperService.AppConfig.CommonResource.DeleteHelp,
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel

    }).then((result) => {
      if (result.value) {
        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._HelperService.Delete_Filter(
          this._HelperService.AppConfig.FilterTypeOption.Terminal
        );
        this._FilterHelperService.SetMerchantConfig(this.TransactionList_Config);
        this.TransactionList_GetData();

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });

  }

  ApplyFilters(event: any, Type: any, ButtonType: any): void {
    this._HelperService.MakeFilterSnapPermanent();
    this.SetSearchRanges();
    this.TransactionList_GetData();

    if (ButtonType == 'Sort') {
      $("#TransactionList_sdropdown").dropdown('toggle');
    } else if (ButtonType == 'Other') {
      $("#TransactionList_fdropdown").dropdown('toggle');
    }
    this.TransactionList_GetData();
    this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  }

  ResetFilters(event: any, Type: any): void {
    this._HelperService.ResetFilterSnap();
    this._FilterHelperService.SetMerchantConfig(this.TransactionList_Config);
    this.SetOtherFilters();
    this.SetSalesRanges();
    this.TransactionList_GetData();

    this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  }

  //#endregion

  ResetFilterUI(): void {
    this.ResetFilterControls = false;
    this._ChangeDetectorRef.detectChanges();

    this.ResetFilterControls = true;
    this._ChangeDetectorRef.detectChanges();
  }
  public TUTr_Filter_Bank_Option: Select2Options;
  TUTr_Filter_Banks_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.getbankaccounts,
      Location: this._HelperService.AppConfig.NetworkLocation.V3.Bank,
      AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
      AccountId: this._HelperService.AppConfig.ActiveOwnerId,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: true,
          Text: false,
        },
        {
          SystemName: "Name",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        },
        // {
        //   SystemName: "Code",
        //   Type: this._HelperService.AppConfig.DataType.Text,
        //   Id: false,
        //   Text: true
        // }
      ]

    };
    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.TUTr_Filter_Bank_Option = {
      placeholder: 'Select Bank',
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }
  TUTr_Filter_Banks_Change(event: any) {
    console.log("dddd", event.data[0])
    this.Form_AddUser.patchValue(
      {
        BankId: event.data[0].ReferenceId,
        BankKey: event.data[0].ReferenceKey,
        // BankAccountNumber:event.data[0].AccountNumber,
      });
  }
  Form_AddUser: FormGroup;
  Form_AddUser_Show() {
    this._HelperService.Icon_Crop_Clear();
    this._HelperService.OpenModal("Form_AddUser_Content");

  }
  Form_AddUser_Close() {
    this._HelperService._FileSelect_Icon_Reset();
    this._HelperService.CloseModal("Form_AddUser_Content");
  }
  Form_AddUser_Load() {
    this._HelperService._FileSelect_Icon_Data.Width = 128;
    this._HelperService._FileSelect_Icon_Data.Height = 128;

    this._HelperService._FileSelect_Poster_Data.Width = 800;
    this._HelperService._FileSelect_Poster_Data.Height = 400;

    this.Form_AddUser = this._FormBuilder.group({
      OperationType: "new",
      Task: this._HelperService.AppConfig.Api.ThankUCash.CashoutInitialize,
      AccountId: this._HelperService.AppConfig.ActiveOwnerId,
      AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
      BankId: [null, Validators.required],
      BankKey: [null, Validators.required],
      // BankAccountNumber: [null, Validators.required],

      StatusCode: this._HelperService.AppConfig.Status.Active,
      Amount: [null, Validators.compose([Validators.required, Validators.min(1), Validators.maxLength(20)])],

    });
  }
  Form_AddUser_Clear() {
    this.Form_AddUser.reset();
    this._HelperService._FileSelect_Icon_Reset();
    this._HelperService._FileSelect_Poster_Reset();
    this.Form_AddUser_Load();
    this._HelperService.GetRandomNumber();
    this._HelperService.GeneratePassoword();
  }
  Form_AddUser_Process(_FormValue: any) {

    swal({
      position: 'top',
      title: this._HelperService.AppConfig.CommonResource.RequestCashOut,
      text: this._HelperService.AppConfig.CommonResource.RequestCashoutHelp,
      // input: 'password',
      // inputPlaceholder: this._HelperService.AppConfig.CommonResource.AccessPin,
      // inputAttributes: {
      //   autocapitalize: 'off',
      //   autocorrect: 'off',
      //   maxLength: "4",
      //   minLength: "4"
      // },
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
    }).then((result) => {
      if (result.value) {


        this._HelperService.IsFormProcessing = true;
        let _OResponse: Observable<OResponse>;
        if (true) {
          _OResponse = this._HelperService.PostData(
            this._HelperService.AppConfig.NetworkLocation.V3.Payments,
            _FormValue
          );
          _OResponse.subscribe(
            _Response => {
              this._HelperService.IsFormProcessing = false;
              if (_Response.Status == this._HelperService.StatusSuccess) {
                this._HelperService.FlashSwalSuccess("New Cashout initialized successfully",
                  "You have initialized New Cashout");
                this._HelperService.ObjectCreated.next(true);
                this.Form_AddUser_Clear();
                this.Form_AddUser_Close();
                if (_FormValue.OperationType == "close") {
                  this.Form_AddUser_Close();
                }
                // this._HelperService.GetAccountCount(this._HelperService.AppConfig.ActiveOwnerKey, this._HelperService.AppConfig.ActiveOwnerId, this._HelperService.AppConfig.DefaultStartTimeAll, this._HelperService.AppConfig.DefaultEndTimeToday)

              } else {
                this._HelperService.NotifyError(_Response.Message);
              }
            },
            _Error => {
              this._HelperService.IsFormProcessing = false;
              this._HelperService.HandleException(_Error);
            }
          );
          return;
        }


        else if (this._HelperService.AppConfig.TerminalPermission.MinimumLimit <= this._HelperService.AppConfig.TerminalsCount && this._HelperService.AppConfig.TerminalPermission.MaximumLimit > this._HelperService.AppConfig.TerminalsCount) {
          _OResponse = this._HelperService.PostData(
            this._HelperService.AppConfig.NetworkLocation.V3.Account,
            _FormValue
          );
          _OResponse.subscribe(
            _Response => {
              this._HelperService.IsFormProcessing = false;
              if (_Response.Status == this._HelperService.StatusSuccess) {
                this._HelperService.FlashSwalSuccess("New pos terminal has been added successfully",
                  "You have successfully created new POS terminal");
                this._HelperService.ObjectCreated.next(true);
                this.Form_AddUser_Clear();
                this.Form_AddUser_Close();
                if (_FormValue.OperationType == "close") {
                  this.Form_AddUser_Close();
                }
                this._HelperService.GetAccountCount(this._HelperService.AppConfig.ActiveOwnerKey, this._HelperService.AppConfig.ActiveOwnerId, this._HelperService.AppConfig.DefaultStartTimeAll, this._HelperService.AppConfig.DefaultEndTimeToday)

              } else {
                this._HelperService.NotifyError(_Response.Message);
              }
            },
            _Error => {
              this._HelperService.IsFormProcessing = false;
              this._HelperService.HandleException(_Error);
            }
          );
        } else {
          this._HelperService.NotifyError('Upgrade Your Subscription')
          this.Form_AddUser_Close();
          this._Router.navigate(['m' + '/' + this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.Upgrade]);
          return;
        }
      }
    });
  }

  GetOverviews(ListOptions: any, Task: string): any {
    this._HelperService.IsFormProcessing = true;

    ListOptions.SearchCondition = '';
    ListOptions = this._DataHelperService.List_GetSearchCondition(ListOptions);
    if (ListOptions.ActivePage == 1) {
      ListOptions.RefreshCount = true;
    }
    var SortExpression = ListOptions.Sort.SortDefaultColumn + ' ' + ListOptions.Sort.SortDefaultOrder;;

    if (ListOptions.Sort.SortDefaultName) {
      ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName.replace(' desc', '');
      ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName.replace(' asc', '');
      ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName + ' ' + ListOptions.Sort.SortDefaultOrder;
    }

    if (ListOptions.Sort.SortColumn != undefined && ListOptions.Sort.SortColumn != null && ListOptions.Sort.SortColumn != '') {
      if (ListOptions.Sort.SortOrder != undefined && ListOptions.Sort.SortOrder != null && ListOptions.Sort.SortOrder != '') {
        SortExpression = ListOptions.Sort.SortColumn + ' ' + ListOptions.Sort.SortOrder;
      }
      else {
        SortExpression = ListOptions.Sort.SortColumn + ' desc';
      }
    }

    // var SearchCondition = '';
    // if (ListOptions.SearchCondition.includes('StatusCode')) {
    //   SearchCondition = '';
    // }else{
    //   SearchCondition = '';
    // }


    var pData = {
      Task: Task,
      TotalRecords: ListOptions.TotalRecords,
      Offset: (ListOptions.ActivePage - 1) * ListOptions.PageRecordLimit,
      Limit: ListOptions.PageRecordLimit,
      RefreshCount: ListOptions.RefreshCount,
      SearchCondition: ListOptions.SearchCondition,
      SortExpression: SortExpression,
      Type: ListOptions.Type,
      ReferenceKey: ListOptions.ReferenceKey,
      StartDate: ListOptions.StartDate,
      EndDate: ListOptions.EndDate,
      ReferenceId: ListOptions.ReferenceId,
      SubReferenceId: ListOptions.SubReferenceId,
      SubReferenceKey: ListOptions.SubReferenceKey,
      AccountId: ListOptions.AccountId,
      AccountKey: ListOptions.AccountKey,
      ListType: ListOptions.ListType,
      IsDownload: false,
    };

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Account, pData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this.OverviewData = _Response.Result.Data as any;


        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);

      });


  }


  public _CashOutConfiguration: any = {}
  GetCashOutConfiguration() {

    this._HelperService.IsFormProcessing = true;
    var Data = {
      Task: 'getcashoutconfiguration',
      AccountId: this._HelperService.AppConfig.ActiveOwnerId,
      AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,


    };
    console.log(this._HelperService.AppConfig.ActiveBankCode)

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Payments, Data);
    _OResponse.subscribe(
      _Response => {



        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._CashOutConfiguration = _Response.Result;

          // Open To See Dummy Data

          // this._LoyalityOverview = {
          //     NewCustomers: 0,
          //     RepeatingCustomers: 0,
          //     VisitsByRepeatingCustomers: 0,
          //     NewCustomerInvoiceAmount: 0.0,
          //     RepeatingCustomerInvoiceAmount: 0.0,
          //     Transaction: 520.0,
          //     TransactionInvoiceAmount: 0.0,

          //     RewardTransaction: 210.0,
          //     RewardAmount: 0.0,
          //     NonRewardTransaction: 0.0,

          //     TucRewardTransaction: 510.0,
          //     TucPlusRewardTransaction: 0,
          //     TucPlusRewardClaimTransaction: 0,

          //     RedeemTransaction: 0,

          //     RewardTransactionPerc: 210.0,
          //     NonRewardTransactionPerc: 0.0,
          // }



        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
        //#endregion



      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }



  Approve() {
    swal({
      title: 'Update Details',

      // text: this._HelperService.AppConfig.CommonResource.UpdateHelp,
      position: this._HelperService.AppConfig.Alert_Position,
      animation: this._HelperService.AppConfig.Alert_AllowAnimation,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      allowOutsideClick: this._HelperService.AppConfig.Alert_AllowOutsideClick,
      allowEscapeKey: this._HelperService.AppConfig.Alert_AllowEscapeKey,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
      showCancelButton: true,
      html:
        ' <label> Do you really want to Approve?  </label>' +

        '<input type="text" placeholder="Comment"  id="swal-input2" class="swal2-input">',


      focusConfirm: false,
      preConfirm: () => {
        return [

          document.getElementById('swal-input2')['value']
        ]
      },
      inputPlaceholder: this._HelperService.AppConfig.CommonResource.AccessPin,
      inputAttributes: {
        autocapitalize: 'off',
        autocorrect: 'off',
        maxLength: "4",
        minLength: "4"
      },
    }).then((result) => {
      if (result.value) {

        this._HelperService.IsFormProcessing = true;
        var PostData = {
          Task: "updatetransaction",
          AccountId: this._HelperService.AppConfig.ActiveOwnerId,
          AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
          ReferenceKey: this.CurrentData.ReferenceKey,
          ReferenceId: this.CurrentData.ReferenceId,
          StatusCode: "transaction.success",
          Comment: result.value[1],



        };

        let _OResponse: Observable<OResponse>;

        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Transaction, PostData);
        _OResponse.subscribe(
          _Response => {
            this._HelperService.IsFormProcessing = false;
            if (_Response.Status == this._HelperService.StatusSuccess) {
              this._HelperService.NotifySuccess("Transaction Approved Successfully");

              this.unclick();

            } else {
              this._HelperService.NotifySuccess(_Response.Message);
            }
          },
          _Error => {
            this._HelperService.IsFormProcessing = false;
            this._HelperService.HandleException(_Error);
          }
        );
      }
    });


  }
  public _CoreUsage: OCoreUsage =
    {
      AccountDisplayName: null,
      BankName: null,
      BankAccountNumber: null,
      BankAccountName: null,

      Charge: null,
      TotalAmount: null,
      ReferenceNumber: null,
      CreatedByDisplayName: null,
      ModifyByDisplayName: null,
      SystemComment: null,

      StatusB: null,
      ProductCategoryName: null,
      PaymentReference: null,
      ProductItemName: null,
      UserRewardAmount: null,
      Amount: null,
      RewardAmount: null,
      Coordinates: null,
      CommisonAmount: null,
      StartDate: null,
      EndDate: null,
      ModifyDate: null,
      CreateDate: null,



      AccountMobileNumber: null,
      AccountKey: null, AccountId: null,
      ReferenceKey: null, ReferenceId: null, Reference: null, StatusCode: null, StatusId: null, StatusName: null, UserAccountKey: null, UserAccountIconUrl: null, UserAccountDisplayName: null, ApiKey: null, ApiName: null, AppKey: null, AppName: null, AppOwnerKey: null, AppOwnerName: null, AppVersionKey: null, AppVersionName: null, FeatureKey: null, FeatureName: null, IpAddress: null, Latitude: null, Longitude: null, ProcessingTime: null, Request: null, RequestTime: null, Response: null, ResponseTime: null, SessionId: null, SessionKey: null, UserAccountTypeCode: null, UserAccountTypeName: null,
    }


  Reject() {
    swal({
      title: 'Update Details',
      // text: this._HelperService.AppConfig.CommonResource.UpdateHelp,
      position: this._HelperService.AppConfig.Alert_Position,
      animation: this._HelperService.AppConfig.Alert_AllowAnimation,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      allowOutsideClick: this._HelperService.AppConfig.Alert_AllowOutsideClick,
      allowEscapeKey: this._HelperService.AppConfig.Alert_AllowEscapeKey,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
      showCancelButton: true,
      html:
        ' <label> Do you really want to Reject?  </label>' +
        '<input type="text" placeholder="Enter Comment"  id="swal-input1" class="swal2-input">',


      focusConfirm: false,
      preConfirm: () => {
        return [
          document.getElementById('swal-input1')['value'],

        ]
      },
      // inputPlaceholder: this._HelperService.AppConfig.CommonResource.AccessPin,
      // inputAttributes: {
      //   autocapitalize: 'off',
      //   autocorrect: 'off',
      //   maxLength: "4",
      //   minLength: "4"
      // },
    }).then((result) => {
      if (result.value) {

        this._HelperService.IsFormProcessing = true;
        var PostData = {
          Task: "updatetransaction",
          AccountId: this._HelperService.AppConfig.ActiveOwnerId,
          AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
          ReferenceKey: this.CurrentData.ReferenceKey,
          ReferenceId: this.CurrentData.ReferenceId,
          StatusCode: "transaction.rejected",
          // AuthPin: result.value[1],
          Comment: result.value[0],

        };

        let _OResponse: Observable<OResponse>;

        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Transaction, PostData);
        _OResponse.subscribe(
          _Response => {
            this._HelperService.IsFormProcessing = false;
            if (_Response.Status == this._HelperService.StatusSuccess) {
              this._HelperService.NotifySuccess("Transaction Rejected Successfully");
              this.unclick();

            } else {
              this._HelperService.NotifySuccess(_Response.Message);
            }
          },
          _Error => {
            this._HelperService.IsFormProcessing = false;
            this._HelperService.HandleException(_Error);
          }
        );
      }
    });


  }

}

export class OCoreUsage {

  public UserRewardAmount: number;
  public BankAccountNumber: number;
  public Charge: number;
  public TotalAmount: number;
  public AccountDisplayName: string;
  public BankName: string;
  public ReferenceNumber: string;
  public CreatedByDisplayName: string;
  public BankAccountName: string;
  public ModifyByDisplayName: string;
  public SystemComment: string;
  public ProductCategoryName: string;
  public ProductItemName: string;
  public RewardAmount: number;
  public Amount: number;
  public CommisonAmount: number;
  public AccountMobileNumber: number;
  public PaymentReference: string;
  public StatusB: string;
  Coordinates?: any;
  public ReferenceId: number;
  public AccountId: number;
  public AccountKey: string;
  public Reference: string;
  public ReferenceKey: string;
  public AppOwnerKey: string;
  public AppOwnerName: string;
  public AppKey: string;
  public AppName: string;
  public ApiKey: string;
  public ApiName: string;
  public AppVersionKey: string;
  public AppVersionName: string;
  public FeatureKey: string;
  public FeatureName: string;
  public UserAccountKey: string;
  public UserAccountDisplayName: string;
  public UserAccountIconUrl: string;
  public UserAccountTypeCode: string;
  public UserAccountTypeName: string;
  public SessionId: string;
  public SessionKey: string;
  public IpAddress: string;
  public Latitude: string;
  public Longitude: string;
  public Request: string;
  public Response: string;
  public RequestTime: Date;
  public StartDate: Date;
  public ModifyDate: Date;
  public EndDate: Date;
  public CreateDate: Date;
  public ResponseTime: Date;
  public ProcessingTime: string;
  public StatusId: number;
  public StatusCode: string;
  public StatusName: string;
}
