import { ChangeDetectorRef, Component, OnInit, OnDestroy, ViewChild } from "@angular/core";
import { FormBuilder, Validators, FormGroup } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import * as Feather from "feather-icons";
import swal from "sweetalert2";
declare var $: any;
declare var moment: any;

import {
  DataHelperService,
  HelperService,
  OList,
  OSelect,
  FilterHelperService,
  OResponse,
} from "../../../../service/service";
import { Observable, Subscription } from 'rxjs';
import { InputFileComponent, InputFile } from 'ngx-input-file';

@Component({
  selector: "tu-tusubaccounts",
  templateUrl: "./tusubaccounts.component.html",
})
export class TUSubAccountsComponent implements OnInit, OnDestroy {
  ispwdContainsNum = true
  ispwdContainsUC = true
  ispwdContainsLC = true
  IsminLength = true
  isPwdContainsSC = true
  ShowPassword: boolean = true;
  titleName: any = '';
  accountKey:any = '';
  accountId:any = ''

  @ViewChild(InputFileComponent)
  private InputFileComponent: InputFileComponent;
  CurrentImagesCount: number = 0;

  ngOnDestroy(): void {
    try {
      this._ObjectSubscription.unsubscribe();
    } catch (error) {

    }

  }
  public ResetFilterControls: boolean = true;
  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef,
    public _FilterHelperService: FilterHelperService
  ) {
    this._HelperService.showAddNewPosBtn = false;
    this._HelperService.showAddNewStoreBtn = false;
    this._HelperService.showAddNewCashierBtn = false;
    this._HelperService.showAddNewSubAccBtn = true;


  }
  public _Roles = [
    // {
    //     'id': 0,
    //     'text': 'Select Role',
    //     'apival': 'Select Role'
    // },
    {
      'id': 10,
      'text': 'Admin',
      'apival': 'merchantadmin'
    },
    {
      'id': 11,
      'text': 'Store Manager',
      'apival': 'merchantstoremanager'
    },

  ];

  public _ObjectSubscription: Subscription = null;

  ngOnInit() {
    this._HelperService.StopClickPropogation();
    Feather.replace();
    this._HelperService.setTheme()
    this.SubAccountsList_Setup();
    this.Form_UpdateSubAccount_Load();
    this.SubAccountsList_Filter_Stores_Load();
    this.InitColConfig();
    this.TUTr_Filter_Stores_Load();
    this.getroles_list();
    // this._HelperService.StopClickPropogation();
    //this.TestNavigation();
    this.ResetPassword_Load()
    this._ObjectSubscription = this._HelperService.ObjectCreated.subscribe(value => {
      this.SubAccountsList_GetData();
    });
  }


  public GetRoles_Option: Select2Options;
  public GetRoles_Transport: any;
  getroles_list() {
    this.GetRoles_Option = {
      placeholder: 'List',
      ajax: this.GetRoles_Transport,
      multiple: false,
    };
  }
  GetRoles_ListChange(event: any) {
    this.Form_UpdateSubAccount.patchValue(
      {
        RoleKey: event.data[0].apival,
        RoleId: event.value

      }
    );
    this.ToggleStoreSelect = false;

    if (this.Form_UpdateSubAccount.controls['RoleKey'].value == 'merchantstoremanager') {
      this.TUTr_Filter_Stores_Load();
      setTimeout(() => {
        this.ToggleStoreSelect = true;
        this.TUTr_Filter_Stores_Load();
      }, 500);
    }
    else {
      this.Form_UpdateSubAccount.patchValue({
        StoreReferenceKey: this._HelperService.AppConfig.ActiveOwnerKey,
        StoreReferenceId: this._HelperService.AppConfig.ActiveOwnerId

      })
    }



  }

  public ToggleStoreSelect: boolean = false;
  public TUTr_Filter_Store_Option: Select2Options;
  TUTr_Filter_Stores_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
      Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceKey",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: true,
          Text: false,
        },
        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        },
        {
          SystemName: "AccountTypeCode",
          Type: this._HelperService.AppConfig.DataType.Text,
          SearchCondition: "=",
          SearchValue: this._HelperService.AppConfig.AccountType.Store
        }
      ]
    };

    var OwnerKey = this._HelperService.UserAccount.AccountId;
    if (this._HelperService.UserAccount.AccountTypeCode != this._HelperService.AppConfig.AccountType.Merchant) {
      OwnerKey = this._HelperService.UserOwner.AccountId;
    }
    _Select.SearchCondition = this._HelperService.GetSearchConditionStrict('', 'OwnerId', this._HelperService.AppConfig.DataType.Number, OwnerKey, '=');
    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.TUTr_Filter_Store_Option = {
      placeholder: 'Select Store',
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }
  TUTr_Filter_Stores_Change(event: any) {



    this.Form_UpdateSubAccount.patchValue(
      {
        StoreReferenceId: event.data[0].ReferenceId,
        StoreReferenceKey: event.data[0].ReferenceKey,
      }


    );

  }

  Form_AddUser_Open() {
    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer
        .MerchantOnboarding,
    ]);
  }

  //#region columnConfig

  TempColumnConfig: any = [
    {
      Name: "Status",
      Value: true,
    },
    {
      Name: "City",
      Value: true,
    },
    {
      Name: "Contact",
      Value: true,
    },
    {
      Name: "Stores",
      Value: true,
    },
    {
      Name: "Reward",
      Value: true,
    },
    {
      Name: "POS",
      Value: true,
    },
    {
      Name: "ActivePOS",
      Value: true,
    },
    {
      Name: "RM",
      Value: true,
    },
    {
      Name: "Added",
      Value: true,
    },
  ];

  ColumnConfig: any = [
    {
      Name: "Status",
      Value: true,
    },
    {
      Name: "City",
      Value: true,
    },
    {
      Name: "Contact",
      Value: true,
    },
    {
      Name: "Stores",
      Value: true,
    },
    {
      Name: "Reward",
      Value: true,
    },
    {
      Name: "POS",
      Value: true,
    },
    {
      Name: "ActivePOS",
      Value: true,
    },
    {
      Name: "RM",
      Value: true,
    },
    {
      Name: "Added",
      Value: true,
    },
  ];

  InitColConfig() {
    var MerchantTableConfig = this._HelperService.GetStorage("BMerchantTable");
    var ColConfigExist: boolean =
      MerchantTableConfig != undefined && MerchantTableConfig != null;
    if (ColConfigExist) {
      this.ColumnConfig = MerchantTableConfig.config;
      this.TempColumnConfig = this._HelperService.CloneJson(
        MerchantTableConfig.config
      );
    }
  }

  OpenEditColModal() {
    this._HelperService.OpenModal("EditCol");
  }

  SaveEditCol() {
    this.ColumnConfig = this._HelperService.CloneJson(this.TempColumnConfig);
    this._HelperService.SaveStorage("BMerchantTable", {
      config: this.ColumnConfig,
    });
    this._HelperService.CloseModal("EditCol");
  }

  //#endregion

  //#region subaccountslist

  public SubAccountsList_Config: OList;
  SubAccountsList_Setup() {
    this.SubAccountsList_Config = {
      Id: null,
      Sort: null,
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetSubAccounts,
      Location: this._HelperService.AppConfig.NetworkLocation.V3.Account,
      Title: "Available SubAccounts",
      StatusType: "default",
      IsDownload: true,
      // Type: this._HelperService.AppConfig.ListType.SubOwner,
      AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
      AccountId: this._HelperService.AppConfig.ActiveOwnerId,
      DefaultSortExpression: "CreateDate desc",
      // SearchBaseCondition: this._HelperService.GetSearchConditionStrict('', 'StatusId', this._HelperService.AppConfig.DataType.Number, 2, '=='),
      TableFields: [
        {
          DisplayName: "Sub-Account Name",
          SystemName: "Name",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "User Name",
          SystemName: "UserName",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null
        },
        {
          DisplayName: "Contact No",
          SystemName: "ContactNumber",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "text-center",
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          DefaultValue: "ThankUCash",
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "Email Address",
          SystemName: "EmailAddress",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: false,
          Sort: false,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "LastActivity On",
          SystemName: "LastActivityDate",
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: "td-date text-right",
          Show: true,
          Search: false,
          Sort: false,
          ResourceId: null,
          NavigateField: "ReferenceKey",
        },
        {
          DisplayName: "Added On",
          SystemName: "CreateDate",
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: "td-date text-right",
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          IsDateSearchField: true,
          NavigateField: "ReferenceKey",
        },
      ]
    };
    this.SubAccountsList_Config = this._DataHelperService.List_Initialize(
      this.SubAccountsList_Config
    );

    this._HelperService.Active_FilterInit(
      this._HelperService.AppConfig.FilterTypeOption.SubAccount,
      this.SubAccountsList_Config
    );

    this.SubAccountsList_GetData();
  }
  SubAccountsList_ToggleOption(event: any, Type: any) {
    if (Type == "date") {
      this._HelperService.AppConfig.DateRangeOptions.startDate = event.start;
      this._HelperService.AppConfig.DateRangeOptions.endDate = event.end;
  }


    if (event != null) {
      for (let index = 0; index < this.SubAccountsList_Config.Sort.SortOptions.length; index++) {
        const element = this.SubAccountsList_Config.Sort.SortOptions[index];
        if (event.SystemName == element.SystemName) {
          element.SystemActive = true;
        }
        else {
          element.SystemActive = false;
        }
      }
    }

    this._HelperService.Update_CurrentFilterSnap(
      event,
      Type,
      this.SubAccountsList_Config


    );

    this.SubAccountsList_Config = this._DataHelperService.List_Operations(
      this.SubAccountsList_Config,
      event,
      Type
    );    
    if (
      (this.SubAccountsList_Config.RefreshData == true)
      && this._HelperService.DataReloadEligibility(Type)
    ) {
      this.SubAccountsList_GetData();
    }

  }
  SubAccountsList_GetData() {
    var TConfig = this._DataHelperService.List_GetData(
      this.SubAccountsList_Config
    );
    this.SubAccountsList_Config = TConfig;
  }
  SubAccountsList_RowSelected(ReferenceData) {

    this.GetSubAccountsDetails(ReferenceData, true);
  }

  //#endregion

  //#region StoreFilter

  public SubAccountsList_Filter_Stores_Option: Select2Options;
  public SubAccountsList_Filter_Stores_Selected = null;
  SubAccountsList_Filter_Stores_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetStores,
      Location: this._HelperService.AppConfig.NetworkLocation.V3.Account,
      AccountKey: this._HelperService.UserAccount.AccountKey,
      AccountId: this._HelperService.UserAccount.AccountId,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },
        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true,
        },
      ],
    };
    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.SubAccountsList_Filter_Stores_Option = {
      placeholder: "Select Store",
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }
  SubAccountsList_Filter_Stores_Change(event: any) {
    this._HelperService.Update_CurrentFilterSnap(
      event,
      this._HelperService.AppConfig.ListToggleOption.Other,
      this.SubAccountsList_Config,
      this._HelperService.AppConfig.OtherFilters.Merchant.Owner
    );

    this.OwnerEventProcessing(event);

  }

  OwnerEventProcessing(event: any): void {
    if (event.value == this.SubAccountsList_Filter_Stores_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict(
        "",
        "StoreReferenceKey",
        this._HelperService.AppConfig.DataType.Text,
        this.SubAccountsList_Filter_Stores_Selected,
        "="
      );
      this.SubAccountsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(
        SearchCase,
        this.SubAccountsList_Config.SearchBaseConditions
      );
      this.SubAccountsList_Filter_Stores_Selected = null;
    } else if (event.value != this.SubAccountsList_Filter_Stores_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict(
        "",
        "StoreReferenceKey",
        this._HelperService.AppConfig.DataType.Text,
        this.SubAccountsList_Filter_Stores_Selected,
        "="
      );
      this.SubAccountsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(
        SearchCase,
        this.SubAccountsList_Config.SearchBaseConditions
      );
      this.SubAccountsList_Filter_Stores_Selected = event.data[0].ReferenceKey;
      this.SubAccountsList_Config.SearchBaseConditions.push(
        this._HelperService.GetSearchConditionStrict(
          "",
          "StoreReferenceKey",
          this._HelperService.AppConfig.DataType.Text,
          this.SubAccountsList_Filter_Stores_Selected,
          "="
        )
      );
    }

    this.SubAccountsList_ToggleOption(
      null,
      this._HelperService.AppConfig.ListToggleOption.ResetOffset
    );
  }

  //#endregion

  SetOtherFilters(): void {
    this.SubAccountsList_Config.SearchBaseConditions = [];
    this.SubAccountsList_Config.SearchBaseCondition = null;

    var CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.Merchant.Owner));
    if (CurrentIndex != -1) {
      this.SubAccountsList_Filter_Stores_Selected = null;
      this.OwnerEventProcessing(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
    }
  }

  //#region filterOperations

  Active_FilterValueChanged(event: any) {
    this._HelperService.Active_FilterValueChanged(event);
    this._FilterHelperService.SetMerchantConfig(this.SubAccountsList_Config);

    //#region setOtherFilters
    this.SetOtherFilters();
    //#endregion

    this.SubAccountsList_GetData();
  }

  RemoveFilterComponent(Type: string, index?: number): void {
    this._FilterHelperService._RemoveFilter_Merchant(Type, index);
    this._FilterHelperService.SetMerchantConfig(this.SubAccountsList_Config);
    
    if (Type == 'Time') {
      this._HelperService.AppConfig.DateRangeOptions.startDate= new Date(2017, 0, 1, 0, 0, 0, 0);
      this._HelperService.AppConfig.DateRangeOptions.endDate=moment().endOf("day");
  }

    this.SetOtherFilters();

    this.SubAccountsList_GetData();
  }

  Save_NewFilter() {
    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.SaveFilterTitle,
      text: this._HelperService.AppConfig.CommonResource.SaveFilterHelp,
      input: "text",
      inputPlaceholder: this._HelperService.AppConfig.CommonResource.FilterName,
      inputAttributes: {
        autocapitalize: "off",
        autocorrect: "off",
        maxLength: "4",
        minLength: "4",
      },
      inputValidator: function (value) {
        if (value === '' || value.length < 4) {
          return 'Enter filter name length greater than 4!'
        }
      },
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Green,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: "Save",
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
    }).then((result) => {
      if (result.value) {
        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._FilterHelperService._BuildFilterName_Merchant(result.value);
        this._HelperService.Save_NewFilter(
          this._HelperService.AppConfig.FilterTypeOption.SubAccount
        );

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });
  }

  Delete_Filter() {

    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.DeleteTitle,
      text: this._HelperService.AppConfig.CommonResource.DeleteHelp,
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel

    }).then((result) => {
      if (result.value) {
        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._HelperService.Delete_Filter(
          this._HelperService.AppConfig.FilterTypeOption.SubAccount
        );
        this._FilterHelperService.SetMerchantConfig(this.SubAccountsList_Config);
        this.SubAccountsList_GetData();

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });

  }

  ApplyFilters(event: any, Type: any, ButtonType: any): void {
    this._HelperService.MakeFilterSnapPermanent();
    this.SubAccountsList_GetData();

    if (ButtonType == 'Sort') {
      $("#SubAccountsList_sdropdown").dropdown('toggle');
    } else if (ButtonType == 'Other') {
      $("#SubAccountsList_fdropdown").dropdown('toggle');
    }

    this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  }


  CloseRowModal(index: number): void {
    $("#SubAccountsList_rdropdown_" + index).dropdown('toggle');
  }

  ResetFilters(event: any, Type: any): void {
    this._HelperService.ResetFilterSnap();
    this._FilterHelperService.SetMerchantConfig(this.SubAccountsList_Config);
    this.SetOtherFilters();

    this.SubAccountsList_GetData();

    this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  }

  //#endregion

  ResetFilterUI(): void {
    this.ResetFilterControls = false;
    this._ChangeDetectorRef.detectChanges();

    this.SubAccountsList_Filter_Stores_Load();

    this.ResetFilterControls = true;
    this._ChangeDetectorRef.detectChanges();
  }

  TestNavigation(): void {
    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.SubAccountDetails.SalesHistory,
      "ReferenceData.ReferenceKey",
      "ReferenceData.ReferenceId",
    ]);
  }


  public Password = null;
  // ResetPassword(referecedata) {
  //     swal({
  //         title: 'Are You Sure You Want To Reset Your Password',
  //         text: 'Click Continue To Reset Your Password',
  //         showCancelButton: true,
  //         position: this._HelperService.AppConfig.Alert_Position,
  //         animation: this._HelperService.AppConfig.Alert_AllowAnimation,
  //         customClass: this._HelperService.AppConfig.Alert_Animation,
  //         allowOutsideClick: this._HelperService.AppConfig.Alert_AllowOutsideClick,
  //         allowEscapeKey: this._HelperService.AppConfig.Alert_AllowEscapeKey,
  //         confirmButtonColor: this._HelperService.AppConfig.Color_Red,
  //         cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
  //         confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
  //         cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
  //         // input: 'password',
  //         // inputPlaceholder: this._HelperService.AppConfig.CommonResource.AccessPin,
  //         // inputAttributes: {
  //         //     autocapitalize: 'off',
  //         //     autocorrect: 'off',
  //         //     maxLength: "4",
  //         //     minLength: "4"
  //         // },
  //     }).then((result) => {
  //         if (result.value) {
  //             this._HelperService.IsFormProcessing = true;
  //             var pData = {
  //                 Task: 'resetsubaccountpassword',
  //                 AccountKey: referecedata.ReferenceKey,
  //                 AccountId:  referecedata.ReferenceId,

  //                 // AuthPin: result.value
  //             };
  //             let _OResponse: Observable<OResponse>;
  //             _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Account, pData);
  //             _OResponse.subscribe(
  //                 _Response => {
  //                     this._HelperService.IsFormProcessing = false;
  //                     if (_Response.Status == this._HelperService.StatusSuccess) {
  //                         this.Password = _Response.Result;
  //                         this._HelperService.NotifySuccess("Password Reset Successfully");


  //                     }
  //                     else {
  //                         this._HelperService.NotifyError(_Response.Message);
  //                     }
  //                 },
  //                 _Error => {

  //                     this._HelperService.HandleException(_Error);
  //                 });
  //         }
  //     });
  // }

  ToggleShowHidePassword(): void {
    this.ShowPassword = !this.ShowPassword;
  }

  pwdValueChange(val) {
    this.ispwdContainsNum = /\d/.test(val)
    this.ispwdContainsUC = /[A-Z]/.test(val)
    this.ispwdContainsLC = /[a-z]/.test(val)
    this.IsminLength = val.length >= 8
    this.isPwdContainsSC = /(?=.*?[#?!@$%^&*-])/.test(val)
  }

  ResetPassword_Load() {
    this.Form_ResetPassword = this._FormBuilder.group({
      AutomaticallyGenerated: true,
      Password: [null, Validators.required],
      askUser: true
    });
  }

  ResetPassword(referecedata) {
    console.log("referecedata", referecedata);
    this.titleName = referecedata.Name
    this.accountKey = referecedata.ReferenceKey,
    this.accountId = referecedata.ReferenceId,
    this.Form_ResetPassword_Show()
  }

  Form_ResetPassword: FormGroup;

  Form_ResetPassword_Show() {
    $('.dropdown-menu').removeClass('show')
    this._HelperService.OpenModal("Form_ResetPassword_Content");
  }

  Form_ResetPassword_Close() {
    this._HelperService.CloseModal("Form_ResetPassword_Content");
    this.ResetPassword_Load()
  }

  Form_ResetPassword_Process() {
    // console.log("this.Form_ResetPassword.value",this.Form_ResetPassword.value);
    this._HelperService.CloseModal("Form_ResetPassword_Content");
    
    setTimeout(() => {      
      if (this.Form_ResetPassword.value.AutomaticallyGenerated) {
        swal({
          title: 'Reset password request?',
          text: 'We will share reset password link to registered email address of this account',
          showCancelButton: true,
          position: this._HelperService.AppConfig.Alert_Position,
          animation: this._HelperService.AppConfig.Alert_AllowAnimation,
          customClass: this._HelperService.AppConfig.Alert_Animation,
          allowOutsideClick: this._HelperService.AppConfig.Alert_AllowOutsideClick,
          allowEscapeKey: this._HelperService.AppConfig.Alert_AllowEscapeKey,
          confirmButtonColor: this._HelperService.AppConfig.Color_Red,
          cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
          confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
          cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
        }).then((result) => {
          if (result.value) {
            this._HelperService.IsFormProcessing = true;
            var pData = {
              Task: 'resetsubaccountpassword',
              AccountKey: this.accountKey,
              AccountId: this.accountId,
              IsAutoGenerated:this.Form_ResetPassword.value.AutomaticallyGenerated,
              IsAskUser: false
              };
            let _OResponse: Observable<OResponse>;
            _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Account, pData);
            _OResponse.subscribe(
              _Response => {
                this._HelperService.IsFormProcessing = false;
                this.ResetPassword_Load()
                if (_Response.Status == this._HelperService.StatusSuccess) {
                  this.Password = _Response.Result;
                  this._HelperService.NotifySuccess("Password Reset Successfully");  
                }
                else {
                  this._HelperService.NotifyError(_Response.Message);
                }
              },
              _Error => {  
                this._HelperService.HandleException(_Error);
              });
          }
        });
      }else{
        this._HelperService.IsFormProcessing = true;
        var pData = {
          Task: 'resetsubaccountpassword',
          AccountKey: this.accountKey,
          AccountId: this.accountId,
          IsAutoGenerated:this.Form_ResetPassword.value.AutomaticallyGenerated,
          Password: this.Form_ResetPassword.value.Password,
          IsAskUser: this.Form_ResetPassword.value.askUser
          };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Account, pData);
        _OResponse.subscribe(
          _Response => {
            this._HelperService.IsFormProcessing = false;
            this.ResetPassword_Load()
            if (_Response.Status == this._HelperService.StatusSuccess) {
              this.Password = _Response.Result;
              this._HelperService.NotifySuccess("Password Reset Successfully");  
            }
            else {
              this._HelperService.NotifyError(_Response.Message);
            }
          },
          _Error => {  
            this._HelperService.HandleException(_Error);
          });
      }
    }, 300);

  }
  //#region Update SubAccount 
  private InitImagePicker(previewurl?: string) {
    if (this.InputFileComponent != undefined) {
      this.CurrentImagesCount = 0;
      if (previewurl) {
        this.InputFileComponent.files[0] = {};
        this.InputFileComponent.files[0].preview = previewurl;
      }
      this._HelperService._InputFileComponent = this.InputFileComponent;
      this.InputFileComponent.onChange = (files: Array<InputFile>): void => {
        if (files.length >= this.CurrentImagesCount) {
          this._HelperService._SetFirstImageOrNone(this.InputFileComponent.files);
        }
        this.CurrentImagesCount = files.length;
      };
    }
  }

  // imgProfileID: any;
  setprofileId(value) {
    // this.imgProfileID = value;
    this._HelperService.isUpdateSubaccountImage = true
    this._HelperService.subaccountImage = this._SubAccountDetails.IconUrl        
  }


  Form_UpdateSubAccount: FormGroup;
  Form_UpdateSubAccount_Show() {
    this._HelperService.OpenModal("Form_UpdateSubAccount_Content");
  }
  Form_UpdateSubAccount_Close() {
    this._HelperService._FileSelect_Icon_Reset();
    this.InputFileComponent.files.pop();
    this._HelperService.CloseModal("Form_UpdateSubAccount_Content");
  }
  Form_UpdateSubAccount_Load() {
    this._HelperService._FileSelect_Icon_Data.Width = 128;
    this._HelperService._FileSelect_Icon_Data.Height = 128;

    this._HelperService._FileSelect_Poster_Data.Width = 800;
    this._HelperService._FileSelect_Poster_Data.Height = 400;

    this.Form_UpdateSubAccount = this._FormBuilder.group({
      OperationType: "new",
      Task: this._HelperService.AppConfig.Api.ThankUCash.UpdateSubAccount,
      AccountKey: this._HelperService.UserAccount.AccountKey,
      AccountId: this._HelperService.UserAccount.AccountId,
      StoreReferenceId: [null],
      StoreReferenceKey: [null],
      RoleId: [null, Validators.required],
      RoleKey: [null, Validators.required],
      StatusCode: this._HelperService.AppConfig.Status.Active,
      FirstName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(256)])],
      LastName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(128)])],
      ContactNumber: [null, Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(14)])],
      EmailAddress: [null, Validators.compose([Validators.required, Validators.email, Validators.minLength(2)])],
      GenderCode: this._HelperService.AppConfig.Gender.Male,
    });
  }
  Form_UpdateSubAccount_Clear() {
    this.Form_UpdateSubAccount.reset();
    this._HelperService._FileSelect_Icon_Reset();
    this._HelperService._FileSelect_Poster_Reset();
    this.Form_UpdateSubAccount_Load();
    this._HelperService.GetRandomNumber();
    this._HelperService.GeneratePassoword();
  }
  Form_UpdateSubAccount_Process(_FormValue: any) {
    this._HelperService.IsFormProcessing = true;

    _FormValue.AccountKey = this._SubAccountDetails.ReferenceKey;
    _FormValue.AccountId = this._SubAccountDetails.ReferenceId;

    var Request = this.CreateSubAccountRequest(_FormValue);
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(
      this._HelperService.AppConfig.NetworkLocation.V3.Account,
      Request
    );
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.FlashSwalSuccess("SubAccount has been updated successfully",
            "Done! You have successfully updated SubAccount");
          // this.Form_UpdateSubAccount_Clear();
          this.Form_UpdateSubAccount_Close();
          if (_FormValue.OperationType == "close") {
            this.Form_UpdateSubAccount_Close();
          }
          this.SubAccountsList_GetData();
        } else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      }
    );
  }

  //#endregion

  CreateSubAccountRequest(_FormValue: any): void {
    var IconContent: any = undefined;

    if (this._HelperService._Icon_Cropper_Data.Content != null) {
      IconContent = this._HelperService._Icon_Cropper_Data;
    }

    _FormValue.IconContent = IconContent;

    return _FormValue;
  }
  //#region SubAccountDetails 

  public _SubAccountDetails: any =
    {
      ManagerName: null,
      BranchName: null,
      ReferenceId: null,
      ReferenceKey: null,
      TypeCode: null,
      TypeName: null,
      SubTypeCode: null,
      SubTypeName: null,
      UserAccountKey: null,
      UserAccountDisplayName: null,
      Name: null,
      Description: null,
      StartDate: null,
      StartDateS: null,
      EndDate: null,
      EndDateS: null,
      SubTypeValue: null,
      MinimumInvoiceAmount: null,
      MaximumInvoiceAmount: null,
      MinimumRewardAmount: null,
      MaximumRewardAmount: null,
      ManagerKey: null,
      ManagerDisplayName: null,
      SmsText: null,
      Comment: null,
      CreateDate: null,
      CreatedByKey: null,
      CreatedByDisplayName: null,
      ModifyDate: null,
      ModifyByKey: null,
      ModifyByDisplayName: null,
      StatusId: null,
      StatusCode: null,
      StatusName: null,
      CreateDateS: null,
      ModifyDateS: null,
      StatusI: null,
      StatusB: null,
      StatusC: null,
    }
  GetSubAccountsDetails(SubAccountsDetails: any, showModal: boolean) {

    this._HelperService.IsFormProcessing = true;
    var pData = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetSubAccount,
      // AccountId: this._HelperService.UserAccount.AccountId,
      // AccountKey: this._HelperService.UserAccount.AccountKey,
      AccountKey: SubAccountsDetails.ReferenceKey,
      AccountId: SubAccountsDetails.ReferenceId,

    }
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Account, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.IsFormProcessing = false;
          this._SubAccountDetails = _Response.Result;
          this._SubAccountDetails.CreateDate = this._HelperService.GetDateS(this._SubAccountDetails.CreateDate);
          this._SubAccountDetails.ModifyDate = this._HelperService.GetDateS(this._SubAccountDetails.ModifyDate);
          this._SubAccountDetails.StatusI = this._HelperService.GetStatusIcon(this._SubAccountDetails.StatusCode);
          this._SubAccountDetails.StatusB = this._HelperService.GetStatusBadge(this._SubAccountDetails.StatusCode);
          this._SubAccountDetails.StatusC = this._HelperService.GetStatusColor(this._SubAccountDetails.StatusCode);
          // this._SubAccountAddress = this._SubAccountDetails.Address;
          // this.TUTr_Filter_Store_Option.placeholder = this._SubAccountDetails.StoreDisplayName;
          this._ChangeDetectorRef.detectChanges();
          this.SubAccountsList_GetData();
          //#region ResponseInit 


          this._SubAccountDetails.EndDateS = this._HelperService.GetDateS(
            this._SubAccountDetails.EndDate
          );
          this._SubAccountDetails.CreateDateS = this._HelperService.GetDateTimeS(
            this._SubAccountDetails.CreateDate
          );
          this._SubAccountDetails.ModifyDateS = this._HelperService.GetDateTimeS(
            this._SubAccountDetails.ModifyDate
          );
          this._SubAccountDetails.StatusI = this._HelperService.GetStatusIcon(
            this._SubAccountDetails.StatusCode
          );
          this._SubAccountDetails.StatusB = this._HelperService.GetStatusBadge(
            this._SubAccountDetails.StatusCode
          );
          this._SubAccountDetails.StatusC = this._HelperService.GetStatusColor(
            this._SubAccountDetails.StatusCode
          );

          if (_Response.Result.RoleId) {
            this.Form_UpdateSubAccount.controls['RoleId'].setValue(_Response.Result.RoleId);
            this.Form_UpdateSubAccount.controls['RoleKey'].setValue(_Response.Result.RoleKey);
            this.Form_UpdateSubAccount.controls['StoreReferenceId'].setValue(_Response.Result.StoreReferenceId);
            this.Form_UpdateSubAccount.controls['StoreReferenceKey'].setValue(_Response.Result.StoreReferenceKey);

          }

          //#endregion

          //#region InitLocationParams 
          if (_Response.Result.Latitude != undefined && _Response.Result.Longitude != undefined) {

            this._HelperService._UserAccount.Latitude = _Response.Result.Latitude;
            this._HelperService._UserAccount.Longitude = _Response.Result.Longitude;

            // this.Form_EditUser_Latitude = _Response.Result.Latitude;
            // this.Form_EditUser_Longitude = _Response.Result.Longitude;


            // this.Form_EditUser.controls['Longitude'].setValue(_Response.Result.Longitude);

          } else {
            this._HelperService._UserAccount.Longitude = this._HelperService._UserAccount.Latitude;
            this._HelperService._UserAccount.Longitude = this._HelperService._UserAccount.Longitude;
          }

          // this.Form_EditUser.controls['StoreId'].setValue(_Response.Result.StoreReferenceId);
          // this.Form_EditUser.controls['StoreKey'].setValue(_Response.Result.StoreReferenceKey);
          //#endregion

          if (showModal) {
            this._HelperService.Icon_Crop_Clear();
            this.InitImagePicker(this._SubAccountDetails.IconUrl);
            this._HelperService.OpenModal('Form_UpdateSubAccount_Content');
          }

        }
        else {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      }
    );
  }

  //#endregion

  //#region Block - UnBlock Code 

  Title: string;
  BlockSubAccount(TItem) {
    if (TItem.StatusCode == 'default.active') {
      this.Title = "Block";
    } else {
      this.Title = "Active";
    }

    this._SubAccountDetails = TItem;
    this._HelperService.OpenModal("BlockPos");

  }

  public Block(): void {
    this._HelperService.IsFormProcessing = true;
    this._HelperService.AppConfig.ShowHeader = true;
    var pData = {
      Task: "disablesubaccount",
      AccountId: this._SubAccountDetails.ReferenceId,
      AccountKey: this._SubAccountDetails.ReferenceKey,
      StatusCode: "default.blocked",
    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Account, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.NotifySuccess("SubAccount Blocked Successfully ");
          this._HelperService.CloseModal("BlockPos");
          this.SubAccountsList_GetData();

        } else {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      }
    );
  }
  public UnBlock(): void {
    this._HelperService.IsFormProcessing = true;
    this._HelperService.AppConfig.ShowHeader = true;
    var pData = {
      Task: "enablesubaccount",
      AccountId: this._SubAccountDetails.ReferenceId,
      AccountKey: this._SubAccountDetails.ReferenceKey,
      StatusCode: "default.blocked",
    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Account, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.NotifySuccess("SubAccount Activated Successfully ");
          this._HelperService.CloseModal("BlockPos");
          this.SubAccountsList_GetData();
        } else {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      }
    );
  }

  //#endregion

}
