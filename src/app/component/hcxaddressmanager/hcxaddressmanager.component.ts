import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { Select2OptionData } from 'ng2-select2';
import { Address } from 'ngx-google-places-autocomplete/objects/address';
import { Observable } from 'rxjs';
import { DataHelperService } from '../../service/datahelper.service';
import { HelperService } from '../../service/helper.service';
import { OResponse, OSelect } from '../../service/object.service';
import { GooglePlaceDirective } from 'ngx-google-places-autocomplete';

@Component({
    selector: 'hcx-addressmanager',
    templateUrl: './hcxaddressmanager.component.html',
    styleUrls: ['./hcxaddressmanager.css']
})
export class HCXAddressManagerComponent implements OnInit {
    ShowCity = true;
    ShowState = true;
    IsAddressSet = false;
    public HCXLocManager_S2States_Option: Select2Options;
    HCXModalLocationManagerId = 'HCXModalLocationManager';
    public HCXLocManager_S2States_Data: Array<Select2OptionData>;
    public HCXLoc_Manager_City_Option: Select2Options;
    // @ViewChild('places') places: GooglePlaceDirective;
    LocationType = 'popup';

    @Input('mapheight') public mapheight: any = '' 

    @Input('config') public config: any =
        {
            locationType: locationType.form,
        };
    @Input('data') public data: any =
        {

            Address: null,

            Latitude: null,
            Longitude: null,

            CityAreaId: null,
            CityAreaCode: null,
            CityAreaName: null,

            CityId: null,
            CityCode: null,
            CityName: null,

            StateId: null,
            StateCode: null,
            StateName: null,

            CountryId: null,
            CountryCode: null,
            CountryName: null,
            PostalCode: null,

            MapAddress: null,

        };
    @Output('OnAddressSet') AddressComponent = new EventEmitter<HCXAddress>();
    constructor(
        private _DataHelperServive: DataHelperService,
        public _HelperService: HelperService
    ) { }
    ngOnInit(): void {
        this._HelperService.setTheme();
        this.HCXModalLocationManagerId = this._HelperService.GenerateGuid();
        if (this.config != undefined && this.config != null) {
            if (this.config.locationType != undefined && this.config.locationType != null) {
                if (this.config.locationType == locationType.form) {
                    this.LocationType = 'form';
                }
            }
        }
        if (this.data.Address != undefined && this.data.Address != null && this.data.Address != "") {
            if (this.data.CountryCode == undefined || this.data.CountryCode == null || this.data.CountryCode == "") {
                this.data.CountryId = this._HelperService.UserCountry.CountryId;
                this.data.CountryCode = this._HelperService.UserCountry.CountryKey;
                this.data.CountryName = this._HelperService.UserCountry.CountryName;
            }
            this.IsAddressSet = true;
            this.HCXLoc_Manager_GetStates();
        }
        else {
            this.data.CountryId = this._HelperService.UserCountry.CountryId;
            this.data.CountryCode = this._HelperService.UserCountry.CountryKey;
            this.data.CountryName = this._HelperService.UserCountry.CountryName;
            this.IsAddressSet = false;
            // this.HCXLoc_Manager_GetStates();
        }
        this.HCXLoc_Manager_GetStates();
    }

    HCXLocManager_OpenManager() {
        this.HCXLocManager_OpenUpdateManager_Clear();
        this._HelperService.OpenModal(this.HCXModalLocationManagerId);
    }
    HCXLocManager_OpenUpdateManager() {
        this._HelperService.OpenModal(this.HCXModalLocationManagerId);
    }
    HCXLocManager_OpenUpdateManager_Clear() {
        this.IsAddressSet = false;
        this.data =
        {
            Latitude: 0,
            Longitude: 0,

            Address: null,
            MapAddress: null,

            CountryId: this._HelperService.UserCountry.CountryId,
            CountryCode: this._HelperService.UserCountry.CountryKey,
            CountryName: this._HelperService.UserCountry.CountryName,

            StateId: 0,
            StateName: null,
            StateCode: null,

            CityId: 0,
            CityCode: null,
            CityName: null,

            CityAreaId: 0,
            CityAreaName: null,
            CityAreaCode: null,

            PostalCode: null,
        }
    }
    HCXLocManager_AddressChange(address: Address) {
        this.data.Latitude = address.geometry.location.lat();
        this.data.Longitude = address.geometry.location.lng();
        this.data.MapAddress = address.formatted_address;
        this.data.Address = address.formatted_address;

        this.data.CityAreaId = 0;
        this.data.CityAreaCode = null;
        this.data.CityAreaName = null;

        this.data.CityId = 0;
        this.data.CityCode = null;
        this.data.CityName = null;

        this.data.StateId = 0;
        this.data.StateCode = null;
        this.data.StateName = null;
        this.data.PostalCode = null;

        var tAddress = this._HelperService.GoogleAddressArrayToJson(address.address_components);;
        if (tAddress.postal_code != undefined && tAddress.postal_code != null && tAddress.postal_code != "") {
            this.data.PostalCode = tAddress.postal_code;
        }
        if (tAddress.country != undefined && tAddress.country != null && tAddress.country != "") {
            this.data.CountryName = tAddress.country;
        }
        if (tAddress.administrative_area_level_1 != undefined && tAddress.administrative_area_level_1 != null && tAddress.administrative_area_level_1 != "") {
            this.data.StateName = tAddress.administrative_area_level_1;
        }
        if (tAddress.locality != undefined && tAddress.locality != null && tAddress.locality != "") {
            this.data.CityName = tAddress.locality;
        }
        if (tAddress.administrative_area_level_2 != undefined && tAddress.administrative_area_level_2 != null && tAddress.administrative_area_level_2 != "") {
            this.data.CityAreaName = tAddress.administrative_area_level_2;
        }
        if (tAddress.administrative_area_level_2 != undefined && tAddress.administrative_area_level_2 != null && tAddress.administrative_area_level_2 != "") {
            this.data.CityAreaName = tAddress.administrative_area_level_2;
        }
        if (this.data.CountryName != this._HelperService.UserCountry.CountryName) {
            this._HelperService.NotifyError('Currently we’re not serving in this area, please add locality within ' + this._HelperService.UserCountry.CountryName);
            this.HCXLocManager_OpenUpdateManager_Clear();
        }
        else {
            this.data.CountryId = this._HelperService.UserCountry.CountryId;
            this.data.CountryCode = this._HelperService.UserCountry.CountryKey;
            this.data.CountryName = this._HelperService.UserCountry.CountryName;
            this.HCXLoc_Manager_GetStates();
        }
    }
    HCXLoc_Manager_GetStates() {
        this.ShowState = false;
        var PData =
        {
            Task: this._HelperService.AppConfig.Api.Core.getstates,
            ReferenceKey: this.data.CountryCode,
            ReferenceId: this.data.CountryId,
            Offset: 0,
            Limit: 1000,
        }
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.State, PData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    if (_Response.Result.Data != undefined) {
                        var _Data = _Response.Result.Data as any[];
                        if (_Data.length > 0) {
                            var finalCat = [];
                            this.ShowState = false;
                            _Data.forEach(element => {
                                var Item = {
                                    id: element.ReferenceId,
                                    key: element.ReferenceKey,
                                    text: element.Name,
                                    additional: element,
                                };
                                if (this.data.StateName != undefined || this.data.StateName != null || this.data.StateName != '') {
                                    if (element.Name == this.data.StateName) {
                                        this.data.StateId = Item.id;
                                        this.data.StateCode = Item.key;
                                        this.data.StateName = Item.text;
                                        this.ShowCity = false;
                                        this.HCXLoc_Manager_City_Load();
                                        setTimeout(() => {
                                            this.ShowCity = true;
                                            this._HelperService.setTheme();
                                        }, 500);
                                    }
                                }
                                finalCat.push(Item);
                            });
                            if (this.data.StateId > 0) {
                                this.HCXLocManager_S2States_Option = {
                                    placeholder: this.data.StateName,
                                    multiple: false,
                                    allowClear: false,
                                };
                                setTimeout(() => {
                                    this.ShowState = true;
                                }, 500);
                            }
                            else {
                                this.HCXLocManager_S2States_Option = {
                                    placeholder: "Select state",
                                    multiple: false,
                                    allowClear: false,
                                };
                                setTimeout(() => {
                                    this.ShowState = true;
                                }, 500);
                            }
                            this.HCXLocManager_S2States_Data = finalCat;
                            this.ShowState = true;
                            if (this.data.CityName != undefined && this.data.CityName != null && this.data.CityName != "") {
                                this.HCXLoc_Manager_GetStateCity(this.data.CityName);
                            }
                        }
                        else {
                            this.HCXLocManager_S2States_Option = {
                                placeholder: "Select state",
                                multiple: false,
                                allowClear: false,
                            };
                            setTimeout(() => {
                                this.ShowState = true;
                            }, 500);
                        }
                    }
                    else {
                        this.HCXLocManager_S2States_Option = {
                            placeholder: "Select state",
                            multiple: false,
                            allowClear: false,
                        };
                        setTimeout(() => {
                            this.ShowState = true;
                        }, 500);
                    }
                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);
                this._HelperService.ToggleField = false;

            });
    }
    public validState = ""
    HCXLoc_Manager_StateSelected(Items) {
        if(Items.value == "43"){
            this.validState =Items.value
            this._HelperService.NotifyError("Please select Valid state");

        }
        else if (Items != undefined && Items.value != undefined && Items.data.length > 0) {
            this.data.StateId = Items.data[0].id;
            this.data.StateCode = Items.data[0].key;
            this.data.StateName = Items.data[0].text;
            this.data.CityId = null;
            this.data.CityCode = null;
            this.data.CityName = null;
            // this.data.Address = null;
            // this.data.MapAddress = null;
            // this.data.Latitude = 0;
            // this.data.Longitude = 0;
            this.ShowCity = false;
            this.HCXLoc_Manager_City_Load();
            setTimeout(() => {
                this.ShowCity = true;
                this._HelperService.setTheme();
            }, 500);
            this.HCXLoc_Manager_SetAddress_Emit();
        }
    }
    HCXLoc_Manager_GetStateCity(CityName) {
        var PData =
        {
            Task: this._HelperService.AppConfig.Api.Core.getcities,
            ReferenceKey: this.data.StateCode,
            ReferenceId: this.data.StateId,
            SearchCondition: this._HelperService.GetSearchConditionStrict('', 'Name', 'text', CityName, '='),
            Offset: 0,
            Limit: 1,
        }
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.City, PData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    if (_Response.Result.Data != undefined) {
                        var _Result = _Response.Result.Data;
                        if (_Result != undefined && _Result != null && _Result.length > 0) {
                            var Item = _Result[0];
                            this.data.CityId = Item.ReferenceId;
                            this.data.CityCode = Item.ReferenceKey;
                            this.data.CityName = Item.Name;
                            this.ShowCity = false;
                            this.HCXLoc_Manager_City_Load();
                            setTimeout(() => {
                                this.ShowCity = true;
                                this._HelperService.setTheme();
                            }, 500);
                            this.HCXLoc_Manager_SetAddress_Emit();
                        }
                        else {
                            this.data.CityId = 0;
                            this.data.CityCode = null;
                            this.data.CityName = null;
                            this.ShowCity = false;
                            this.HCXLoc_Manager_City_Load();
                            setTimeout(() => {
                                this.ShowCity = true;
                                this._HelperService.setTheme();
                            }, 500);
                        }
                    }
                    else {
                        this.data.CityId = 0;
                        this.data.CityCode = null;
                        this.data.CityName = null;
                        this.ShowCity = false;
                        this.HCXLoc_Manager_City_Load();
                        setTimeout(() => {
                            this.ShowCity = true;
                            this._HelperService.setTheme();
                        }, 500);
                    }
                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
                this._HelperService.setTheme();
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);
                this._HelperService.ToggleField = false;

            });
    }
    HCXLoc_Manager_City_Load() {
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.Core.getcities,
            Location: this._HelperService.AppConfig.NetworkLocation.V3.City,
            ReferenceId: this.data.StateId,
            ReferenceKey: this.data.StateCode,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "Name",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true,
                },
            ],
        };
        var _Transport = this._DataHelperServive.S2_BuildList(_Select) as any;
        if (this.data.CityName != undefined || this.data.CityName != null && this.data.CityName != '') {
            this.HCXLoc_Manager_City_Option = {
                placeholder: this.data.CityName,
                ajax: _Transport,
                multiple: false,
                allowClear: false,
            };
        }
        else {
            this.HCXLoc_Manager_City_Option = {
                placeholder: "Select City",
                ajax: _Transport,
                multiple: false,
                allowClear: false,
            };
        }

    }
    HCXLoc_Manager_City_Change(Items: any) {
        if (Items != undefined && Items.value != undefined && Items.data.length > 0) {
            this.data.CityId = Items.data[0].ReferenceId;
            this.data.CityCode = Items.data[0].ReferenceKey;
            this.data.CityName = Items.data[0].Name;
            this.HCXLoc_Manager_SetAddress_Emit();
        }
    }
    HCXLoc_Manager_SetAddress() {
        if (this.data.Address == undefined || this.data.Address == null || this.data.Address == "") {
            this._HelperService.NotifyError("Please enter address")
        }
        else if (this.data.CountryId == undefined || this.data.CountryId == null || this.data.CountryId == 0) {
            this._HelperService.NotifyError("Please select country")
        }
        else if (this.data.StateId == undefined || this.data.StateId == null || this.data.StateId == 0) {
            this._HelperService.NotifyError("Please select state")
        }
        else if (this.data.CityId == undefined || this.data.CityId == null || this.data.CityId == 0) {
            this._HelperService.NotifyError("Please select city")
        }
        else {
            this.IsAddressSet = true;
            this.AddressComponent.emit(this.data);
            this._HelperService.CloseModal(this.HCXModalLocationManagerId);
        }
    }
    HCXLoc_Manager_SetAddress_Emit() {
        if(this.validState == "43"){
            this.data.StateId = 43
        }
        this.IsAddressSet = true;
        this.AddressComponent.emit(this.data);
    }
    HCXLoc_Manager_SetAddress_Cancel() {
        // this.HCXLocManager_OpenUpdateManager_Clear();
        this._HelperService.CloseModal(this.HCXModalLocationManagerId);
    }
}
export enum locationType {
    form,
    popup
}

export interface HCXAddressConfig {
    locationType?: locationType | locationType.popup;
}
export interface HCXAddress {
    Address?: string | null;
    Latitude?: number | null;
    Longitude?: number | null;

    CityAreaId?: number | null;
    CityAreaCode?: string | null;
    CityAreaName?: string | null;


    CityId?: number | null;
    CityCode?: string | null;
    CityName?: string | null;

    StateId?: number | null;
    StateCode?: string | null;
    StateName?: string | null;

    CountryId?: number | null;
    CountryCode?: string | null;
    CountryName?: string | null;

    PostalCode?: string | null;
    MapAddress?: string | null;
}



import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { GooglePlaceModule } from 'ngx-google-places-autocomplete';
import { AgmCoreModule } from '@agm/core';
import { Select2Module } from 'ng2-select2';
@NgModule({
    declarations: [
        HCXAddressManagerComponent,
    ],
    imports: [
        TranslateModule,
        CommonModule,
        FormsModule,
        Select2Module,
        ReactiveFormsModule,
        GooglePlaceModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyB9DMDX78ll840bMxjGL4WUfjAYXfi8vZo'
        }),
    ],
    providers: [],
    exports: [HCXAddressManagerComponent],
    // bootstrap: [TUConsoleComponent]
})
export class HCXAddressManagerModule { }
