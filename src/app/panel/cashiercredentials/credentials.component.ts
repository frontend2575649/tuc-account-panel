import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Params, Router } from "@angular/router";
import { Observable } from 'rxjs';
import { HelperService, OResponse } from "../../service/service";
import { TranslateService } from '@ngx-translate/core';
import swal from 'sweetalert2';
declare var $: any;


@Component({
    selector: "tu-credentials",
    templateUrl: "./credentials.component.html",
})
export class CredentialsComponent implements OnInit {
    _Form_Login_Processing = false;
    _Show_Form_ResetPassword = false;
    _Form_Login: FormGroup;
    _Form_Otp: FormGroup;

    ispwdContainsNum = true
    ispwdContainsUC = true
    ispwdContainsLC = true
    IsminLength = true
    isPwdContainsSC = true
    ShowPasswordPopup: boolean = true;
    titleName: any = '';

    URLData: any = {
        ActiveReferenceKey: null,
        ActiveReferenceId: null,
        Code: null
    };

    constructor(
        public _ActivatedRoute: ActivatedRoute,
        public _FormBuilder: FormBuilder,
        public _TranslateService: TranslateService,
        public _Router: Router,
        public _HelperService: HelperService,
    ) {

        
        this._HelperService.ShowDateRange = false;


        this.Form_EditPassword_Load();

        this._ActivatedRoute.params.subscribe((params: Params) => {

            this.URLData.ActiveReferenceKey = params['referencekey'];
            this.URLData.ActiveReferenceId = params['referenceid'];
            this.URLData.Code = params['code'];

        });
    }
    ngOnInit() {
        this.GetCashiersDetails();
        this.ResetPassword_Load()
    }

    Form_EditPassword: FormGroup;
    Form_EditPassword_Load() {
        this.Form_EditPassword = this._FormBuilder.group({
            OperationType: 'new',
            // ReferenceKey: this._HelperService.UserAccount.AccountKey,
            Task: this._HelperService.AppConfig.Api.Core.GetBranch,
            OldPassword: [null, Validators.required],
            NewPassword: [null, Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(20), Validators.pattern(this._HelperService.AppConfig.ValidatorRegex.Password)])],
        });
    }
    Form_EditPassword_Clear() {
        this.Form_EditPassword.reset();
    }
    Form_EditPassword_Process(_FormValue: any) {
        swal({
            position: 'top',
            title: 'Update password',
            // text: this._HelperService.AppConfig.CommonResource.UpdateHelp,
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Red,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
        }).then((result) => {
            if (result.value) {
                _FormValue.ReferenceKey = this._HelperService.UserAccount.AccountKey;
                // _FormValue.AuthPin = result.value;
                this._HelperService.IsFormProcessing = true;
                let _OResponse: Observable<OResponse>;
                if (_FormValue.OldPassword != _FormValue.NewPassword) {
                    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Account, _FormValue);
                    _OResponse.subscribe(
                        _Response => {
                            this._HelperService.IsFormProcessing = false;
                            if (_Response.Status == this._HelperService.StatusSuccess) {
                                this._HelperService.NotifySuccess('Password updated');
                                this.Form_EditPassword_Clear();
                            }
                            else {
                                this._HelperService.NotifyError('Enter valid old password');
                            }
                        },
                        _Error => {
                            this._HelperService.IsFormProcessing = false;
                            this._HelperService.HandleException(_Error);
                        });
                }
                else {
                    this._HelperService.NotifyError('Old Password and New Password should not be same');
                }

            }
        });

    }
    public _CashierDetails: any =
    {
        ManagerName: null,
        BranchName: null,
        ReferenceId: null,
        ReferenceKey: null,
        TypeCode: null,
        TypeName: null,
        SubTypeCode: null,
        SubTypeName: null,
        UserAccountKey: null,
        UserAccountDisplayName: null,
        Name: null,
        Description: null,
        StartDate: null,
        StartDateS: null,
        EndDate: null,
        EndDateS: null,
        SubTypeValue: null,
        MinimumInvoiceAmount: null,
        MaximumInvoiceAmount: null,
        MinimumRewardAmount: null,
        MaximumRewardAmount: null,
        ManagerKey: null,
        ManagerDisplayName: null,
        SmsText: null,
        Comment: null,
        CreateDate: null,
        CreatedByKey: null,
        CreatedByDisplayName: null,
        ModifyDate: null,
        ModifyByKey: null,
        ModifyByDisplayName: null,
        StatusId: null,
        StatusCode: null,
        StatusName: null,
        CreateDateS: null,
        ModifyDateS: null,
        StatusI: null,
        StatusB: null,
        StatusC: null,
    }
GetCashiersDetails() {

    this._HelperService.IsFormProcessing = true;

    var pData = {
        Task: this._HelperService.AppConfig.Api.ThankUCash.GetCashier,
        AccountKey: this._HelperService.AppConfig.ActiveReferenceKey,
      AccountId: this._HelperService.AppConfig.ActiveReferenceId,


    }
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Account, pData);
    _OResponse.subscribe(
        _Response => {
            if (_Response.Status == this._HelperService.StatusSuccess) {
                this._HelperService.IsFormProcessing = false;
                this._CashierDetails = _Response.Result;
            }
            else {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.NotifyError(_Response.Message);
            }
        },
        _Error => {
            this._HelperService.HandleException(_Error);
        }
    );
}

public ShowPasswordDiv: boolean = false;
ShowPassword() {
    this.ShowPasswordDiv = !this.ShowPasswordDiv;
}

// ResetPassword() {
//     swal({
//       title: 'Are You Sure You Want To Reset Your Password?',
//       text: 'Click Continue To Reset Your Password',
//       showCancelButton: true,
//       position: this._HelperService.AppConfig.Alert_Position,
//       animation: this._HelperService.AppConfig.Alert_AllowAnimation,
//       customClass: this._HelperService.AppConfig.Alert_Animation,
//       allowOutsideClick: this._HelperService.AppConfig.Alert_AllowOutsideClick,
//       allowEscapeKey: this._HelperService.AppConfig.Alert_AllowEscapeKey,
//       confirmButtonColor: this._HelperService.AppConfig.Color_Red,
//       cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
//       confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
//       cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
//     //   input: 'password',
//     //   inputPlaceholder: this._HelperService.AppConfig.CommonResource.AccessPin,
//     //   inputAttributes: {
//     //     autocapitalize: 'off',
//     //     autocorrect: 'off',
//     //     maxLength: "4",
//     //     minLength: "4"
//     //   },
//     }).then((result) => {
//       if (result.value) {
//         this._HelperService.IsFormProcessing = true;
//         var pData = {
//           Task: 'resetcashierpassword',
//           AccountKey: this._HelperService.AppConfig.ActiveReferenceKey,
//           AccountId: this._HelperService.AppConfig.ActiveReferenceId,

//         //   AuthPin: result.value
//         };
//         let _OResponse: Observable<OResponse>;
//         _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Account, pData);
//         _OResponse.subscribe(
//           _Response => {
//             this._HelperService.IsFormProcessing = false;
//             if (_Response.Status == this._HelperService.StatusSuccess) {
//                 this.GetCashiersDetails();
           
//               this._HelperService.NotifySuccess("Password Reset Successfully");
              
//             }
//             else {
//               this._HelperService.NotifyError(_Response.Message);
//             }
//           },
//           _Error => {

//             this._HelperService.HandleException(_Error);
//           });
//       }
//     });
// }

public Password = null;
ToggleShowHidePassword(): void {
    this.ShowPasswordPopup = !this.ShowPasswordPopup;
  }

  pwdValueChange(val) {
    this.ispwdContainsNum = /\d/.test(val)
    this.ispwdContainsUC = /[A-Z]/.test(val)
    this.ispwdContainsLC = /[a-z]/.test(val)
    this.IsminLength = val.length >= 8
    this.isPwdContainsSC = /(?=.*?[#?!@$%^&*-])/.test(val)
  }

  ResetPassword_Load() {
    this.Form_ResetPassword = this._FormBuilder.group({
      AutomaticallyGenerated: true,
      Password: [null, Validators.required],
      askUser: true
    });
  }

  ResetPassword() {
    // this.titleName = referecedata.Name
    this.Form_ResetPassword_Show()
  }

  Form_ResetPassword: FormGroup;

  Form_ResetPassword_Show() {
    $('.dropdown-menu').removeClass('show')
    this._HelperService.OpenModal("Form_ResetPassword_Content");
  }

  Form_ResetPassword_Close() {
    this._HelperService.CloseModal("Form_ResetPassword_Content");
    this.ResetPassword_Load()
  }

  Form_ResetPassword_Process() {
    console.log("this.Form_ResetPassword.value",this.Form_ResetPassword.value);
    this._HelperService.CloseModal("Form_ResetPassword_Content");
    
    setTimeout(() => {      
      if (this.Form_ResetPassword.value.AutomaticallyGenerated) {
        swal({
          title: 'Reset password request?',
          text: "You'll be able to view the password in credentials tab",
          showCancelButton: true,
          position: this._HelperService.AppConfig.Alert_Position,
          animation: this._HelperService.AppConfig.Alert_AllowAnimation,
          customClass: this._HelperService.AppConfig.Alert_Animation,
          allowOutsideClick: this._HelperService.AppConfig.Alert_AllowOutsideClick,
          allowEscapeKey: this._HelperService.AppConfig.Alert_AllowEscapeKey,
          confirmButtonColor: this._HelperService.AppConfig.Color_Red,
          cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
          confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
          cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
        }).then((result) => {
          if (result.value) {
            this._HelperService.IsFormProcessing = true;
            var pData = {
              Task: 'resetcashierpassword',
              AccountKey: this._HelperService.AppConfig.ActiveReferenceKey,
              AccountId: this._HelperService.AppConfig.ActiveReferenceId,
              IsAutoGenerated:this.Form_ResetPassword.value.AutomaticallyGenerated,
              IsAskUser: false
              };
            let _OResponse: Observable<OResponse>;
            _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Account, pData);
            _OResponse.subscribe(
              _Response => {
                this._HelperService.IsFormProcessing = false;
                this.ResetPassword_Load()
                if (_Response.Status == this._HelperService.StatusSuccess) {
                  this.Password = _Response.Result;
                  this._HelperService.NotifySuccess("Password Reset Successfully");  
                  this.GetCashiersDetails()
                }
                else {
                  this._HelperService.NotifyError(_Response.Message);
                }
              },
              _Error => {  
                this._HelperService.HandleException(_Error);
              });
          }
        });
      }else{
        this._HelperService.IsFormProcessing = true;
        var pData = {
          Task: 'resetcashierpassword',
          AccountKey: this._HelperService.AppConfig.ActiveReferenceKey,
          AccountId: this._HelperService.AppConfig.ActiveReferenceId,
          IsAutoGenerated:this.Form_ResetPassword.value.AutomaticallyGenerated,
          Password: this.Form_ResetPassword.value.Password,
          IsAskUser: this.Form_ResetPassword.value.askUser
          };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Account, pData);
        _OResponse.subscribe(
          _Response => {
            this._HelperService.IsFormProcessing = false;
            this.ResetPassword_Load()
            if (_Response.Status == this._HelperService.StatusSuccess) {
              this.Password = _Response.Result;
              this._HelperService.NotifySuccess("Password Reset Successfully");  
              this.GetCashiersDetails()
            }
            else {
              this._HelperService.NotifyError(_Response.Message);
            }
          },
          _Error => {  
            this._HelperService.HandleException(_Error);
          });
      }
    }, 300);

  }

}
